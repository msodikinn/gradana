<?php
return [
    'perbankan_img_path' => 'uploads/perbankan/',
    'pemohon_img_path' => 'uploads/pemohon/',
    'developer_img_path' => 'uploads/developer/',
    'investor_img_path' => 'uploads/investor/',
    'user_img_path' => 'uploads/users/',
    'jobpemohon_img_path' => 'uploads/jobpemohon/',
    'jobpasangan_img_path' => 'uploads/jobpasangan/',
];
