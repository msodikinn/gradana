Vue.component('linkid', {
  data: function() {
    return { count: 0 };
  },
});


var vm = new Vue({
  http: {
    root: '/root',
    headers: {
      'X-CSRF-TOKEN': document.querySelector('#token').getAttribute('value')
    }
  },

  el: '#EditController',

  Vue.use(VueRouter)

  var router = new VueRouter({
    history: false
  }),

  router.map({
    '/admin/perbankan/detail/:id': {
        component: linkid
    },
  }),

  data: {
      childs: {},
      pchilds: {id:'', perbankanid: '', nasabah_bulan: '', nasabah_tahun: '', jenis: '', nama_bank:'', rekening_kartu:'', saldo_limit_plafon:''},
      jenisN: [{value:'Tabungan / Giro / Deposito', text:'Tabungan / Giro / Deposito'},
        {value:'Kartu Kredit BCA', text:'Kartu Kredit BCA'},
        {value:'Kartu Kredit Lainnya', text:'Kartu Kredit Lainnya'},
        {value:'Kredit / Pinjaman', text:'Kredit / Pinjaman'}],
      selected: ''
  },

  created: function(){
      this.fetchChild()
  },

  methods: {
      fetchChild: function (id) {
          var id = this.$route.params.id;
          this.$http.get('/admin/perbankan/api/pchilds/fetch/' + id).then(function(response) {
              this.$set('childs', response.data)
              console.log(id)
          })
      },

      editChild: function (id) {
          var userbank = this.pchilds
          alert(userbank.jenis)

          this.pchilds = {id:'', perbankanid: '', nasabah_bulan: '', nasabah_tahun: '', jenis: '', nama_bank:'', rekening_kartu:'', saldo_limit_plafon:''}

          this.$http.put('/admin/perbankan/api/pchilds/' + id, userbank).then(function (response) {
              console.log(response.data)
          })
          // alert(userbank.saldo_limit_plafon)
          window.location.reload()
      },

      showChild: function (id) {
          this.$http.get('/admin/perbankan/api/pchilds/edit/' + id).then(function(response) {
              var data = response.data
              this.pchilds.id                 = data.id
              this.pchilds.nasabah_bulan      = data.nasabah_bulan
              this.pchilds.nasabah_tahun      = data.nasabah_tahun
              this.pchilds.jenis              = data.jenis
              this.selected                   = data.jenis
              this.pchilds.nama_bank          = data.nama_bank
              this.pchilds.rekening_kartu     = data.rekening_kartu
              this.pchilds.saldo_limit_plafon = data.saldo_limit_plafon
          })
      },

      delChild: function (id) {
          var ConfirmBox = confirm("Hapus data perbankan ini?")

          if(ConfirmBox) {
            this.$http.delete('/admin/perbankan/api/pchilds/' + id)
            window.location.reload()
          }
      },

      addChild: function () {
        //
      },

      computed: {
          //
      }
  }
})
