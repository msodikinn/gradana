$(document).ready(function() {
	 $('#content').summernote({
	  toolbar: [
	            ['undo', ['undo', 'redo']],
	            ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
	            ['font', ['strikethrough', 'superscript', 'subscript', ]],
	            ['fontsize', ['fontname', 'fontsize', 'color']],
	            ['para', ['ul', 'ol', 'paragraph', 'height']],
	            ['table', ['table', 'link', 'hr']], //, 'picture', 'video'
	            ['fullscreen', ['fullscreen', 'codeview', 'help']],
	          ]
	        });
	 $("#publishdate").datetimepicker({format: 'MM/DD/YYYY'}).parent().css("position :relative");
       $('input[type=file]').on('change', function(e){
       	mapuploadfile = $(this)[0].files[0];
       	var readImg = new FileReader();
       	readImg.readAsDataURL(mapuploadfile);
       	readImg.onload = function(e) {
       		$('#imgshow').attr('src',e.target.result);
       		$('#btnhapus').removeClass('hide');
       	}
       });
       $('#btnhapus').click(function(){
       	$(this).addClass('hide');
       	$('#imgshow').attr('src',null);
       	$('input[type=file]').val(null);
       });
});
var vm = new Vue({
	el :'#formaction',
	data : {
		publishoptionvalue : 'Now',
		scheduleshow : false,
	},
	methods:{
		publishedchange:function(e) {
			if (e.target.value == 'Schedule') {
				this.scheduleshow = true;
			} else {
				this.scheduleshow = false;
			}
		}
	},
});

