var vm = new Vue({
el :'#formdata',
data : {
	gallerychoose : false,
	textaddphotobutton : 'Add Photo',
	listchooses : [],
	listpictures : [],
	isaddphoto: true,
	listphotoidvalue : [],
},
methods : {
	viewaddphotogallery:function(){
		this.gallerychoose = !this.gallerychoose;
		if (this.isaddphoto) {
			this.textaddphotobutton = 'Close Photo';
		} else {
			this.textaddphotobutton = 'Add Photo';
		}
		this.isaddphoto = !this.isaddphoto;
	},
	addpicturechoose : function(datalist){
		for (x in datalist) {
			this.listchooses.push({
				img_name : datalist[x].img,
				id : datalist[x].id,
			});
		}
	},
	addpictureingallery : function(datalist){
		for (x in datalist) {
			idtik = datalist[x].id;
			this.listphotoidvalue.push(idtik);
			this.listpictures.push({
				img_name : datalist[x].img,
				id : idtik,
			});
		}
	},
	addtothis : function(elem, e) {
		eltarget = e.target;
		id = eltarget.getAttribute('idimg');
		this.listphotoidvalue.push(id);
		this.listpictures.push({
			img_name : eltarget.getAttribute('src'),
			id : id,
		});
		this.listchooses.$remove(elem);
	},
	removefromthis : function(elem, e) {
		eltarget = e.target;
		id = parseInt(eltarget.getAttribute('idimg'));
		var index = this.listphotoidvalue.indexOf(id);
		this.listphotoidvalue.splice(index, 1);
		this.listchooses.push({
			img_name : eltarget.getAttribute('src'),
			id : id,
		});
		this.listpictures.$remove(elem);
	}
}
});
