<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investor extends Model
{
    protected $table = 'investors';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class, 'userid');
    }

    public function investoralokasis()
    {
        return $this->hasmany(InvestorAlokasi::class, 'investorid');
    }

    public function investor_cat()
    {
        return $this->belongsTo(Investor_cat::class, 'catid');
    }

    public function applications()
    {
        return $this->hasMany(Application::class, 'investorid');
    }

    public function editUrl()
    {
        return url("/admin/investor/edit/{$this->id}");
    }

    public function delUrl()
    {
        return url("/admin/investor/delete/{$this->id}");
    }

    public function detail()
    {
        return url("/admin/investor/detail/{$this->id}");
    }

    public function approved()
    {
        return url("/admin/investor/approved/{$this->id}");
    }
}
