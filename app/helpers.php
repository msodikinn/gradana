<?php

use Carbon\Carbon;

function format($date)
{
    if ($date == "0000-00-00") {
        return null;
    } else {
        return date('d F Y', strtotime($date));
    }
}

function formatLengkap($date)
{
    return date('d F Y H:i:s T', strtotime($date));
}

function tanggal($tgl)
{
    return date('d', strtotime($tgl));
}

function bulan($bln)
{
    return date('M', strtotime($bln));
}

function uang($uang)
{
    return number_format($uang,0,",",".");
}

function statusPay($pay)
{
    if ($pay <= 15) {
        return "OK";
    }else{
        return "Terlambat";
    }
}

function badPay($payment)
{
    $late = 0;
    foreach ($payment as $p) {
        $a = statusPay(tanggal($p->tgl_pembayaran));
        if($a == "Terlambat")
        {
            $late = $late + 1;
        }
    }
    return $late;
}

function defaultpic()
{
    return "/assets/img/default-user-image.png";
}

function defaultcompany()
{
    return "/assets/img/default-company-image.jpg";
}

function ainterest($asses)
{
    $max = $asses->interest_annum_max;
    $min = $asses->interest_annum_min;
    $rating = $asses->credit_rating;
    $first = $rating / 5;
    $second = ($max - $min) * $first;
    return $max - $second;
}

function permission()
{
    return Sentinel::getuser()->status;
}

function idActive()
{
    return Sentinel::getuser()->id;
}

function approve($id)
{
    if ($id == 0) {
        return "Unapproved";
    }else{
        return  "Approved";
    }
}

function rngBulan()
{
    for ($month=1; $month <= 12; $month++) {
        $bln = date_parse_from_format("n", $month);

    };
    return $bln;
}
