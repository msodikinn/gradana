<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sentinel;

class JobSuamiIstri extends Model
{
    protected $table = 'job_suami_istris';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function propinsis()
    {
        return $this->belongsTo(Propinsi::class, 'propinsi');
    }

    public function kotas()
    {
        return $this->belongsTo(Kota::class, 'kota');
    }

    public function desas()
    {
        return $this->belongsTo(Desa::class, 'kelurahan');
    }

    public function kecamatans()
    {
        return $this->belongsTo(Kecamatan::class, 'kecamatan');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'userid');
    }

    public function pemohon()
    {
        return $this->belongsTo(Pemohon::class, 'pemohonid');
    }

    public function penghasilan()
    {
        return $this->hasOne(Penghasilan::class, 'pemohonid');
    }

    public function editUrl()
    {
        return url("/admin/jobsuamiistri/edit/{$this->id}");
    }

    public function delUrl()
    {
        return url("/admin/jobsuamiistri/delete/{$this->id}");
    }

    public function detailUrl()
    {
        return url("/admin/jobsuamiistri/detail/{$this->id}");
    }

    public function userUrl()
    {
        return url("/admin/users/{$this->user->id}");
    }

    public function filterJobPasangan()
    {
        if(Sentinel::getUser()->status == "Admin"){
            $jobpasangan = JobSuamiIstri::with('penghasilan')->get();
        }else{
            $jobpasangan = JobSuamiIstri::with('penghasilan')->where('userid', idActive())->get();
        }

        return $jobpasangan;
    }
}
