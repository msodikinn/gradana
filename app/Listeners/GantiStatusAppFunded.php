<?php

namespace App\Listeners;

use App\Events\InvestorMembayar;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class GantiStatusAppFunded
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InvestorMembayar  $event
     * @return void
     */
    public function handle(InvestorMembayar $event)
    {
        //
    }
}
