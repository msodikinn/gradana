<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perbankan_child extends Model
{
    protected $table = 'perbankan_children';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function perbankan()
    {
        return $this->belongsTo(Perbankan::class, 'perbankanid');
    }

    public function editUrl()
    {
        return url("/admin/perbankan_child/edit/{$this->id}");
    }

    public function delUrl()
    {
        return url("/admin/perbankan_child/delete/{$this->id}");
    }

    // public function editChild()
    // {
    //     return url("/admin/perbankan/pchild/edit/{$this->id}");
    // }
    public function editChild()
    {
        return url("/admin/perbankan_child/edit/{$this->id}");
    }

    public function delChild()
    {
        return url("/admin/perbankan_child/delete/{$this->id}/{$this->perbankanid}");
    }
}

