<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Propinsi extends Model
{
    protected $table = 'provinces';
    public $timestamps = false;

    public function propinsi()
    {
        return $this->hasMany(Pemohon::class, 'propinsi_ktp');
    }

    public function kota()
    {
        return $this->hasMany(Kota::class, 'province_id');
    }
}
