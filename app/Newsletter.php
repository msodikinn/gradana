<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $table = 'newsletters';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function edit()
    {
        return url("/admin/newsletter/edit/{$this->id}");
    }

    public function del()
    {
        return url("/admin/newsletter/delete/{$this->id}");
    }

    public function detail()
    {
        return url("/admin/newsletter/detail/{$this->id}");
    }

    public function send()
    {
        return url("/admin/newsletter/send/{$this->id}");
    }
}
