<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $table = 'surveys';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function application()
    {
        return $this->belongsTo(Application::class, 'appid');
    }

    public function editUrl()
    {
        return url("/admin/survey/edit/{$this->id}");
    }

    public function delUrl()
    {
        return url("/admin/survey/delete/{$this->id}");
    }

    public function detailUrl()
    {
        return url("/admin/survey/detail/{$this->id}");
    }

    public function appUrl()
    {
        return url("/admin/application/detail/{$this->application->id}");
    }
}
