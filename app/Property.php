<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = 'properties';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class, 'userid');
    }

    public function developer()
    {
        return $this->belongsTo(Developer::class, 'devid');
    }

    public function galleries()
    {
        return $this->hasMany(Gallery::class, 'propid');
    }

    public function subproperties()
    {
        return $this->hasMany(SubProperty::class, 'propid');
    }

    public function editUrl()
    {
        return url("/admin/property/edit/{$this->id}");
    }

    public function delUrl()
    {
        return url("/admin/property/delete/{$this->id}");
    }

    public function detailUrl()
    {
        return url("/admin/property/detail/{$this->id}");
    }

    public function userUrl()
    {
        return url("/admin/users/{$this->user->id}");
    }

    public function devUrl()
    {
        return url("/admin/developer/detail/{$this->devid}");
    }

    public function approved()
    {
        return url("/admin/property/approved/{$this->id}");
    }
}
