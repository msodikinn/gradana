<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table = 'districts';
    public $timestamps = false;

    public function kota()
    {
        return $this->hasMany(Kota::class, 'regency_id');
    }

    public function desa()
    {
        return $this->hasMany(Desa::class, 'district_id');
    }
}
