<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sentinel;

class Perbankan extends Model
{
    protected $table = 'perbankans';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class, 'userid');
    }

    public function pemohon()
    {
        return $this->belongsTo(Pemohon::class, 'pemohonid');
    }

    public function perbankan_childs()
    {
        return $this->hasMany(Perbankan_child::class, 'perbankanid');
    }

    public function editUrl()
    {
        return url("/admin/perbankan/edit/{$this->id}");
    }

    public function delUrl()
    {
        return url("/admin/perbankan/delete/{$this->id}");
    }

    public function detailUrl()
    {
        return url("/admin/perbankan/detail/{$this->id}");
    }

    public function userUrl()
    {
        return url("/admin/users/{$this->user->id}");
    }

    public function editChild()
    {
        return url("/admin/perbankan_child/edit/{$this->perbankan_child->first()->id}");
    }

    public function filterPerbankan()
    {
        if(Sentinel::getUser()->status == "Admin"){
            $Perbankan = Perbankan::all();
        }else{
            $Perbankan = Perbankan::where('userid', idActive())->get();
        }

        return $Perbankan;
    }
}
