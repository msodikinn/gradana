<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investor_cat extends Model
{
    protected $table = 'investor_cats';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function investor()
    {
        return $this->hasMany(Investor::class, 'catid');
    }

    public function editUrl()
    {
        return url("/admin/investor_cat/edit/{$this->id}");
    }

    public function delUrl()
    {
        return url("/admin/investor_cat/delete/{$this->id}");
    }
}
