<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assesment extends Model
{
    protected $table = 'assesments';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function application()
    {
        return $this->belongsTo(Application::class, 'appid');
    }

    public function editUrl()
    {
        return url("/admin/assesment/edit/{$this->id}");
    }

    public function delUrl()
    {
        return url("/admin/assesment/delete/{$this->id}");
    }

    public function detailUrl()
    {
        return url("/admin/assesment/detail/{$this->id}");
    }

    public function appUrl()
    {
        return url("/admin/application/detail/{$this->application->id}");
    }
}
