<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Application;
use App\Developer;
use App\Property;
use Carbon\Carbon;
use Mail;

class SendReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mengirim reminder angsuran kepada borrower.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $appApproved = Application::where('approved', '1')->get();

        foreach ($appApproved as $aa)
        {
            $id = $aa->id;
            $email = $aa->user->email;
            $fullname = $aa->user->fullname;
            $namaDev = $aa->developer->nama;
            $namaProp = $aa->property->nama;
            Mail::queue('emails.payreminder', ['id' => $id, 'email' => $email, 'fullname' => $fullname, 'namaDev' => $namaDev, 'namaProp' => $namaProp], function ($m) use ($email, $fullname)
            {
                $m->to($email, $fullname);
                $m->subject('Gradana Payment Reminder');
            });
        }
    }
}
