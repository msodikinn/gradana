<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'galleries';

    protected $guarded = ['id'];

    public function properyimg()
    {
        return $this->hasMany(Propertyimg::class, 'gallery_id');
    }
    public function property()
    {
    	return $this->belongsTo(Property::class, 'propid');
    }
}
