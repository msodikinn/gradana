<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sentinel;

class JobPemohon extends Model
{
    protected $table = 'job_pemohons';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function propinsis()
    {
        return $this->belongsTo(Propinsi::class, 'propinsi');
    }

    public function kotas()
    {
        return $this->belongsTo(Kota::class, 'kota');
    }

    public function desas()
    {
        return $this->belongsTo(Desa::class, 'kelurahan');
    }

    public function kecamatans()
    {
        return $this->belongsTo(Kecamatan::class, 'kecamatan');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'userid');
    }

    public function pemohon()
    {
        return $this->belongsTo(Pemohon::class, 'pemohonid');
    }

    public function penghasilan()
    {
        return $this->hasOne(Penghasilan::class, 'pemohonid');
    }

    public function editUrl()
    {
        return url("/admin/jobpemohon/edit/{$this->id}");
    }

    public function delUrl()
    {
        return url("/admin/jobpemohon/delete/{$this->id}");
    }

    public function detailUrl()
    {
        return url("/admin/jobpemohon/detail/{$this->id}");
    }

    public function userUrl()
    {
        return url("/admin/users/{$this->user->id}");
    }

    public function filterJobPemohon()
    {
        if(Sentinel::getUser()->status == "Admin"){
            $jobP = JobPemohon::all();
        }else{
            $jobP = JobPemohon::where('userid', idActive())->get();
        }

        return $jobP;
    }
}
