<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sentinel;

class Keluarga extends Model
{
    protected $table = 'keluargas';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class, 'userid');
    }

    public function pemohon()
    {
        return $this->belongsTo(Pemohon::class, 'pemohonid');
    }

    public function editUrl()
    {
        return url("/admin/keluarga/edit/{$this->id}");
    }

    public function delUrl()
    {
        return url("/admin/keluarga/delete/{$this->id}");
    }

    public function detailUrl()
    {
        return url("/admin/keluarga/detail/{$this->id}");
    }

    public function userUrl()
    {
        return url("/admin/users/{$this->user->id}");
    }

    public function filterKeluarga()
    {
        if(Sentinel::getUser()->status == "Admin"){
            $keluarga = Keluarga::all();
        }else{
            $keluarga = Keluarga::where('userid', idActive())->get();
        }

        return $keluarga;
    }
}
