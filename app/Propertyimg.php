<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Propertyimg extends Model
{
    protected $table = 'propertyimgs';
    protected $guarded = ['id'];
    public $timestamps = true;

    public function gallery()
    {
        return $this->belongsTo(Gallery::class, 'gallery_id');
    }
}
