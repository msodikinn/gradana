<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sentinel;

class SuamiIstri extends Model
{
    protected $table = 'suami_istris';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class, 'userid');
    }

    public function pemohon()
    {
        return $this->belongsTo(Pemohon::class, 'pemohonid');
    }

    public function editUrl()
    {
        return url("/admin/suamiistri/edit/{$this->id}");
    }

    public function delUrl()
    {
        return url("/admin/suamiistri/delete/{$this->id}");
    }

    public function detailUrl()
    {
        return url("/admin/suamiistri/detail/{$this->id}");
    }

    public function userUrl()
    {
        return url("/admin/users/{$this->user->id}");
    }

    public function filterPasangan()
    {
        if(Sentinel::getUser()->status == "Admin"){
            $pasangan = SuamiIstri::all();
        }else{
            $pasangan = SuamiIstri::where('userid', idActive())->get();
        }

        return $pasangan;
    }
}
