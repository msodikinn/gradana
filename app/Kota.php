<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    protected $table = 'regencies';
    public $timestamps = false;

    public function propinsi()
    {
        return $this->belongsTo(Propinsi::class, 'province_id');
    }

    public function kecamatan()
    {
        return $this->hasMany(Kecamatan::class, 'regency_id');
    }
}
