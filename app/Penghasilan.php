<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sentinel;

class Penghasilan extends Model
{
    protected $table = 'penghasilans';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class, 'userid');
    }

    public function application()
    {
        return $this->belongsTo(Application::class, 'userid');
    }

    public function pemohon()
    {
        return $this->belongsTo(Pemohon::class, 'pemohonid');
    }

    public function jobpemohon()
    {
        return $this->belongsTo(JobPemohon::class, 'pemohonid');
    }

    public function jobsuamiistri()
    {
        return $this->belongsTo(JobSuamiIstri::class, 'pemohonid');
    }

    public function editUrl()
    {
        return url("/admin/penghasilan/edit/{$this->id}");
    }

    public function delUrl()
    {
        return url("/admin/penghasilan/delete/{$this->id}");
    }

    public function detailUrl()
    {
        return url("/admin/penghasilan/detail/{$this->id}");
    }

    public function userUrl()
    {
        return url("/admin/users/{$this->user->id}");
    }

    public function filterPenghasilan()
    {
        if(Sentinel::getUser()->status == "Admin"){
            $Penghasilan = Penghasilan::all();
        }else{
            $Penghasilan = Penghasilan::where('userid', idActive())->get();
        }

        return $Penghasilan;
    }
}
