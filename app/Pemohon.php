<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sentinel;

class Pemohon extends Model
{
    protected $table = 'pemohons';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function users()
    {
        return $this->belongsTo(User::class, 'userid');
    }

    public function application()
    {
        return $this->hasOne(Application::class, 'pemohonid');
    }

    public function jobpemohon()
    {
        return $this->hasOne(JobPemohon::class, 'pemohonid');
    }

    public function suamiistri()
    {
        return $this->hasOne(SuamiIstri::class, 'pemohonid');
    }

    public function jobsuamiistri()
    {
        return $this->hasOne(JobSuamiIstri::class, 'pemohonid');
    }

    public function keluarga()
    {
        return $this->hasOne(Keluarga::class, 'pemohonid');
    }

    public function penghasilan()
    {
        return $this->hasOne(Penghasilan::class, 'pemohonid');
    }

    public function perbankan()
    {
        return $this->hasOne(Perbankan::class, 'pemohonid');
    }

    public function propinsis()
    {
        return $this->belongsTo(Propinsi::class, 'propinsi_ktp');
    }

    public function editUrl()
    {
        return url("/admin/pemohon/edit/{$this->id}");
    }

    public function delUrl()
    {
        return url("/admin/pemohon/delete/{$this->id}");
    }

    public function detailUrl()
    {
        return url("/admin/pemohon/detail/{$this->id}");
    }

    public function userUrl()
    {
        return url("/admin/users/{$this->users->id}");
    }

    public function approved()
    {
        return url("/admin/pemohon/approved/{$this->id}");
    }

    public function unapproved()
    {
        return url("/admin/pemohon/upapproved/{$this->id}");
    }
	
	public function ragu()
    {
        return url("/admin/pemohon/ragu/{$this->id}");
    }
	
	

    public function filterPemohon()
    {
        if(Sentinel::getUser()->status == "Admin"){
            $pem = Pemohon::all();
        }else{
            $pem = Pemohon::where('userid', idActive())->get();
        }

        return $pem;
    }
}
