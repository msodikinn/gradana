<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sentinel;

class Pembayaran extends Model
{
    protected $table = 'pembayarans';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function users()
    {
        return $this->belongsTo(User::class, 'nasabah');
    }

    public function application()
    {
        return $this->belongsTo(Application::class, 'appid');
    }

    public function editUrl()
    {
        return url("/admin/pembayaran/edit/{$this->id}");
    }

    public function delUrl()
    {
        return url("/admin/pembayaran/delete/{$this->id}");
    }

    public function detailUrl()
    {
        return url("/admin/pembayaran/detail/{$this->id}");
    }

    public function appUrl()
    {
        return url("/admin/application/detail/{$this->application->id}");
    }

    public function approved()
    {
        return url("/admin/pembayaran/approved/{$this->id}");
    }

    public function payment()
    {
        if(Sentinel::getUser()->status == "Admin"){
            $byr = Pembayaran::all();
        }elseif(Sentinel::getUser()->status == "Investor"){
            $byr = Pembayaran::where('nasabah', Sentinel::getUser()->id)->get();
        }else{
            $byr = Pembayaran::where('nasabah', Sentinel::getUser()->id)->get();
        }

        return $byr;
    }
}
