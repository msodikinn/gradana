<?php
namespace App\Helpers;
class Payment {
    public static function getTotal($query){
        $total = [];
        foreach ($query as $q) {
            $t = $q->periode;
            $b = $q->jangka_waktu;
            $total[] = ($t*12) + $b;
        }
        return $total;
    }

    public static function getSingle($t, $b){
        $totalSingle = ($t*12) + $b;
        return $totalSingle;
    }
}
