<?php
namespace App\Helpers;
class Score {
    // Plafon / Angsuran Max
    public static function getPlafon($thp) {
        if ($thp >= 50000000 AND $thp < 10000000) {
            return $thp * 0.4;
        } elseif ($thp >= 10000000 AND $thp < 25000000) {
            return $thp * 0.5;
        } elseif ($thp >= 25000000) {
            return $thp * 0.65;
        } else {
            return $thp;
        }
    }

    // Skor Jumlah Hutang
    public static function getJmlHutang($hutang) {
        if ($hutang < 10) {
            return 5;
        } elseif ($hutang >= 10 AND $hutang < 20) {
            return 4;
        } elseif ($hutang >= 20 AND $hutang < 30) {
            return 3;
        } elseif ($hutang >= 30 AND $hutang < 40) {
            return 2;
        } elseif ($hutang >= 40 AND $hutang < 50) {
            return 1;
        } elseif ($hutang >= 50) {
            return 0;
        }
    }

    // Skor Pinjaman High Interest
    public static function getHI($net, $pinjHI) {
        $percent60 = $net * 0.6;
        $percent75 = $net * 0.75;
        $percent90 = $net * 0.9;
        $percent100 = $net;
        $percent105 = $net * 1.05;

        if ($pinjHI <= $percent60) {
            return 5;
        } elseif ($pinjHI > $percent60 AND $pinjHI <= $percent75) {
            return 4;
        } elseif ($pinjHI > $percent75 AND $pinjHI <= $percent90) {
            return 3;
        } elseif ($pinjHI >= $percent100) {
            return 2;
        } elseif ($pinjHI > $percent100 AND $pinjHI <= $percent105) {
            return 1;
        } elseif ($pinjHI > $percent105) {
            return 0;
        }
    }

    // Skor jumlah kartu kredit
    public static function getCC($cc) {
        $jmlCC = explode(' ',trim($cc));
        // dd($jmlCC[3]);
        if ($jmlCC[0] == "5") {
            return 5;
        } elseif ($jmlCC[0] == "4") {
            return 4;
        } elseif ($jmlCC[0] == "3") {
            return 3;
        } elseif ($jmlCC[0] == "2") {
            return 2;
        } elseif ($jmlCC[0] == "1") {
            return 1;
        } elseif ($jmlCC[0] == "0") {
            return 5;
        }
    }

    // Total limit kartu kredit
    public static function getTotLimit($limit, $thp) {
        $percent50 = $thp * 0.5;
        $percent40 = $thp * 0.4;
        $percent30 = $thp * 0.3;
        $percent20 = $thp * 0.2;

        if ($limit >= $percent50) {
            return 5;
        } elseif ($limit >= $percent40 AND $limit < $percent50) {
            return 4;
        } elseif ($limit >= $percent30 AND $limit < $percent40) {
            return 3;
        } elseif ($limit >= $percent20 AND $limit < $percent30) {
            return 2;
        } elseif ($limit < $percent20) {
            return 1;
        }
    }

    // lama kerja
    public static function getLamaKerja($lama) {
        if ($lama == "Tidak Bekerja") {
            return 0;
        } elseif ($lama == "≥ 5 tahun") {
            return 5;
        } elseif ($lama == "4 ≤ lama kerja < 5 tahun") {
            return 4;
        } elseif ($lama == "3 ≤ lama kerja < 4 tahun") {
            return 3;
        } elseif ($lama == "2 ≤ lama kerja < 3 tahun") {
            return 2;
        } elseif ($lama == "< 2 tahun") {
            return 1;
        }
    }

    // total lama kerja
    public static function getTotLamaKerja($total) {
        if ($total == "Tidak Bekerja") {
            return 0;
        } elseif ($total == "≥ 5 tahun") {
            return 5;
        } elseif ($total == "4 ≤ total < 5 tahun") {
            return 4;
        } elseif ($total == "3 ≤ total < 4 tahun") {
            return 3;
        } elseif ($total == "2 ≤ total < 3 tahun") {
            return 2;
        } elseif ($total == "< 2 tahun") {
            return 1;
        }
    }

    // saldo 3 bulan
    public static function getSaldo3Bln($saldo, $thp) {
        $percent40 = $thp * 0.4;
        $percent30 = $thp * 0.3;
        $percent20 = $thp * 0.2;
        $percent10 = $thp * 0.1;

        if ($saldo >= $percent40) {
            return 5;
        } elseif ($saldo >= $percent30 AND $saldo < $percent40) {
            return 4;
        } elseif ($saldo >= $percent20 AND $saldo < $percent30) {
            return 3;
        } elseif ($saldo >= $percent10 AND $saldo < $percent20) {
            return 2;
        } elseif ($saldo < $percent10) {
            return 1;
        }
    }

    // kelengkapan data
    public static function getKelengkapan($data) {
        if ($data == "100% Complete") {
            return 5;
        } elseif ($data == "95% ≤ data < 100% Complete") {
            return 4;
        } elseif ($data == "90% ≤ data < 95% Complete") {
            return 3;
        } elseif ($data == "80% ≤ data < 90% Complete") {
            return 2;
        } elseif ($data == "< 80% Complete") {
            return 1;
        } elseif ($data == "0% Complete") {
            return 0;
        }
    }

    // akurasi data
    public static function getAkurasi($data) {
        if ($data == "100% Accurate") {
            return 5;
        } elseif ($data == "95% ≤ data < 100% Accurate") {
            return 4;
        } elseif ($data == "90% ≤ data < 95% Accurate") {
            return 3;
        } elseif ($data == "80% ≤ data < 90% Accurate") {
            return 2;
        } elseif ($data == "< 80% Accurate") {
            return 1;
        } elseif ($data == "0% Accurate") {
            return 0;
        }
    }

    // asset bank
    public static function getAsset($asset, $pinj) {
        $percent50 = $pinj * 0.5;
        $percent40 = $pinj * 0.4;
        $percent30 = $pinj * 0.3;
        $percent20 = $pinj * 0.2;

        if ($asset >= $percent50) {
            return 5;
        } elseif ($asset >= $percent40 AND $asset < $percent50) {
            return 4;
        } elseif ($asset >= $percent30 AND $asset < $percent40) {
            return 3;
        } elseif ($asset >= $percent20 AND $asset < $percent30) {
            return 2;
        } elseif ($asset < $percent20) {
            return 1;
        }
    }
}
