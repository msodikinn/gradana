<?php
namespace App\Helpers;
class Support {
	public static function getmonthname($i){
		if ($i == 1) {
			return 'Januari';
		} else if ($i == 2) {
			return 'Februari';
		} else if ($i == 3) {
			return 'Maret';
		} else if ($i == 4) {
			return 'April';
		} else if ($i == 5) {
			return 'Mei';
		} else if ($i == 6) {
			return 'Juni';
		} else if ($i == 7) {
			return 'Juli';
		} else if ($i == 8) {
			return 'Agustus';
		} else if ($i == 9) {
			return 'September';
		} else if ($i == 10) {
			return 'Oktober';
		} else if ($i == 11) {
			return 'November';
		}
		return 'Desember';
	}
}