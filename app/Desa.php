<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Desa extends Model
{
    protected $table = 'villages';
    public $timestamps = false;

    public function kecamatan()
    {
        return $this->hasMany(Kecamatan::class, 'district_id');
    }
}
