<?php namespace App;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends EloquentUser {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes to be fillable from the model.
	 *
	 * A dirty hack to allow fields to be fillable by calling empty fillable array
	 *
	 * @var array
	 */
	protected $fillable = [];
	protected $guarded = ['id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	/**
	* To allow soft deletes
	*/
	use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function applications()
    {
        return $this->hasMany(Application::class, 'userid');
    }

    public function investors()
    {
        return $this->hasOne(Investor::class, 'userid');
    }

    public function pemohon()
    {
        return $this->hasOne(Pemohon::class, 'userid');
    }

    public function jaminan()
    {
        return $this->hasMany(Jaminan::class, 'userid');
    }

    public function suamiistri()
    {
        return $this->hasMany(SuamiIstri::class, 'userid');
    }

    public function Keluarga()
    {
        return $this->hasMany(Keluarga::class, 'userid');
    }

    public function pekerjaan()
    {
        return $this->hasMany(Pekerjaan::class, 'userid');
    }

    public function penghasilan()
    {
        return $this->hasMany(Penghasilan::class, 'userid');
    }

    public function perbankan()
    {
        return $this->hasMany(Perbankan::class, 'userid');
    }

    public function pemohonUrl()
    {
        return url("/admin/pemohon/detail/{$this->id}");
    }
}
