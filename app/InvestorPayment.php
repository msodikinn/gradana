<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvestorPayment extends Model
{
    // protected $table = 'investorpayments';
    protected $table = 'pembayarans';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function application()
    {
        return $this->belongsTo(Application::class, 'appid');
    }

    public function investorAlokasi()
    {
        return $this->belongsTo(InvestorAlokasi::class, 'alokasiid');
    }

    public function editUrl()
    {
        return url("/admin/investorpayment/edit/{$this->id}");
    }

    public function delUrl()
    {
        return url("/admin/investorpayment/delete/{$this->id}");
    }

    public function detailUrl()
    {
        return url("/admin/investorpayment/detail/{$this->id}");
    }

    public function appUrl()
    {
        return url("/admin/application/detail/{$this->application->id}");
    }

    public function invesUrl()
    {
        return url("/admin/investor/detail/{$this->investor->id}");
    }

    public function approved()
    {
        return url("/admin/investorpayment/approved/{$this->id}");
    }

    public function unapproved()
    {
        return url("/admin/investorpayment/unapproved/{$this->id}");
    }

}
