<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Developer extends Model
{
    protected $table = 'developers';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function applications()
    {
        return $this->hasMany(Application::class, 'devid');
    }

    public function properties()
    {
        return $this->hasMany(Property::class, 'devid');
    }

    public function editUrl()
    {
        return url("/admin/developer/edit/{$this->id}");
    }

    public function delUrl()
    {
        return url("/admin/developer/delete/{$this->id}");
    }

    public function detailUrl()
    {
        return url("/admin/developer/detail/{$this->id}");
    }

    public function approved()
    {
        return url("/admin/developer/approved/{$this->id}");
    }
}
