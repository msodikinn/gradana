<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubProperty extends Model
{
    protected $table = 'subproperties';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function property()
    {
        return $this->belongsTo(Property::class, 'propid');
    }

    public function editUrl()
    {
        return url("/admin/property/editsub/");
    }

    public function delUrl()
    {
        return url("/admin/property/delete/product/{$this->id}");
    }
}
