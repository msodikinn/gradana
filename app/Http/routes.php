<?php

Route::model('blogcategory', 'App\BlogCategory');
Route::model('blog', 'App\Blog');
Route::model('users', 'App\User');

Route::pattern('slug', '[a-z0-9- _]+');


require(app_path() . '/Http/routes/admin.php');
require(app_path() . '/Http/routes/front.php');
