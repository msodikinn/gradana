<?php

namespace App\Http\Requests;

class PictureRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
			'pic_file' => 'required|image',
		];
    }
}

