<?php

namespace App\Http\Requests;

class GalleryRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'catid' => 'required',
            'jenis_usaha' => 'required',
            'nama' => 'required|min:3',
            'telp' => 'required',
            'alamat' => 'required'
        ];
    }
}

