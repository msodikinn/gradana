<?php

Route::group(array('prefix' => 'admin'), function () {

    # Error pages should be shown without requiring login
    Route::get('404', function () {
        return View('admin/404');
    });
    Route::get('500', function () {
        return View::make('admin/500');
    });

    Route::controller('site', 'PropinsiController');

    # Lock screen
    Route::get('{id}/lockscreen', array('as' => 'lockscreen', 'uses' => 'UsersController@lockscreen'));

    # All basic routes defined here
    Route::get('signin', array('as' => 'signin', 'uses' => 'AuthController@getSignin'));
    Route::post('signin', 'AuthController@postSignin');
    Route::post('signup', array('as' => 'signup', 'uses' => 'AuthController@postSignup'));
    Route::post('forgot-password', array('as' => 'forgot-password', 'uses' => 'AuthController@postForgotPassword'));
    Route::get('login2', function () {
        return View::make('admin/login2');
    });

    # Forgot Password Confirmation
    Route::get('forgot-password/{userId}/{passwordResetCode}', array('as' => 'forgot-password-confirm', 'uses' => 'AuthController@getForgotPasswordConfirm'));
    Route::post('forgot-password/{userId}/{passwordResetCode}', 'AuthController@postForgotPasswordConfirm');

    # Logout
    Route::get('logout', array('as' => 'logout', 'uses' => 'AuthController@getLogout'));

    # Account Activation
    Route::get('activate/{userId}/{activationCode}', array('as' => 'activate', 'uses' => 'AuthController@getActivate'));
});

Route::group(array('prefix' => 'admin', 'middleware' => 'SentinelAdmin'), function () {
    # Dashboard / Index
    Route::get('/', array('as' => 'dashboard','uses' => 'JoshController@showHome'));

    #################### PERMOHONAN ####################
    Route::group(array('prefix' => 'permohonan'), function () {
        Route::get('/', array('as' => 'permohonan','uses' => 'PermohonanController@index'));
        Route::get('create', array('as' => 'permohonanCreate', 'uses' => 'PermohonanController@create'));
        Route::post('create', 'PermohonanController@store');
        Route::get('detail/{id}', 'PermohonanController@show');
        Route::get('delete/{id}', 'PermohonanController@delete');
        Route::get('edit/{id}', array('as' => 'permohonanEdit', 'uses' => 'PermohonanController@edit'));
        Route::put('edit/{id}', 'PermohonanController@update');
        Route::get('delete/{id}', 'PermohonanController@destroy');
    });

    #################### INVESTOR CATEGORY ####################
    Route::group(array('prefix' => 'investor_cat'), function () {
        Route::get('/', array('as' => 'investor_cat','uses' => 'Investor_catController@index'));
        Route::get('create', array('as' => 'investor_catCreate', 'uses' => 'Investor_catController@create'));
        Route::post('create', 'Investor_catController@store');
        Route::get('delete/{id}', 'Investor_catController@delete');
        Route::get('edit/{id}', array('as' => 'investor_catEdit', 'uses' => 'Investor_catController@edit'));
        Route::put('edit/{id}', 'Investor_catController@update');
        Route::get('delete/{id}', 'Investor_catController@destroy');
    });

    #################### INVESTOR ADMIN ####################
    Route::group(array('prefix' => 'investor'), function () {
        Route::get('/', array('as' => 'investor','uses' => 'InvestorController@index'));
        Route::get('create', array('as' => 'investorCreate', 'uses' => 'InvestorController@create'));
        Route::post('create', 'InvestorController@store');
        Route::get('detail/{id}', 'InvestorController@show');
        Route::get('delete/{id}', 'InvestorController@delete');
        Route::get('edit/{id}', array('as' => 'investorEdit', 'uses' => 'InvestorController@edit'));
        Route::put('edit/{id}', 'InvestorController@update');
        Route::get('delete/{id}', 'InvestorController@destroy');
        Route::get('approved/{id}', array('as' => 'investorApproved', 'uses' => 'InvestorController@approved'));
        Route::get('borrower', 'InvestorController@avaborrow');
        Route::get('borrower/{appid}', 'InvestorController@borrowDetail');
    });

    #################### INVESTOR ALOKASI ####################
    Route::group(array('prefix' => 'investor/alokasi'), function () {
        Route::get('/', array('as' => 'alokasi','uses' => 'InvestorAlokasiController@index'));
        Route::get('create', array('as' => 'alokasiCreate', 'uses' => 'InvestorAlokasiController@create'));
        Route::post('create', 'InvestorAlokasiController@store');
        Route::get('detail/{id}', 'InvestorAlokasiController@show');
        Route::get('delete/{id}', 'InvestorAlokasiController@delete');
        Route::get('edit/{id}', array('as' => 'alokasiEdit', 'uses' => 'InvestorAlokasiController@edit'));
        Route::put('edit/{id}', 'InvestorAlokasiController@update');
        Route::get('delete/{id}', 'InvestorAlokasiController@destroy');
    });

    #################### INVESTOR PAYMENT ####################
    Route::group(array('prefix' => 'investorpayment'), function () {
        Route::get('/', array('as' => 'investorpayment','uses' => 'InvestorPaymentController@index'));
        // Route::get('create', array('as' => 'investorpaymentCreate', 'uses' => 'InvestorPaymentController@create'));
        Route::get('create/{alokasiid}/{appid}', array('as' => 'investorpaymentCreate','uses' => 'InvestorPaymentController@create'));
        Route::post('create/{id}', 'InvestorPaymentController@store');
        // Route::post('create', 'InvestorPaymentController@store');
        Route::get('detail/{id}', 'InvestorPaymentController@show');
        Route::get('delete/{id}', 'InvestorPaymentController@delete');
        Route::get('edit/{id}', array('as' => 'investorpaymentEdit', 'uses' => 'InvestorPaymentController@edit'));
        Route::put('edit/{id}', 'InvestorPaymentController@update');
        Route::get('delete/{id}', 'InvestorPaymentController@destroy');
        Route::get('approved/{id}', array('as' => 'applicationApproved', 'uses' => 'InvestorPaymentController@approved'));
        Route::get('unapproved/{id}', array('as' => 'applicationUnapproved', 'uses' => 'InvestorPaymentController@unapproved'));
    });


    #################### DEVELOPER ####################
    Route::group(array('prefix' => 'developer'), function () {
        Route::get('/', array('as' => 'developer','uses' => 'DeveloperController@index'));
        Route::get('create', array('as' => 'developerCreate', 'uses' => 'DeveloperController@create'));
        Route::post('create', 'DeveloperController@store');
        Route::get('detail/{id}', 'DeveloperController@show');
        Route::get('delete/{id}', 'DeveloperController@delete');
        Route::get('edit/{id}', array('as' => 'developerEdit', 'uses' => 'DeveloperController@edit'));
        Route::put('edit/{id}', 'DeveloperController@update');
        Route::get('delete/{id}', 'DeveloperController@destroy');
        Route::get('approved/{id}', array('as' => 'developerApproved', 'uses' => 'DeveloperController@approved'));
    });

    #################### PROPERTY ####################
    Route::group(array('prefix' => 'property'), function () {
        Route::get('/', array('as' => 'property','uses' => 'PropertyController@index'));
        Route::get('create', array('as' => 'propertyCreate', 'uses' => 'PropertyController@create'));
        Route::post('create', 'PropertyController@store');
        Route::get('detail/{id}', 'PropertyController@show');
        Route::get('delete/{id}', 'PropertyController@delete');
        Route::get('edit/{id}', array('as' => 'propertyEdit', 'uses' => 'PropertyController@edit'));
        Route::put('edit/{id}', 'PropertyController@update');
        Route::get('delete/{id}', 'PropertyController@destroy');
        Route::get('approved/{id}', array('as' => 'propertyApproved', 'uses' => 'PropertyController@approved'));
        Route::post('createsub', array('as' => 'createsub','uses' => 'PropertyController@createsub'));
        Route::put('editsub', array('as' => 'editsub','uses' => 'PropertyController@editsub'));
        Route::get('delete/product/{id}', 'PropertyController@destroysub');
    });

    #################### PROPERTY IMG ####################
    Route::group(array('prefix' => 'property_img'), function () {
        Route::get('/', array('as' => 'property_img','uses' => 'Property_imgController@index'));
        Route::get('create', array('as' => 'property_imgCreate', 'uses' => 'Property_imgController@create'));
        Route::post('create', 'Property_imgController@store');
        Route::get('detail/{id}', 'Property_imgController@show');
        Route::get('delete/{id}', 'Property_imgController@delete');
        Route::get('edit/{id}', array('as' => 'property_imgEdit', 'uses' => 'Property_imgController@edit'));
        Route::put('edit/{id}', 'Property_imgController@update');
        Route::get('delete/{id}', 'Property_imgController@destroy');
    });

    #################### PEMOHON ####################
    Route::group(array('prefix' => 'pemohon'), function () {
        Route::get('/', array('as' => 'pemohon','uses' => 'PemohonController@index'));
        Route::get('create', array('as' => 'pemohonCreate', 'uses' => 'PemohonController@create'));
        Route::post('create', 'PemohonController@store');
        Route::get('detail/{id}', 'PemohonController@show');
        Route::get('delete/{id}', 'PemohonController@delete');
        Route::get('edit/{id}', array('as' => 'pemohonEdit', 'uses' => 'PemohonController@edit'));
        Route::put('edit/{id}', 'PemohonController@update');
        Route::get('delete/{id}', 'PemohonController@destroy');
        Route::get('approved/{id}', array('as' => 'pemohonApproved', 'uses' => 'PemohonController@approved'));
        Route::get('unapproved/{id}', array('as' => 'pemohonUnapproved', 'uses' => 'PemohonController@unapproved'));
        Route::get('ragu/{id}', array('as' => 'pemohonRagu', 'uses' => 'PemohonController@ragu'));
    });

    #################### SUAMI/ISTRI ####################
    Route::group(array('prefix' => 'suamiistri'), function () {
        Route::get('/', array('as' => 'suamiistri','uses' => 'Suami_istriController@index'));
        Route::get('create', array('as' => 'suamiistriCreate', 'uses' => 'Suami_istriController@create'));
        Route::post('create', 'Suami_istriController@store');
        Route::get('detail/{id}', 'Suami_istriController@show');
        Route::get('delete/{id}', 'Suami_istriController@delete');
        Route::get('edit/{id}', array('as' => 'suamiistriEdit', 'uses' => 'Suami_istriController@edit'));
        Route::put('edit/{id}', 'Suami_istriController@update');
        Route::get('delete/{id}', 'Suami_istriController@destroy');
    });

    #################### KELUARGA ####################
    Route::group(array('prefix' => 'keluarga'), function () {
        Route::get('/', array('as' => 'keluarga','uses' => 'KeluargaController@index'));
        Route::get('create', array('as' => 'keluargaCreate', 'uses' => 'KeluargaController@create'));
        Route::post('create', 'KeluargaController@store');
        Route::get('detail/{id}', 'KeluargaController@show');
        Route::get('delete/{id}', 'KeluargaController@delete');
        Route::get('edit/{id}', array('as' => 'keluargaEdit', 'uses' => 'KeluargaController@edit'));
        Route::put('edit/{id}', 'KeluargaController@update');
        Route::get('delete/{id}', 'KeluargaController@destroy');
    });

    #################### JOB PEMOHON ####################
    Route::group(array('prefix' => 'jobpemohon'), function () {
        Route::get('/', array('as' => 'jobPemohon','uses' => 'JobPemohonController@index'));
        Route::get('create', array('as' => 'jobPemohonCreate', 'uses' => 'JobPemohonController@create'));
        Route::post('create', 'JobPemohonController@store');
        Route::get('detail/{id}', 'JobPemohonController@show');
        Route::get('delete/{id}', 'JobPemohonController@delete');
        Route::get('edit/{id}', array('as' => 'jobPemohonEdit', 'uses' => 'JobPemohonController@edit'));
        Route::put('edit/{id}', 'JobPemohonController@update');
        Route::get('delete/{id}', 'JobPemohonController@destroy');
    });

    #################### JOB SUAMI/ISTRI ####################
    Route::group(array('prefix' => 'jobsuamiistri'), function () {
        Route::get('/', array('as' => 'jobSuamiIstri','uses' => 'JobSuamiIstriController@index'));
        Route::get('create', array('as' => 'jobSuamiIstriCreate', 'uses' => 'JobSuamiIstriController@create'));
        Route::post('create', 'JobSuamiIstriController@store');
        Route::get('detail/{id}', 'JobSuamiIstriController@show');
        Route::get('delete/{id}', 'JobSuamiIstriController@delete');
        Route::get('edit/{id}', array('as' => 'jobSuamiIstriEdit', 'uses' => 'JobSuamiIstriController@edit'));
        Route::put('edit/{id}', 'JobSuamiIstriController@update');
        Route::get('delete/{id}', 'JobSuamiIstriController@destroy');
    });

    #################### PENGHASILAN ####################
    Route::group(array('prefix' => 'penghasilan'), function () {
        Route::get('/', array('as' => 'penghasilan','uses' => 'PenghasilanController@index'));
        Route::get('create', array('as' => 'penghasilanCreate', 'uses' => 'PenghasilanController@create'));
        Route::post('create', 'PenghasilanController@store');
        Route::get('detail/{id}', 'PenghasilanController@show');
        Route::get('delete/{id}', 'PenghasilanController@delete');
        Route::get('edit/{id}', array('as' => 'penghasilanEdit', 'uses' => 'PenghasilanController@edit'));
        Route::put('edit/{id}', 'PenghasilanController@update');
        Route::get('delete/{id}', 'PenghasilanController@destroy');
    });

    #################### PERBANKAN ####################
    Route::group(array('prefix' => 'perbankan'), function () {
        Route::get('/', array('as' => 'perbankan','uses' => 'PerbankanController@index'));
        Route::get('create', array('as' => 'perbankanCreate', 'uses' => 'PerbankanController@create'));
        Route::post('create', 'PerbankanController@store');
        Route::get('detail/{id}', 'PerbankanController@show');
        Route::get('delete/{id}', 'PerbankanController@delete');
        Route::get('edit/{id}', array('as' => 'perbankanEdit', 'uses' => 'PerbankanController@edit'));
        Route::put('edit/{id}', 'PerbankanController@update');
        Route::get('delete/{id}', 'PerbankanController@destroy');

        // Route::get('api/pchilds/fetch/{id}', function($id){
        //     $b = Request::url();
        //     // $b = $_SERVER['REQUEST_URI'];
        //     $a = substr($b, strrpos($b, '/') + 1);
        //     // dd($a);
        //     return App\Perbankan_child::where('perbankanid', $id)->get();
        // });
        // Route::get('api/pchilds/edit/{id}', 'Perbankan_childController@edit');
        // Route::put('api/pchilds/{id}', function(Request $request, $id){
        //     App\Perbankan_child::findOrFail($id)->update(Request::all());
        //     return Response::json(Request::all());
        // });
        // Route::delete('api/pchilds/{id}', function($id) {
        //     App\perbankan_child::findOrFail($id)->delete();
        // });
    });

    #################### PERBANKAN CHILDREN ####################
    Route::group(array('prefix' => 'perbankan_child'), function () {
        Route::post('create', array('as' => 'pChildCreate', 'uses' => 'Perbankan_childController@store'));
        // Route::get('detail/{id}', 'Perbankan_childController@show');
        // Route::get('delete/{id}', 'Perbankan_childController@delete');
        Route::get('edit/{id}', array('as' => 'pChildEdit', 'uses' => 'Perbankan_childController@edit'));
        Route::put('edit/{id}', 'Perbankan_childController@update');
        Route::get('delete/{id}/{perbankanid}', 'Perbankan_childController@destroy');
    });

    #################### APPLICATION ####################
    Route::group(array('prefix' => 'application'), function () {
        Route::get('/', array('as' => 'application','uses' => 'ApplicationController@index'));
        Route::get('create', array('as' => 'applicationCreate', 'uses' => 'ApplicationController@create'));
        Route::post('create', 'ApplicationController@store');
        Route::get('detail/{id}', 'ApplicationController@show');
        Route::get('delete/{id}', 'ApplicationController@delete');
        Route::get('edit/{id}', array('as' => 'applicationEdit', 'uses' => 'ApplicationController@edit'));
        Route::put('edit/{id}', 'ApplicationController@update');
        Route::get('delete/{id}', 'ApplicationController@destroy');
        Route::get('approved/{id}', array('as' => 'applicationApproved', 'uses' => 'ApplicationController@approved'));
		Route::get('unapproved/{id}', array('as' => 'applicationApproved', 'uses' => 'ApplicationController@unapproved'));
		Route::get('scored/{id}', array('as' => 'applicationApproved', 'uses' => 'ApplicationController@scored'));
		Route::get('sends/{id}', array('as' => 'applicationSends', 'uses' => 'ApplicationController@sends'));
    });

    #################### CREDIT ASSESMENT ####################
    Route::group(array('prefix' => 'assesment'), function () {
        Route::get('/', array('as' => 'assesment','uses' => 'AssesmentController@index'));
        Route::get('create', array('as' => 'assesmentCreate', 'uses' => 'AssesmentController@create'));
        Route::post('create', 'AssesmentController@store');
        Route::get('detail/{id}', 'AssesmentController@show');
        Route::get('delete/{id}', 'AssesmentController@delete');
        Route::get('edit/{id}', array('as' => 'assesmentEdit', 'uses' => 'AssesmentController@edit'));
        Route::put('edit/{id}', 'AssesmentController@update');
        Route::get('delete/{id}', 'AssesmentController@destroy');
    });

    #################### CREDIT SCORING ####################
    Route::group(array('prefix' => 'scoring'), function () {
        Route::get('/', array('as' => 'scoring','uses' => 'ScoringController@index'));
        Route::get('choose/{pemohonid}/{appid}', 'ScoringController@choose');
        Route::get('create', array('as' => 'scoringCreate', 'uses' => 'ScoringController@create'));
        Route::post('create', 'ScoringController@store');
        Route::get('detail/{id}', 'ScoringController@show');
        Route::get('delete/{id}', 'ScoringController@delete');
        Route::get('edit/{id}', array('as' => 'scoringEdit', 'uses' => 'ScoringController@edit'));
        Route::put('edit/{id}', 'ScoringController@update');
        Route::get('delete/{id}', 'ScoringController@destroy');
    });

    #################### TRANSAKSI SURVEY ####################
    Route::group(array('prefix' => 'survey'), function () {
        Route::get('/', array('as' => 'survey','uses' => 'SurveyController@index'));
        Route::get('create', array('as' => 'surveyCreate', 'uses' => 'SurveyController@create'));
        Route::post('create', 'SurveyController@store');
        Route::get('detail/{id}', 'SurveyController@show');
        Route::get('delete/{id}', 'SurveyController@delete');
        Route::get('edit/{id}', array('as' => 'surveyEdit', 'uses' => 'SurveyController@edit'));
        Route::put('edit/{id}', 'SurveyController@update');
        Route::get('delete/{id}', 'SurveyController@destroy');
    });

    #################### TRANSAKSI PEMBAYARAN ####################
    Route::group(array('prefix' => 'pembayaran'), function () {
        Route::get('/', array('as' => 'pembayaran','uses' => 'PembayaranController@index'));
        Route::get('create/{id}', array('as' => 'pembayaranCreate', 'uses' => 'PembayaranController@create'));
        Route::post('create/{id}', 'PembayaranController@store');
        Route::get('detail/{id}', 'PembayaranController@show');
        Route::get('delete/{id}', 'PembayaranController@delete');
        Route::get('edit/{id}', array('as' => 'pembayaranEdit', 'uses' => 'PembayaranController@edit'));
        Route::put('edit/{id}', 'PembayaranController@update');
        Route::get('delete/{id}', 'PembayaranController@destroy');
        Route::get('approved/{id}', array('as' => 'applicationApproved', 'uses' => 'PembayaranController@approved'));
    });

    #################### NEWSLETTER ####################
    Route::group(array('prefix' => 'newsletter'), function () {
        Route::get('/', array('as' => 'newsletter','uses' => 'NewsletterController@index'));
        Route::get('create', array('as' => 'newsletterCreate', 'uses' => 'NewsletterController@create'));
        Route::post('create', 'NewsletterController@store');
        Route::get('manage', 'NewsletterController@manage');
        Route::get('edit/{id}', array('as' => 'newsletterEdit', 'uses' => 'NewsletterController@edit'));
        Route::put('edit/{id}', 'NewsletterController@update');
        Route::get('delete/{id}', 'NewsletterController@delete');
        Route::get('detail/{id}', 'NewsletterController@detail');
        Route::get('send/{id}', 'NewsletterController@sendMail');
    });

    #############################################################################################################

    # User Management
    Route::group(array('prefix' => 'users'), function () {
        Route::get('/', array('as' => 'users', 'uses' => 'UsersController@index'));
        Route::get('data',['as' => 'users.data', 'uses' =>'UsersController@data']);
        Route::get('create', 'UsersController@create');
        Route::post('create', 'UsersController@store');
        Route::get('{userId}/delete', array('as' => 'delete/user', 'uses' => 'UsersController@destroy'));
        Route::get('{userId}/confirm-delete', array('as' => 'confirm-delete/user', 'uses' => 'UsersController@getModalDelete'));
        Route::get('{userId}/restore', array('as' => 'restore/user', 'uses' => 'UsersController@getRestore'));
        Route::get('{userId}', array('as' => 'users.show', 'uses' => 'UsersController@show'));
        Route::post('{userId}/passwordreset', array('as' => 'passwordreset', 'uses' => 'UsersController@passwordreset'));
    });
    Route::resource('users', 'UsersController');

    Route::get('deleted_users',array('as' => 'deleted_users','before' => 'Sentinel', 'uses' => 'UsersController@getDeletedUsers'));

    # Group Management
    Route::group(array('prefix' => 'groups'), function () {
        Route::get('/', array('as' => 'groups', 'uses' => 'GroupsController@index'));
        Route::get('data',['as' => 'groups.data', 'uses' =>'GroupsController@data']);
        Route::get('create', array('as' => 'create/group', 'uses' => 'GroupsController@create'));
        Route::post('create', 'GroupsController@store');
        Route::get('{groupId}/edit', array('as' => 'update/group', 'uses' => 'GroupsController@edit'));
        Route::post('{groupId}/edit', 'GroupsController@update');
        Route::get('{groupId}/delete', array('as' => 'delete/group', 'uses' => 'GroupsController@destroy'));
        Route::get('{groupId}/confirm-delete', array('as' => 'confirm-delete/group', 'uses' => 'GroupsController@getModalDelete'));
        Route::get('{groupId}/restore', array('as' => 'restore/group', 'uses' => 'GroupsController@getRestore'));
    });
    /*routes for blog*/
    Route::group(array('prefix' => 'blog'), function () {
        Route::get('/', array('as' => 'blogs', 'uses' => 'BlogController@index'));
        Route::get('create', array('as' => 'create/blog', 'uses' => 'BlogController@create'));
        Route::post('create', 'BlogController@store');
        Route::get('{blog}/edit', array('as' => 'update/blog', 'uses' => 'BlogController@edit'));
        Route::post('{blog}/edit', 'BlogController@update');
        Route::get('{blog}/delete', array('as' => 'delete/blog', 'uses' => 'BlogController@destroy'));
        Route::get('{blog}/confirm-delete', array('as' => 'confirm-delete/blog', 'uses' => 'BlogController@getModalDelete'));
        Route::get('{blog}/restore', array('as' => 'restore/blog', 'uses' => 'BlogController@getRestore'));
        Route::get('{blog}/show', array('as' => 'blog/show', 'uses' => 'BlogController@show'));
        Route::post('{blog}/storecomment', array('as' => 'restore/blog', 'uses' => 'BlogController@storecomment'));
    });

    /*routes for blog category*/
    Route::group(array('prefix' => 'blogcategory'), function () {
        Route::get('/', array('as' => 'blogcategories', 'uses' => 'BlogCategoryController@index'));
        Route::get('data',['as' => 'blogcategories.data', 'uses' =>'BlogCategoryController@data']);
        Route::get('create', array('as' => 'create/blogcategory', 'uses' => 'BlogCategoryController@create'));
        Route::post('create', 'BlogCategoryController@store');
        Route::get('{blogcategory}/edit', array('as' => 'update/blogcategory', 'uses' => 'BlogCategoryController@edit'));
        Route::post('{blogcategory}/edit', 'BlogCategoryController@update');
        Route::get('{blogcategory}/delete', array('as' => 'delete/blogcategory', 'uses' => 'BlogCategoryController@destroy'));
        Route::get('{blogcategory}/confirm-delete', array('as' => 'confirm-delete/blogcategory', 'uses' => 'BlogCategoryController@getModalDelete'));
        Route::get('{blogcategory}/restore', array('as' => 'restore/blogcategory', 'uses' => 'BlogCategoryController@getRestore'));
    });

    /*routes for file*/
    Route::group(array('prefix' => 'file'), function () {
        Route::post('create', 'FileController@store');
        Route::post('createmulti', 'FileController@postFilesCreate');
        Route::delete('delete', 'FileController@delete');
    });

    Route::get('crop_demo', function () {
        return redirect('admin/imagecropping');
    });
    Route::post('crop_demo','JoshController@crop_demo');

    /* laravel example routes */
    # datatables
    Route::get('datatables', 'DataTablesController@index');
    Route::get('datatables/data', array('as' => 'admin.datatables.data', 'uses' => 'DataTablesController@data'));

    # editable datatables
    Route::get('editable_datatables', 'DataTablesController@editableDatatableIndex');
    Route::get('editable_datatables/data', array('as' => 'admin.editable_datatables.data', 'uses' => 'DataTablesController@editableDatatableData'));
    Route::post('editable_datatables/create','DataTablesController@editableDatatableStore');
    Route::post('editable_datatables/{id}/update', 'DataTablesController@editableDatatableUpdate');
    Route::get('editable_datatables/{id}/delete', array('as' => 'admin.editable_datatables.delete', 'uses' => 'DataTablesController@editableDatatableDestroy'));

    //tasks section
    Route::post('task/create', 'TaskController@store');
    Route::get('task/data', 'TaskController@data');
    Route::post('task/{task}/edit', 'TaskController@update');
    Route::post('task/{task}/delete', 'TaskController@delete');

    Route::group(array('prefix' => 'picture'), function () {
        Route::get('/', array('as' => 'picture', 'uses' => 'PictureController@index'));
        Route::get('create', array('as' => 'create/picture', 'uses' => 'PictureController@create'));
        Route::post('create', 'PictureController@store');
        Route::get('data',['as' => 'picture.data', 'uses' =>'PictureController@data']);
        Route::get('{pictureId}/edit', array('as' => 'update/picture', 'uses' => 'PictureController@edit'));
        Route::post('{pictureId}/edit', 'PictureController@update');
        Route::get('{pictureId}/delete', array('as' => 'delete/picture', 'uses' => 'PictureController@destroy'));
        Route::get('{pictureId}/confirm-delete', array('as' => 'confirm-delete/picture', 'uses' => 'PictureController@getModalDelete'));
        //route for multiple file

        Route::post('createmultifile', 'PictureController@postFilesCreatemultiple');
        Route::delete('deletefile', 'PictureController@deletemultiple');
    });

    Route::group(array('prefix' => 'gallery'), function () {
        Route::get('/', array('as' => 'gallery', 'uses' => 'GalleryController@index'));
        Route::get('create', array('as' => 'create/gallery', 'uses' => 'GalleryController@create'));
        Route::post('create', 'GalleryController@store');
        Route::get('data',['as' => 'gallery.data', 'uses' =>'GalleryController@data']);
        Route::get('{galleryId}/edit', array('as' => 'update/gallery', 'uses' => 'GalleryController@edit'));
        Route::post('{galleryId}/edit', 'GalleryController@update');
        Route::get('{galleryId}/delete', array('as' => 'delete/gallery', 'uses' => 'GalleryController@destroy'));
        Route::get('{galleryId}/confirm-delete', array('as' => 'confirm-delete/gallery', 'uses' => 'GalleryController@getModalDelete'));
    });

    Route::group(array('prefix' => 'dashboard'), function () {
        Route::get('index', 'DashboardController@index');
        Route::get('applicationgraph', 'DashboardController@applicationgraph');
    });
    # Remaining pages will be called from below controller method
    # in real world scenario, you may be required to define all routes manually

    Route::get('{name?}', 'JoshController@showView');
});
