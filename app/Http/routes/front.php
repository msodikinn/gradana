<?php

#FrontEndController

//Route::get('/', array('as' => 'subscribe-comingsoon','uses' => 'SubscribeController@index'));
Route::post('/subscribe', array( 'as' => 'sendsub', 'uses' =>'SubscribeController@saveSubs'));
Route::get('/confirm', 'SubscribeController@confirm');
Route::get('/', array('as' => 'index', 'uses' => 'IndexController@index'));
Route::get('/contact', 'ContactController@index');
Route::post('/contact', array('as' => 'contact','uses' => 'FrontEndController@postContact'));

Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);
Route::get('/about', 'AboutController@index');

#################### PROPERTY PAGE ####################
Route::get('/property', 'PropertyFrontController@index');
Route::get('property-detail/{id}', [
    'as' => 'property-detail',
    'uses' => 'PropertyFrontController@detail',
]);

Route::post('/result','PropertyFrontController@resultSimulator');

Route::get('/faq', 'FaqController@index');
Route::get('/admin/profile/{id}', array('as' => 'adminProfile','uses' => 'UserFrontController@adminProfile'));
Route::get('/termborrow', function() {
    return view('frontend.partials._termBorrower');
});

Route::get('/lender', 'IndexController@lender');
Route::get('/borrower', 'IndexController@borrower');

#################### LENDER PAGE ####################
Route::group(array('prefix' => 'lender'), function () {
    Route::get('/dashboard', 'LenderController@dashboard');
    Route::get('/history', 'LenderController@history');
    Route::get('investor-form', 'LenderController@investorForm');
	Route::get('historydetail/{id}', [
        'as' => 'historydetail',
        'uses' => 'LenderController@historyDetail',
    ]);
	Route::post('investorpost', [
        'as' => 'investorpost',
        'uses' => 'LenderController@investorPost',
    ]);
    Route::get('/available-loans', 'LenderController@availableLoans');
    Route::get('available-loans-detail/{id}', [
        'as' => 'available-loans-detail',
        'uses' => 'LenderController@availableLoansDetail',
    ]);
    Route::get('/borrower-card', 'LenderController@borrowerCard');
    Route::get('borrower-card-detail/{id}', [
        'as' => 'borrower-card-detail',
        'uses' => 'LenderController@borrowerCardDetail',
    ]);

});

#################### APPLICANT PAGE ####################
Route::group(array('prefix' => 'applicant'), function () {
    Route::get('/', array('as' => 'applicant','uses' => 'ApplicantFrontController@applicant'));
    Route::get('/dashboard', array('as' =>'dashboard', 'uses' => 'ApplicantFrontController@disini'));
    // Route::get('profile/{id}', array('as' => 'applicantProfile','uses' => 'ApplicantFrontController@profile'));
    Route::get('form', array('as' => 'applicantCreate', 'uses' => 'ApplicantFrontController@applicantform'));
    Route::post('form', 'ApplicantFrontController@applicantstore');
    Route::get('/quick-signup', 'ApplicantFrontController@quickSignupMail');
});

#################### Investor page #####################
Route::group(array('prefix' => 'investor'), function () {
    Route::get('/', array('as' => 'investorfront','uses' => 'InvestorFrontController@investor'));
    Route::get('profile', array('as' => 'investorProfile','uses' => 'InvestorFrontController@profile'));
    Route::get('profile/edit/{id}', 'InvestorFrontController@edit');
    Route::get('form', array('as' => 'investorCreate', 'uses' => 'InvestorFrontController@investorform'));
    Route::post('form', 'InvestorFrontController@investorstore');
	//Route::get('form', array('as' => 'investorPost', 'uses' => 'InvestorFrontController@investorPost'));
//     Route::get('/terminves', function() {
//     return view('frontend.partials._termInvestor');
// });

});
// Route::group(array('prefix' => 'investor-profile', 'middleware' => 'SentinelUser'), function () {
//     Route::controller('lender', 'LenderController');
//     Route::controller('/', 'InvestorProfileController');
// });

Route::get('register', array('as' => 'register', 'uses' => 'FrontEndController@getRegister'));
Route::post('register','FrontEndController@postRegister');

Route::get('/login', array('as' => 'login', 'uses' => 'FrontEndController@getLogin'));
Route::post('login','FrontEndController@postLogin');
Route::get('activate/{userId}/{activationCode}',array('as' =>'activate','uses'=>'FrontEndController@getActivate'));

Route::group(array('prefix' => 'forgot-password'), function () {
    Route::get('/',array('as' => 'forgot-password','uses' => 'FrontEndController@getForgotPassword'));
    Route::post('/','FrontEndController@postForgotPassword');
    # Forgot Password Confirmation
    Route::get('/{userId}/{passwordResetCode}', array('as' => 'forgot-password-confirm', 'uses' => 'FrontEndController@getForgotPasswordConfirm'));
    Route::post('/{userId}/{passwordResetCode}', 'FrontEndController@postForgotPasswordConfirm');
});

Route::get('/remind', 'UserFrontController@remind');
Route::post('/remind', 'UserFrontController@remindPost');

# My account display and update details
Route::group(array('middleware' => 'SentinelUser'), function () {
    Route::get('my-account', 'FrontEndController@myAccount');
    Route::get('my-account/edit', array('as' => 'my-account', 'uses' => 'FrontEndController@edit'));
    Route::put('my-account/edit', 'FrontEndController@update');
});

Route::get('logout', array('as' => 'logout','uses' => 'FrontEndController@getLogout'));

Route::group(array('prefix' => 'blog'), function () {
    Route::get('/', array('as' => 'blog', 'uses' => 'BlogController@IndexFrontend'));
    // Route::get('{slug}/tag', 'BlogController@getBlogTagFrontend');
    Route::get('{slug?}', 'BlogController@getBlogFrontend');
    // Route::post('{blog}/comment', 'BlogController@storeCommentFrontend');
});

Route::group(array('prefix' => 'auth'), function () {
    Route::get('facebook', 'SocialAuthController@loginWithFacebook');
    // Route::get('facebook/callback', 'Auth\FacebookAuthController@handleProviderCallback');
    Route::get('google', 'SocialAuthController@redirectToProvider');
    // Route::get('google/callback', 'GoogleController@handleProviderCallback');
    Route::get('linkedin', 'SocialAuthController@redirectToProvider');
    // Route::get('linkedin/callback', 'Auth\LinkedinAuthController@handleProviderCallback');
    Route::get('twitter', 'SocialAuthController@redirectToProvider');
    // Route::get('twitter/callback', 'Auth\TwitterAuthController@handleProviderCallback');
});
// Route::get('{name?}', 'JoshController@showFrontEndView');

# End of frontend views
