<?php namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use File;
use Hash;
use Illuminate\Support\Facades\Request;
use Lang;
use Mail;
use Redirect;
use Sentinel;
use URL;
use View;
use Datatables;
use App\Pemohon;
use App\JobPemohon;
use App\SuamiIstri;
use App\JobSuamiIstri;
use App\Penghasilan;
use App\Keluarga;
use App\Perbankan;
use App\Perbankan_child;
use App\Application;
use App\Pembayaran;
use App\Helpers\Payment;

class UsersController extends JoshController
{

    public function index()
    {
        // if (Sentinel::getuser()->status == "Admin") {
        //     $users = User::All();
        //     dd("adasdd");
        // }else{
        //     $users = User::where('id', Sentinel::getuser()->id);
        // }

        // // Show the page
        return View('admin.users.index');
    }

    /*
     * Pass data through ajax call
     */
    public function data()
    {
        if (Sentinel::getuser()->status == "Admin") {
            $users = User::All();
        }else{
            $users = User::where('id', Sentinel::getuser()->id)->get(['id', 'fullname', 'email','created_at']);
        }

        return Datatables::of($users)
            ->edit_column('created_at','{!! format($created_at) !!}')
            ->add_column('status',function($user){
                if($activation = Activation::completed($user))
                    return 'Activated';
                else
            		return 'Pending';

            })
            ->add_column('actions',function($user) {
                $actions = '<a href='. route('admin.users.show', $user->id) .'><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href='. route('admin.users.edit', $user->id) .'><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';

                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href='. route('confirm-delete/user', $user->id) .' data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            }

    )
            ->make(true);
    }

    public function create()
    {
        // Get all the available groups
        $groups = Sentinel::getRoleRepository()->all();
        $countries = $this->countries;
        // Show the page
        return View('admin/users/create', compact('groups', 'countries'));
    }

    public function store(UserRequest $request)
    {
        $group = Sentinel::findRoleById($request->get('group'));

        //upload image
        if ($file = $request->file('pic_file')) {
            $fileName = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName = 'uploads/users/';
            $destinationPath = public_path() . '/' . $folderName;
            $safeName = $folderName . str_random(10) . '.' . $extension;
            $file->move($destinationPath, $safeName);
            $request['pic'] = $safeName;
        }
        //check whether use should be activated by default or not
        $activate = $request->get('activate') ? true : false;

        try {
            // Register the user
            $user = Sentinel::register($request->except('_token', 'password_confirm', 'group', 'activate', 'pic_file'), $activate);

            $data = User::latest()->first();
            $data->status = $group->name;
            $data->save();

            //add user to 'User' group
            $role = Sentinel::findRoleById($request->get('group'));
            if ($role) {
                $role->users()->attach($user);
            }
            //check for activation and send activation mail if not activated by default
            if (!$request->get('activate')) {
                // Data to be used on the email view
                $data = array(
                    'user' => $user,
                    'activationUrl' => URL::route('activate', [$user->id, Activation::create($user)->code]),
                );

                // Send the activation code through email
                Mail::send('emails.register-activate', $data, function ($m) use ($user) {
                    $m->to($user->email, $user->fullname);
                    $m->subject('Welcome ' . $user->fullname);
                });
            }

            // Redirect to the home page with success menu
            return Redirect::route('admin.users.index')->with('success', Lang::get('users/message.success.create'));

        } catch (LoginRequiredException $e) {
            $error = Lang::get('admin/users/message.user_login_required');
        } catch (PasswordRequiredException $e) {
            $error = Lang::get('admin/users/message.user_password_required');
        } catch (UserExistsException $e) {
            $error = Lang::get('admin/users/message.user_exists');
        }

        // Redirect to the user creation page
        return Redirect::back()->withInput()->with('error', $error);
    }

    public function edit($user = null)
    {
        // Get this user groups
        $userRoles = $user->getRoles()->lists('name', 'id')->all();
        $key = array_keys($userRoles);

        // Get a list of all the available groups
        $roles = Sentinel::getRoleRepository()->all();

        $status = Activation::completed($user);

        $countries = $this->countries;

        // Show the page
        return View('admin/users/edit', compact('user', 'roles', 'userRoles', 'countries', 'status', 'key'));
    }

    public function update(User $user, UserRequest $request)
    {

        try {
            $user->fullname = $request->get('fullname');
            $user->email = $request->get('email');
            $user->tempat_lahir = $request->get('tempat_lahir');
            $user->dob = $request->get('dob');
            $user->bio = $request->get('bio');
            $user->gender = $request->get('gender');
            $user->country = $request->get('country');
            $user->state = $request->get('state');
            $user->city = $request->get('city');
            $user->address = $request->get('address');
            $user->postal = $request->get('postal');
            $user->kecamatan = $request->get('kecamatan');
            $user->kelurahan = $request->get('kelurahan');

            if ($password = $request->has('password')) {
                $user->password = Hash::make($request->password);
            }


            // is new image uploaded?
            if ($file = $request->file('pic_file')) {
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = 'uploads/users/';
                $destinationPath = public_path() . '/' . $folderName;
                $safeName = $folderName . str_random(10) . '.' . $extension;
                $file->move($destinationPath, $safeName);
                //delete old pic if exists
                if (File::exists(public_path() . $folderName . $user->pic)) {
                    File::delete(public_path() . $folderName . $user->pic);
                }

                //save new file path into db
                $user->pic = $safeName;

            }

            //save record
            $user->save();

            // Get the current user groups
            $userRoles = $user->roles()->lists('id')->all();

            // Get the selected groups
            $selectedRoles = $request->get('groups', array());

            // Groups comparison between the groups the user currently
            // have and the groups the user wish to have.
            $rolesToAdd = array_diff($selectedRoles, $userRoles);
            $rolesToRemove = array_diff($userRoles, $selectedRoles);

            // Assign the user to groups
            foreach ($rolesToAdd as $roleId) {
                $role = Sentinel::findRoleById($roleId);

                $role->users()->attach($user);
            }

            // Remove the user from groups
            foreach ($rolesToRemove as $roleId) {
                $role = Sentinel::findRoleById($roleId);

                $role->users()->detach($user);
            }

            // Activate / De-activate user
            $status = $activation = Activation::completed($user);
            if ($request->get('activate') != $status) {
                if ($request->get('activate')) {
                    $activation = Activation::exists($user);
                    if ($activation) {
                        Activation::complete($user, $activation->code);
                    }
                } else {
                    //remove existing activation record
                    Activation::remove($user);
                    //add new record
                    Activation::create($user);

                    //send activation mail
                    $data = array(
                        'user' => $user,
                        'activationUrl' => URL::route('activate', $user->id, Activation::exists($user)->code),
                    );

                    // Send the activation code through email
                    Mail::send('emails.register-activate', $data, function ($m) use ($user) {
                        $m->to($user->email, $user->fullname);
                        $m->subject('Welcome ' . $user->fullname);
                    });

                }
            }

            // Was the user updated?
            if ($user->save()) {
                // Prepare the success message
                $success = Lang::get('users/message.success.update');

                // Redirect to the user page
                return Redirect::route('admin.users.edit', $user)->with('success', $success);
            }

            // Prepare the error message
            $error = Lang::get('users/message.error.update');
        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = Lang::get('users/message.user_not_found', compact('user'));
            // Redirect to the user management page
            return Redirect::route('admin.users.index')->with('error', $error);
        }

        // Redirect to the user page
        return Redirect::route('admin.users.edit', $user)->withInput()->with('error', $error);
    }

    public function getDeletedUsers()
    {
        // Grab deleted users
        $users = User::onlyTrashed()->get();

        // Show the page
        return View('admin/deleted_users', compact('users'));
    }


    public function getModalDelete($id = null)
    {
        $model = 'users';
        $confirm_route = $error = null;
        try {
            // Get user information
            $user = Sentinel::findById($id);

            // Check if we are not trying to delete ourselves
            if ($user->id === Sentinel::getUser()->id) {
                // Prepare the error message
                $error = Lang::get('users/message.error.delete');

                return View('admin/layouts/modal_confirmation', compact('error', 'model', 'confirm_route'));
            }
        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = Lang::get('users/message.user_not_found', compact('id'));
            return View('admin/layouts/modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
        $confirm_route = route('delete/user', ['id' => $user->id]);
        return View('admin/layouts/modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id = null)
    {
        try {
            // Get user information
            $user = Sentinel::findById($id);

            // Check if we are not trying to delete ourselves
            if ($user->id === Sentinel::getUser()->id) {
                // Prepare the error message
                $error = Lang::get('admin/users/message.error.delete');

                // Redirect to the user management page
                return Redirect::route('admin.users.index')->with('error', $error);
            }

            // Delete the user
            //to allow soft deleted, we are performing query on users model instead of Sentinel model
            //$user->delete();
            User::destroy($id);

            // Prepare the success message
            $success = Lang::get('users/message.success.delete');

            // Redirect to the user management page
            return Redirect::route('admin.users.index')->with('success', $success);
        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = Lang::get('admin/users/message.user_not_found', compact('id'));

            // Redirect to the user management page
            return Redirect::route('admin.users.index')->with('error', $error);
        }
    }

    public function getRestore($id = null)
    {
        try {
            // Get user information
            $user = User::withTrashed()->find($id);

            // Restore the user
            $user->restore();

            // create activation record for user and send mail with activation link
            $data = array(
                'user' => $user,
                'activationUrl' => URL::route('activate', [$user->id, Activation::create($user)->code]),
            );

            // Send the activation code through email
            Mail::send('emails.register-activate', $data, function ($m) use ($user) {
                $m->to($user->email, $user->fullname);
                $m->subject('Dear ' . $user->fullname . '! Active your account');
            });


            // Prepare the success message
            $success = Lang::get('users/message.success.restored');

            // Redirect to the user management page
            return Redirect::route('deleted_users')->with('success', $success);
        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = Lang::get('users/message.user_not_found', compact('id'));

            // Redirect to the user management page
            return Redirect::route('deleted_users')->with('error', $error);
        }
    }

    public function show($id)
    {
        try {
            // Get the user information
            if (Sentinel::getuser()->status == "Applicant") {
                $detailPemohon = Pemohon::where('userid', $id)->first();
                $detailJobPemohon = JobPemohon::where('userid', $id)->first();
                $detailPasangan = SuamiIstri::where('userid', $id)->first();
                $detailJobPasangan = JobSuamiIstri::where('userid', $id)->first();
                $detailKeluarga = Keluarga::where('userid', $id)->first();
                $detailPenghasilan = Penghasilan::where('userid', $id)->first();
                $detailPerbankan = Perbankan::where('userid', $id)->first();
                $detailPerbankanChild = $detailPerbankan->perbankan_childs;
                $detailApplication = app('App\Application')->apl();
                $total = Payment::getTotal($detailApplication);
            }
            $detailPayment = app('App\Pembayaran')->payment();
            $user = Sentinel::findUserById($id);

            //get country name
            if ($user->country) {
                $user->country = $this->countries[$user->country];
            }

        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = Lang::get('users/message.user_not_found', compact('id'));

            // Redirect to the user management page
            return Redirect::route('admin.users.index')->with('error', $error);
        }

        // Show page
        return View('admin.users.show', compact(['user', 'detailPemohon', 'detailJobPemohon', 'detailPasangan', 'detailJobPasangan', 'detailKeluarga', 'detailPenghasilan', 'detailPerbankan', 'detailPerbankanChild', 'detailApplication', 'detailPayment', 'total']));
    }

    public function passwordreset($id)
    {
        if (Request::ajax()) {
            $data = Request::all();
            $user = Sentinel::findUserById($id);
            $password = Request::get('password');
            $user->password = Hash::make($password);
            $user->save();

        }
    }

    public function lockscreen($id)
    {
        $user = Sentinel::findUserById($id);
        return View('admin/lockscreen',compact('user'));
    }

    public function adminProfile()
    {
        dd("admin profile");
    }
}
