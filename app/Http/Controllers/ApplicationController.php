<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Helpers\Payment;
use App\Application;
use App\Developer;
use App\Property;
use App\Investor;
use App\User;
use App\Keluarga;
use App\JobSuamiIstri;
use App\JobPemohon;
use App\SuamiIstri;
use App\Penghasilan;
use App\Perbankan;
use App\Pemohon;
use Input;
use Session;
use Redirect;
use Sentinel;
use Image;
use File;
use Mail;

class ApplicationController extends JoshController
{
    public function index(Request $request)
    {

        if ($request->iduser) {
            $data['apl'] = app('App\Application')->apl()->where('userid', (int)$request->iduser);
            // dd($data['apl']->first()->user);
            // dd($data['apl']);
            $data['fromPemohon'] = "true";
            // $data['apl'] = $app->
        }else{
            $data['apl'] = app('App\Application')->apl();
        }

        $data['total'] = Payment::getTotal($data['apl']);

        return view('admin.application.index', $data);
    }

    public function create(Request $request)
    {
        $data = [
            'dev'       => Developer::all(),
            'prop'      => Property::all(),
            'inves'     => Investor::all(),
            'jenis'     => $this->jenisApplication,
            'tujuan'    => $this->tujuan,
            'bayar'     => $this->sistemBayar,
            'status'    => $this->statusApp,
            'jaminan'   => $this->jaminanApp
        ];

        $perbankan = null;
        if ($request->backid) {
            $perbankan = Perbankan::find($request->backid);
        }

        if (!is_null(session('pem'))) {
            $data['users'] = User::where('id', session('pem'))->first();
        } else if (!is_null($perbankan)) {
            $data['users'] = $perbankan->user()->first();
        }
        if (!is_null($perbankan)) {
            // $data['penghasilanid'] = $request->backid;
            $data['keluargaid'] = Keluarga::where('pemohonid', '=', $perbankan->pemohonid)->first()->id;
            if ($perbankan->pemohon->status_kawin != "Lajang") {
                $data['jobsuamiistriid'] = JobSuamiIstri::where('pemohonid', '=', $perbankan->pemohonid)->first()->id;
                $data['suamisitriid'] = SuamiIstri::where('pemohonid', '=', $perbankan->pemohonid)->first()->id;
            }
            $data['jobpemohonid'] = JobPemohon::where('pemohonid', '=', $perbankan->pemohonid)->first()->id;
            // $data['penghasilanid'] = Penghasilan::where('pemohonid', '=', $perbankan->pemohonid)->first()->id;
            $data['pemohonid'] = $perbankan->pemohonid;
            $data['perbankanid'] = $request->backid;
        } else {
            $data['penghasilanid'] = '';
            $data['keluargaid'] = '';
            $data['jobsuamiistriid'] = '';
            $data['suamisitriid'] = '';
            $data['jobpemohonid'] = '';
            $data['pemohonid'] = '';
            $data['perbankanid'] = '';
        }

        return view('admin.application.create', $data);
    }

    public function store(Request $request)
    {
        if (Sentinel::getuser()->status == "Admin") {
            $input = $request->all();
        }else{
            $input = $request->except(['investorid', 'devid', 'pem']);
        }

        Application::create($input);

        alert()->success('Application form has been created.');
        return redirect('/admin/application');
    }

    public function show($id)
    {
        $data = [
            'detail'    => Application::find($id),
            'users'     => User::all(),
            'jenis'     => $this->jenisApplication,
            'tujuan'    => $this->tujuan,
            'bayar'     => $this->sistemBayar,
            'status'    => $this->statusApp,
            'jaminan'   => $this->jaminanApp
        ];
        $data['dev'] = Developer::all();
        $data['prop'] = Property::all();
        $data['inves'] = Investor::all();
        $data['total'] = Payment::getSingle($data['detail']->periode, $data['detail']->jangka_waktu);

        return view('admin.application.show', $data);
    }

    public function edit($id)
    {
        $data = [
            'detail'    => Application::find($id),
            'users'     => User::all(),
            'jenis'     => $this->jenisApplication,
            'tujuan'    => $this->tujuan,
            'bayar'     => $this->sistemBayar,
            'status'    => $this->statusApp,
            'jaminan'   => $this->jaminanApp
        ];
        $data['dev'] = Developer::all();
        $data['prop'] = Property::all();
        $data['inves'] = Investor::all();

        return view('admin.application.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $detail = Application::find($id);

        $input = $request->all();
        $detail->fill($input)->save();

        alert()->success('Application form has been updated.');
        return back();//redirect('/admin/application');
    }

    public function destroy($id)
    {
        Application::find($id)->delete();

        alert()->success('Application has been deleted.');
        return redirect('/admin/application');
    }

    public function approved($id)
    {
        $app = Application::find($id);
        $app->approved = 1;
        $app->status = "Verified";
        $app->save();

        alert()->success('Application ID '. $app->id .' has been approved.');
        return back();
    }

    public function unapproved($id)
    {
        $app = Application::find($id);
        $app->approved = 0;
        $app->status = "Document Complete / Verification in Progress";
        $app->save();

        alert()->success('Application ID '. $app->id .' has been unapproved.');
        return back();
    }
	
	public function scored($id)
    {
        $app = Application::find($id);
         $app->score = 3;
        // $app->status = "Document Complete / Verification in Progress";
         $app->save();

        alert()->success('Application ID '. $app->id .' has been scored.');
        return back();
    }
	
	public function sends($id)
    {
        $app = Application::find($id);
        $ids = $app->userid;
		$data['user'] = User::where('id', $ids)->first();
		$data['pemohon'] = Pemohon::where('userid', $ids)->first();
		$data['applicant'] = Application::where('userid', $ids)->first();
		$data['jobpemohon'] = JobPemohon::where('userid', $ids)->first();
		
						
			$dataemail = [
                        'register_as'  => $data['user']->status,
                        'email' => $data['user']->email,
						'fullname_clean'=>$data['pemohon']->fullname,
						'jenis_pekerjaan'=>$data['jobpemohon']->jenis_pekerjaan,
						'kantor'=>$data['jobpemohon']->nama_perusahaan,
					
                        ];
		
						
            Mail::send('emails.applicant.regis_application', $dataemail, function ($mail) use ($data)
            {
				
				$email = $data['user']->email;
				$user = $data['user']->fullname;
                $filepdf = 'Terms-and-Conditions-for-Investee.pdf';
                $mail->to($email,$user);
				
                $mail->attach(public_path().'/uploads/pdf/'.$filepdf);
         
                $mail->subject('Verifikasi Aplikan Gradana');
            });
		
		
        alert()->success('Application ID '. $data['pemohon']->fullname .' has been send to aplikan.');
        return back();
     }
	
	
}
