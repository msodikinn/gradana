<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Mail;
use Input;
use App\User;
use App\Investor;
use App\Newsletter;
use Carbon\Carbon;

class NewsletterController extends Controller
{
    public function index()
    {
        $data['countInvestor'] = Investor::all()->count();
        $data['news'] = Newsletter::orderBy('created_at', 'desc')->limit(15)->get();

        return view('admin.newsletter.newsletter', $data);
    }

    public function create()
    {
        $data['countInvestor'] = Investor::all()->count();

        return view('admin.newsletter.create', $data);
    }

    public function store(Request $request){
        $data = $request->all();
        Newsletter::create($data);

        alert()->success('Newsletter has been created');
        return back();
    }

    public function manage(){
        // dd(public_path());
        $data['totalNewsletter'] = Newsletter::All()->count();
        $data['listNewsletter'] = Newsletter::orderBy('created_at', 'desc')->get();

        return View('/admin/newsletter/manage', $data);
    }

    public function edit($id){
        $data['newsletter'] = Newsletter::find($id);
        $data['countInvestor'] = Investor::all()->count();

        return view('admin/newsletter/edit', $data);
    }

    public function update(Request $request, $id){
        $data = Input::all();
        $newsletter = Newsletter::find($id)->update($data);

        alert()->success('Newsletter has been updated.');
        return redirect('/admin/newsletter/manage');
    }

    public function delete($id){
        $data = Newsletter::find($id);
        $data->delete();

        alert()->success('Delete newsletter success');
        return back();
    }

    public function detail($id)
    {
        $data['detail'] = Newsletter::findOrFail($id);

        return view('/admin/newsletter/detail', $data);
    }

    public function sendMail($id)
    {
        $investorActive = Investor::all();

        $newsletter = Newsletter::find($id);
        $newsletter->last_send = Carbon::now();
        $newsletter->save();

        foreach ($investorActive as $ua)
        {
            $pesan = str_replace("[name]", $ua->nama, $newsletter->message);
            $pesan = str_replace("[jenisusaha]", $ua->jenis_usaha, $pesan);
            $pesan = str_replace("[alamat]", $ua->alamat, $pesan);
            $pesan = str_replace("[telp]", $ua->telp, $pesan);
            $pesan = str_replace("[email]", $ua->email, $pesan);

            Mail::queue('admin.newsletter.send', ['messages' => $pesan], function ($mail) use ($ua, $newsletter)
            {
                $nama = $ua->nama;
                $email = $ua->email;
                $mail->to($email, $nama);
                $mail->subject($newsletter->subject);
            });
        }

        alert()->success('Newsletter has been sending in background');
        return redirect('/admin/newsletter/manage');
    }
}
