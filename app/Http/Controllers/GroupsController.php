<?php namespace App\Http\Controllers;

use App\Http\Requests\GroupRequest;
use Lang;
use Redirect;
use Sentinel;
use View;
use Datatables;

class GroupsController extends JoshController
{
    /**
     * Show a list of all the groups.
     *
     * @return View
     */
    public function index()
    {
        return View('admin/groups/index');
    }

    /**
     * Group create.
     *
     * @return View
     */
    public function create()
    {
        // Show the page
        return View('admin/groups/create');
    }

    /**
     * Group create form processing.
     *
     * @return Redirect
     */
    public function store(GroupRequest $request)
    {
    	if (Sentinel::getRoleRepository()->where('name', '=', $request->get('name'))->count() > 0) {
    		return Redirect::route('create/group')->withInput()->with('error', Lang::get('groups/message.error.group_exists'));
    	}

        if ($role = Sentinel::getRoleRepository()->createModel()->create([
            'name' => $request->get('name'),
            'slug' => str_slug($request->get('name')),
        	'permissions' => array(
        			'isdeveloper' => intval($request->get('isdeveloper')),
        			'isproperty' => intval($request->get('isproperty')),
        			'isinvestor' => intval($request->get('isinvestor')),
        		),
        ])
        ) {
            // Redirect to the new group page
            return Redirect::route('groups')->with('success', Lang::get('groups/message.success.create'));
        }

        // Redirect to the group create page
        return Redirect::route('create/group')->withInput()->with('error', Lang::get('groups/message.error.create'));
    }

    /**
     * Group update.
     *
     * @param  int $id
     * @return View
     */
    public function edit($id = null)
    {


        try {
            // Get the group information
            $role = Sentinel::findRoleById($id);

			$isdeveloper = 0;
			$isproperty = 0;
			$isinvestor = 0;
			$permission = $role->permissions;
			if (!is_null($permission) && !empty($permission)) {
				if (isset($permission['admin']) && !is_null($permission['admin']) && !empty($permission['admin']) && $permission['admin'] == 1) {
					$isdeveloper = 1;
					$isproperty = 1;
					$isinvestor = 1;
				} else {
					$isdeveloper = $permission['isdeveloper'];
					$isproperty = $permission['isproperty'];
					$isinvestor = $permission['isinvestor'];
				}
			}
			$role->{'isdeveloper'} = $isdeveloper;
			$role->{'isproperty'} = $isproperty;
			$role->{'isinvestor'} = $isinvestor;

        } catch (GroupNotFoundException $e) {
            // Redirect to the groups management page
            return Redirect::route('groups')->with('error', Lang::get('groups/message.group_not_found', compact('id')));
        }

        // Show the page
        return View('admin/groups/edit', compact('role'));
    }

    /**
     * Group update form processing page.
     *
     * @param  int $id
     * @return Redirect
     */
    public function update($id = null, GroupRequest $request)
    {
    	if (Sentinel::getRoleRepository()->where('name', '=', $request->get('name'))->where('id', '<>', $id)->count() > 0) {
    		return Redirect::route('create/group')->withInput()->with('error', Lang::get('groups/message.error.group_exists'));
    	}
        try {
            // Get the group information
            $group = Sentinel::findRoleById($id);
        } catch (GroupNotFoundException $e) {
            // Redirect to the groups management page
            return Rediret::route('groups')->with('error', Lang::get('groups/message.group_not_found', compact('id')));
        }

        // Update the group data
        $group->name = $request->get('name');
        $group->permissions = array(
        		'isdeveloper' => intval($request->get('isdeveloper')),
        		'isproperty' => intval($request->get('isproperty')),
        		'isinvestor' => intval($request->get('isinvestor')),
        	);

        // Was the group updated?
        if ($group->save()) {
            // Redirect to the group page
            return Redirect::route('groups')->with('success', Lang::get('groups/message.success.update'));
        } else {
            // Redirect to the group page
            return Redirect::route('update/group', $id)->with('error', Lang::get('groups/message.error.update'));
        }

    }

    /**
     * Delete confirmation for the given group.
     *
     * @param  int $id
     * @return View
     */
    public function getModalDelete($id = null)
    {
        $model = 'groups';
        $confirm_route = $error = null;
        try {
            // Get group information
            $role = Sentinel::findRoleById($id);


            $confirm_route = route('delete/group', ['id' => $role->id]);
            return View('admin/layouts/modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = Lang::get('admin/groups/message.group_not_found', compact('id'));
            return View('admin/layouts/modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Delete the given group.
     *
     * @param  int $id
     * @return Redirect
     */
    public function destroy($id = null)
    {
        try {
            // Get group information
            $role = Sentinel::findRoleById($id);

            // Delete the group
            $role->delete();

            // Redirect to the group management page
            return Redirect::route('groups')->with('success', Lang::get('groups/message.success.delete'));
        } catch (GroupNotFoundException $e) {
            // Redirect to the group management page
            return Redirect::route('groups')->with('error', Lang::get('groups/message.group_not_found', compact('id')));
        }
    }

    public function data()
    {
    	$roles = Sentinel::getRoleRepository()->all();

    	return Datatables::of($roles)
    	->edit_column('created_at','{!! format($created_at) !!}')
    	->add_column('noofusers', function($role) {  return $role->users()->count(); })
    	->add_column('actions',function($role) {
    		$actions = '<a href='. route('update/group', $role->id) .'><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit group"></i></a>';

    		if ($role->id !== 1) {
    			if($role->users()->count()) {
    				$actions .= '<a href="#" data-toggle="modal" data-target="#users_exists" data-name="'.$role->name.'" class="users_exists"><i class="livicon" data-name="warning-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="'.Lang::get('groups/form.users_exists').'"></i></a>';
    			} else {
    				$actions .= '<a href="'.route('confirm-delete/group', $role->id).'" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="'.Lang::get('groups/form.delete_group').'"></i></a>';
    			}
    		}
    		return $actions;
    	}
    	)
    	->make(true);
    }
}
