<?php namespace App\Http\Controllers;

use App\Http\Requests\AdminRequest;
use App\Application;
use App\Helpers\Support;

class DashboardController extends JoshController
{
	public function index() //this is example data json
	{
		$grafikdata[] =[
				'label' => 'aa',
				'color' => 'red',
				'data' => [["aa",1],["bb",2],["cc",3]]
				];
		$grafikdata[] =[
				'label' => 'sonia',
				'color' => 'blue',
				'data' => [["aa",6],["bb",7],["cc",8]]
				];

		$result = (object) array();
		$result->{'grafikdata'} = $grafikdata;
		$result->{'options'} = [
				'grid' => [
						'hoverable' => true,
						'borderWidth' => 1,
				],
				'series' => [
						'shadowSize' => 0,
						'lines' => [
								'show' => true
						],
						"points" => [
								'show' => true
						]
				],
				'xaxis' => [
					'tickLength' => 0,
					'mode' => 'categories'
				]
		];
		return response()->json($result);
	}

	public function applicationgraph(AdminRequest $request) {
		$month = $request['monthselected'];
		$year = $request['yearselected'];
		$modechart = $request['modechart'];
		$graphtype = $request['graphtype'];

		$grafikdata = [];
		$datas = [];
		$message = '';
		$count = 0;
		if ($modechart == 'monthly') {
			$dateselected = $year.'-'.$month.'-01';
			$endday = date('t', strtotime($dateselected));
			for ($i = 1; $i <= $endday; $i++) {
				$arow = [];
				$arow[] = $i;
				$datequery = (strlen(''.$i) > 1 ? $i : '0'.$i);
				$dateoperator = $year.'-'.$month.'-'.$datequery;
				$tgl = $datequery.'/'.$month.'/'.$year;
				if ($graphtype == 'loan') {
					$count = Application::whereRaw("date(updated_at) = '$dateoperator'")->count();
					$message = 'Loan '.$tgl.' = '.$count;
				} else if ($graphtype == 'loanapproved') {
					$count = Application::whereRaw("date(updated_at) = '$dateoperator'")->where('approved', '=', 1)->count();
					$message = 'Loan Approved '.$tgl.' = '.$count;
				}
				$arow[] = $count;
				$arow[] = $message;
				$datas[] = $arow;
			}
		} else {
			for ($i = 1; $i <= 12; $i++) {
				$dateoperator = $year.'-'.(strlen(''.$i) > 1 ? $i : '0'.$i);
				$arow = [];
				$monthname = Support::getmonthname($i);
				$arow[] = $monthname;
				if ($graphtype == 'loan') {
					$count = Application::whereRaw("DATE_FORMAT(updated_at, '%Y-%m') = '$dateoperator'")->count();
					$message = 'Loan '.$monthname.' = '.$count;
				} else if ($graphtype == 'loanapproved') {
					$count = Application::whereRaw("DATE_FORMAT(updated_at, '%Y-%m') = '$dateoperator'")->where('approved', '=', 1)->count();
					$message = 'Loan Approved '.$monthname.' = '.$count;
				}
				$arow[] = $count;
				$arow[] = $message;
				$datas[] = $arow;
			}
		}
		if ($graphtype == 'loan') {
			$grafikdata[] = [
					'color' => 'green',
					'data' => $datas
					];
		} else if ($graphtype == 'loanapproved') {
			$grafikdata[] = [
					'color' => 'blue',
					'data' => $datas
			];
		}

		$result = (object) array();
		$result->{'grafikdata'} = $grafikdata;
		$result->{'options'} = [
				'grid' => [
						'hoverable' => true,
						'borderWidth' => 1,
				],
				'series' => [
						'shadowSize' => 0,
						'lines' => [
								'show' => true
						],
						"points" => [
								'show' => true
						]
				],
				'xaxis' => [
						'tickLength' => 0,
						'mode' => 'categories'
				]
		];
		return response()->json($result);
	}
}
