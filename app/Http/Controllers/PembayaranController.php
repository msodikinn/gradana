<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Helpers\Payment;
use App\Application;
use App\Pembayaran;
use App\User;
use Input;
use DB;
use Carbon;
use Session;
use Redirect;
use Sentinel;

class PembayaranController extends Controller
{
    public function index()
    {
        $data['byr'] = app('App\Pembayaran')->payment();
        $data['apl'] = app('App\Application')->apl();
        $data['total'] = Payment::getTotal($data['apl']);

        return view('admin.pembayaran.index', $data);
    }

    public function create($id)
    {
        $data['app'] = app('App\Application')->apl();

        $data['appCreate'] = Application::find($id);
        // dd($data['appCreate']);
        switch ($data['appCreate']->status) {
            case "Document Incomplete":
                alert()->error('Payment failed, document incomplete');
                return back();
            case "Document Complete / Verification in Progress":
                alert()->error('Payment failed, verification in progress');
                return back();
            case "Verified":
                alert()->error('Payment failed, waiting to be funded');
                return back();
            case "Fully Paid":
                alert()->error('Payment failed, document fully paid');
                return back();
            default:
                break;
        }

        $data['appCreate'] = Application::find($id);
        $hitung = DB::table('pembayarans')
            ->orderBy('created_at')
            ->where('appid', $id)
            ->select('appid', DB::raw('count(*) as total'))
            ->groupBy('appid')
            ->get();

        if(!isset($hitung[0]->total))
        {
            $data['totalBayar'] = 0;
        }else{
            $data['totalBayar'] = $hitung[0]->total;
        }

        return view('admin.pembayaran.create', $data);
    }

    public function store(Request $request, $id)
    {
        $input = $request->except('angsuran');
        Pembayaran::create($input);

        $latest = Pembayaran::latest()->first();
        $latest->bulan = date('F Y', strtotime($latest->tgl_pembayaran));
        $latest->save();

        alert()->success('Payment has been created.');
        return redirect('/admin/pembayaran');
    }

    public function show($id)
    {
        $data['detail'] = Pembayaran::find($id);
        $data['app'] = Application::all();

        return view('admin.pembayaran.show', $data);
    }

    public function edit($id)
    {
        $data['detail'] = Pembayaran::find($id);
        $data['app'] = Application::all();

        return view('admin.pembayaran.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $detail = Pembayaran::find($id);

        $input = $request->except('angsuran');
        $detail->fill($input)->save();

        $detail->bulan = date('F Y', strtotime($detail->tgl_pembayaran));
        $detail->save();

        alert()->success('Payment has been updated.');
        return back();//redirect('/admin/pembayaran');
    }

    public function destroy($id)
    {
        Pembayaran::find($id)->delete();

        alert()->success('Payment has been deleted.');
        return redirect('/admin/pembayaran');
    }

    public function approved($id)
    {
        $pay = Pembayaran::find($id);
        $pay->approved = 1;
        $pay->save();

        alert()->success('Payment has been approved.');
        return back();
    }
}
