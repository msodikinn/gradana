<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Keluarga;
use App\JobSuamiIstri;
use App\JobPemohon;
use App\SuamiIstri;
use App\Penghasilan;
use App\Perbankan;
use App\User;
use App\Pemohon;
use App\Application;
use Input;
use Session;
use Redirect;
use Sentinel;

class KeluargaController extends JoshController
{
    public function index()
    {
        $data['kel'] = app('App\Keluarga')->filterKeluarga();

        return view('admin.keluarga.index', $data);
    }

    public function create(Request $request)
    {
    	$jobsuamiistri = null;
    	if ($request->backid) {
    		$jobsuamiistri = JobSuamiIstri::find($request->backid);
            $jobpemohon = JobPemohon::find($request->backid);
    	}
    	if (!is_null(session('pem'))) {
        	$data['users'] = User::where('id', session('pem'))->first();
    	} else if (!is_null($jobsuamiistri)) {
    		$data['users'] = $jobsuamiistri->user()->first();
    	}

    	if (!is_null($jobsuamiistri)) {
    		$data['jobsuamiistriid'] = $request->backid;
    		$data['suamisitriid'] = SuamiIstri::where('pemohonid', '=', $jobsuamiistri->pemohonid)->first()->id;
    		$data['jobpemohonid'] = JobPemohon::where('pemohonid', '=', $jobsuamiistri->pemohonid)->first()->id;
    		$data['pemohonid'] = $jobsuamiistri->pemohonid;
        }elseif (!is_null($jobpemohon)) {
            $data['jobpemohonid'] = JobPemohon::where('pemohonid', '=', $jobpemohon->pemohonid)->first()->id;
            $data['pemohonid'] = $jobpemohon->pemohonid;
    	} else {
    		$data['jobsuamiistriid'] = '';
    		$data['suamisitriid'] = '';
    		$data['jobpemohonid'] = '';
    		$data['pemohonid'] = '';
    	}
        $data['hub'] = $this->hubunganKel;

        return view('admin.keluarga.create', $data);
    }

    public function store(Request $request)
    {
        $input = $request->except('pem');
        $keluarga = Keluarga::create($input);

        alert()->success('Keluarga terdekat has been created.');
        return redirect('/admin/perbankan/create?backid='.$keluarga->id)->with('pem', $request->userid);
    }

    public function show($id)
    {
        $data['detail'] = Keluarga::find($id);

        return view('admin.keluarga.show', $data);
    }

    public function edit($id)
    {
        $data['users'] = User::All();
        $keluarga = Keluarga::find($id);
        $data['detail'] = $keluarga;
        $data['hub'] = $this->hubunganKel;

        $data['backid'] = $id;
        $pemohonid = $keluarga->pemohonid;
        $data['pemohonid'] = $pemohonid;
        $data['jobpemohonid'] = '';
        $data['suamisitriid'] = '';
        $data['jobsuamiistriid'] = '';
        $data['keluargaid'] = $id;
        $data['penghasilanid'] = '';
        $data['perbankanid'] = '';
        $data['applicationid'] = '';

        $jobpemohon = JobPemohon::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($jobpemohon)) {
        	$data['jobpemohonid'] = $jobpemohon->id;
        }
        $suamisitri = SuamiIstri::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($suamisitri)) {
        	$data['suamisitriid'] = $suamisitri->id;
        }
        $jobsuamiistri = JobSuamiIstri::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($jobsuamiistri)) {
        	$data['jobsuamiistriid'] = $jobsuamiistri->id;
        }
        $penghasilan = Penghasilan::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($penghasilan)) {
        	$data['penghasilanid'] = $penghasilan->id;
        }
        $perbankan = Perbankan::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($perbankan)) {
        	$data['perbankanid'] = $perbankan->id;
        }
        $application = Application::where('userid', '=', $data['detail']->userid)->get();
        $data['countApp'] = count($application);
        if (!is_null($application))
        {
            if($data['countApp'] == 1)
            {
               $data['applicationid'] = $application[0]->id;
            }
        }

        return view('admin.keluarga.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $data = Keluarga::find($id);

        $input = $request->all();
        $data->fill($input)->save();

        alert()->success('Keluarga terdekat has been updated.');
        return redirect('/admin/keluarga');
    }

    public function destroy($id)
    {
        Keluarga::find($id)->delete();

        alert()->success('Keluarga terdekat has been deleted.');
        return redirect('/admin/keluarga');
    }
}
