<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Investor;
use App\Investor_cat;
use Input;
use Session;
use Sentinel;
use Redirect;

class Investor_catController extends Controller
{
    public function index()
    {
        $data['cat'] = Investor_cat::All();

        return view('admin.investor_cat.index', $data);
    }

    public function create()
    {
        return view('admin.investor_cat.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        Investor_cat::create($input);

        alert()->success('Lender Category has been created.');
        return redirect('/admin/investor_cat');
    }

    public function edit($id)
    {
        $data['cat'] = Investor_cat::find($id);

        return view('admin.investor_cat.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $cat = Investor_cat::find($id);

        $input = $request->all();
        $cat->fill($input)->save();

        alert()->success('Lender Category has been update.');
        return back();
    }

    public function destroy($id)
    {
        $cat = Investor_cat::find($id)->delete();

        alert()->success('Lender Category has been deleted.');
        return redirect('/admin/investor_cat');
    }
}
