<?php namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\MessageBag;
use Sentinel;
use View;
use App\Application;
use App\Developer;
use App\Investor;
use DB;
use App\Blog;

class JoshController extends Controller {

	protected $countries = array(
			""   => "Select Country",
			"AF" => "Afghanistan",
			"AL" => "Albania",
			"DZ" => "Algeria",
			"AS" => "American Samoa",
			"AD" => "Andorra",
			"AO" => "Angola",
			"AI" => "Anguilla",
			"AR" => "Argentina",
			"AM" => "Armenia",
			"AW" => "Aruba",
			"AU" => "Australia",
			"AT" => "Austria",
			"AZ" => "Azerbaijan",
			"BS" => "Bahamas",
			"BH" => "Bahrain",
			"BD" => "Bangladesh",
			"BB" => "Barbados",
			"BY" => "Belarus",
			"BE" => "Belgium",
			"BZ" => "Belize",
			"BJ" => "Benin",
			"BM" => "Bermuda",
			"BT" => "Bhutan",
			"BO" => "Bolivia",
			"BA" => "Bosnia and Herzegowina",
			"BW" => "Botswana",
			"BV" => "Bouvet Island",
			"BR" => "Brazil",
			"IO" => "British Indian Ocean Territory",
			"BN" => "Brunei Darussalam",
			"BG" => "Bulgaria",
			"BF" => "Burkina Faso",
			"BI" => "Burundi",
			"KH" => "Cambodia",
			"CM" => "Cameroon",
			"CA" => "Canada",
			"CV" => "Cape Verde",
			"KY" => "Cayman Islands",
			"CF" => "Central African Republic",
			"TD" => "Chad",
			"CL" => "Chile",
			"CN" => "China",
			"CX" => "Christmas Island",
			"CC" => "Cocos (Keeling) Islands",
			"CO" => "Colombia",
			"KM" => "Comoros",
			"CG" => "Congo",
			"CD" => "Congo, the Democratic Republic of the",
			"CK" => "Cook Islands",
			"CR" => "Costa Rica",
			"CI" => "Cote d'Ivoire",
			"HR" => "Croatia (Hrvatska)",
			"CU" => "Cuba",
			"CY" => "Cyprus",
			"CZ" => "Czech Republic",
			"DK" => "Denmark",
			"DJ" => "Djibouti",
			"DM" => "Dominica",
			"DO" => "Dominican Republic",
			"EC" => "Ecuador",
			"EG" => "Egypt",
			"SV" => "El Salvador",
			"GQ" => "Equatorial Guinea",
			"ER" => "Eritrea",
			"EE" => "Estonia",
			"ET" => "Ethiopia",
			"FK" => "Falkland Islands (Malvinas)",
			"FO" => "Faroe Islands",
			"FJ" => "Fiji",
			"FI" => "Finland",
			"FR" => "France",
			"GF" => "French Guiana",
			"PF" => "French Polynesia",
			"TF" => "French Southern Territories",
			"GA" => "Gabon",
			"GM" => "Gambia",
			"GE" => "Georgia",
			"DE" => "Germany",
			"GH" => "Ghana",
			"GI" => "Gibraltar",
			"GR" => "Greece",
			"GL" => "Greenland",
			"GD" => "Grenada",
			"GP" => "Guadeloupe",
			"GU" => "Guam",
			"GT" => "Guatemala",
			"GN" => "Guinea",
			"GW" => "Guinea-Bissau",
			"GY" => "Guyana",
			"HT" => "Haiti",
			"HM" => "Heard and Mc Donald Islands",
			"VA" => "Holy See (Vatican City State)",
			"HN" => "Honduras",
			"HK" => "Hong Kong",
			"HU" => "Hungary",
			"IS" => "Iceland",
			"IN" => "India",
			"ID" => "Indonesia",
			"IR" => "Iran (Islamic Republic of)",
			"IQ" => "Iraq",
			"IE" => "Ireland",
			"IL" => "Israel",
			"IT" => "Italy",
			"JM" => "Jamaica",
			"JP" => "Japan",
			"JO" => "Jordan",
			"KZ" => "Kazakhstan",
			"KE" => "Kenya",
			"KI" => "Kiribati",
			"KP" => "Korea, Democratic People's Republic of",
			"KR" => "Korea, Republic of",
			"KW" => "Kuwait",
			"KG" => "Kyrgyzstan",
			"LA" => "Lao People's Democratic Republic",
			"LV" => "Latvia",
			"LB" => "Lebanon",
			"LS" => "Lesotho",
			"LR" => "Liberia",
			"LY" => "Libyan Arab Jamahiriya",
			"LI" => "Liechtenstein",
			"LT" => "Lithuania",
			"LU" => "Luxembourg",
			"MO" => "Macau",
			"MK" => "Macedonia, The Former Yugoslav Republic of",
			"MG" => "Madagascar",
			"MW" => "Malawi",
			"MY" => "Malaysia",
			"MV" => "Maldives",
			"ML" => "Mali",
			"MT" => "Malta",
			"MH" => "Marshall Islands",
			"MQ" => "Martinique",
			"MR" => "Mauritania",
			"MU" => "Mauritius",
			"YT" => "Mayotte",
			"MX" => "Mexico",
			"FM" => "Micronesia, Federated States of",
			"MD" => "Moldova, Republic of",
			"MC" => "Monaco",
			"MN" => "Mongolia",
			"MS" => "Montserrat",
			"MA" => "Morocco",
			"MZ" => "Mozambique",
			"MM" => "Myanmar",
			"NA" => "Namibia",
			"NR" => "Nauru",
			"NP" => "Nepal",
			"NL" => "Netherlands",
			"AN" => "Netherlands Antilles",
			"NC" => "New Caledonia",
			"NZ" => "New Zealand",
			"NI" => "Nicaragua",
			"NE" => "Niger",
			"NG" => "Nigeria",
			"NU" => "Niue",
			"NF" => "Norfolk Island",
			"MP" => "Northern Mariana Islands",
			"NO" => "Norway",
			"OM" => "Oman",
			"PK" => "Pakistan",
			"PW" => "Palau",
			"PA" => "Panama",
			"PG" => "Papua New Guinea",
			"PY" => "Paraguay",
			"PE" => "Peru",
			"PH" => "Philippines",
			"PN" => "Pitcairn",
			"PL" => "Poland",
			"PT" => "Portugal",
			"PR" => "Puerto Rico",
			"QA" => "Qatar",
			"RE" => "Reunion",
			"RO" => "Romania",
			"RU" => "Russian Federation",
			"RW" => "Rwanda",
			"KN" => "Saint Kitts and Nevis",
			"LC" => "Saint LUCIA",
			"VC" => "Saint Vincent and the Grenadines",
			"WS" => "Samoa",
			"SM" => "San Marino",
			"ST" => "Sao Tome and Principe",
			"SA" => "Saudi Arabia",
			"SN" => "Senegal",
			"SC" => "Seychelles",
			"SL" => "Sierra Leone",
			"SG" => "Singapore",
			"SK" => "Slovakia (Slovak Republic)",
			"SI" => "Slovenia",
			"SB" => "Solomon Islands",
			"SO" => "Somalia",
			"ZA" => "South Africa",
			"GS" => "South Georgia and the South Sandwich Islands",
			"ES" => "Spain",
			"LK" => "Sri Lanka",
			"SH" => "St. Helena",
			"PM" => "St. Pierre and Miquelon",
			"SD" => "Sudan",
			"SR" => "Suriname",
			"SJ" => "Svalbard and Jan Mayen Islands",
			"SZ" => "Swaziland",
			"SE" => "Sweden",
			"CH" => "Switzerland",
			"SY" => "Syrian Arab Republic",
			"TW" => "Taiwan, Province of China",
			"TJ" => "Tajikistan",
			"TZ" => "Tanzania, United Republic of",
			"TH" => "Thailand",
			"TG" => "Togo",
			"TK" => "Tokelau",
			"TO" => "Tonga",
			"TT" => "Trinidad and Tobago",
			"TN" => "Tunisia",
			"TR" => "Turkey",
			"TM" => "Turkmenistan",
			"TC" => "Turks and Caicos Islands",
			"TV" => "Tuvalu",
			"UG" => "Uganda",
			"UA" => "Ukraine",
			"AE" => "United Arab Emirates",
			"GB" => "United Kingdom",
			"US" => "United States",
			"UM" => "United States Minor Outlying Islands",
			"UY" => "Uruguay",
			"UZ" => "Uzbekistan",
			"VU" => "Vanuatu",
			"VE" => "Venezuela",
			"VN" => "Viet Nam",
			"VG" => "Virgin Islands (British)",
			"VI" => "Virgin Islands (U.S.)",
			"WF" => "Wallis and Futuna Islands",
			"EH" => "Western Sahara",
			"YE" => "Yemen",
			"ZM" => "Zambia",
			"ZW" => "Zimbabwe"
	);

    protected $jenis = array(
        "Rumah" => "Rumah",
        "Apartemen" => "Apartemen",
        "Ruko/Villa" => "Ruko/Villa",
        "Lainnya" => "Lainnya",
    );

    protected $tujuan = array(
        "Pembelian" => "Pembelian",
        "Pembangunan" => "Pembangunan",
        "Renovasi" => "Renovasi",
        "Beli Kavling" => "Beli Kavling",
        "Take Over" => "Take Over",
        "Refinancing" => "Refinancing",
    );

    protected $jenisApplication = array(
        "Cicilan DP" => "Cicilan DP",
        "KPR Full" => "KPR Full",
        "A/R Factoring" => "A/R Factoring",
        "Kredit Koperasi" => "Kredit Koperasi",
    );

    protected $sistemBayar = array(
        "Angsuran" => "Angsuran",
        "Xtra" => "Xtra",
        "Dana Siaga" => "Dana Siaga",
    );

    protected $statusKawin = array(
        "Lajang" => "Lajang",
        "Menikah" => "Menikah",
        "Duda/Janda" => "Duda/Janda",
    );

    protected $statusRumah = array(
        "Landed Residential" => "Landed Residential",
        "Apartment" => "Apartment",
        "Lot" => "Lot",
        "Shophouses" => "Shophouses",
    );

    protected $statusPendidikan = array(
        "SD" => "SD",
        "SMP" => "SMP",
        "SMA" => "SMA",
        "D1" => "D1",
        "D2" => "D2",
        "D3" => "D3",
        "S1" => "S1",
        "S2" => "S2",
        "S3" => "S3",
    );

    protected $statusNikah = array(
        "Suami" => "Suami",
        "Istri" => "Istri",
    );

    protected $jenisPekerjaan = array(
        "Karyawan Swasta" => "Karyawan Swasta",
        "Pegawai Negeri" => "Pegawai Negeri",
        "Wiraswasta" => "Wiraswasta",
        "Profesional" => "Profesional",
        "Ibu Rumah Tangga" => "Ibu Rumah Tangga",
        "Lainnya" => "Lainnya",
    );

    protected $sumberInfo = array(
        "Karyawan" => "Karyawan",
        "Wiraswasta" => "Wiraswasta",
        "Profesional" => "Profesional",
        "Ibu Rumah Tangga" => "Ibu Rumah Tangga",
    );

    protected $jenisNasabah = array(
        "" => "",
        // "Tabungan / Giro / Deposito" => "Tabungan / Giro / Deposito",
        "Kartu Kredit" => "Kartu Kredit",
        "Angsuran yang sedang berjalan" => "Angsuran yang sedang berjalan",
    );

    protected $statusApp = array(
        "Document Incomplete" => "Document Incomplete",
        "Document Complete / Verification in Progress" => "Document Complete / Verification in Progress",
        "Verified" => "Verified",
        "Funded" => "Funded",
        "Installment on Progress" => "Installment on Progress",
        "Fully Paid" => "Fully Paid",
    );

    protected $jaminanApp = array(
        "Saham" => "Saham",
        "Tanah (A/R Factoring)" => "Tanah (A/R Factoring)",
    );

    protected $rating = array(
        "0" => "0",
        "1" => "1",
        "2" => "2",
        "3" => "3",
        "4" => "4",
        "5" => "5"
    );

    protected $ratingJmlCC = array(
        "5 nilai - 0 kartu kredit" => "0 kartu kredit",
        "5 nilai - 1 kartu kredit" => "1 kartu kredit",
        "4 nilai - 2 kartu kredit" => "2 kartu kredit",
        "3 nilai - 3 kartu kredit" => "3 kartu kredit",
        "2 nilai - 4 kartu kredit" => "4 kartu kredit",
        "1 nilai - 5 atau lebih kartu kredit" => "≥ 5 kartu kredit"
    );

    protected $ratingLamaKerja = array(
        "Tidak Bekerja" => "Tidak Bekerja",
        "≥ 5 tahun" => "≥ 5 tahun",
        "4 ≤ lama kerja < 5 tahun" => "4 ≤ lama kerja < 5 tahun",
        "3 ≤ lama kerja < 4 tahun" => "3 ≤ lama kerja < 4 tahun",
        "2 ≤ lama kerja < 3 tahun" => "2 ≤ lama kerja < 3 tahun",
        "< 2 tahun" => "< 2 tahun"
    );

    protected $ratingTotalLamaKerja = array(
        "Tidak Bekerja" => "Tidak Bekerja",
        "≥ 5 tahun" => "≥ 5 tahun",
        "4 ≤ total < 5 tahun" => "4 ≤ total < 5 tahun",
        "3 ≤ total < 4 tahun" => "3 ≤ total < 4 tahun",
        "2 ≤ total < 3 tahun" => "2 ≤ total < 3 tahun",
        "< 2 tahun" => "< 2 tahun"
    );

    protected $ratingData = array(
        "0% Complete" => "0% Complete",
        "100% Complete" => "100% Complete",
        "95% ≤ data < 100% Complete" => "95% ≤ data < 100% Complete",
        "90% ≤ data < 95% Complete" => "90% ≤ data < 95% Complete",
        "80% ≤ data < 90% Complete" => "80% ≤ data < 90% Complete",
        "< 80% Complete" => "< 80% Complete"
    );

    protected $ratingDataAkurasi = array(
        "0% Accurate" => "0% Accurate",
        "100% Accurate" => "100% Accurate",
        "95% ≤ data < 100% Accurate" => "95% ≤ data < 100% Accurate",
        "90% ≤ data < 95% Accurate" => "90% ≤ data < 95% Accurate",
        "80% ≤ data < 90% Accurate" => "80% ≤ data < 90% Accurate",
        "< 80% complete" => "< 80% Accurate"
    );

    protected $jenisUsaha = array(
        "Pertanian, Kehutanan, Perikanan dan Maritim" => "Pertanian, Kehutanan, Perikanan dan Maritim",
        "Pertambangan dan Penggalian" => "Pertambangan dan Penggalian",
        "Industri Pengolahan" => "Industri Pengolahan",
        "Penyedia Tenaga Listrik/Gas/Air" => "Penyedia Tenaga Listrik/Gas/Air",
        "Konstruksi" => "Konstruksi",
        "Perdagangan, Ekspor-Impor, dan Distribusi" => "Perdagangan, Ekspor-Impor, dan Distribusi",
        "Akomodasi (Hotel/Pariwisata)" => "Akomodasi (Hotel/Pariwisata)",
        "Notaris, Pengacara, dan Jasa Akuntansi" => "Notaris, Pengacara, dan Jasa Akuntansi",
        "Bisnis Berbasis Tunai (Minimarket, SPBU, Rumah Makan, Parkir)" => "Bisnis Berbasis Tunai (Minimarket, SPBU, Rumah Makan, Parkir)",
        "Usaha Barang Antik/Seni, Logam Mulia, dan Perhiasan" => "Usaha Barang Antik/Seni, Logam Mulia, dan Perhiasan",
        "Transportasi dan Pergudangan" => "Transportasi dan Pergudangan",
        "Biro Pariwisata" => "Biro Pariwisata",
        "Dealer Kendaraan (Mobil, Perahu, dan Sepeda Motor)" => "Dealer Kendaraan (Mobil, Perahu, dan Sepeda Motor)",
        "Konsultan" => "Konsultan",
        "Hiburan (Hiburan, Budaya, dan Seni)" => "Hiburan (Hiburan, Budaya, dan Seni)",
        "Layanan Umum (Rumah Sakit, Pendidikan)" => "Layanan Umum (Rumah Sakit, Pendidikan)",
        "Yayasan/NGO/Lembaga Non-Hukum" => "Yayasan/NGO/Lembaga Non-Hukum",
        "Jasa Keuangan (Bank/Non-Bank/Penukaran Mata Uang/Pengiriman Uang)" => "Jasa Keuangan (Bank/Non-Bank/Penukaran Mata Uang/Pengiriman Uang)",
        "Informasi dan Telekomunikasi" => "Informasi dan Telekomunikasi",
        "Lainnya" => "Lainnya",
        "Pemerintahan" => "Pemerintahan",
        "Tempat Hiburan" => "Tempat Hiburan",
        "Pejabat Pembuat Akta Tanah (PPAT)" => "Pejabat Pembuat Akta Tanah (PPAT)",
        "Konsultan Keuangan" => "Konsultan Keuangan",
        "Perumahan dan Pengembang/Agen Properti" => "Perumahan dan Pengembang/Agen Properti",
    );

    protected $statusBayar = array(
        "Tunai" => "Tunai",
        "Transfer" => "Transfer",
        "Cek" => "Cek",
        "Giro" => "Giro",
    );

    protected $jml_alokasi = array(
        "1" => "1",
        "2" => "2",
        "3" => "3",
        "> 3" => "> 3",
    );

    protected $tenor = array(
        "24 bulan" => "24 bulan",
        "36 bulan" => "36 bulan",
    );

    protected $penempatan_dana = array(
        "Pembayaran kas penuh ke developer" => "Pembayaran kas penuh ke developer",
        "Pembayaran jumlah DP saja ke developer" => "Pembayaran jumlah DP saja ke developer",
    );

    protected $hubunganKel = array(
        "Orang Tua" => "Orang Tua",
        "Saudara Kandung" => "Saudara Kandung",
        "Keluarga Non-Inti" => "Keluarga Non-Inti",
        "Lainnya" => "Lainnya",
    );

	/**
	 * Message bag.
	 *
	 * @var Illuminate\Support\MessageBag
	 */
	protected $messageBag = null;

	/**
	 * Initializer.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->messageBag = new MessageBag;

	}

	/**
	* Crop Demo
	*/
	public function crop_demo()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$targ_w = $targ_h = 150;
			$jpeg_quality = 99;

			$src = base_path().'/public/assets/img/cropping-image.jpg';
		//dd($src);
			$img_r = imagecreatefromjpeg($src);

			$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

			imagecopyresampled($dst_r,$img_r,0,0,intval($_POST['x']),intval($_POST['y']), $targ_w,$targ_h, intval($_POST['w']),intval($_POST['h']));

			header('Content-type: image/jpeg');
			imagejpeg($dst_r,null,$jpeg_quality);

			exit;
		}
	}

    public function showHome()
    {
    	if(Sentinel::check()) {
    		$minyear = date('Y');
    		$maxyear = $minyear;

    		$dataoption = Application::select(DB::Raw('DATE_FORMAT(min(updated_at), \'%Y\') as minyear'))->first()->minyear;
    		if ($dataoption < $minyear) {
    			$minyear = $dataoption;
    		}

    		$dataoption = Application::select(DB::Raw('DATE_FORMAT(max(updated_at), \'%Y\') as minyear'))->first()->minyear;
    		if ($dataoption > $maxyear) {
    			$maxyear = $dataoption;
    		}

    		$yearoptions = [];
    		for ($i = $minyear; $i <= $maxyear; $i++) {
    			$yearoptions[$i] = $i;
    		}

    		$developers = Developer::all();
    		$list = '';
    		foreach ($developers as $developer) {
    			$list .= $developer->nama.'-'.$developer->applications->count();
    		}

			return View('admin/index',[
					'yearoptions' => $yearoptions,
					'top5developers' => DB::select("select id, nama, countdata from (select dev.id, dev.nama, (select count(*) from applications app where app.devid = dev.id) as countdata from developers dev) as data where countdata > 0  order by countdata desc limit 5"),
					'top5investors' => DB::select("select id, nama, countdata from(select inv.id, inv.nama, (select count(*) from applications app where app.investorid = inv.id) as countdata from investors inv) as data where countdata > 0  order by countdata desc limit 5"),
					'top5properties' => DB::select("select id, nama, countdata from(select prop.id, prop.nama, (select count(*) from applications app where app.propid = prop.id) as countdata from properties prop) as data where countdata > 0  order by countdata desc limit 5"),
					'blogs' => Blog::take(5)->orderBy('updated_at')->get(),
					'loancount' => number_format(Application::count(),0,",","."),
					'loanapprovedcount' => number_format(Application::where('approved' ,'=', 1)->count(),0,",","."),
			]);
    	}
		return Redirect::to('admin/signin')->with('error', 'You must be logged in!');
    }

    public function showView($name=null)
    {

    	if(View::exists('admin/'.$name))
		{
			if(Sentinel::check())
				return View('admin/'.$name);
			else
				return Redirect::to('admin/signin')->with('error', 'You must be logged in!');
		}
		else
		{
			return View('admin/404');
		}
    }

    public function showFrontEndView($name=null)
    {

        if(View::exists($name))
        {
            return View($name);
        }
        else
        {
            return View('admin/404');
        }
    }


}
