<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Developer;
use App\Application;
use App\Pemohon;
use App\Property;
use App\User;
use Input;
use Session;
use Redirect;
use Sentinel;
use Alert;
use Config;
use File;
use Image;

class DeveloperController extends JoshController
{
    public function index()
    {
        $data['dev'] = Developer::All();

        return view('admin.developer.index', $data);
    }

    public function create()
    {
        return view('admin.developer.create');
    }

    public function store(Request $request)
    {
        $input = $request->except(['img']);
        Developer::create($input);
        $dev = Developer::latest()->first();

        $imgPath = Config::get('gradana.developer_img_path');
        $realPath = public_path().'/'.$imgPath;
        !file_exists($realPath) ? File::makeDirectory($realPath, 0775, true) : "";

        if($request->hasFile('img')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('img')->getClientOriginalExtension();
            Image::make($request->img)->save($realPath.$filename);
            $dev->img = $imgPath.$filename;
            $dev->save();
        }

        alert()->success('Developer has been created.');
        return redirect('/admin/developer');
    }

    public function show($id)
    {
        $data = [
            'detail'    => Developer::find($id),
            'users'     => User::all(),
            'dev'       => Developer::all(),
            'statusr'   => $this->statusRumah
        ];
        $data['prop'] = Property::where('devid', $data['detail']->id)->get();

        return view('admin.developer.show', $data);
    }

    public function edit($id)
    {
        $data['detail'] = Developer::find($id);

        return view('admin.developer.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $dev = Developer::find($id);

        $imgPath = Config::get('gradana.developer_img_path');
        $realPath = public_path().'/'.$imgPath;
        !file_exists($realPath) ? File::makeDirectory($realPath, 0775, true) : '' ;

        if($request->hasFile('img')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('img')->getClientOriginalExtension();
            Image::make(Input::file('img'))->save($realPath.$filename);
            if (File::exists(public_path() .'/'. $dev->img)) {
                File::delete(public_path() .'/'. $dev->img);
            }

            $dev->img = $imgPath.$filename;
        }

        $input = $request->except(['img']);
        $dev->fill($input)->save();

        alert()->success('Developer has been updated.');
        return redirect('/admin/developer');
    }

    public function destroy($id)
    {
        $dev = Developer::find($id)->delete();

        alert()->success('Developer has been deleted.');
        return redirect('/admin/developer');
    }

    public function approved($id)
    {
        $dev = Developer::find($id);
        $dev->approved = 1;
        $dev->save();

        alert()->success('Developer '. $dev->nama .' has been approved.');
        return back();
    }
}
