<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
// use Laravel\Socialite\Facades\Socialite;
use Auth;
use Exception;

class LinkedinAuthController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        // dd(\Socialite::with('linkedin'));
        // return Socialite::driver('linkedin')->redirect();
    }

    /**
     * Obtain the user information from linkedin.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        // $user = Socialite::driver('linkedin')->user();
        // dd($user);
        // $user->token;
    }
}
