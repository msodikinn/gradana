<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Developer;
use App\Property;
use App\SubProperty;
use App\User;
use App\Gallery;
use Input;
use Session;
use Redirect;
use Sentinel;

class PropertyController extends JoshController
{
    public function index()
    {
        $data['prop'] = Property::All();

        return view('admin.property.index', $data);
    }

    public function create()
    {
        return view('admin.property.create', [
            'users'     => User::all(),
            'dev'       => Developer::all(),
            'prop'      => Property::all(),
            'statusr'    => $this->statusRumah,
            'galeries'    => Gallery::where('propid', '=', 0)->get(),
        ]);
    }

    public function store(Request $request)
    {
        $input = $request->except(['statProp', 'idDev', 'galleryidlist']);
        $property = Property::create($input);
        $this->addgalleryinproperty($request ,$property);

        alert()->success('Property has been created.');

        if($request->statProp){
            return redirect('/admin/developer/detail/'.$request->idDev);
        }else{
            return redirect('/admin/property');
        }
    }

    public function show($id)
    {
        $data = [
            'detail' => Property::with('subproperties')->find($id),
            'users' => User::all(),
            'dev' => Developer::all(),
            'prop' => Property::all()
        ];
        $gallery = $data['detail']->galleries->first();
        if($gallery == null){
            $data['pics'] = "";
        }else{
            $data['pics'] = $gallery->properyimg()->get();
        }

        return view('admin.property.show', $data);
    }

    public function edit($id)
    {
    	$property = Property::find($id);
        return view('admin.property.edit', [
            'detail'    => $property,
            'users'     => User::all(),
            'dev'       => Developer::all(),
            'statusr'   => $this->statusRumah,
        	'galeries'    => Gallery::where('propid', '=', 0)->get(),
        	'propertyphotos'    => $property->galleries,
        ]);
    }

    public function update(Request $request, $id)
    {
        $detail = Property::find($id);
        $this->addgalleryinproperty($request ,$detail);

        $input = $request->except(['galleryidlist']);
        $detail->fill($input)->save();

        alert()->success('Property has been updated')->autoclose(3500);
        return back();
    }

    public function destroy($id)
    {
    	$property = Property::find($id);
    	$this->deleteimageindproperty($property);
        $property->delete();

        alert()->success('Property has been deleted')->autoclose(3500);
        return back();
    }

    private function deleteimageindproperty($property){
    	Gallery::where('propid' ,'=', $property->id)->update(['propid' => 0]);
    }
    private function addgalleryinproperty($request, $property){
    	$this->deleteimageindproperty($property);
    	if ($request->galleryidlist) {
    		Gallery::wherein('id', explode(',', $request->galleryidlist))->update(['propid' => $property->id]);
    	}
    }

    public function createsub(Request $request)
    {
        $num1 = $this->cleanSeparator($request->range_harga_min);
        $num2 = $this->cleanSeparator($request->range_harga_max);

        $input = $request->except(['statProp', 'idDev', 'galleryidlist']);
        $input['range_harga_min'] = $num1;
        $input['range_harga_max'] = $num2;
        SubProperty::create($input);

        alert()->success('Product has been created');
        return back();
    }

    public function editsub(Request $request)
    {
        $num1 = $this->cleanSeparator($request->range_harga_min);
        $num2 = $this->cleanSeparator($request->range_harga_max);

        $sub = SubProperty::find($request->id);
        $input = $request->all();
        $input['range_harga_min'] = $num1;
        $input['range_harga_max'] = $num2;
        $sub->fill($input)->save();

        alert()->success('Product has been updated')->autoclose(3500);
        return back();
    }

    public function destroysub($id)
    {
        SubProperty::find($id)->delete();

        alert()->success('Product has been deleted')->autoclose(3500);
        return back();
    }

    function cleanSeparator($num)
    {
        return (int)str_replace( '.', '', $num );
    }
}
