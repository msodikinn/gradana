<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Pemohon;
use App\JobPemohon;
use App\SuamiIstri;
use App\JobSuamiIstri;
use App\Keluarga;
use App\Penghasilan;
use App\Perbankan;
use App\Perbankan_child;
use App\Investor;
use App\Investor_cat;
use Sentinel;
use Config;
use Input;
use Image;
use File;

use App\Application;
use App\Developer;
use App\Property;
use Carbon\Carbon;
use Mail;
use DB;

class ApplicantFrontController extends JoshController
{
    public function disini()
    {
        echo "Hello";
        /*$user_id = Sentinel::getUser()->id;

        // dd($user_id);
        $data['applications'] = Application::where('userid', $user_id)->get()->first();
        $data['app'] = Application::where('userid', $user_id)->get();
        // $data['properties'] = Property::where('userid', $user_id)->get();
        $data['properties'] = DB::table('properties')->select(DB::raw('*'))->where('userid', $user_id)->get();
		// Category::where('title', 'LIKE', '%'.$q.'%')->paginate(5);
        // $applications = app('App\Application')->apl()->where('userid', 30);
        // print_r($applications->status);
        
		
		$inmin = 15;
		
		$inmax = 20;

		$actuali_credit = 3 ; 

		$mincredit = 1;	
		
		$maxcredit = 5;
		
		//$hasil = $inmax - ($actuali_credit/$mincredit)- $maxcredit * ($inmin - $inmax) ;
		
				
		if(!empty($data['applications']->pinjaman)){
			
			if($data['applications']->scheme == "skema_a"){	
			$cek = number_format($data['applications']->pinjaman,0,",",".");
			$format = strlen($data['applications']->pinjaman);
			$explode = explode(".",$cek);
			
			if($format > 9 and $format < 13){
				$data['rupiah'] = $explode[0]." Miliar";
				
			}
			if($format > 6 and $format < 10){
				$data['rupiah'] = $explode[0]." Juta";
				
			}
			
			$bulanan = 0.2 * $data['applications']->pinjaman;
			 // echo $bulanan;
			//400jt + [ 24/12 x 17% x 400jt ]/24
			$bulan2 = ($data['applications']->jangka_waktu / 12  * 0.17 * $bulanan);
			//echo $bulan2;
			$bulan = ($bulan2 + $bulanan)/$data['applications']->jangka_waktu ;
			
			$cek1 = number_format(ceil($bulan),0,",",".");
			$format1 = strlen(ceil($bulan));
			$explode1 = explode(".",$cek1);
			
			if($format1 > 9 and $format1 < 13){
				$data['rp'] = $explode1[0]." Miliar";
				
			}
			if($format1 > 6 and $format1 < 10){
				$data['rp'] = $explode1[0]." Juta";
				
			}
			
			}else{
				
			$cek = number_format($data['applications']->pinjaman_bln,0,",",".");
			$format = strlen($data['applications']->pinjaman_bln);
			$explode = explode(".",$cek);
			
			if($format > 9 and $format < 13){
				$data['rupiah'] = $explode[0]." Miliar";
				
			}
			if($format > 6 and $format < 10){
				$data['rupiah'] = $explode[0]." Juta";
				
			}
			
			$bulanan = 0.2 * $data['applications']->pinjaman;
			 // echo $bulanan;
			//400jt + [ 24/12 x 17% x 400jt ]/24
			$bulan2 = ($data['applications']->jangka_waktu / 12  * 0.17 * $bulanan);
			//echo $bulan2;
			$bulan = ($bulan2 + $bulanan)/$data['applications']->jangka_waktu ;
			
			$cek1 = number_format(ceil($bulan),0,",",".");
			$format1 = strlen(ceil($bulan));
			$explode1 = explode(".",$cek1);
			
			if($format1 > 9 and $format1 < 13){
				$data['rp'] = $explode1[0]." Miliar";
				
			}
			if($format1 > 6 and $format1 < 10){
				$data['rp'] = $explode1[0]." Juta";
				
			}
			
				
			}
		
		
		
		}
		
		
		
		
		return view('frontend.applicant.applicant_dashboard', $data);*/
		
    }

    public function applicant()
    {
        //return view('frontend.applicant.applicantform_first');
        // return view('frontend.applicant.applicantform');
		
		if (Sentinel::check()){
           $data['user'] = Sentinel::getuser();
            if (Sentinel::getuser()->status ==
                "Investor") {
                alert()->error('Anda sudah mendaftar sebagai Investor');
                return back();
            }
        }else{
            alert()->error('Silahkan login terlebih dahulu');
            return view('frontend.login');
        }

        $data['pendidikan'] = $this->statusPendidikan;
        $data['kawin'] = $this->statusKawin;
        $data['rumah'] = $this->statusRumah;
        $data['jenis'] = $this->jenisPekerjaan;
        $data['usaha'] = $this->jenisUsaha;
        $data['status'] = $this->statusNikah;
        $data['jenis'] = $this->jenisPekerjaan;
        $data['usaha'] = $this->jenisUsaha;
		
			$data['dev']       = Developer::all();
            $data['prop']      = Property::all();
			$data['inves']     = Investor::all();
            $data['jenis']     = $this->jenisApplication;
            $data['tujuan']    = $this->tujuan;
            $data['bayar']     = $this->sistemBayar;
            $data['status']    = $this->statusApp;
            $data['jaminan']   = $this->jaminanApp;
        

        return view('frontend.applicant.applicantform', $data);
		
    }

    public function quickSignupMail()
    {
        $dataemail = [
                        'fullname'  => Sentinel::getuser()->fullname,
                        'email' => Sentinel::getuser()->email,
                    ];

        Mail::send('emails.applicant.quick_signup', $dataemail, function ($mail)
        {
            $mail->to('toink90@gmail.com', 'TOINK');
            // $mail->bcc('tantowiachmad@gmail.com', 'Achmad Tantowi');
            //$mail->bcc('jr.pikong@gmail.com', 'Deny Pradia Utama');
            $mail->subject('Quick Signup Application');
        });

        alert()->success('Email sent');

        return back();
    }

    public function applicantform()
    {
        if (Sentinel::check()){
            $data['user'] = Sentinel::getuser();
            if (Sentinel::getuser()->status ==
                "Investor") {
                alert()->error('Anda sudah mendaftar sebagai Investor');
                return back();
            }
        }else{
            alert()->error('Silahkan login terlebih dahulu');
            return view('frontend.login');
        }

        $data['pendidikan'] = $this->statusPendidikan;
        $data['kawin'] = $this->statusKawin;
        $data['rumah'] = $this->statusRumah;
        $data['jenis'] = $this->jenisPekerjaan;
        $data['usaha'] = $this->jenisUsaha;
        $data['status'] = $this->statusNikah;
        $data['jenis'] = $this->jenisPekerjaan;
        $data['usaha'] = $this->jenisUsaha;
		


        return view('frontend.applicant.applicantform', $data);
    }

    public function applicantstore(Request $request)
    {
        // create pemohon
        $inputpemohon = $request->only(['userid',
                                        'fullname_clean',
                                        'tmp_lahir',
                                        'ktp',
                                        'npwp',
                                        'nama_ibu',
                                        'status_kawin',
                                        'tanggungan',
                                        'pendidikan',
                                        'status_rumah',
                                        'alamat_ktp',
                                        'kelurahan_ktp',
                                        'kecamatan_ktp',
                                        'propinsi_ktp',
                                        'kota_ktp',
                                        'postal_ktp',
                                        'telp',
                                        // 'fax',
                                        'hp1',
                                        'hp2',
                                        'facebook_id',
                                        'linkedin_id',
                                        'amount_balance'
                                        // 'cust_verified',
                                        // 'cust_verified_date',
                                        // 'jml_anggota',
                                        // 'simp_wajib',
                                        // 'simp_pokok',
                                        // 'sumber_dana',
                                        // 'bentuk_usaha',
                                        // 'nomor_akta',
                                        // 'domisili_akta',
                                        // 'nama_penanggungjawab',
                                        // 'contact_person',
                                        // 'contact_position'
                                        ]);
        Pemohon::create($inputpemohon);
        $pm = Pemohon::latest()->first();
        $pemohonid = $pm->id;
		
		
		//create applicant
        $inputapplicant = array(
            'userid' => $request->userid,
            'propid' => $request->propid,
            'pemohonid' => $pemohonid,
            'devid' => $request->devid,
            'investorid' => $request->investorid,
            'tujuan' => $request->tujuan,
            'jenis' => $request->jenis,
            'uang_muka' => $request->uang_muka,
            'harga_property' => $request->harga_property,
            'sis_bayar' =>$request->sis_bayar,
            'scheme' => $request->scheme,
            'pinjaman' => $request->pinjaman,
            'pinjaman_bln' => $request->pinjaman_bln,
            'periode' => $request->periode,
            'status' => $request->status,
           
        );
        Application::create($inputapplicant);
        $appli = Application::latest()->first();
        $appli->pemohonid = $pemohonid;
        $appli->save();

		
		//create job pemohon
        $inputjobpem = array(
            'userid' => $request->userid,
            'jenis_pekerjaan' => $request->jenis_pekerjaan_jobpm,
            'nama_perusahaan' => $request->nama_perusahaan_jobpm,
            'bidang_usaha' => $request->bidang_usaha_jobpm,
            'bidang_usaha_detail' => $request->bidang_usaha_detail_jobpm,
            'tanggal_pendirian' => $request->tanggal_pendirian_jobpm,
            'alamat' => $request->alamat_jobpm,
            'kelurahan' => $request->kelurahan_jobpm,
            'kecamatan' =>$request->kecamatan_jobpm,
            'kota' => $request->kota_jobpm,
            'postal' => $request->postal_jobpm,
            'tlp_ktr_hunting' => $request->tlp_ktr_hunting_jobpm,
            'tlp_ktr_direct' => $request->tlp_ktr_direct_jobpm,
            'fax' => $request->fax_jobpm,
            'unit_kerja' => $request->unit_kerja_jobpm,
            'jabatan' => $request->jabatan_jobpm,
            'bulan' => $request->bulan_jobpm,
            'tahun' => $request->tahun_jobpm,
            'omset_bln' =>$request->omset_bln_jobpm,
            'kepemilikan' => $request->kepemilikan_jobpm,
            'margin_untung' => $request->margin_untung_jobpm,
            'nama_perusahaan_ago' => $request->nama_perusahaan_ago_jobpm,
            'jenis_usaha_ago' => $request->jenis_usaha_ago_jobpm,
            'jabatan_ago' => $request->jabatan_ago_jobpm,
            'unit_kerja_ago' => $request->unit_kerja_ago_jobpm,
            'bulan_ago' => $request->bulan_ago_jobpm,
            'tahun_ago' => $request->tahun_ago_jobpm,
            'telp' => $request->telp,
        );
        JobPemohon::create($inputjobpem);
        $jobpm = JobPemohon::latest()->first();
        $jobpm->pemohonid = $pemohonid;
        $jobpm->save();

        // create suami istri
        $inputsuamiistri = array(
            'userid' => $request->userid,
            'status' => $request->status_suami,
            'nama' => $request->nama_suami,
            'tempat_lahir' => $request->tempat_lahir_suami,
            'tgl_lahir' => $request->tgl_lahir_suami,
            'ktp' => $request->ktp_suami,
            'npwp' => $request->npwp_suami,
            'telp' =>$request->telp_suami,
            'hp' => $request->hp_suami,
        );
        SuamiIstri::create($inputsuamiistri);
        $suamiistri = SuamiIstri::latest()->first();
        $suamiistri->pemohonid = $pemohonid;
        $suamiistri->save();

        // create job suami istri
        $inputjobsuami = array(
            'userid' => $request->userid,
            'jenis_pekerjaan' => $request->jenis_pekerjaan_jobsuami,
            'nama_perusahaan' => $request->nama_perusahaan_jobsuami,
            'bidang_usaha' => $request->bidang_usaha_jobsuami,
            'bidang_usaha_detail' => $request->bidang_usaha_detail_jobsuami,
            'tanggal_pendirian' => $request->tanggal_pendirian_jobsuami,
            'alamat' => $request->alamat_jobsuami,
            'kelurahan' => $request->kelurahan_jobsuami,
            'kecamatan' =>$request->kecamatan_jobsuami,
            'kota' => $request->kota_jobsuami,
            'postal' => $request->postal_jobsuami,
            'tlp_ktr_hunting' => $request->tlp_ktr_hunting_jobsuami,
            'tlp_ktr_direct' => $request->tlp_ktr_direct_jobsuami,
            'fax' => $request->fax_jobsuami,
            'unit_kerja' => $request->unit_kerja_jobsuami,
            'jabatan' => $request->jabatan_jobsuami,
            'bulan' => $request->bulan_jobsuami,
            'tahun' => $request->tahun_jobsuami,
            'omset_bln' =>$request->omset_bln_jobsuami,
            'kepemilikan' => $request->kepemilikan_jobsuami,
            'margin_untung' => $request->margin_untung_jobsuami,
            'nama_perusahaan_ago' => $request->nama_perusahaan_ago_jobsuami,
            'jenis_usaha_ago' => $request->jenis_usaha_ago_jobsuami,
            'jabatan_ago' => $request->jabatan_ago_jobsuami,
            'unit_kerja_ago' => $request->unit_kerja_ago_jobsuami,
            'bulan_ago' => $request->bulan_ago_jobsuami,
            'tahun_ago' => $request->tahun_ago_jobsuami,
            'telp' => $request->telp_jobsuami,
        );
        JobSuamiIstri::create($inputjobsuami);
        $jobsuami = JobSuamiIstri::latest()->first();
        $jobsuami->pemohonid = $pemohonid;
        $jobsuami->save();

        // create keluarga
        $inputkeluarga = array(
            'userid' => $request->userid,
            'nama' => $request->nama_keluarga,
            'alamat' => $request->alamat_keluarga,
            // 'kelurahan' => $request->kelurahan_keluarga,
            // 'kecamatan' => $request->kecamatan_keluarga,
            // 'propinsi' => $request->propinsi_keluarga,
            // 'kota' =>$request->kota_keluarga,
            'telp' => $request->telp_keluarga,
            'hp' =>$request->hp_keluarga,
            'hub' =>$request->hub_keluarga,
        );
        Keluarga::create($inputkeluarga);
        $keluarga = Keluarga::latest()->first();
        $keluarga->pemohonid = $pemohonid;
        $keluarga->save();

        // create penghasilan
        $inputpenghasilan = array(
            'userid' => $request->userid,
            'joint_income' => $request->joint_income_penghasilan,
            'pemohon' => $request->pemohon_penghasilan,
            'suami_istri' => $request->suami_istri_penghasilan,
            'tambahan' => $request->tambahan_penghasilan,
            'biaya_rt' => $request->biaya_rt_penghasilan,
            'angsuran_lain' => $request->angsuran_lain_penghasilan,
        );
        Penghasilan::create($inputpenghasilan);
        $penghasilan = Penghasilan::latest()->first();
        $penghasilan->pemohonid = $pemohonid;
        $penghasilan->save();

        // create perbankan
        $inputperbankan = array(
            'userid' => $request->userid,
            'sumber_info' => $request->sumber_info_perbankan,
        );
        Perbankan::create($inputperbankan);
        $pb = Perbankan::latest()->first();
        $pb->pemohonid = $pemohonid;
        $pb->save();

        // save image
        $imgPath = Config::get('gradana.pemohon_img_path');
        $realPath = public_path().'/'.$imgPath;
        !file_exists($realPath) ? File::makeDirectory($realPath) : "";

        if($request->hasFile('ktp_img')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('ktp_img')->getClientOriginalExtension();
            Image::make($request->ktp_img)->save($realPath.$filename);
            $pm->ktp_img = $imgPath.$filename;
        }
        if($request->hasFile('npwp_img')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('npwp_img')->getClientOriginalExtension();
            Image::make($request->npwp_img)->save($realPath.$filename);
            $pm->npwp_img = $imgPath.$filename;
        }
        if($request->hasFile('kk_img')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('kk_img')->getClientOriginalExtension();
            Image::make($request->kk_img)->save($realPath.$filename);
            $pm->kk_img = $imgPath.$filename;
        }
        if($request->hasFile('tagihan_listrik')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('tagihan_listrik')->getClientOriginalExtension();
            Image::make($request->tagihan_listrik)->save($realPath.$filename);
            $pm->tagihan_listrik = $imgPath.$filename;
        }
        if($request->hasFile('tagihan_air')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('tagihan_air')->getClientOriginalExtension();
            Image::make($request->tagihan_air)->save($realPath.$filename);
            $pm->tagihan_air = $imgPath.$filename;
        }
        if($request->hasFile('tagihan_telp')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('tagihan_telp')->getClientOriginalExtension();
            Image::make($request->tagihan_telp)->save($realPath.$filename);
            $pm->tagihan_telp = $imgPath.$filename;
        }
        if($request->hasFile('surat_ket_kerja')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('surat_ket_kerja')->getClientOriginalExtension();
            Image::make($request->surat_ket_kerja)->save($realPath.$filename);
            $pm->surat_ket_kerja = $imgPath.$filename;
        }
        if($request->hasFile('bi_checking')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('bi_checking')->getClientOriginalExtension();
            Image::make($request->bi_checking)->save($realPath.$filename);
            $pm->bi_checking = $imgPath.$filename;
        }
        if($request->hasFile('akta_img')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('akta_img')->getClientOriginalExtension();
            Image::make($request->akta_img)->save($realPath.$filename);
            $pm->akta_img = $imgPath.$filename;
        }
        if($request->hasFile('ssp')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('ssp')->getClientOriginalExtension();
            Image::make($request->ssp)->save($realPath.$filename);
            $pm->ssp = $imgPath.$filename;
        }
        $pm->save();

        $imgPath = Config::get('gradana.perbankan_img_path');
        $realPath = public_path().'/'.$imgPath;
		 !file_exists($realPath) ? File::makeDirectory($realPath) : "";

        if($request->hasFile('rekening_koran')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('rekening_koran')->getClientOriginalExtension();
            Image::make($request->rekening_koran)->save($realPath.$filename);
            $pb->rekening_koran = $imgPath.$filename;
        }
        if($request->hasFile('rekening_koran2')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('rekening_koran2')->getClientOriginalExtension();
            Image::make($request->rekening_koran2)->save($realPath.$filename);
            $pb->rekening_koran2 = $imgPath.$filename;
        }
        $pb->save();

        $dataemail = [
                        'fullname'  => Sentinel::getuser()->fullname,
                        'email' => Sentinel::getuser()->email,
                    ];

        // Mail::send('emails.applicant.regis_application', $dataemail, function ($mail)
        // {
        //     $mail->to('toink90@gmail.com', 'TOINK');
        //     // $mail->bcc('tantowiachmad@gmail.com', 'Achmad Tantowi');
        //     //$mail->bcc('jr.pikong@gmail.com', 'Deny Pradia Utama');
        //     $mail->subject('Application Create');
        // });

        alert()->success('Pemohon telah dicreate');
        // return redirect('/admin/jobpemohon/create?backid='.$pm->id)->with('pem', $request->userid);
        return back();
    }

    public function profile()
    {
        dd("borrower profile");
    }
}
