<?php

namespace App\Http\Controllers;

use App\Blog;
use App\BlogCategory;
use App\BlogComment;
use App\Http\Requests;
use App\Http\Requests\BlogCommentRequest;
use App\Http\Requests\BlogRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Response;
use Sentinel;
use DB;


class BlogController extends JoshController
{
    private $tags;

    public function __construct()
    {
        parent::__construct();
        $this->tags = Blog::allTags();
    }

    /**
     * @return \Illuminate\View\View
     */
    public function IndexFrontend()
    {
        // Grab all the blogs
        $data['blogs'] = Blog::latest()->simplePaginate(5);
        $data['blogs']->setPath('blog');
        $data['tags'] = $this->tags;
        // Show the page
        return View('frontend.blog.blog', $data);
    }

    /**
     * @param string $slug
     * @return \Illuminate\View\View
     */
    public function getBlogFrontend($slug = '')
    {
        // $cat = BlogCategory::all();sata
        $recent = Blog::all()->take(5);

        if ($slug == '') {
            $blog = Blog::first();
        }
        try {
            $blog = Blog::findBySlugOrIdOrFail($slug);
            $blog->increment('views');
        } catch (ModelNotFoundException $e) {
            return Response::view('404', array(), 404);
        }
        // Show the page
        return View('frontend.blog.blogpost', compact(['blog', 'recent']));

    }

    /**
     * @param $tag
     * @return \Illuminate\View\View
     */
    public function getBlogTagFrontend($tag)
    {
        $blogs = Blog::withAnyTags($tag)->simplePaginate(5);
        $blogs->setPath('blog/' . $tag . '/tag');
        $tags = $this->tags;
        return View('blog', compact('blogs', 'tags'));
    }

    /**
     * @param BlogCommentRequest $request
     * @param Blog $blog
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeCommentFrontend(BlogCommentRequest $request, Blog $blog)
    {
        $blogcooment = new BlogComment($request->all());
        $blogcooment->blog_id = $blog->id;
        $blogcooment->save();

        return redirect('blog/' . $blog->slug);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Grab all the blogs
        $blogs = Blog::all();
        // Show the page
        return View('admin.blog.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $blogcategory = BlogCategory::lists('title', 'id');
        return view('admin.blog.create', compact('blogcategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    private function insertdata($blog, $request){
    	$statuspublised = $request->get('publishedstatus');
    	if ($statuspublised == 'Schedule') {
    		$blog->publishdate = DB::Raw('STR_TO_DATE(\''.$request->get('publishdate').'\', \'%m/%d/%Y\')');
    	} else if ($statuspublised == 'Now') {
    		$blog->publishdate = DB::Raw('now()');
    	} else {
    		$blog->publishdate = null;
    	}
    	return $blog;
    }
    public function store(BlogRequest $request)
    {
        $blog = new Blog($request->except('image', 'tags', 'publishdate'));

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName = '/uploads/blog/';
            $destinationPath = public_path() . $folderName;
            $picture = str_random(10) . '.' . $extension;
            while (file_exists($destinationPath . $picture)) {
            	$picture = str_random(10) . '.' . $extension;
            }
            $request->file('image')->move($destinationPath, $picture);
            $blog->image = $picture;
        }
        $blog->user_id = Sentinel::getUser()->id;
        $blog->slug = str_slug($request->title, '-');
        $blog = $this->insertdata($blog, $request);
        $blog->save();

        $blog->tag($request->tags);

        if ($blog->id) {
            return redirect('admin/blog')->with('success', trans('blog/message.success.create'));
        } else {
            return Redirect::route('admin/blog')->withInput()->with('error', trans('blog/message.error.create'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  Blog $blog
     * @return view
     */
    public function show(Blog $blog)
    {
        $comments = BlogComment::all();
        return view('admin.blog.show', compact('blog', 'comments', 'tags'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Blog $blog
     * @return view
     */
    public function edit(Blog $blog)
    {
        $blogcategory = BlogCategory::lists('title', 'id');
        return view('admin.blog.edit', compact('blog', 'blogcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(BlogRequest $request, Blog $blog)
    {
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName = '/uploads/blog/';
            $destinationPath = public_path() . $folderName;
            $picture = str_random(10) . '.' . $extension;
            while (file_exists($destinationPath . $picture)) {
            	$picture = str_random(10) . '.' . $extension;
            }
            $request->file('image')->move($destinationPath, $picture);
            if ($blog->image && file_exists($destinationPath.$blog->image)) {
            	unlink($destinationPath.$blog->image);
            }
            $blog->image = $picture;
        }

        $blog->retag($request['tags']);

        if ($request->imageremove == 1 && $blog->image) {
        	$folderName = '/uploads/blog/';
        	$destinationPath = public_path() . $folderName;
        	if (file_exists($destinationPath.$blog->image)) {
        		unlink($destinationPath.$blog->image);
        	}
        	$blog->image = null;
        }

        $blog = $this->insertdata($blog, $request);
        if ($blog->update($request->except('image', 'imageremove', '_method', 'tags', 'publishdate'))) {
            return redirect('admin/blog')->with('success', trans('blog/message.success.update'));
        } else {
            return Redirect::route('admin/blog')->withInput()->with('error', trans('blog/message.error.update'));
        }
    }

    /**
     * Remove blog.
     *
     * @param $website
     * @return Response
     */
    public function getModalDelete(Blog $blog)
    {
        $model = 'blog';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('delete/blog', ['id' => $blog->id]);
            return View('admin/layouts/modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('blog/message.error.delete', compact('id'));
            return View('admin/layouts/modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy(Blog $blog)
    {
    	if ($blog->image) {
    		$folderName = '/uploads/blog/';
    		$destinationPath = public_path() . $folderName;
    		if (file_exists($destinationPath.$blog->image)) {
    			unlink($destinationPath.$blog->image);
    		}
    	}
        if ($blog->delete()) {
            return redirect('admin/blog')->with('success', trans('blog/message.success.delete'));
        } else {
            return Redirect::route('admin/blog')->withInput()->with('error', trans('blog/message.error.delete'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function storecomment(BlogCommentRequest $request, Blog $blog)
    {
        $blogcooment = new BlogComment($request->all());
        $blogcooment->blog_id = $blog->id;
        $blogcooment->save();

        return redirect('admin/blog/' . $blog->id . '/show');
    }
}
