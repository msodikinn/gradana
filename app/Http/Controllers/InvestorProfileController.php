<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Investor;
use App\Investor_cat;
use Input;
use File;
use Sentinel;
use Redirect;
use Config;
use View;
use Image;

class InvestorProfileController extends JoshController
{
    public function getIndex()
    {
        $data['user'] = Sentinel::getUser();
        $data['countries'] = $this->countries;
        $data['cat'] = Investor_cat::all();
        $data['investor'] = Investor::where('userid', $data['user']->id)->first();
        // dd($data['investor']);
        return View::make('frontend.investor.investor_profile', $data);
    }

    public function postIndex(Request $request)
    {
        $data['user'] = Sentinel::getUser();

        $investor = Investor::where('userid', $data['user']->id)->first();
        $investor->catid = $request->catid;
        $investor->jenis_usaha = $request->jenis_usaha;
        $investor->email = $request->email;
        $investor->alamat = $request->alamat;
        $investor->telp = $request->telp;

        // save image
        $imgPath = Config::get('gradana.investor_img_path');
        $realPath = public_path().'/'.$imgPath;
        !file_exists($realPath) ? File::makeDirectory($realPath) : "";

        if($request->hasFile('img')) {

            $filename = date("ymd").str_random(6).'.'.Input::file('img')->getClientOriginalExtension();
            Image::make($request->img)->save($realPath.$filename);
            $investor->img = $imgPath.$filename;
            if (File::exists(public_path() .'/'. $imgPath . '/' . $investor->img)) {
                File::delete(public_path() .'/'. $imgPath . '/' . $investor->img);
            }
        }

        if ($investor->save()) {
            alert()->success('Lender has been Saved!');
            return redirect('investor-profile');
        }

        alert()->error('Failed saving lender!');
        Redirect::to('investor-profile');
    }
}
