<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Application;
use App\Survey;
use Input;
use Session;
use Redirect;
use Sentinel;

class SurveyController extends Controller
{
    public function index()
    {
        $data['sur'] = Survey::All();

        return view('admin.survey.index', $data);
    }

    public function create()
    {
        $data['app'] = Application::all();

        return view('admin.survey.create', $data);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        Survey::create($input);

        alert()->success('Transaksi Survey has been created.');
        return redirect('/admin/survey');
    }

    public function show($id)
    {
        $data['detail'] = Survey::find($id);
        $data['app'] = Application::all();

        return view('admin.survey.show', $data);
    }

    public function edit($id)
    {
        $data['detail'] = Survey::find($id);
        $data['app'] = Application::all();

        return view('admin.survey.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $detail = Survey::find($id);

        $input = $request->all();
        $detail->fill($input)->save();

        alert()->success('Transaksi Survey has been updated.');
        return back();//redirect('/admin/survey');
    }

    public function destroy($id)
    {
        Survey::find($id)->delete();

        alert()->success('Transaksi Survey has been deleted.');
        return redirect('/admin/survey');
    }
}
