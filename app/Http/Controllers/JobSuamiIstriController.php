<?php

namespace App\Http\Controllers;

use App\Application;
use App\Desa;
use App\Http\Requests;
use App\JobPemohon;
use App\JobSuamiIstri;
use App\Kecamatan;
use App\Keluarga;
use App\Kota;
use App\Pemohon;
use App\Penghasilan;
use App\Perbankan;
use App\Propinsi;
use App\SuamiIstri;
use App\User;
use Illuminate\Http\Request;
use Input;
use Redirect;
use Sentinel;
use Session;
use Image;
use File;
use Config;

class JobSuamiIstriController extends JoshController
{
    public function index()
    {
        $data['jobsi'] = app('App\JobSuamiIstri')->filterJobPasangan();

        return view('admin.job_suami_istri.index', $data);
    }

    public function create(Request $request)
    {
    	$suamiistri = null;
    	if ($request->backid) {
    		$suamiistri = SuamiIstri::find($request->backid);
    	}
    	if (!is_null(session('pem'))) {
        	$data['users'] = User::where('id', session('pem'))->first();
    	} else if (!is_null($suamiistri)) {
    		$data['users'] = $suamiistri->user()->first();
    	}
        $data['jenis'] = $this->jenisPekerjaan;
        $data['usaha'] = $this->jenisUsaha;
        $data['propinsi'] = array('' => '');
        foreach(Propinsi::all() as $row)
            $data['propinsi'][$row->id] = $row->name;

        if (!is_null($suamiistri)) {
        	$data['suamisitriid'] = $request->backid;
        	$data['jobpemohonid'] = JobPemohon::where('pemohonid', '=', $suamiistri->pemohonid)->first()->id;
        	$data['pemohonid'] = $suamiistri->pemohonid;
        } else {
        	$data['suamisitriid'] = '';
        	$data['jobpemohonid'] = '';
        	$data['pemohonid'] = '';
        }

        return view('admin.job_suami_istri.create', $data);
    }

    public function store(Request $request)
    {
        $input = $request->except('pem', 'surat_ket_kerja', 'suami_istri');
        $jobsuamiistri = JobSuamiIstri::create($input);
        $jobSuamiIstri = JobSuamiIstri::latest()->first();

        $penghasilan = Penghasilan::where('pemohonid', $request->pemohonid)->first();
        $penghasilan->suami_istri = $request->suami_istri;
        $penghasilan->save();

        $imgPath = Config::get('gradana.jobpemohon_img_path');
        $realPath = public_path().'/'.$imgPath;
        !file_exists($realPath) ? File::makeDirectory($realPath) : "";

        if($request->hasFile('surat_ket_kerja')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('surat_ket_kerja')->getClientOriginalExtension();
            Image::make($request->surat_ket_kerja)->save($realPath.$filename);
            $jobSuamiIstri->surat_ket_kerja = $imgPath.$filename;
            $jobSuamiIstri->save();
        }

        alert()->success('Job Suami/istri has been created.');
        return redirect('/admin/keluarga/create?backid='.$jobsuamiistri->id)->with('pem', $request->userid);
    }

    public function show($id)
    {
        $data['detail'] = JobSuamiIstri::find($id);

        return view('admin.job_suami_istri.show', $data);
    }

    public function edit($id)
    {
        $data['users'] = User::all();
        $data['jenis'] = $this->jenisPekerjaan;
        $jobsuamiistri = JobSuamiIstri::find($id);
        $data['detail'] = $jobsuamiistri;
        $data['detailPenghasilan'] = Penghasilan::where('pemohonid', $data['detail']->pemohonid)->first();
        $data['usaha'] = $this->jenisUsaha;
        $data['propinsi'] = array('' => '');
        foreach(Propinsi::all() as $row)
            $data['propinsi'][$row->id] = $row->name;

        $data['kota'] = array('' => '');
        foreach(Kota::where('province_id', $data['detail']->propinsi)->get() as $row)
            $data['kota'][$row->id] = $row->name;

        $data['kecamatan'] = array('' => '');
        foreach(Kecamatan::where('regency_id', $data['detail']->kota)->get() as $row)
            $data['kecamatan'][$row->id] = $row->name;

        $data['desa'] = array('' => '');
        foreach(Desa::where('district_id', $data['detail']->kecamatan)->get() as $row)
            $data['desa'][$row->id] = $row->name;

        $data['backid'] = $id;
        $pemohonid = $jobsuamiistri->pemohonid;
        $data['pemohonid'] = $pemohonid;
        $data['jobpemohonid'] = '';
        $data['suamisitriid'] = '';
        $data['jobsuamiistriid'] = $id;
        $data['keluargaid'] = '';
        $data['penghasilanid'] = '';
        $data['perbankanid'] = '';
        $data['applicationid'] = '';

        $jobpemohon = JobPemohon::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($jobpemohon)) {
        	$data['jobpemohonid'] = $jobpemohon->id;
        }
        $suamisitri = SuamiIstri::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($suamisitri)) {
        	$data['suamisitriid'] = $suamisitri->id;
        }
        $keluarga = Keluarga::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($keluarga)) {
        	$data['keluargaid'] = $keluarga->id;
        }
        $penghasilan = Penghasilan::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($penghasilan)) {
        	$data['penghasilanid'] = $penghasilan->id;
        }
        $perbankan = Perbankan::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($perbankan)) {
        	$data['perbankanid'] = $perbankan->id;
        }
        $application = Application::where('userid', '=', $data['detail']->userid)->get();
        $data['countApp'] = count($application);
        if (!is_null($application))
        {
            if($data['countApp'] == 1)
            {
               $data['applicationid'] = $application[0]->id;
            }
        }

        return view('admin.job_suami_istri.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $jobpasangan = JobSuamiIstri::find($id);

        $imgPath = Config::get('gradana.pemohon_img_path');
        $realPath = public_path().'/'.$imgPath;
        !file_exists($realPath) ? File::makeDirectory($realPath) : null ;

        if($request->hasFile('surat_ket_kerja')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('surat_ket_kerja')->getClientOriginalExtension();
            Image::make($request->surat_ket_kerja)->save($realPath.$filename);
            if (File::exists(public_path() .'/'. $jobpasangan->surat_ket_kerja)) {
                File::delete(public_path() .'/'. $jobpasangan->surat_ket_kerja);
            }

            $jobpasangan->surat_ket_kerja = $imgPath.$filename;
        }

        $input = $request->except('surat_ket_kerja', 'suami_istri');
        $jobpasangan->fill($input)->save();

        $penghasilan = Penghasilan::where('pemohonid', $jobpasangan->pemohonid)->first();
        $penghasilan->suami_istri = $request->suami_istri;
        $penghasilan->save();

        alert()->success('Job Suami/istri has been updated.');
        return back(); //redirect('/admin/jobsuamiistri');
    }

    public function destroy($id)
    {
        $inves = JobSuamiIstri::find($id)->delete();

        if (File::exists(public_path() .'/'. $inves->surat_ket_kerja)) {
            File::delete(public_path() .'/'. $inves->surat_ket_kerja);
        }

        alert()->success('Job Suami/istri has been deleted.');
        return redirect('/admin/jobsuamiistri');
    }
}
