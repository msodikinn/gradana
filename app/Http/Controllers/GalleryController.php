<?php namespace App\Http\Controllers;

use Lang;
use Redirect;
use View;
use Datatables;
use App\Property;
use App\Propertyimg;
use App\Gallery;
use App\Http\Requests\GalleryRequest;

class GalleryController extends JoshController
{
	public function index()
	{
		return View('admin/gallery/index');
	}
	public function create()
	{
        $property = Property::All();
		$propertyimgs = Propertyimg::where('gallery_id', '=', 0)->get();
		$imgshows = [];
		foreach ($propertyimgs as $propertyimg) {
			$arow['id'] = $propertyimg->id;
			$showfile = $propertyimg->img_name;
			if (!is_null($propertyimg->thumbnailsizelist) && !empty($propertyimg->thumbnailsizelist)) {
				$arraydata = json_decode($propertyimg->thumbnailsizelist);
				if (!empty($arraydata)) {
					$showfile = $arraydata[0];
				}
			}
			$arow['img_name'] = $showfile;
			$imgshows[] = (object) $arow;
		}
		return View('admin/gallery/create', [
				'imgshows' => $imgshows,
                'property' => $property
		]);
	}

	public function store(GalleryRequest $request)
    {
        $a = Property::find($request->galleryname)->nama;
        $nama = $a . " gallery";
		$gallery = new Gallery();
		$gallery = $this->insertdata($gallery, $request, $nama);

		return Redirect::route('gallery')->with('success', 'Gallery tesimpan');
	}

	public function data()
	{
		$galleries = Gallery::all();

		return Datatables::of($galleries)
		->edit_column('created_at','{!! format($created_at) !!}')
		->edit_column('propertyname', function($gallery) {
			if ($gallery->property) {
				if ($gallery->property->nama) {
					return $gallery->property->nama;
				} else {
					return '-';
				}
			}
			return '';
		})
		->add_column('actions',function($gallery) {
			$actions = '<a href='. route('update/gallery', $gallery->id) .'><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit gallery"></i></a>';
			if ($gallery->property) {
				$actions .= '<a href="#" data-toggle="modal" data-target="#gallerycannotdeleted" class="gallerycannotdeleted"><i class="livicon" data-name="warning-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="GAleri tidak bisa di hapus"></i></a>';
			} else {
				$actions .= '<a href="'.route('confirm-delete/gallery', $gallery->id).'" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="'.Lang::get('gallery/form.delete_gallery').'"></i></a>';
			}
			return $actions;
		})
		->make(true);
	}

	private function removepicture($gallery) {
		if ($gallery->properyimg) {
			Propertyimg::where('gallery_id', '=', $gallery->id)->update(['gallery_id' => 0]);
		}
	}
	private function insertdata($gallery, $request, $nama) {
        $gallery->propid = $request->galleryname;
		$gallery->galleryname = $nama;

		if (is_null($gallery->id)) {
			$gallery->save();
		}

		$this->removepicture($gallery);
		if ($request->photolist) {
			Propertyimg::wherein('id', explode(',', $request->photolist))->update(['gallery_id' => $gallery->id]);
		}
		return $gallery;
	}

    public function edit($galleryId = null)
    {
        try {
            $gallery = Gallery::with('properyimg')->find($galleryId);
            $imgshows = Propertyimg::where('gallery_id', '=', 0)->get();
            $imggalleries = $gallery->properyimg;
        } catch (GalleryNotFoundException $e) {
            return Redirect::route('gallery')->with('error', 'Gallery ini tidak ada');
        }
        return View('admin/gallery/edit', [
                'gallery' => $gallery,
                'imgshows' => $imgshows,
                'imggalleries' => $imggalleries,
        ]);
    }

	public function update($galleryId = null, GalleryRequest $request)
	{
		try {
			$gallery = Gallery::with('properyimg')->find($galleryId);
		} catch (GalleryNotFoundException $e) {
			return Rediret::route('gallery')->with('error', Lang::get('gallery/message.gallery_not_found', compact('id')));
		}
        // dd($request);
        $a = Property::find($request->id)->nama;
        $nama = $a . " gallery";
        $request->galleryname = $request->id;
		$gallery = $this->insertdata($gallery, $request, $nama);
		if ($gallery->save()) {
			return Redirect::route('gallery')->with('success', Lang::get('gallery/message.success.update'));
		} else {
			return Redirect::route('update/gallery', $galleryId)->with('error', Lang::get('gallery/message.error.update'));
		}
	}
	public function getModalDelete($galleryId = null)
	{
		$model = 'gallery';
		$confirm_route = $error = null;
		try {
			$gallery = Gallery::find($galleryId);
			$confirm_route = route('delete/gallery', ['id' => $gallery->id]);
			return View('admin/layouts/modal_confirmation', compact('error', 'model', 'confirm_route'));
		} catch (GalleryNotFoundException $e) {
			$error = Lang::get('admin/gallery/message.gallery_not_found', compact('id'));
			return View('admin/layouts/modal_confirmation', compact('error', 'model', 'confirm_route'));
		}
	}
	public function destroy($galleryId = null)
	{
		try {
			$gallery = Gallery::with('properyimg')->find($galleryId);
			$this->removepicture($gallery);
			$gallery->delete();
			return Redirect::route('gallery')->with('success', Lang::get('gallery/message.success.delete'));
		} catch (GalleryNotFoundException $e) {
			return Redirect::route('gallery')->with('error', Lang::get('gallery/message.gallery_not_found', compact('id')));
		}
	}
}
