<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Propinsi;
use App\Kota;
use App\Kecamatan;
use App\Desa;
use View;
use Input;

class PropinsiController extends Controller
{
    public function getIndex()
    {
        $propinsi = array('' => '');
        foreach(Propinsi::all() as $row)
            $propinsi[$row->id] = $row->propinsi;

        return View::make('index', array(
            'propinsi' => $propinsi
        ));
    }

    public function postData()
    {
        switch(Input::get('type')):
            case 'kota_ktp':
                $return = '';
                $return = '<option value="">Silahkan pilih Kabupaten / Kota</option>';
                foreach(Kota::where('province_id', Input::get('id'))->get() as $row)
                    $return .= "<option value='$row->id'>$row->name</option>";
                return $return;
                break;
            case 'kecamatan_ktp':
                $return = '';
                $return = '<option value="">Silahkan pilih Kecamatan</option>';
                foreach(Kecamatan::where('regency_id', Input::get('id'))->get() as $row)
                    $return .= "<option value='$row->id'>$row->name</option>";
                return $return;
                break;
            case 'kelurahan_ktp':
                $return = '';
                $return = '<option value="">Silahkan Pilih Desa / Kelurahan</option>';
                foreach(Desa::where('district_id', Input::get('id'))->get() as $row)
                    $return .= "<option value='$row->id'>$row->name</option>";
                return $return;
                break;
            case 'kota':
                $return = '';
                $return = '<option value="">Silahkan pilih Kabupaten / Kota</option>';
                foreach(Kota::where('province_id', Input::get('id'))->get() as $row)
                    $return .= "<option value='$row->id'>$row->name</option>";
                return $return;
                break;
            case 'kecamatan':
                $return = '';
                $return = '<option value="">Silahkan pilih Kecamatan</option>';
                foreach(Kecamatan::where('regency_id', Input::get('id'))->get() as $row)
                    $return .= "<option value='$row->id'>$row->name</option>";
                return $return;
                break;
            case 'kelurahan':
                $return = '';
                $return = '<option value="">Silahkan Pilih Desa / Kelurahan</option>';
                foreach(Desa::where('district_id', Input::get('id'))->get() as $row)
                    $return .= "<option value='$row->id'>$row->name</option>";
                return $return;
                break;
        endswitch;
    }
}
