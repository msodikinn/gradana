<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Investor;
use App\InvestorAlokasi;
use Input;
use Mail;
use Sentinel;
use Redirect;
use Config;
use PDF;

class InvestorAlokasiController extends JoshController
{
    public function index()
    {
        if (Sentinel::getUser()->inRole('investor'))
        {
            $inves = Investor::where('userid', Sentinel::getuser()->id)->first();
            $data['alokasi'] = $inves->investoralokasis;
            return view('admin.investoralokasi.index', $data);
        }elseif(Sentinel::getUser()->inRole('admin')){
            $data['alokasi'] = InvestorAlokasi::All();
            return view('admin.investoralokasi.index', $data);
        }else{
            alert()->error('Anda tidak memiliki otorisasi');
            return Redirect::route('dashboard');
        }
    }

    public function create()
    {
        if (permission() == "Investor") {
            $data['inves'] = Investor::where('userid', Sentinel::getuser()->id)->first();
        }elseif(permission() == "Admin"){
            alert()->error("Admin tidak dapat Create Alokasi Dana Lender");
            return back();
            // $data['inves'] = Investor::All();
        }else{
            alert()->error('Anda tidak memiliki otorisasi');
            return Redirect::route('dashboard');
        }

        $data['jmlAlokasi'] = $this->jml_alokasi;
        $data['penempatanDana'] = $this->penempatan_dana;
        $data['tenor'] = $this->tenor;

        return view('admin.investoralokasi.create', $data);
    }

    public function store(Request $request)
    {
        $input = $request->All();
        InvestorAlokasi::create($input);

        $data['alokasi'] = InvestorAlokasi::latest()->first();

        $data['inves'] = $data['alokasi']->investor;
        // $data['namauser'] = $data['inves']->user->fullname;

        $pdf = PDF::loadView('frontend.investor.investorpdf', $data)->save(public_path().'/uploads/pdf/LenderTermsInfo.pdf');

        Mail::send('emails.alokasidana', ['alokasi' => $data['alokasi']], function ($m) use ($data){
            $m->to($data['alokasi']->investor->email, $data['alokasi']->investor->nama)
              ->subject('Alokasi Dana Information')
              ->attach(public_path().'/uploads/pdf/LenderTermsInfo.pdf');
        });

        alert()->success('Alokasi dana has been created.');
        return redirect('/admin/investor/alokasi');
    }

    public function show($id)
    {
        $data['detail'] = InvestorAlokasi::find($id);
        $data['inves'] = $data['detail']->investor->first();
        $data['payment'] = $data['detail']->investorPayments;
        // dd($data['payment']);
        return view('admin.investoralokasi.show', $data);
    }

    public function edit($id)
    {
        $data['detail'] = InvestorAlokasi::find($id);
        $data['inves'] = $data['detail']->investor->first();
        $data['jmlAlokasi'] = $this->jml_alokasi;
        $data['penempatanDana'] = $this->penempatan_dana;
        $data['tenor'] = $this->tenor;

        return view('admin.investoralokasi.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $alokasi = InvestorAlokasi::find($id);
        $input = $request->All();
        $alokasi->fill($input)->save();

        alert()->success('Alokasi dana has been updated.');
        return redirect('/admin/investor/alokasi/detail/'.$alokasi->id);
    }

    public function destroy($id)
    {
        $alokasi = InvestorAlokasi::find($id)->delete();

        alert()->success('Alokasi dana has been deleted.');
        return redirect('/admin/investor/alokasi');
    }
}
