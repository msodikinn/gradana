<?php

namespace App\Http\Controllers;

use Activation;
use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\User;
use App\Pemohon;
use App\JobPemohon;
use App\SuamiIstri;
use App\JobSuamiIstri;
use App\Keluarga;
use App\Penghasilan;
use App\Perbankan;
use App\Investor;
use App\InvestorAlokasi;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use File;
use Hash;
use Illuminate\Http\Request;
use Lang;
use Mail;
use Config;
use Redirect;
use Reminder;
use Sentinel;
use URL;
use View;
use Input;
use Image;
use Session;
use App;
use Validator;


class FrontEndController extends JoshController
{

    private $user_activation = true;

    public function getLogin()
    {
        // Is the user logged in?
        if (Sentinel::check()) {
            return Redirect::to('my-account');
        }

        // Show the login page
        return View::make('frontend.login');
        //return back();
    }

    public function postLogin(Request $request)
    {
        try {
            // Try to log the user in
            if (Sentinel::authenticate($request->only('email', 'password'))) {
                return Redirect::route("my-account")->with('success', Lang::get('auth/message.login.success'));
                return back();
            } else {
                alert()->error('Login failed');
                return Redirect::to('login');
                //return Redirect::back()->withInput()->withErrors($validator);
            }

        } catch (UserNotFoundException $e) {
            $this->messageBag->add('email', Lang::get('auth/message.account_not_found'));
        } catch (NotActivatedException $e) {
            $this->messageBag->add('email', Lang::get('auth/message.account_not_activated'));
        } catch (UserSuspendedException $e) {
            $this->messageBag->add('email', Lang::get('auth/message.account_suspended'));
        } catch (UserBannedException $e) {
            $this->messageBag->add('email', Lang::get('auth/message.account_banned'));
        } catch (ThrottlingException $e) {
            $delay = $e->getDelay();
            $this->messageBag->add('email', Lang::get('auth/message.account_suspended', compact('delay')));
        }

        // Ooops.. something went wrong
        return Redirect::back();
    }

    public function myAccount(User $user)
    {
        $data['user'] = Sentinel::getUser();
        // dd($data['user']);
        switch (Sentinel::getuser()->status) {
            case "Applicant":
                $data['countries'] = $this->countries;
                $data['detailPemohon'] = Pemohon::where('userid', $data['user']->id)->first();
                $data['detailJobPemohon'] = JobPemohon::where('userid', $data['user']->id)->first();
                $data['detailPasangan'] = SuamiIstri::where('userid', $data['user']->id)->first();
                $data['detailJobPasangan'] = JobSuamiIstri::where('userid', $data['user']->id)->first();
                $data['detailKeluarga'] = Keluarga::where('userid', $data['user']->id)->first();
                $data['detailPenghasilan'] = Penghasilan::where('userid', $data['user']->id)->first();
                $data['detailPerbankan'] = Perbankan::where('userid', $data['user']->id)->first();
                if(!empty($data['detailPerbankan'])){
                    $data['detailPerbankanChild'] = $data['detailPerbankan']->perbankan_childs;
                }
                // dd($data['detailJobPemohon']);
                return View::make('frontend.applicant.applicant_profile', $data);
            case "Investor":
                $data['investor'] = Investor::where('userid', $data['user']->id)->first();
				//dd($data['investor']);
                $data['alokasi'] = $data['investor']->investoralokasis->take(10);
                return View::make('frontend.investor.investor_profile', $data);
            case "Admin":
                // return Redirect::to('/admin');
                return Redirect::to('/admin');
            default:
                return back();
        }
        // return View::make('frontend.applicant.applicant_edit', $data);
    }

    public function edit()
    {
        $data['user'] = Sentinel::getuser();
        $data['pemohon'] = $data['user']->pemohon;
        $data['countries'] = $this->countries;
        $data['investor'] = $data['user']->applications;

        // dd($data['countries']);
        return view('frontend.applicant.applicant_edit', $data);
    }

    public function update(Request $request, User $user)
    {
        // dd('wer');
        // $rules = array(
        //     'password'         => 'required',
        //     'password_confirm' => 'required|same:password'
        // );

        // // do the validation ----------------------------------
        // // validate against the inputs from our form
        // $validator = Validator::make(Input::all(), $rules);

        // // check if the validator failed -----------------------
        // if ($validator->fails()) {

        //     // get the error messages from the validator
        //     $messages = $validator->messages();

        //     // redirect our user back to the form with the errors from the validator
        //     return Redirect::to('ducks')->withErrors($validator);
        // }


        $user = Sentinel::getUser();

        $user->fullname = $request->fullname;
        $user->email = $request->email;
        $user->dob = $request->dob;
        $user->bio = $request->bio;
        $user->gender = $request->gender;
        $user->country = $request->country;
        $user->state = $request->state;
        $user->city = $request->city;
        $user->address = $request->address;
        $user->postal = $request->postal;
        $user->kecamatan = $request->kecamatan;
        $user->kelurahan = $request->kelurahan;

        if ($password = $request->password) {
            $user->password = Hash::make($password);
        }

        $imgPath = Config::get('gradana.user_img_path');
	
		$realPath = public_path().'/'.$imgPath;
        !file_exists($realPath) ? File::makeDirectory($realPath) : null ;

        if($request->hasFile('pic')) {
            $filename = str_random(10).'.'.Input::file('pic')->getClientOriginalExtension();
            Image::make(Input::file('pic'))->save($realPath.$filename);
            if (File::exists(public_path() .'/'. $user->pic)) {
                File::delete(public_path() .'/'. $user->pic);
            }
            $user->pic = $imgPath.$filename;
        }

        if ($user->save()) {
            alert()->success('User has been Saved!');
            return Redirect::to('my-account');
        }

        alert()->error('Failed saving user!');
        return Redirect::to('my-account');
    }

    public function getRegister()
    {
        $data['negara'] = $this->countries;
        return view('frontend.register', $data);
    }

    public function postRegister(UserRequest $request)
    {
        $activate = $this->user_activation; //make it false if you don't want to activate user automatically it is declared above as global variable

        try {
            // Register the user
            $user = Sentinel::register($request->only(['fullname'
                                                        , 'email'
                                                        , 'password'
                                                        // , 'dob'
                                                        // , 'country'
                                                        // , 'address'
                                                        // , 'state'
                                                        // , 'city'
                                                        // , 'postal'
                                                        , 'status'
                                                    ]), $activate);

            //add user to 'User' group
            $role = Sentinel::findRoleByName($request->status);
            $role->users()->attach($user);

            //save avatar
            $imgPath = Config::get('gradana.user_img_path');
            $realPath = public_path().'/'.$imgPath;
            //print_r(public_path());
            !file_exists($realPath) ? File::makeDirectory($realPath) : null ;

            if($request->hasFile('pic')) {
                $filename = str_random(10).'.'.Input::file('pic')->getClientOriginalExtension();
                Image::make(Input::file('pic'))->save($realPath.$filename);
                if (File::exists(public_path() .'/'. $user->pic)) {
                    File::delete(public_path() .'/'. $user->pic);
                }

                $user->pic = $imgPath.$filename;
            }
            $user->save();

            // login user automatically
            Sentinel::login($user, false);
            $filepdf='';

            // print_r($filepdf);

            /*$dataemail = [
                        'register_as'  => Sentinel::getuser()->status,
                        'email' => Sentinel::getuser()->email,
                        ];

            Mail::send('emails.applicant.regis_application', $dataemail, function ($mail)
            {
                $emailuser  = Sentinel::getuser()->email;
                $fullname   = Sentinel::getuser()->fullname;
                if(Sentinel::getuser()->status == 'Applicant'){
                    $filepdf = 'Terms-and-Conditions-for-Investee.pdf';
                } else {
                    $filepdf = 'Terms-and-Conditions-for-Investor.pdf';
                }
                $mail->to($emailuser, $fullname);
                // $mail->attachData($pdf, 'invoice.pdf');
                $mail->attach(public_path().'/uploads/pdf/'.$filepdf);

                // $mail->bcc('tantowiachmad@gmail.com', 'Achmad Tantowi');
                //$mail->bcc('jr.pikong@gmail.com', 'Deny Pradia Utama');
                $mail->subject('New SignUp');
            });
		*/
            // Redirect to the home page with success menu
            alert()->success('Register Successfully, redirected to account page...');
            return Redirect::route("my-account");

        } catch (UserExistsException $e) {
            $this->messageBag->add('email', Lang::get('auth/message.account_already_exists'));
        }

        // Ooops.. something went wrong
        alert()->error('Ooops.. something went wrong');
        return Redirect::back()->withInput()->withErrors($this->messageBag);
    }

    public function getActivate($userId, $activationCode)
    {
        // Is the user logged in?
        if (Sentinel::check()) {
            return Redirect::route('my-account');
        }

        $user = Sentinel::findById($userId);

        if (Activation::complete($user, $activationCode)) {
            // Activation was successfull
            return Redirect::route('login')->with('success', Lang::get('auth/message.activate.success'));
        } else {
            // Activation not found or not completed.
            $error = Lang::get('auth/message.activate.error');
            return Redirect::route('login')->with('error', $error);
        }
    }

    public function getForgotPassword()
    {
        // Show the page
        if(Sentinel::check()) {
          return Redirect::route('my-account');
        }

        return View::make('frontend.forgotpwd');
    }

    public function postForgotPassword(Request $request)
    {
        try {
            // Get the user password recovery code
            //$user = Sentinel::FindByLogin($request->get('email'));
            $user = Sentinel::findByCredentials(['email' => $request->email]);
            if (!$user) {
                return Redirect::route('forgot-password')->with('error', Lang::get('auth/message.account_email_not_found'));
            }

            $activation = Activation::completed($user);
            // if (!$activation) {
            //     return Redirect::route('forgot-password')->with('error', Lang::get('auth/message.account_not_activated'));
            // }

            $reminder = Reminder::exists($user) ?: Reminder::create($user);
            // Data to be used on the email view
            $data = array(
                'user' => $user,
                //'forgotPasswordUrl' => URL::route('forgot-password-confirm', $user->getResetPasswordCode()),
                'forgotPasswordUrl' => URL::route('forgot-password-confirm', [$user->id, $reminder->code]),
            );

            // Send the activation code through email
            Mail::send('emails.forgot-password', $data, function ($m) use ($user) {
                $m->to($user->email, $user->fullname);
                $m->subject('Account Password Recovery');
            });
        } catch (UserNotFoundException $e) {
            // Even though the email was not found, we will pretend
            // we have sent the password reset code through email,
            // this is a security measure against hackers.
        }

        //  Redirect to the forgot password
        return Redirect::to(URL::previous())->with('success', Lang::get('auth/message.forgot-password.success'));
    }

    public function getForgotPasswordConfirm($userId, $passwordResetCode = null)
    {
        if (!$user = Sentinel::findById($userId)) {
            // Redirect to the forgot password page
            return Redirect::route('forgot-password')->with('error', Lang::get('auth/message.account_not_found'));
        }

        if($reminder = Reminder::exists($user))
        {
            if($passwordResetCode == $reminder->code)
            {
                return View::make('frontend.forgotpwd-confirm', compact(['userId', 'passwordResetCode']));
            }
            else{
                return 'code does not match';
            }
        }
        else
        {
            return 'does not exists';
        }


        // Show the page
     //   return View::make('frontend.forgotpwd-confirm', compact(['userId', 'passwordResetCode']));
    }

    public function postForgotPasswordConfirm(Request $request, $userId, $passwordResetCode = null)
    {

        $user = Sentinel::findById($userId);
        if (!$reminder = Reminder::complete($user, $passwordResetCode, $request->get('password'))) {
            // Ooops.. something went wrong
            return Redirect::route('login')->with('error', Lang::get('auth/message.forgot-password-confirm.error'));
        }

        // Password successfully reseted
        return Redirect::route('login')->with('success', Lang::get('auth/message.forgot-password-confirm.success'));
    }

    public function postContact(Request $request)
    {

        // Data to be used on the email view
        $data = array(
            'contact-name' => $request->get('contact-name'),
            'contact-email' => $request->get('contact-email'),
            'contact-msg' => $request->get('contact-msg'),
        );

        // Send the activation code through email
        Mail::send('emails.contact', compact('data'), function ($m) use ($data) {
            $m->from($data['contact-email'], $data['contact-name']);
            $m->to('email@domain.com', @trans('general.site_name'));
            $m->subject('Received a mail from ' . $data['contact-name']);

        });

        //Redirect to contact page
        return Redirect::to('contact')->with('success', Lang::get('auth/message.contact.success'));
    }

    public function getLogout()
    {
        // Log the user out
        Sentinel::logout();

        // Redirect to the users page
        alert()->success('You have been logout');
        return Redirect::to('/');
    }
}
