<?php

namespace App\Http\Controllers;

use App\Application;
use App\Desa;
use App\Http\Requests;
use App\JobPemohon;
use App\JobSuamiIstri;
use App\Kecamatan;
use App\Keluarga;
use App\Kota;
use App\Pemohon;
use App\Penghasilan;
use App\Perbankan;
use App\Propinsi;
use App\SuamiIstri;
use App\User;
use Illuminate\Http\Request;
use Input;
use Image;
use Redirect;
use Sentinel;
use File;
use Config;

class JobPemohonController extends JoshController
{
    public function index()
    {
        $data['jobp'] = app('App\JobPemohon')->filterJobPemohon();

        return view('admin.job_pemohon.index', $data);
    }

    public function create(Request $request)
    {
    	if (!is_null(session('pem'))) {
        	$data['users'] = User::where('id', session('pem'))->first();
    	} else if ($request->backid) {
    		$data['users'] =  Pemohon::find($request->backid)->users()->first();
    	}

        $data['jenis'] = $this->jenisPekerjaan;
        $data['usaha'] = $this->jenisUsaha;
        $data['propinsi'] = array('' => '');
        foreach(Propinsi::all() as $row)
            $data['propinsi'][$row->id] = $row->name;

        if ($request->backid) {
        	$data['pemohonid'] = $request->backid;
        } else {
        	$data['pemohonid'] = '';
        }

        return view('admin.job_pemohon.create', $data);
    }

    public function store(Request $request)
    {
        $input = $request->except('pem', 'surat_ket_kerja', 'pemohon');
        JobPemohon::create($input);
        $jobPemohon = JobPemohon::latest()->first();

        $penghasilan = new Penghasilan();
        $penghasilan->pemohon = $request->pemohon;
        $penghasilan->pemohonid = $request->pemohonid;
        $penghasilan->userid = $request->userid;
        $penghasilan->save();

        $imgPath = Config::get('gradana.jobpemohon_img_path');
        $realPath = public_path().'/'.$imgPath;
        !file_exists($realPath) ? File::makeDirectory($realPath) : "";

        if($request->hasFile('surat_ket_kerja')) {
            // dd(Input::file('surat_ket_kerja'));
            $filename = date("ymd").str_random(6).'.'.Input::file('surat_ket_kerja')->getClientOriginalExtension();
            Image::make($request->surat_ket_kerja)->save($realPath.$filename);
            $jobPemohon->surat_ket_kerja = $imgPath.$filename;
            $jobPemohon->save();
        }
        // dd($request->hasFile('surat_ket_kerja'));
        alert()->success('Job Pemohon has been created.');
        if ($jobPemohon->pemohon->status_kawin == "Lajang") {
            return redirect('/admin/keluarga/create?backid='.$jobPemohon->id)->with('pem', $request->userid);
        }else{
            return redirect('/admin/suamiistri/create?backid='.$jobPemohon->id)->with('pem', $request->userid);
        }
    }

    public function show($id)
    {
        $data['detail'] = JobPemohon::find($id);

        return view('admin.job_pemohon.show', $data);
    }

    public function edit($id)
    {
        $data['users'] = User::all();
        $data['jenis'] = $this->jenisPekerjaan;
        $jobpemohon = JobPemohon::find($id);
        $data['detail'] = $jobpemohon;
        $data['detailPenghasilan'] = Penghasilan::where('pemohonid', $data['detail']->pemohonid)->first();
        $data['usaha'] = $this->jenisUsaha;
        $data['propinsi'] = array('' => '');
        foreach(Propinsi::all() as $row)
            $data['propinsi'][$row->id] = $row->name;

        $data['kota'] = array('' => '');
        foreach(Kota::where('province_id', $data['detail']->propinsi)->get() as $row)
            $data['kota'][$row->id] = $row->name;

        $data['kecamatan'] = array('' => '');
        foreach(Kecamatan::where('regency_id', $data['detail']->kota)->get() as $row)
            $data['kecamatan'][$row->id] = $row->name;

        $data['desa'] = array('' => '');
        foreach(Desa::where('district_id', $data['detail']->kecamatan)->get() as $row)
            $data['desa'][$row->id] = $row->name;

        $data['backid'] = $id;
        $pemohonid = $jobpemohon->pemohonid;
        $data['pemohonid'] = $pemohonid;
        $data['jobpemohonid'] = $id;
        $data['suamisitriid'] = '';
        $data['jobsuamiistriid'] = '';
        $data['keluargaid'] = '';
        $data['penghasilanid'] = '';
        $data['perbankanid'] = '';
        $data['applicationid'] = '';

        $suamisitri = SuamiIstri::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($suamisitri)) {
        	$data['suamisitriid'] = $suamisitri->id;
        }
        $jobsuamiistri = JobSuamiIstri::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($jobsuamiistri)) {
        	$data['jobsuamiistriid'] = $jobsuamiistri->id;
        }
        $keluarga = Keluarga::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($keluarga)) {
        	$data['keluargaid'] = $keluarga->id;
        }
        $penghasilan = Penghasilan::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($penghasilan)) {
        	$data['penghasilanid'] = $penghasilan->id;
        }
        $perbankan = Perbankan::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($perbankan)) {
        	$data['perbankanid'] = $perbankan->id;
        }
        $application = Application::where('userid', '=', $data['detail']->userid)->get();
        $data['countApp'] = count($application);
        if (!is_null($application))
        {
            if($data['countApp'] == 1)
            {
               $data['applicationid'] = $application[0]->id;
            }
        }

        return view('admin.job_pemohon.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $job = JobPemohon::find($id);

        $imgPath = Config::get('gradana.pemohon_img_path');
        $realPath = public_path().'/'.$imgPath;
        !file_exists($realPath) ? File::makeDirectory($realPath) : null ;

        if($request->hasFile('surat_ket_kerja')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('surat_ket_kerja')->getClientOriginalExtension();
            Image::make($request->surat_ket_kerja)->save($realPath.$filename);
            if (File::exists(public_path() .'/'. $job->surat_ket_kerja)) {
                File::delete(public_path() .'/'. $job->surat_ket_kerja);
            }

            $job->surat_ket_kerja = $imgPath.$filename;
        }

        $input = $request->except('surat_ket_kerja', 'pemohon');
        $job->fill($input)->save();

        $penghasilan = Penghasilan::where('pemohonid', $job->pemohonid)->first();
        $penghasilan->pemohon = $request->pemohon;
        $penghasilan->save();

        alert()->success('Job Pemohon has been updated.');
        return back(); //redirect('/admin/jobpemohon');
    }

    public function destroy($id)
    {
        $job = JobPemohon::find($id)->delete();

        if (File::exists(public_path() .'/'. $job->surat_ket_kerja)) {
            File::delete(public_path() .'/'. $job->surat_ket_kerja);
        }

        alert()->success('Job Pemohon has been deleted.');
        return redirect('/admin/jobpemohon');
    }
}
