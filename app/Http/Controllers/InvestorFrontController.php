<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Pemohon;
use App\JobPemohon;
use App\SuamiIstri;
use App\JobSuamiIstri;
use App\Keluarga;
use App\Penghasilan;
use App\Perbankan;
use App\Perbankan_child;
use App\Investor;
use App\Investor_cat;
use App\InvestorAlokasi;
use Sentinel;
use Config;
use Input;
use Image;
use App\Application;
use App\Developer;
use App\Property;
use Carbon\Carbon;
use Mail;
use PDF;

class InvestorFrontController extends JoshController
{
    public function investor()
    {
        return view('frontend.investor.investor');
    }

    // public function profile($id)
    // {
    //     $data['user'] = User::find($id);
    //     $data['investor'] = Investor::where('userid', $id)->get();
    //     $data['alokasi'] => $data['investor']->investoralokasis;
    //     $data['cat'] = Investor_cat::All();

    //     return view('frontend.investor.investor_profile', $data);
    // }

    public function investorform()
    {
        if (Sentinel::check()){
            $inves = Investor::where('userid', Sentinel::getuser()->id)->first();
            if(!is_null($inves))
            {
                alert()->error('Anda telah mengisi profile Pemberi Pinjaman');
                return redirect('/my-account');
            }else{
                return view('frontend.investor.investorform', $data);
            }

            $data['user'] = Sentinel::getuser();
            $data['cat'] = Investor_cat::all();
            $data['jmlAlokasi'] = $this->jml_alokasi;
            $data['penempatanDana'] = $this->penempatan_dana;
            $data['tenor'] = $this->tenor;

            if (Sentinel::getuser()->status == "Applicant") {
                alert()->error('Anda sudah mendaftar sebagai Peminjam');
                return back();
            }elseif(Sentinel::getuser()->status == "Investor"){
            }
        }else{
            alert()->error('Silahkan login terlebih dahulu');
            return view('frontend.login');
        }
    }

    public function investorstore(Request $request)
    {
        $inputinves = $request->all();
        Investor::create($inputinves);

        $data['in'] = Investor::latest()->first();
        $data['namauser'] = $data['in']->user->fullname;

        $data['alokasi'] = InvestorAlokasi::latest()->first();

        $data['inves'] = $data['alokasi']->investor;
        // $data['namauser'] = $data['inves']->user->fullname;

        $pdf = PDF::loadView('frontend.investor.investorpdf', $data)->save(public_path().'/uploads/pdf/LenderTermsInfo.pdf');

        Mail::send('emails.alokasidana', ['alokasi' => $data['alokasi']], function ($m) use ($data){
            $m->to($data['alokasi']->investor->email, $data['alokasi']->investor->nama)
              ->subject('Alokasi Dana Information')
              ->attach(public_path().'/uploads/pdf/LenderTermsInfo.pdf');
        });

        // save image
        $imgPath = Config::get('gradana.investor_img_path');
        $realPath = public_path().'/'.$imgPath;
        !file_exists($realPath) ? File::makeDirectory($realPath) : "";

        if($request->hasFile('img')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('img')->getClientOriginalExtension();
            Image::make($request->img)->save($realPath.$filename);
            $in->img = $imgPath.$filename;
        }
        $in->save();

        alert()->success('Lender telah dicreate');
        return redirect('/my-account');
    }
}
