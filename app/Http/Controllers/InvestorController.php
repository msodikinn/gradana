<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\Payment;
use App\User;
use App\Application;
use App\Investor;
use App\InvestorAlokasi;
use App\Investor_cat;
use App\Assesment;
use Input;
use Sentinel;
use Redirect;
use Config;
use File;
use Image;

class InvestorController extends JoshController
{
    public function index()
    {
        if (Sentinel::getUser()->inRole('investor')){
            $data['inves'] = Investor::where('userid', Sentinel::getuser()->id)->get();
            return view('admin.investor.index', $data);
        }elseif(Sentinel::getUser()->inRole('admin')){
            $data['inves'] = Investor::All();
            return view('admin.investor.index', $data);
			
        }else{
            alert()->error('Anda tidak memiliki otorisasi');
            return Redirect::route('dashboard');
        }
    }

    public function create()
    {
        $data['userid'] = Sentinel::getUser()->fullname;
        $data['cat'] = Investor_cat::All();
        $data['jmlAlokasi'] = $this->jml_alokasi;
        $data['penempatanDana'] = $this->penempatan_dana;
        $data['tenor'] = $this->tenor;

        return view('admin.investor.create', $data);
    }

    public function store(Request $request)
    {
        $input = $request->except(['img']);
        Investor::create($input);
        $inves = Investor::latest()->first();
        $inves->userid = $request->userid;
        $inves->save();

        $imgPath = Config::get('gradana.investor_img_path');
        $realPath = public_path().'/'.$imgPath;
        !file_exists($realPath) ? File::makeDirectory($realPath) : "";

        if($request->hasFile('img')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('img')->getClientOriginalExtension();
            Image::make($request->img)->save($realPath.$filename);
            $inves->img = $imgPath.$filename;
            $inves->save();
        }

        alert()->success('Lender has been created.');
        return redirect('/admin/investor');
    }

    public function show($id)
    {
        $data['detail'] = Investor::find($id);
        $data['alokasi'] = $data['detail']->investoralokasis;
        // dd($data['alokasi']);

        return view('admin.investor.show', $data);
    }

    public function edit($id)
    {
        $data['cats'] = Investor_cat::All();
        $data['detail'] = Investor::find($id);
        $data['jmlAlokasi'] = $this->jml_alokasi;
        $data['penempatanDana'] = $this->penempatan_dana;
        $data['tenor'] = $this->tenor;

        return view('admin.investor.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $inves = Investor::find($id);

        $imgPath = Config::get('gradana.investor_img_path');
        $realPath = public_path().'/'.$imgPath;
        !file_exists($realPath) ? File::makeDirectory($realPath) : '' ;

        if($request->hasFile('img')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('img')->getClientOriginalExtension();
            Image::make(Input::file('img'))->save($realPath.$filename);
            if (File::exists(public_path() .'/'. $inves->img)) {
                File::delete(public_path() .'/'. $inves->img);
            }

            $inves->img = $imgPath.$filename;
        }

        $input = $request->except(['img']);
        $inves->fill($input)->save();

        alert()->success('Lender has been updated.');
        return redirect('/admin/investor/detail/'.$inves->id);
    }

    public function destroy($id)
    {
        $inves = Investor::find($id)->delete();

        alert()->success('Lender has been deleted.');
        return redirect('/admin/investor');
    }

    public function avaborrow()
    {
        $data['app'] = Application::where(['investorid' => 0])->get();
        $data['durasi'] = Payment::getTotal($data['app']);
        $data['alokasi'] = InvestorAlokasi::all();

        return view('admin.investor.availableborrower', $data);
    }

    public function borrowDetail($appid)
    {
        $data['app'] = Application::where('id', $appid)->first();
        $data['user'] = $data['app']->user;
        $data['asses'] = $data['app']->assesment;

        if($data['asses'] == null)
        {
            alert()->error('Assesment not available');
            return back();
        }

        $data['property'] = $data['app']->property;
        $data['rating'] = $data['app']->assesment->scoring;
        $gallery = $data['property']->galleries->first();
        if($gallery == null){
            $data['pics'] = "";
        }else{
            $data['pics'] = $gallery->properyimg()->get();
        }
        $data['total'] = Payment::getSingle($data['app']->periode, $data['app']->jangka_waktu);

        return view('admin.investor.borrowerdetail', $data);
    }
}
