<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Application;
use App\User;
use App\InvestorPayment;
use App\InvestorAlokasi;
use App\Investor;
use Input;
use Session;
use Redirect;
use Sentinel;

class InvestorPaymentController extends JoshController
{
    public function index()
    {
        $user = Sentinel::getuser();
        $data['invespay'] = InvestorPayment::All();
        $data['inves'] = Investor::All();

        return view('admin.investorpayment.index', $data);
    }

    public function create($alokasiid, $appid)
    {
        if (Sentinel::getuser()->status == "Admin") {
            alert()->error('Admin not allowed to create payment.');
            return back();
        }

        $data['app'] = Application::find($appid);
        $data['alokasi'] = InvestorAlokasi::find($alokasiid);
        $data['status'] = $this->statusBayar;

        return view('admin.investorpayment.create', $data);
    }

    public function store(Request $request)
    {
        $app = Application::find($request->id);

        switch ($app->status) {
            case "Document Incomplete":
                alert()->error('Document incomplete, payment failed.');
                return back();
            case "Document Complete / Verification in Progress":
                alert()->error('Verification in progress, payment failed.');
                return back();
            case "Verified":
                $app->status = "Funded";
                $app->save();
                alert()->success('Payment success, change application status to Funded.');
                break;
            case "Funded":
                $inves = Investor::where('userid', Sentinel::getuser()->id)->first();
                $app->status = "Installment on Progress ";
                $app->investorid = $inves->id;
                $app->save();
                alert()->success('Payment success, change status to Installment on Progress.');
                break;
                // return back();
            case "Installment on Progress":
                $app->status = "Installment on Progress";
                $app->save();
                alert()->success('Payment success');
                break;
            case "Fully Paid":
                alert()->error('Fully paid, payment failed');
                return back();
            default:
                alert()->error('Status empty, payment failed.');
                return back();
        }

        $input = $request->all();
        InvestorPayment::create($input);

        return redirect('/admin/investorpayment');
    }

    public function show($id)
    {
        $data['detail'] = InvestorPayment::find($id);
        $data['app'] = Application::all();
        $data['inves'] = Investor::all();

        return view('admin.investorpayment.show', $data);
    }

    public function edit($id)
    {
        $data['detail'] = InvestorPayment::find($id);
        $data['app'] = Application::all();
        $data['inves'] = Investor::all();

        return view('admin.investorpayment.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $detail = InvestorPayment::find($id);

        $input = $request->all();
        $detail->fill($input)->save();

        alert()->success('Payment has been updated.');
        return back();//redirect('/admin/investorpayment');
    }

    public function destroy($id)
    {

        InvestorPayment::find($id)->delete();

        alert()->success('Payment has been deleted.');
        return redirect('/admin/investorpayment');
    }

    public function approved($id)
    {
        $invesPay = InvestorPayment::find($id);
        $invesPay->approved = 1;
        $invesPay->save();

        alert()->success('Payment has been approved.');
        return back();
    }

    public function unapproved($id)
    {
        $invesPay = InvestorPayment::find($id);
        $invesPay->approved = 0;
        $invesPay->save();

        alert()->success('Payment has been unapproved.');
        return back();
    }
}
