<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\SuamiIstri;
use App\JobSuamiIstri;
use App\Keluarga;
use App\Penghasilan;
use App\Perbankan;
use App\Pemohon;
use App\JobPemohon;
use App\Application;
use App\User;
use Input;
use Session;
use Redirect;
use Sentinel;

class Suami_istriController extends JoshController
{
    public function index()
    {
        $data['sis'] = app('App\SuamiIstri')->filterPasangan();
        return view('admin.suamiistri.index', $data);
    }

    public function create(Request $request)
    {
    	$jobpemohon = null;
    	if ($request->backid) {
    		$jobpemohon = JobPemohon::find($request->backid);
    	}
    	if (!is_null(session('pem'))) {
    		$data['users'] = User::where('id', session('pem'))->first();
    	} else if (!is_null($jobpemohon)) {
    		$data['users'] = $jobpemohon->user()->first();
    	}

        $data['status'] = $this->statusNikah;
        if (!is_null($jobpemohon)) {
        	$data['jobpemohonid'] = $request->backid;
        	$data['pemohonid'] = $jobpemohon->pemohonid;
        } else {
        	$data['jobpemohonid'] = '';
        	$data['pemohonid'] = '';
        }

        return view('admin.suamiistri.create', $data);
    }

    public function store(Request $request)
    {
        $input = $request->except('pem');
        $suamiIstri = SuamiIstri::create($input);

        alert()->success('Data Suami/istri has been created.');
        return redirect('/admin/jobsuamiistri/create?backid='.$suamiIstri->id)->with('pem', $request->userid);
    }

    public function show($id)
    {
        $data['detail'] = SuamiIstri::find($id);

        return view('admin.suamiistri.show', $data);
    }

    public function edit($id)
    {
        $data['users'] = User::All();
        $suamiistri = SuamiIstri::find($id);
        $data['detail'] = $suamiistri;
        $data['status'] = $this->statusNikah;

        $data['backid'] = $id;
        $pemohonid = $suamiistri->pemohonid;
        $data['pemohonid'] = $pemohonid;
        $data['jobpemohonid'] = '';
        $data['suamisitriid'] = $id;
        $data['jobsuamiistriid'] = '';
        $data['keluargaid'] = '';
        $data['penghasilanid'] = '';
        $data['perbankanidid'] = '';
        $data['applicationid'] = '';

        $jobpemohon = JobPemohon::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($jobpemohon)) {
        	$data['jobpemohonid'] = $jobpemohon->id;
        }
        $jobsuamiistri = JobSuamiIstri::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($jobsuamiistri)) {
        	$data['jobsuamiistriid'] = $jobsuamiistri->id;
        }
        $keluarga = Keluarga::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($keluarga)) {
        	$data['keluargaid'] = $keluarga->id;
        }
        $penghasilan = Penghasilan::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($penghasilan)) {
        	$data['penghasilanid'] = $penghasilan->id;
        }
        $perbankan = Perbankan::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($perbankan)) {
        	$data['perbankanid'] = $perbankan->id;
        }
        $application = Application::where('userid', '=', $data['detail']->userid)->get();
        $data['countApp'] = count($application);
        if (!is_null($application))
        {
            if($data['countApp'] == 1)
            {
               $data['applicationid'] = $application[0]->id;
            }
        }

        return view('admin.suamiistri.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $data = SuamiIstri::find($id);

        $input = $request->all();
        $data->fill($input)->save();

        alert()->success('Data Suami/istri has been updated.');
        return redirect('/admin/suamiistri');
    }

    public function destroy($id)
    {
        SuamiIstri::find($id)->delete();

        alert()->success('Data Suami/istri has been deleted.');
        return redirect('/admin/suamiistri');
    }
}
