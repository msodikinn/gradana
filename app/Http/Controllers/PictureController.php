<?php namespace App\Http\Controllers;

use File;
use Lang;
use Redirect;
use View;
use Datatables;
use App\Http\Requests\PictureRequest;
use App\Propertyimg;
use App\Helpers\Thumbnail;
use App\Http\Requests\FileUploadRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Response;
use stdClass;

class PictureController extends JoshController
{
	public function index()
	{
		return View('admin/picture/index');
	}

	public function create()
	{
		return View('admin/picture/create');
	}

	private function makeothersize($width, $height, $filenametoset, $safeName, $extension) {
		list($origWidth, $origHeight) = getimagesize($filenametoset);
		if ($origWidth < $width) {
			$width = $origWidth;
		}
		if ($origHeight < $height) {
			$height = $origHeight;
		}
		$newfile = str_replace('.'.$extension, '_'.$width.'_'.$height, $filenametoset).'.'.$extension;
		$newfilevalue = str_replace('.'.$extension, '_'.$width.'_'.$height, $safeName).'.'.$extension;
		if ($extension == "jpg" || $extension == "jpeg") {
			$image = @imagecreatefromjpeg($filenametoset);
			if ($image) {
				$newImage = imagecreatetruecolor($width, $height);
				imagecopyresampled($newImage, $image, 0, 0, 0, 0, $width, $height, $origWidth, $origHeight);
				imagejpeg($newImage, $newfile, 80);
				return $newfilevalue;
			}
		} else if ($extension == "png") {
			$image = @imagecreatefrompng($filenametoset);
			if ($image) {
				$newImage = imagecreatetruecolor($width, $height);
				imagecopyresampled($newImage, $image, 0, 0, 0, 0, $width, $height, $origWidth, $origHeight);
				imagepng($newImage, $newfile, 80);
				return $newfilevalue;
			}
		} else if ($extension == "bmp") {
			$image = @imagecreatefromwbmp($filenametoset);
			if ($image) {
				$newImage = imagecreatetruecolor($width, $height);
				imagecopyresampled($newImage, $image, 0, 0, 0, 0, $width, $height, $origWidth, $origHeight);
				imagewbmp($newImage, $newfile, 80);
				return $newfilevalue;
			}
		}
		return null;
	}

	private function setdatainsert($propertyimg, $destinationPath, $safeName, $extension) {
		$thumbnailsizes = [
				['width' => 100, 'height' => 100],
				['width' => 200, 'height' => 200],
		];
		$cancreatethumbnail = false;
		$filenamethumbnail = [];
		foreach ($thumbnailsizes as $thumbnailsize) {
			$thumbnailfilename = $this->makeothersize($thumbnailsize['width'], $thumbnailsize['height'], $destinationPath . $safeName, $safeName, $extension);
			if (is_null($thumbnailfilename)) {
				break;
			} else {
				$filenamethumbnail[] = $thumbnailfilename;
			}
		}
		$propertyimg->img_name = $safeName;
		$propertyimg->img_path = $destinationPath;
		$propertyimg->thumbnailsizelist = json_encode($filenamethumbnail);
		return $propertyimg;
	}

	public function store(pictureRequest $request)
	{
		if ($file = $request->file('pic_file')) {
			if ($file->getClientSize() > 103000) {
				return Redirect::route('create/picture')->withInput()->with('error', 'Ukuran file maksimal 100KB');
			}
			$fileName = $file->getClientOriginalName();
			$extension = $file->getClientOriginalExtension() ?: 'png';
			$folderName = '/uploads/picture/';
			$destinationPath = public_path() . $folderName;
			$safeName = str_random(10) . '.' . $extension;
			while (file_exists($destinationPath . $safeName)) {
				$safeName = str_random(10) . '.' . $extension;
			}
			$file->move($destinationPath, $safeName);
			$request['pic'] = $safeName;

			$propertyimg = new Propertyimg();
			$propertyimg = $this->setdatainsert($propertyimg, $destinationPath, $safeName, $extension);
			$propertyimg->save();

			return Redirect::route('picture')->with('success', 'Gambar tesimpan');
		}
		return Redirect::route('create/picture')->withInput()->with('error', 'Pilih File gambar dulu');
	}

	public function data()
	{
		$propertyimgs = Propertyimg::all();

		return Datatables::of($propertyimgs)
		->edit_column('created_at','{!! format($created_at) !!}')
		->edit_column('galleryname', function($propertyimg) {
			if ($propertyimg->gallery) {
				return $propertyimg->gallery->galleryname;
			}
			return '';
		})
		->edit_column('imageshow',function($propertyimg) {
			$showfile = $propertyimg->img_name;
			if (!is_null($propertyimg->thumbnailsizelist) && !empty($propertyimg->thumbnailsizelist)) {
				$arraydata = json_decode($propertyimg->thumbnailsizelist);
				if (!empty($arraydata)) {
					$showfile = $arraydata[0];
				}
			}
			$img = '<img src="/uploads/picture/'.$showfile.'">';
			return $img;
		})
		->add_column('actions',function($propertyimg) {
			$actions = '<a href='. route('update/picture', $propertyimg->id) .'><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit picture"></i></a>';
			if ($propertyimg->gallery()->count() > 0) {
				$actions .= '<a href="#" data-toggle="modal" data-target="#picturecannotdeleted" class="picturecannotdeleted"><i class="livicon" data-name="warning-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Gambar tidak bisa di hapus"></i></a>';
			} else {
				$actions .= '<a href="'.route('confirm-delete/picture', $propertyimg->id).'" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="'.Lang::get('picture/form.delete_picture').'"></i></a>';
			}
			return $actions;
			})
		->make(true);
	}

	public function edit($id = null) {
		try {
			$propertyimg = Propertyimg::find($id);
		} catch (PropertyimgNotFoundException $e) {
            return Redirect::route('picture')->with('error', 'Gambar ini tidak ada');
        }
        return View('admin/picture/edit', [
        		'propertyimg' => $propertyimg,
        ]);
	}
	private function deletefiles($propertyimg) {
		$folderName = '/uploads/picture/';
		$destinationPath = public_path() . $folderName;
		if ($propertyimg->img_name) {
			@unlink($destinationPath.$propertyimg->img_name);
		}
		if ($propertyimg->thumbnailsizelist) {
			foreach (json_decode($propertyimg->thumbnailsizelist) as $thumbnail) {
				@unlink($destinationPath.$thumbnail);
			}
		}
	}
	public function update($id = null, PictureRequest $request)
	{
		if ($file = $request->file('pic_file')) {
			if ($file->getClientSize() > 103000) {
				return Redirect::route('update/picture', $id)->withInput()->with('error', 'Ukuran file maksimal 100KB');
			}
			$fileName = $file->getClientOriginalName();
			$extension = $file->getClientOriginalExtension() ?: 'png';
			$folderName = '/uploads/picture/';
			$destinationPath = public_path() . $folderName;
			$safeName = str_random(10) . '.' . $extension;
			while (file_exists($destinationPath . $safeName)) {
				$safeName = str_random(10) . '.' . $extension;
			}
			$file->move($destinationPath, $safeName);
			$request['pic'] = $safeName;

			Thumbnail::generate_image_thumbnail($destinationPath . $safeName, $destinationPath . 'thumb_' . $safeName);

			try {
				$propertyimg = Propertyimg::find($id);
				$this->deletefiles($propertyimg);
			} catch (PropertyimgNotFoundException $e) {
				return Redirect::route('picture')->with('error', 'picture ini tidak ada');
			}
// 			$propertyimg = $this->setdatainsert($propertyimg, $destinationPath, $safeName, $extension);
			$propertyimg->img_name = $safeName;
			$propertyimg->img_path = $destinationPath;
			$propertyimg->thumbnailsizelist = json_encode(['thumb_' . $safeName]);

			if ($propertyimg->save()) {
				return Redirect::route('picture')->with('success', 'Gambar telah disimpan');
			}
			return Redirect::route('update/picture', $id)->with('error', 'Kesalahan update');
		}
		return Redirect::route('update/picture', $id)->withInput()->with('error', 'Pilih file gambar dulu');
	}

	public function getModalDelete($id = null)
	{
		$model = 'picture';
		$confirm_route = $error = null;
		try {
			$propertyimg = Propertyimg::find($id);

			$confirm_route = route('delete/picture', ['id' => $propertyimg->id]);
			return View('admin/layouts/modal_confirmation', compact('error', 'model', 'confirm_route'));
		} catch (PropertyimgNotFoundException $e) {
			$error = 'Gambar ini tidak ada';
			return View('admin/layouts/modal_confirmation', compact('error', 'model', 'confirm_route'));
		}
	}
	public function destroy($id = null)
	{
		try {
			$propertyimg = Propertyimg::find($id);
			$this->deletefiles($propertyimg);
			$propertyimg->delete();
			return Redirect::route('picture')->with('success', 'Gambar telah dihapus');
		} catch (GroupNotFoundException $e) {
			return Redirect::route('picture')->with('error', 'Gambar ini tidak ada');
		}
	}

	//filemultiple
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param FileUploadRequest $request
	 * @return Response
	 */
	public function postFilesCreatemultiple(FileUploadRequest $request)
	{
		$destinationPath = public_path() . '/uploads/picture/';

		$file_temp = $request->file('file');
		$extension  = $file_temp->getClientOriginalExtension() ?: 'png';
		$safeName        = str_random(10).'.'.$extension;

		$Propertyimg = Propertyimg::create([
				'img_name' => $safeName,
				'img_path' => $destinationPath,
				'thumbnailsizelist' => json_encode(['thumb_' . $safeName]),
		]);

		$file_temp->move($destinationPath, $safeName);

		Thumbnail::generate_image_thumbnail($destinationPath . $safeName, $destinationPath . 'thumb_' . $safeName);

		$success = new stdClass();
		$success->name = $safeName;
		$success->size = $file_temp->getClientSize();
		$success->url = URL::to('/uploads/picture/'.$safeName);
		$success->thumbnailUrl =  URL::to('/uploads/picture/thumb_'.$safeName);
		$success->deleteUrl = URL::to('admin/picture/deletefile?_token='.csrf_token().'&id='.$Propertyimg->id);
		$success->deleteType = 'DELETE';
		$success->fileID = $Propertyimg->id;

		return Response::json(array( 'files'=> array($success)), 200);
	}

	public function deletemultiple(Request $request)
	{
		if(isset($request->id)) {
			$upload = Propertyimg::find($request->id);
			$upload->delete();

			unlink(public_path('uploads/picture/'.$upload->img_name));
			unlink(public_path('uploads/picture/thumb_'.$upload->img_name));

			if(!isset(Propertyimg::find($upload->id)->img_name)) {
				$success = new stdClass();
				$success->{$upload->img_name} = true;
				return Response::json(array('files' => array($success)), 200);
			}
		}
	}
	//____end multiplefile
}
