<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Helpers\Payment;
use App\Application;
use App\Developer;
use App\Pembayaran;
use App\Pemohon;
use App\JobPemohon;
use App\SuamiIstri;
use App\JobSuamiIstri;
use App\Keluarga;
use App\Penghasilan;
use App\Perbankan;
use App\Investor;
use App\InvestorAlokasi;
use App\User;
use Session;
use Redirect;
use Sentinel;
use View;
use DB;

class LenderController extends JoshController
{
    public function __construct( Application $applications )
    {
        $this->models = $applications;
    }

    public function dashboard()
    {
        $user_id = Sentinel::getUser()->id;
        $status_user = Sentinel::getUser()->status;
        $nama_user = Sentinel::getUser()->fullname;
		
		$data['inv'] = DB::table('investors')->select(DB::raw('*'))->where('userid', $user_id)->get();
		// $data['listBorrowers'] = Application::where(['investorid' => $data['inv'][0]->id])->get();
		$data['listBorrowers'] = DB::table('applications')->select(DB::raw('*'))->where('investorid', $data['inv'][0]->id)->get();
		$data['tt'] = DB::table('investors')->select(DB::raw('*'))->where('nama', $nama_user)->get();
		$ids = $data['tt'][0]->id;
		$data['byr'] = Pembayaran::where(['appid'=> 4])->get();
		$data['dt'] = DB::table('applications')->where('investorid', $ids)->get();
		$data['total'] = DB::table('applications')->select(DB::raw('sum(pinjaman) as pinjamans'))->where('investorid', $ids)->get();
		$data['total1'] = DB::table('applications')->select(DB::raw('sum(pinjaman) as pinjamanss'))->where('scheme', 'skema_a')->get();
		$data['total2'] = DB::table('applications')->select(DB::raw('sum(pinjaman_bln) as pinjamansss'))->where('scheme', 'skema_b')->get();
		$data['pengguna'] = DB::table('applications')->select(DB::raw('count(userid) as peminjam'))->where('investorid', $ids)->get();
		$data['rr'] = DB::table('applications')->select(DB::raw('*'))->where('investorid', $ids)->get();
		$data['peminjam'] = $data['pengguna'][0]-> peminjam;		
		
		$tot = $data['total1'][0]->pinjamanss + $data['total2'][0]->pinjamansss;
		$cek = number_format(ceil($tot),0,",",".");
		$format = strlen(ceil($tot));
		$explode = explode(".",$cek);
		
		if($format > 9 and $format < 13){
			if($explode[1] != '000'){
			$data['rp'] = $explode[0].".".substr($explode[1],0,1)." Miliar";	
			}else{
			$data['rp'] = $explode[0]." Miliar";	
			}
						
		}
		if($format > 6 and $format < 10){
			$data['rp'] = $explode[0]." Juta";
			
		}
		
		// $inmin = 15;						1 => 15%
		// $inmax = 20;						2 => 16%
		// $actuali_credit = 3 ; 			3 => 17%
		// $mincredit = 1;					4 => 18%
		// $maxcredit = 5;					5 => 19%
		//$hasil = $inmax - ($actuali_credit/$mincredit)- $maxcredit * ($inmin - $inmax) ;
		
		// Skema B
		
		$t = count($data['dt']);
		
	for($i=0;$i<$t;$i++)	
	{	
		if($data['dt'][$i]->scheme == "skema_a"){
		
		$harga_kpr_andi = 2400000000;
		$harga_kas_keras = $data['dt'][$i]->pinjaman;
		$bulanan = 0.2 * $data['dt'][$i]->pinjaman;
		$bulan2 = ($data['dt'][$i]->jangka_waktu / 12  * 0.17 * $bulanan);
		$bulan = ($bulan2 + $bulanan)/$data['dt'][$i]->jangka_waktu ;
		$untung = (0.17 * 2/12 * $bulanan) + ($harga_kpr_andi - $harga_kas_keras);  
		
		
		$bulanan1 = 0.2 * $data['dt'][$i]->pinjaman;
		$bulan3 = ($data['dt'][$i]->jangka_waktu / 12  * 0.17 * $bulanan1);
		$bulan1 = ($bulan3 + $bulanan1)/$data['dt'][$i]->jangka_waktu ;
		$untung1 = (0.17 * 2/12 * $bulanan1);
		
			
		}else{
		//Skema A

		$bulanan1 = 0.2 * $data['dt'][$i]->pinjaman;
		$bulan3 = ($data['dt'][$i]->jangka_waktu / 12  * 0.17 * $bulanan1);
		$bulan1 = ($bulan3 + $bulanan1)/$data['dt'][$i]->jangka_waktu ;
		$untung1 = (0.17 * 2/12 * $bulanan1);
		
	
		$bulanan = 0.2 * $data['dt'][$i]->pinjaman;
		$bulan2 = ($data['dt'][$i]->jangka_waktu / 12  * 0.17 * $bulanan);
		$bulan = ($bulan2 + $bulanan)/$data['dt'][$i]->jangka_waktu ;
		$untung = (0.17 * 2/12 * $bulanan); 
		
		
		}
			
		// $dataemail = [
                        // 'fullname'  => Sentinel::getuser()->fullname,
                        // 'email' => Sentinel::getuser()->email,
                    // ];

        // Mail::send('emails.applicant.quick_signup', $dataemail, function ($mail)
        // {
        //    $mail->to('toink90@gmail.com', 'TOINK');
            // $mail->bcc('tantowiachmad@gmail.com', 'Achmad Tantowi');
            //$mail->bcc('jr.pikong@gmail.com', 'Deny Pradia Utama');
        //    $mail->subject('Quick Signup Application');
        //});

        //alert()->success('Email sent');
	}
		
		$data['cicilan'] = $bulan + $bulan1;
		$data['untungnya'] = ceil($untung+$untung1);
		
		$cek2 = number_format(ceil($data['untungnya']),0,",",".");
		$format2 = strlen(ceil($data['untungnya']));
		$explode2 = explode(".",$cek2);
		
		if($format2 > 9 and $format2 < 13){
			$data['untungs'] = $explode2[0]." Miliar";
			
		}
		if($format2 > 6 and $format2 < 10){
			$data['untungs'] = $explode2[0]." Juta";
			
		}
		
		$cek1 = number_format(ceil($data['cicilan']),0,",",".");
		$format1 = strlen(ceil($data['cicilan']));
		$explode1 = explode(".",$cek1);
		
		if($format1 > 9 and $format1 < 13){
			$data['rupiah'] = $explode1[0]." Miliar";
			
		}
		if($format1 > 6 and $format1 < 10){
			$data['rupiah'] = $explode1[0]." Juta";
			
		}
		
		
		
		
		

		return View::make('frontend.lender.dashboard', $data);
		                  
					 
    }

    public function history()
    {
        $data['byr'] = Pembayaran::where(['appid'=> 4])->get();
        return View::make('frontend.lender.history', $data);
    }

	public function historyDetail($id)
    {
		
		$data['bayar'] = Pembayaran::find($id);
      
		return View::make('frontend.lender.historydetail', $data);
				
    }

	
    public function borrowerCard()
    {
        $user_id = Sentinel::getUser()->id;
        $data['availableBorrowers'] = Application::where(['investorid' => $user_id])->get();
        $data['durasi'] = Payment::getTotal($data['availableBorrowers']);
        return View::make('frontend.lender.borrower-card', $data);
    }

    public function borrowerCardDetail($id)
    {
        $data['app'] = Application::where('id', $id)->get()->first();
        $data['user'] = $data['app']->user;
        $data['asses'] = $data['app']->assesment;

        $data['byr'] = Pembayaran::where(['appid'=> 4])->get();
        $data['apl'] = app('App\Application')->apl();
        $data['total'] = Payment::getTotal($data['apl']);

        if($data['asses'] == null)
        {
            alert()->error('Assesment not available');
            return back();
        }

        $data['property'] = $data['app']->property;
        $data['rating'] = $data['app']->assesment->scoring;
        $gallery = $data['property']->galleries->first();
        if($gallery == null){
            $data['pics'] = "";
        }else{
            $data['pics'] = $gallery->properyimg()->get();
        }
        $data['total'] = Payment::getSingle($data['app']->periode, $data['app']->jangka_waktu);

        return View::make('frontend.lender.borrower-card-detail', $data);
    }

    public function availableLoans()
    {
        $data['availableLoans'] = Application::where(['investorid' => 0])->get();
        $data['durasi'] = Payment::getTotal($data['availableLoans']);
        return View::make('frontend.lender.available-loans', $data);
    }
	
	public function investorForm()
    {
	   $user_id = Sentinel::getUser()->id;
       $data['identity'] = User::where(['id' => $user_id])->first();
       $data['inves'] = Investor::where(['userid' => $user_id])->first();
	   return view('frontend.lender.investor-form',$data);
    }
	
	//public function investorPost(Request $request,$user_id)
	public function investorPost(Request $request)
    {
	    $user_id = Sentinel::getUser()->id;
        $data['identity'] = User::where(['id' => $user_id])->first();
        $data['inves'] = Investor::where(['userid' => $user_id])->first();
       
	    $nama = $request->input('nama');
	    $email = $request->input('email');
	    $alamat = $request->input('alamat');
	    $telpon = $request->input('telp');
	    $alokasi = $request->input('alokasi_dana');
	    $jml_alokasi = $request->input('jml_alokasi');
	    $tenor = $request->input('tenor');
	    $penempatan_dana = $request->input('penempatan_dana');
		$datetime1 = date("Y-m-d H:i:s");
		$datetime2 = date("Y-m-d H:i:s");
        // alert()->success('Keluarga terdekat has been updated.');
        // return redirect('/lender/dashboard');
		//return view('frontend.lender.investor-form');
		
		//$name = $request->input('fullname');
		$values = array('userid' => $user_id,'nama' => $nama,'email' =>$email,'alamat'=>$alamat,'telp'=>$telpon,'created_at'=>$datetime1,'updated_at'=>$datetime2);
		DB::table('investors')->insert($values);
		
		$id = DB::table('investors')->max('id');
		
		$value = array('investorid' => $id,'alokasi_dana' => $alokasi,'jml_alokasi' =>$jml_alokasi,'tenor'=>$tenor,'penempatan_dana'=>$penempatan_dana,'created_at'=>$datetime1,'updated_at'=>$datetime2);
		DB::table('investoralokasi')->insert($value);
		alert()->success('Invertor Form has been updated.');
        return redirect('/lender/dashboard');
    }


    public function availableLoansDetail($id)
    {
        $data['app'] = Application::where('id', $id)->get()->first();
        $data['user'] = $data['app']->user;
        $data['asses'] = $data['app']->assesment;

        if($data['asses'] == null)
        {
            alert()->error('Assesment not available');
            return back();
        }

        $data['property'] = $data['app']->property;
        $data['rating'] = $data['app']->assesment->scoring;
        $gallery = $data['property']->galleries->first();
        if($gallery == null){
            $data['pics'] = "";
        }else{
            $data['pics'] = $gallery->properyimg()->get();
        }
        $data['total'] = Payment::getSingle($data['app']->periode, $data['app']->jangka_waktu);

        return view::make('frontend.lender.available-loans-detail', $data);
    }

    public function getIndex()
    {
        $data['user'] = Sentinel::getUser();
        $data['investor'] = Investor::where('userid', $data['user']->id)->first();
        $data['lender'] = User::find($data['user']->id)->Pemohon()->get();

        // dd($data['investor']);
        return View::make('frontend.investor.lender', $data);
    }
	
	
    public function getDetail($id)
    {
        try {
            // Get the user information
            $detailPemohon = Pemohon::where('id', $id)->first();
            $detailJobPemohon = JobPemohon::where('pemohonid', $id)->first();
            $detailPasangan = SuamiIstri::where('pemohonid', $id)->first();
            $detailJobPasangan = JobSuamiIstri::where('pemohonid', $id)->first();
            $detailKeluarga = Keluarga::where('pemohonid', $id)->first();
            $detailPenghasilan = Penghasilan::where('pemohonid', $id)->first();
            $detailPerbankan = Perbankan::where('pemohonid', $id)->first();
            $detailPerbankanChild = $detailPerbankan->perbankan_childs;
            $detailApplication = app('App\Application')->apl();
            $total = Payment::getTotal($detailApplication);
            $detailPayment = app('App\Pembayaran')->payment();

        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = Lang::get('users/message.user_not_found', compact('id'));

            // Redirect to the user management page
            return Redirect::route('admin.users.index')->with('error', $error);
        }

        // Show the page
        return View('frontend.investor.lender_detail', compact(['detailPemohon', 'detailJobPemohon', 'detailPasangan', 'detailJobPasangan', 'detailKeluarga', 'detailPenghasilan', 'detailPerbankan', 'detailPerbankanChild', 'detailApplication', 'detailPayment', 'total']));
    }
}
