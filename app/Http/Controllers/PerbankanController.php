<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Perbankan;
use App\Perbankan_child;
use App\Pemohon;
use App\Penghasilan;
use App\User;
use App\Keluarga;
use App\JobSuamiIstri;
use App\JobPemohon;
use App\SuamiIstri;
use App\Application;
use Input;
use Session;
use Redirect;
use Sentinel;
use Config;
use File;
use URL;
use Image;

class PerbankanController extends JoshController
{
    public function index()
    {
        $data['perb'] = app('App\Perbankan')->filterPerbankan();

        return view('admin.perbankan.index', $data);
    }

    public function create(Request $request)
    {
    	$keluarga = null;
        if ($request->backid) {
            $keluarga = Keluarga::find($request->backid);
        }
        if (!is_null(session('pem'))) {
            $data['users'] = User::where('id', session('pem'))->first();
        } else if (!is_null($keluarga)) {
            $data['users'] = $keluarga->user()->first();
        }

        if (!is_null($keluarga)) {
            $data['keluargaid'] = $request->backid;

            if($keluarga->pemohon->status_kawin != "Lajang")
            {
                $data['jobsuamiistriid'] = JobSuamiIstri::where('pemohonid', '=', $keluarga->pemohonid)->first()->id;
                $data['suamisitriid'] = SuamiIstri::where('pemohonid', '=', $keluarga->pemohonid)->first()->id;
            }

            $data['jobpemohonid'] = JobPemohon::where('pemohonid', '=', $keluarga->pemohonid)->first()->id;
            $data['pemohonid'] = $keluarga->pemohonid;
        } else {
            $data['keluargaid'] = '';
            $data['jobsuamiistriid'] = '';
            $data['suamisitriid'] = '';
            $data['jobpemohonid'] = '';
            $data['pemohonid'] = '';
        }

        return view('admin.perbankan.create', $data);
    }

    public function store(Request $request)
    {
        Perbankan::create($request->except(['rekening_koran', 'rekening_koran2', 'pem']));
        $perbankan = Perbankan::latest()->first();

        $imgPath = 'uploads/perbankan/';
        $realPath = public_path().'/'.$imgPath;

        if($request->hasFile('rekening_koran')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('rekening_koran')->getClientOriginalExtension();
            Image::make($request->rekening_koran)->save($realPath.$filename);
            $perbankan->rekening_koran = $imgPath.$filename;
        }

        if($request->hasFile('rekening_koran2')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('rekening_koran2')->getClientOriginalExtension();
            Image::make($request->rekening_koran2)->save($realPath.$filename);
            $perbankan->rekening_koran2 = $imgPath.$filename;
        }

        $perbankan->save();

        alert()->success('Perbankan has been created.');
        // return redirect('/admin/pemohon');
        return redirect('/admin/application/create?backid='.$perbankan->id)->with('pem', $request->userid);
    }

    public function show(Request $request, $id)
    {
        // $a = URL::current();
        // dd($a);
        // $id = substr($request->url(), strrpos($request->url(), '/') + 1);
        // dd(Perbankan_child::where('perbankanid', $id)->get());
        $data['detail'] = Perbankan::find($id);
        $data['users'] = User::all();
        $data['child'] = Perbankan_child::where('perbankanid', $data['detail']->id)->get();
        $data['jenis'] = $this->jenisNasabah;
        return view('admin.perbankan.show', $data);
    }

    public function edit($id)
    {
        $data['users'] = User::all();
        $perbankan = Perbankan::find($id);
        $data['child'] = Perbankan_child::where('perbankanid', $perbankan->id)->get();
        $data['detail'] = $perbankan;
        $data['jenis'] = $this->jenisNasabah;

        $data['backid'] = $id;
        $pemohonid = $perbankan->pemohonid;
        $data['pemohonid'] = $pemohonid;
        $data['jobpemohonid'] = '';
        $data['suamisitriid'] = '';
        $data['jobsuamiistriid'] = '';
        $data['keluargaid'] = '';
        $data['penghasilanid'] = '';
        $data['perbankanid'] = $id;
        $data['applicationid'] = '';

        $jobpemohon = JobPemohon::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($jobpemohon)) {
        	$data['jobpemohonid'] = $jobpemohon->id;
        }
        $suamisitri = SuamiIstri::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($suamisitri)) {
        	$data['suamisitriid'] = $suamisitri->id;
        }
        $jobsuamiistri = JobSuamiIstri::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($jobsuamiistri)) {
        	$data['jobsuamiistriid'] = $jobsuamiistri->id;
        }
        $penghasilan = Penghasilan::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($penghasilan)) {
        	$data['penghasilanid'] = $penghasilan->id;
        }
        $keluarga = Keluarga::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($keluarga)) {
        	$data['keluargaid'] = $keluarga->id;
        }
        $application = Application::where('userid', '=', $data['detail']->userid)->get();
        $data['countApp'] = count($application);
        if (!is_null($application))
        {
            if($data['countApp'] == 1)
            {
               $data['applicationid'] = $application[0]->id;
            }
        }

        return view('admin.perbankan.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $data = Perbankan::find($id);

        $imgPath = 'uploads/perbankan/';
        $realPath = public_path().'/'.$imgPath;

        if(!file_exists($realPath)) {
            File::makeDirectory($realPath);
        }

        if($request->hasFile('rekening_koran')) {
            $extension = Input::file('rekening_koran')->getClientOriginalExtension() ?: 'png';
            $filename = str_random(10) . '.' . $extension;
            Image::make(Input::file('rekening_koran')->getRealPath())->save($realPath.$filename);
            if (File::exists(public_path() .'/'. $data->rekening_koran)) {
                File::delete(public_path() .'/'. $data->rekening_koran);
            }

            $data->rekening_koran = $imgPath.$filename;
        }

        if($request->hasFile('rekening_koran2')) {
            $extension = Input::file('rekening_koran')->getClientOriginalExtension() ?: 'png';
            $filename = str_random(10) . '.' . $extension;
            $request->file('rekening_koran2')->move($realPath, $filename);
            if (File::exists(public_path() .'/'. $data->rekening_koran2)) {
                File::delete(public_path() .'/'. $data->rekening_koran2);
            }

            $data->rekening_koran2 = $imgPath.$filename;
        }

        $data->save();

        $input = $request->except(['rekening_koran', 'rekening_koran2']);
        $data->fill($input)->save();

        alert()->success('Perbankan has been updated.');
        return back(); //redirect('/admin/perbankan');
    }

    public function destroy($id)
    {
        $del = Perbankan::find($id);
        $del->delete();

        if (File::exists(public_path() .'/'. $del->rekening_koran)) {
            File::delete(public_path() .'/'. $del->rekening_koran);
        }

        if (File::exists(public_path() .'/'. $del->rekening_koran2)) {
            File::delete(public_path() .'/'. $del->rekening_koran2);
        }

        alert()->success('Perbankan has been deleted.');
        return redirect('/admin/perbankan');
    }
}
