<?php

namespace App\Http\Controllers;

use App\Application;
use App\Assesment;
use App\Helpers\Score;
use App\Http\Requests;
use App\JobPemohon;
use App\Perbankan;
use App\Perbankan_child;
use App\Scoring;
use DB;
use Illuminate\Http\Request;
use Input;
use Redirect;
use Sentinel;
use Session;

class ScoringController extends JoshController
{
    public function index()
    {
        $data['score'] = Scoring::with('application.assesment')->get();
        $data['app'] = DB::table('applications')
            ->whereNotExists(function ($query) {
                $query->select(DB::raw('id'))
                      ->from('scorings')
                      ->whereRaw('scorings.appid = applications.id');
            })->get();
        return view('admin.scoring.index', $data);
    }

    public function choose($pemohonid, $appid)
    {
        $data['pchilds'] = Perbankan::with('perbankan_childs')
                    ->where('pemohonid', $pemohonid)
                    ->first();
					
		print_r($data['pchilds']);
        $data['appid'] = Application::find($appid)->id;
		
        $data['angsuran'] = 0;
        $jml_cc = 0;
        $data['limit_cc'] = 0;

        foreach ($data['pchilds']->perbankan_childs as $pchild) {
            switch ($pchild->jenis) {
                case "Angsuran yang sedang berjalan":
                    $data['angsuran'] = $pchild->jml_cicilan_bln + $data['angsuran'];
                    break;
                case "Kartu Kredit":
                    $jml_cc = $jml_cc + 1;
                    $data['limit_cc'] = $data['limit_cc'] + $pchild->limit_kartu;
                    break;
                default:
                    echo "";
            }
        }

        if ($jml_cc == 1 || $jml_cc == 0) {
            $data['nilai_cc'] = 5;
        } elseif ($jml_cc == 2) {
            $data['nilai_cc'] = 4;
        } elseif ($jml_cc == 3) {
            $data['nilai_cc'] = 3;
        } elseif ($jml_cc == 4) {
            $data['nilai_cc'] = 2;
        } elseif ($jml_cc >= 5) {
            $data['nilai_cc'] = 1;
        }

        $lamaKerja = JobPemohon::where('pemohonid', $pemohonid)
                    ->first();

        $lama = date("Y") - $lamaKerja->tahun;

        if ($lama >= 5) {
            $data['lama_kerja'] = "≥ 5 tahun";
        }elseif($lama >= 4 && $lama < 5){
            $data['lama_kerja'] = "4 ≤ lama kerja < 5 tahun";
        }elseif($lama >= 3 && $lama < 4){
            $data['lama_kerja'] = "3 ≤ lama kerja < 4 tahun";
        }elseif($lama >= 2 && $lama < 3){
            $data['lama_kerja'] = "2 ≤ lama kerja < 3 tahun";
        }elseif($lama < 2){
            $data['lama_kerja'] = "< 2 tahun";
        }

        $data['ratingJmlCC'] = $this->ratingJmlCC;
        $data['ratingLamaKerja'] = $this->ratingLamaKerja;
        $data['ratingTotalLamaKerja'] = $this->ratingTotalLamaKerja;
        $data['ratingData'] = $this->ratingData;
        $data['ratingDataAkurasi'] = $this->ratingDataAkurasi;

        // $data['pchilds'] = Scoring::with('application.pemohon.perbankan.perbankan_childs')->find(40);
        // dd($data['pchilds']->application->pemohon->perbankan->perbankan_childs);

        return view('admin.scoring.create', $data);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        Scoring::create($input);

        alert()->success('Credit Scoring has been created.');
        return redirect('/admin/scoring');
    }

    public function show($id)
    {
        $data['detail'] = Scoring::with('application.assesment')->find($id);
        $ph = Scoring::with('application.penghasilan')->find($id);

        $app = $data['detail']->application;
        $data['period'] = (int)$app->periode;

        $data['jangkaWaktu'] = ($data['period'] * 12) + (int)$app->jangka_waktu;
        $data['pinjaman'] = (int)$app->pinjaman;
        $data['appid'] = $app->id;

        $data['annumMin'] = (float)$data['detail']->application->assesment->interest_annum_min;
        $data['annumMax'] = (float)$data['detail']->application->assesment->interest_annum_max;

        $data['thp'] = $ph->application->user->penghasilan->first()->pemohon;
        $data['user'] = $ph->application->user;

        $data['netPlafon'] = Score::getPlafon($data['thp']) - $data['detail']->jml_hutang;

        // Jumlah Hutang (mencari percentase)
        $percentJmlHtg = ($data['detail']->jml_hutang / $data['thp']) * 100;
        $data['scoreJmlHtg'] = Score::getJmlHutang($percentJmlHtg);
        $data['cscoreJmlHtg'] = $data['scoreJmlHtg'] * 0.2;

        // Pinjaman High Interest
        $first = (1 + $data['annumMax']) / 100;
        $second = pow($first, $data['period']);
        $third = $second * $data['pinjaman'];
        $data['pinjHI'] = round(($third) / $data['jangkaWaktu']);
        $data['scoreHI'] = Score::getHI($data['netPlafon'], $data['pinjHI']);
        $data['cscoreHI']  = $data['scoreHI'] * 0.3;

        // Jumlah Kartu Kredit
        $data['scoreCC'] = Score::getCC($data['detail']->jml_cc);
        if($data['detail']->jml_cc == "5 nilai - 0 kartu kredit") {
            $data['wscoreCC'] = "7.5%";
            $data['cscoreCC'] = $data['scoreCC'] * 0.075;;
        } else {
            $data['wscoreCC'] = "5.0%";
            $data['cscoreCC'] = $data['scoreCC'] * 0.05;
        }

        // Total limit kartu kredit
        $data['scoreLimit'] = Score::getTotLimit($data['detail']->limit_cc, $data['thp']);
        if($data['detail']->jml_cc == "5 nilai - 0 kartu kredit") {
            $data['wscoreLimit'] = "-";
            $data['cscoreLimit'] = "0";
            $data['scoreLimit'] = "-";
        } else {
            $data['wscoreLimit'] = "2.5%";
            $data['cscoreLimit'] = $data['scoreLimit'] * 0.025;
        }

        // Skor lama kerja
        $data['scoreLamaKerja'] = Score::getLamaKerja($data['detail']->lama_kerja);
        $data['cscoreLamaKerja'] = $data['scoreLamaKerja'] * 0.075;

        // Skor total lama kerja
        $data['scoreTotLamaKerja'] = Score::getTotLamaKerja($data['detail']->total_lama_kerja);
        $data['cscoreTotLamaKerja'] = $data['scoreTotLamaKerja'] * 0.075;

        // Skor saldo 3 bulan
        $data['scoreSaldo'] = Score::getSaldo3Bln($data['detail']->saldo_3_bln, $data['thp']);
        if($data['scoreSaldo'] >= 3) {
            $data['cscoreSaldo'] = $data['scoreSaldo'] * 0.175;
            $data['wscoreSaldo'] = "17.5%";
        } elseif ($data['scoreSaldo'] < 3) {
            $data['cscoreSaldo'] = $data['scoreSaldo'] * 0.125;
            $data['wscoreSaldo'] = "12.5%";
        }

        // Kelengkapan data
        $data['scoreLengkap'] = Score::getKelengkapan($data['detail']->kelengkapan_data);
        $data['cscoreLengkap'] = $data['scoreLengkap'] * 0.05;

        // Akurasi data
        $data['scoreAkurasi'] = Score::getAkurasi($data['detail']->akurasi_data);
        $data['cscoreAkurasi'] = $data['scoreAkurasi'] * 0.05;

        // Asset Bank
        $data['scoreAssetBank'] = Score::getAsset($data['detail']->jumlah_asset_bank, $data['pinjaman']);
        if($data['scoreSaldo'] >= 3) {
            $data['scoreAssetBank'] = "-";
            $data['cscoreAssetBank'] = "0";
            $data['wscoreAssetBank'] = "-";
        } elseif ($data['scoreSaldo'] < 3) {
            $data['cscoreAssetBank'] = $data['scoreAssetBank'] * 0.05;
            $data['wscoreAssetBank'] = "5.0%";
        }

        $data['ave'] = round($data['cscoreJmlHtg'] + $data['cscoreHI'] + $data['cscoreCC'] + $data['cscoreLimit'] + $data['cscoreLamaKerja'] + $data['cscoreTotLamaKerja'] + $data['cscoreSaldo'] + $data['cscoreLengkap'] + $data['cscoreAkurasi'] + $data['cscoreAssetBank']);

        $data['detail']->application->assesment->credit_rating = $data['ave'];
        $data['detail']->application->assesment->save();

        return view('admin.scoring.show', $data);
    }

    public function edit($id)
    {
        $data['detail'] = Scoring::find($id);
        $data['asses'] = Assesment::all();
        $data['ratingJmlCC'] = $this->ratingJmlCC;
        $data['ratingLamaKerja'] = $this->ratingLamaKerja;
        $data['ratingTotalLamaKerja'] = $this->ratingTotalLamaKerja;
        $data['ratingData'] = $this->ratingData;
        $data['ratingDataAkurasi'] = $this->ratingDataAkurasi;
        // $ph = Scoring::with('assesment.application.user.penghasilan')->get();
        // $app = $data['detail']->assesment->application;
        // $data['jangkaWaktu'] = (int)$app->jangka_waktu;
        // $data['pinjaman'] = (int)$app->pinjaman;
        // $data['period'] = (int)$app->periode;

        // foreach ($ph as $a) {
        //     $data['annumMin'] = (float)$a->assesment->interest_annum_min;
        //     $data['annumMax'] = (float)$a->assesment->interest_annum_max;
        // }

        // foreach ($ph as $p){
        //     $data['thp'] = (int)$p->assesment->application->user->penghasilan[0]->pemohon;
        //     $data['user'] = $p->assesment->application->user;
        // };
        // $data['netPlafon'] = Score::getPlafon($data['thp']) - $data['detail']->jml_hutang;

        // $first = (1 + $data['annumMax']) / 100;
        // $second = pow($first, $data['period']);
        // $third = $second * $data['pinjaman'];
        // $data['pinjHI'] = ($third) / $data['jangkaWaktu'];

        return view('admin.scoring.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $detail = Scoring::find($id);

        $input = $request->all();
        $detail->fill($input)->save();

        $ph = Scoring::with('assesment.application.user.penghasilan')->get();
        $app = $detail->assesment->application;
        $period = (int)$app->periode;
        $jangkaWaktu = ($period * 12) + (int)$app->jangka_waktu;
        $pinjaman = (int)$app->pinjaman;
        $appid = $app->id;

        foreach ($ph as $a) {
            $annumMax = (float)$a->assesment->interest_annum_max;
        }

        foreach ($ph as $p){
            $thp = (int)$p->assesment->application->user->penghasilan[0]->pemohon;
            $user = $p->assesment->application->user;
        };
        $netPlafon = Score::getPlafon($thp) - $detail->jml_hutang;
        $percentJmlHtg = ($request->jml_hutang / $thp) * 100;
        $scoreJmlHtg = Score::getJmlHutang($percentJmlHtg);
        $cscoreJmlHtg = $scoreJmlHtg * 0.2;

        $first = (1 + $annumMax) / 100;
        $second = pow($first, $period);
        $third = $second * $pinjaman;
        $pinjHI = ($third) / $jangkaWaktu;
        $scoreHI = Score::getHI($netPlafon, $pinjHI);
        $cscoreHI  = $scoreHI * 0.3;

        $scoreCC = Score::getCC($detail->jml_cc);
        if($detail->jml_cc == "5 nilai - 0 kartu kredit") {
            $wscoreCC = "7.5%";
            $cscoreCC = $scoreCC * 0.075;;
        } else {
            $wscoreCC = "5.0%";
            $cscoreCC = $scoreCC * 0.05;
        }

        $scoreLimit = Score::getTotLimit($detail->limit_cc, $thp);
        if($detail->jml_cc == "5 nilai - 0 kartu kredit") {
            $wscoreLimit = "-";
            $cscoreLimit = "0";
            $scoreLimit = "-";
        } else {
            $wscoreLimit = "2.5%";
            $cscoreLimit = $scoreLimit * 0.025;
        }

        $scoreLamaKerja = Score::getLamaKerja($detail->lama_kerja);
        $cscoreLamaKerja = $scoreLamaKerja * 0.075;

        $scoreTotLamaKerja = Score::getTotLamaKerja($detail->total_lama_kerja);
        $cscoreTotLamaKerja = $scoreTotLamaKerja * 0.075;

        $scoreSaldo = Score::getSaldo3Bln($detail->saldo_3_bln, $thp);
        if($scoreSaldo >= 3) {
            $cscoreSaldo = $scoreSaldo * 0.175;
            $wscoreSaldo = "17.5%";
        } elseif ($scoreSaldo < 3) {
            $cscoreSaldo = $scoreSaldo * 0.125;
            $wscoreSaldo = "12.5%";
        }

        $scoreLengkap = Score::getKelengkapan($detail->kelengkapan_data);
        $cscoreLengkap = $scoreLengkap * 0.05;

        $scoreAkurasi = Score::getAkurasi($detail->akurasi_data);
        $cscoreAkurasi = $scoreAkurasi * 0.05;

        $scoreAssetBank = Score::getAsset($detail->jumlah_asset_bank, $pinjaman);
        if($scoreSaldo >= 3) {
            $scoreAssetBank = "-";
            $cscoreAssetBank = "0";
            $wscoreAssetBank = "-";
        } elseif ($scoreSaldo < 3) {
            $cscoreAssetBank = $scoreAssetBank * 0.05;
            $wscoreAssetBank = "5.0%";
        }

        $ave = $cscoreJmlHtg + $cscoreHI + $cscoreCC + $cscoreLimit + $cscoreLamaKerja + $cscoreTotLamaKerja + $cscoreSaldo + $cscoreLengkap + $cscoreAkurasi + $cscoreAssetBank;

        $detail->pinj_high_interest = $pinjHI;
        $detail->save();

        $asses = $detail->assesment;
        $asses->credit_rating = round($ave);
        $asses->save();

        alert()->success('Credit Scoring has been updated.');
        return back();
    }

    public function destroy($id)
    {
        Scoring::find($id)->delete();

        alert()->success('Delete Credit Scoring success.');
        return redirect('/admin/scoring');
    }
}
