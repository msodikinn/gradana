<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Subscribe;
use Input;
use Mail;

class SubscribeController extends Controller
{
    public function index()
    {
        return view('frontend.subscribes.comingsoon');
    }

    public function saveSubs(Request $request)
    {
        if($request->home){
            $data = $request->except('home');
        }else{
            $data = $request->all();
        }

        Subscribe::create($data);
        $subs = Subscribe::latest()->first();
        $arr = explode("@", $subs->email);
        $first = $arr[0];

        Mail::send('emails.subscribes', ['subs' => $subs], function ($m) use ($subs, $first){
            $m->to($subs->email, $first)->subject('Gradana Website reminder');
        });

        if($request->home){
            alert()->success('Email Anda sukses kami daftarkan');
            return redirect('/');
        }else{
            alert()->success('Your email have been added for website notified');
            return redirect('/confirm');
        }
    }

    public function confirm()
    {
        return view('frontend.subscribes.confirm');
    }
}
