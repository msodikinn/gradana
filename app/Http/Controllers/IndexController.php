<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use View;
use App\Application;
use App\Pembayaran;
use App\Blog;
use Mail;

class IndexController extends Controller
{
    public function index()
    {
        $data['blog'] = Blog::paginate(6);
        $app = Application::All();
        $total = $app->count();
        $data['countApproved'] = $app->where('approved', 1)->count();
        $data['sumApproved'] = $app->where('approved', 1)->sum('pinjaman');
        $full = $app->where('approved', 1)->where('status', 'Fully Paid')->count();
        $data['success'] = ($full / $total) * 100;

        $tgl = Pembayaran::select('tgl_pembayaran')->get();

        // hitung bad debt
        // looping setiap application
        $bad = 0;
        $minus = 0;
        $badCredit = 0;
        // foreach ($app as $a) {
        //     $appid = $a->id;
        //     $start = $a->installment_times;
        //     $blnStart = date("m",strtotime($start));
        //     $current = date('Y-m-d');
        //     $lama = ($a->periode * 12) + $a->jangka_waktu;
        //     $jatuhTempo = date('Y-m-d', strtotime("+$lama months", strtotime($start)));

        //     for ($x = 0; $x <= $lama; $x++) {
        //         $bulanCounter = date('F Y', strtotime("+$x months", strtotime($start)));
        //         if(strtotime($bulanCounter) <= strtotime($current))
        //         {

        //             // looping di setiap pembayaran (sub dari application)
        //             $pay = Pembayaran::where('appid', $appid)->where('bulan', $bulanCounter)->first();

        //             if (isset($pay))      // jika ada pembayaran
        //             {
        //                 $bad = $bad;
        //                 $minus = 0;
        //             }else{                // jika tidak ada pembayaran
        //                 $minus = $minus + 1;
        //                 if($minus >= 2)
        //                 {
        //                     $bad = $bad + 1;
        //                 }

        //                 if($bad == 2)
        //                 {
        //                     $badCredit = $badCredit + 1;
        //                     $bad = 0;
        //                 }
        //             }
        //         }
        //     }
        // }
        // $data['badCredit'] = ($badCredit / $total) * 100;


        return View('frontend/index', $data);
    }

    public function lender()
    {
        return View('frontend.lender');
    }

    public function borrower()
    {
        return View('frontend.borrower');
    }
}
