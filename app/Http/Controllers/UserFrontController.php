<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Pemohon;
use App\JobPemohon;
use App\SuamiIstri;
use App\JobSuamiIstri;
use App\Keluarga;
use App\Penghasilan;
use App\Perbankan;
use App\Perbankan_child;
use App\Investor;
use App\Investor_cat;
use Sentinel;
use Config;
use Input;
use Image;

use App\Application;
use App\Developer;
use App\Property;
use Carbon\Carbon;
use Mail;

class UserFrontController extends JoshController
{
    public function remind()
    {
      return view('frontend.tes');
    }

    public function remindPost()
    {
        $appApproved = Application::where('approved', '1')->get();

        foreach ($appApproved as $aa)
        {
            $id = $aa->id;
            $email = $aa->user->email;
            $fullname = $aa->user->fullname;
            $namaDev = $aa->developer->nama;
            $namaProp = $aa->property->nama;
            Mail::queue('emails.payreminder', ['id' => $id, 'email' => $email, 'fullname' => $fullname, 'namaDev' => $namaDev, 'namaProp' => $namaProp], function ($m) use ($email, $fullname)
            {
                $m->to($email, $fullname);
                $m->subject('Gradana Payment Reminder');
            });
        }
    }
}
