<?php

namespace App\Http\Controllers;

use Lang;
use Datatables;
use URL;
use App\BlogCategory;
use App\Http\Requests\BlogCategoryRequest;

class BlogCategoryController extends JoshController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return View('admin.blogcategory.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.blogcategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(BlogCategoryRequest $request)
    {
        $blogcategory = new BlogCategory($request->all());

        if ($blogcategory->save()) {
            return redirect('admin/blogcategory')->with('success', trans('blogcategory/message.success.create'));
        } else {
            return Redirect::route('admin/blogcategory')->withInput()->with('error', trans('blogcategory/message.error.create'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit(BlogCategory $blogcategory)
    {
        return view('admin.blogcategory.edit', compact('blogcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(BlogCategoryRequest $request, BlogCategory $blogcategory)
    {
        if ($blogcategory->update($request->all())) {
            return redirect('admin/blogcategory')->with('success', trans('blogcategory/message.success.update'));
        } else {
            return Redirect::route('admin/blogcategory')->withInput()->with('error', trans('blogcategory/message.error.update'));
        }
    }

    /**
     * Remove blog.
     *
     * @param $website
     * @return Response
     */
    public function getModalDelete(BlogCategory $blogcategory)
    {
        $model = 'blogcategory';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('delete/blogcategory', ['id' => $blogcategory->id]);
            return View('admin/layouts/modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('blogcategory/message.error.delete', compact('id'));
            return View('admin/layouts/modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy(BlogCategory $blogcategory)
    {
        if ($blogcategory->delete()) {
            return redirect('admin/blogcategory')->with('success', trans('blogcategory/message.success.delete'));
        } else {
            return Redirect::route('admin/blogcategory')->withInput()->with('error', trans('blogcategory/message.error.delete'));
        }
    }
	public function data() {
		$blogscategories = BlogCategory::all();
		return Datatables::of($blogscategories)
		->add_column('blogcount', function($bcategory) {
			return $bcategory->blog()->count();
		})
		->edit_column('created_at','{!! format($created_at) !!}')
		->add_column('actions',function($bcategory) {
			$actions = '<a href="'.URL::to('admin/blogcategory/' . $bcategory->id . '/edit' ).'"><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="'.Lang::get('blogcategory/form.update-blog').'"></i></a>';
			if($bcategory->blog()->count()) {
				$actions .= '<a href="#" data-toggle="modal" data-target="#blogcategory_exists" data-name="'.$bcategory->title.'" class="blogcategory_exists"><i class="livicon" data-name="warning-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="'.Lang::get('blogcategory/form.blogcategoryexists').'"></i></a>';
			} else {
				$actions .= '<a href="'.route('confirm-delete/blogcategory', $bcategory->id).'" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="'.Lang::get('blogcategory/form.deleteblogcategory').'"></i></a>';
			}
			return $actions;
		})
		->make(true);
	}
}
