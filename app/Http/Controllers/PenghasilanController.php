<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Penghasilan;
use App\Pemohon;
use App\User;
use Input;
use Session;
use Redirect;
use Sentinel;
use App\Application;
use App\Keluarga;
use App\JobSuamiIstri;
use App\JobPemohon;
use App\SuamiIstri;
use App\Perbankan;

class PenghasilanController extends Controller
{
    public function index()
    {
        $data['hasil'] = app('App\Penghasilan')->filterPenghasilan();

        return view('admin.penghasilan.index', $data);
    }

    public function create(Request $request)
    {
    	$keluarga = null;
    	if ($request->backid) {
    		$keluarga = Keluarga::find($request->backid);
    	}
    	if (!is_null(session('pem'))) {
        	$data['users'] = User::where('id', session('pem'))->first();
        } else if (!is_null($keluarga)) {
        	$data['users'] = $keluarga->user()->first();
        }

        if (!is_null($keluarga)) {
        	$data['keluargaid'] = $request->backid;

            if($keluarga->pemohon->status_kawin != "Lajang")
            {
            	$data['jobsuamiistriid'] = JobSuamiIstri::where('pemohonid', '=', $keluarga->pemohonid)->first()->id;
        		$data['suamisitriid'] = SuamiIstri::where('pemohonid', '=', $keluarga->pemohonid)->first()->id;
            }

    		$data['jobpemohonid'] = JobPemohon::where('pemohonid', '=', $keluarga->pemohonid)->first()->id;
    		$data['pemohonid'] = $keluarga->pemohonid;
        } else {
        	$data['keluargaid'] = '';
        	$data['jobsuamiistriid'] = '';
        	$data['suamisitriid'] = '';
        	$data['jobpemohonid'] = '';
        	$data['pemohonid'] = '';
        }

        return view('admin.penghasilan.create', $data);
    }

    public function store(Request $request)
    {
        $penghasilan = Penghasilan::create($request->except('pem'));
        alert()->success('Penghasilan has been created.');
        return redirect('/admin/perbankan/create?backid='.$penghasilan->id)->with('pem', $request->userid);
    }

    public function show($id)
    {
        $data['detail'] = Penghasilan::find($id);

        return view('admin.penghasilan.show', $data);
    }

    public function edit($id)
    {
        $data['users'] = User::all();
        $penghasilan = Penghasilan::find($id);
        $data['detail'] = $penghasilan;

        $data['backid'] = $id;
        $pemohonid = $penghasilan->pemohonid;
        $data['pemohonid'] = $pemohonid;
        $data['jobpemohonid'] = '';
        $data['suamisitriid'] = '';
        $data['jobsuamiistriid'] = '';
        $data['keluargaid'] = '';
        $data['penghasilanid'] = $id;
        $data['perbankanid'] = '';
        $data['applicationid'] = '';

        $jobpemohon = JobPemohon::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($jobpemohon)) {
        	$data['jobpemohonid'] = $jobpemohon->id;
        }
        $suamisitri = SuamiIstri::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($suamisitri)) {
        	$data['suamisitriid'] = $suamisitri->id;
        }
        $jobsuamiistri = JobSuamiIstri::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($jobsuamiistri)) {
        	$data['jobsuamiistriid'] = $jobsuamiistri->id;
        }
        $keluarga = Keluarga::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($keluarga)) {
        	$data['keluargaid'] = $keluarga->id;
        }
        $perbankan = Perbankan::where('pemohonid', '=', $pemohonid)->first();
        if (!is_null($perbankan)) {
        	$data['perbankanid'] = $perbankan->id;
        }
        $application = Application::where('userid', '=', $data['detail']->userid)->get();
        $data['countApp'] = count($application);
        if (!is_null($application))
        {
            if($data['countApp'] == 1)
            {
               $data['applicationid'] = $application[0]->id;
            }
        }

        return view('admin.penghasilan.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $data = Penghasilan::find($id);

        $input = $request->all();
        $data->fill($input)->save();

        alert()->success('Penghasilan has been updated.');
        return redirect('/admin/penghasilan');
    }

    public function destroy($id)
    {
        Penghasilan::find($id)->delete();

        alert()->success('Penghasilan has been deleted.');
        return redirect('/admin/penghasilan');
    }
}
