<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Perbankan;
use App\Perbankan_child;
use App\User;
use URL;

class Perbankan_childController extends JoshController
{
    public function store(Request $request)
    {
        Perbankan_child::create($request->all());

        alert()->success('Perbankan child has been created.');
        return back(); //redirect('/admin/perbankan');
    }

    public function apiChild(Request $request){
        $a = URL::current();
        $id = substr($a, strrpos($a, '/') + 1);

        return back(); //Perbankan_child::where('perbankanid', $id)->get();
        // return Perbankan_child::all();
    }

    // public function edit($id)
    // {
    //     return Perbankan_child::findOrFail($id);
    // }

    public function edit($id)
    {
        $data['pchild'] = Perbankan_child::find($id);
        $data['jenis'] = $this->jenisNasabah;

        return view('admin.perbankan.editpchild', $data);
    }

    public function update(Request $request, $id)
    {
        $pchild = Perbankan_child::find($id);

        $input = $request->all();
        $pchild->fill($input)->save();

        alert()->success('Perbankan Child has been updated.');
        return back(); //redirect('/admin/perbankan/detail/'.$pchild->perbankanid);
    }

    public function destroy($id,$pid)
    {
        $del = Perbankan_child::find($id);
        $del->delete();

        alert()->success('Perbankan Child has been deleted.');
        return back(); //redirect('/admin/perbankan/detail/'.$pid);
    }
}
