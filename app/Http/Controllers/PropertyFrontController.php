<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;

use App\Http\Requests;

class PropertyFrontController extends Controller
{
    public function index()
    {
        $a = 100000000;
        $b = 1.12;
        $c = 2;
        $hasil_pangkat = pow($b, $c);
        $hasil = ($a*$hasil_pangkat)/24;
        $propertys = Property::all();
        return view('frontend.property', compact('propertys','hasil'));
    }

    public function detail($id)
    {
        $views = Property::where('id', $id)->get()->first();
        return view('frontend.property-detail', compact('views'));
    }


}
