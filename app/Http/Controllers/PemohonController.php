<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Pemohon;
use App\SuamiIstri;
use App\JobSuamiIstri;
use App\JobPemohon;
use App\Keluarga;
use App\Penghasilan;
use App\Perbankan;
use App\Perbankan_child;
use App\Application;
use App\User;
use App\Propinsi;
use App\Kota;
use App\Kecamatan;
use App\Desa;
use Input;
use Redirect;
use Sentinel;
use Config;
use Image;
use File;
use App\Helpers\Payment;

class PemohonController extends JoshController
{
    public function index()
    {
        $data['users'] = User::all();
        $data['pem'] = app('App\Pemohon')->filterPemohon();

        return view('admin.pemohon.index', $data);
    }

    public function create()
    {
        $data['users'] = User::all();
        $data['pendidikan'] = $this->statusPendidikan;
        $data['kawin'] = $this->statusKawin;
        $data['rumah'] = $this->statusRumah;
        $data['propinsi'] = array('' => '');
        foreach(Propinsi::all() as $row)
            $data['propinsi'][$row->id] = $row->name;

        return view('admin.pemohon.create', $data);
    }

    public function store(Request $request)
    {
        $input = $request->except(['ktp_img', 'npwp_img', 'kk_img', 'tagihan_listrik', 'tagihan_air', 'tagihan_telp', 'bi_checking', 'akta_img', 'ssp']);
        Pemohon::create($input);
        $pm = Pemohon::latest()->first();

        $imgPath = Config::get('gradana.pemohon_img_path');
        $realPath = public_path().'/'.$imgPath;
        !file_exists($realPath) ? File::makeDirectory($realPath) : "";

        if($request->hasFile('ktp_img')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('ktp_img')->getClientOriginalExtension();
            Image::make($request->ktp_img)->save($realPath.$filename);
            $pm->ktp_img = $imgPath.$filename;
        }
        if($request->hasFile('npwp_img')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('npwp_img')->getClientOriginalExtension();
            Image::make($request->npwp_img)->save($realPath.$filename);
            $pm->npwp_img = $imgPath.$filename;
        }
        if($request->hasFile('kk_img')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('kk_img')->getClientOriginalExtension();
            Image::make($request->kk_img)->save($realPath.$filename);
            $pm->kk_img = $imgPath.$filename;
        }
        if($request->hasFile('tagihan_listrik')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('tagihan_listrik')->getClientOriginalExtension();
            Image::make($request->tagihan_listrik)->save($realPath.$filename);
            $pm->tagihan_listrik = $imgPath.$filename;
        }
        if($request->hasFile('tagihan_air')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('tagihan_air')->getClientOriginalExtension();
            Image::make($request->tagihan_air)->save($realPath.$filename);
            $pm->tagihan_air = $imgPath.$filename;
        }
        if($request->hasFile('tagihan_telp')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('tagihan_telp')->getClientOriginalExtension();
            Image::make($request->tagihan_telp)->save($realPath.$filename);
            $pm->tagihan_telp = $imgPath.$filename;
        }
        if($request->hasFile('bi_checking')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('bi_checking')->getClientOriginalExtension();
            Image::make($request->bi_checking)->save($realPath.$filename);
            $pm->bi_checking = $imgPath.$filename;
        }
        if($request->hasFile('akta_img')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('akta_img')->getClientOriginalExtension();
            Image::make($request->akta_img)->save($realPath.$filename);
            $pm->akta_img = $imgPath.$filename;
        }
        if($request->hasFile('ssp')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('ssp')->getClientOriginalExtension();
            Image::make($request->ssp)->save($realPath.$filename);
            $pm->ssp = $imgPath.$filename;
        }

        $pm->save();

        alert()->success('Pemohon has been created.');
        return redirect('/admin/jobpemohon/create?backid='.$pm->id)->with('pem', $request->userid);
    }

    public function show($id)
    {
        // $data['detail'] = Pemohon::find($id);
        if (permission() == "Applicant" || permission() == "Admin") {
            $data['detailPemohon'] = Pemohon::find($id);
            $data['propinsi'] = Propinsi::where('id', $data['detailPemohon']->propinsi_ktp)->first();
            $data['kota'] = Kota::where('id', $data['detailPemohon']->kota_ktp)->first();
            $data['kec'] = Kecamatan::where('id', $data['detailPemohon']->kecamatan_ktp)->first();
            $data['kel'] = Desa::where('id', $data['detailPemohon']->kelurahan_ktp)->first();
            $data['detailJobPemohon'] = JobPemohon::where('pemohonid', $id)->first();
            $data['propinsi_jobpemohon'] = Propinsi::where('id', $data['detailJobPemohon']->propinsi)->first();
            $data['kota_jobpemohon'] = Kota::where('id', $data['detailJobPemohon']->kota)->first();
            $data['kec_jobpemohon'] = Kecamatan::where('id', $data['detailJobPemohon']->kecamatan)->first();
            $data['kel_jobpemohon'] = Desa::where('id', $data['detailJobPemohon']->kelurahan)->first();
            $data['detailPasangan'] = SuamiIstri::where('pemohonid', $id)->first();
            $data['detailJobPasangan'] = JobSuamiIstri::where('pemohonid', $id)->first();
            $data['propinsi_jobpasangan'] = Propinsi::where('id', $data['detailJobPasangan']->propinsi)->first();
            $data['kota_jobpasangan'] = Kota::where('id', $data['detailJobPasangan']->kota)->first();
            $data['kec_jobpasangan'] = Kecamatan::where('id', $data['detailJobPasangan']->kecamatan)->first();
            $data['kel_jobpasangan'] = Desa::where('id', $data['detailJobPasangan']->kelurahan)->first();
            $data['detailKeluarga'] = Keluarga::where('pemohonid', $id)->first();
            $data['detailPenghasilan'] = Penghasilan::where('pemohonid', $id)->first();
            $data['detailPerbankan'] = Perbankan::where('pemohonid', $id)->first();
            $data['detailPerbankanChild'] = $data['detailPerbankan']->perbankan_childs;
            $data['detailApplication'] = app('App\Application')->apl();
            $total = Payment::getTotal($data['detailApplication']);
        }
        $data['detailPayment'] = app('App\Pembayaran')->payment();
        $user = Sentinel::findUserById($id);

        return view('admin.pemohon.show', $data);
    }

    public function edit($id)
    {
        $data['users'] = User::all();
        $data['detail'] = Pemohon::find($id);
        $data['pendidikan'] = $this->statusPendidikan;
        $data['kawin'] = $this->statusKawin;
        $data['rumah'] = $this->statusRumah;
        $data['propinsi'] = array('' => '');
        foreach(Propinsi::all() as $row)
            $data['propinsi'][$row->id] = $row->name;

        $data['kota'] = array('' => '');
        foreach(Kota::where('province_id', $data['detail']->propinsi_ktp)->get() as $row)
            $data['kota'][$row->id] = $row->name;

        $data['kecamatan'] = array('' => '');
        foreach(Kecamatan::where('regency_id', $data['detail']->kota_ktp)->get() as $row)
            $data['kecamatan'][$row->id] = $row->name;

        $data['desa'] = array('' => '');
        foreach(Desa::where('district_id', $data['detail']->kecamatan_ktp)->get() as $row)
            $data['desa'][$row->id] = $row->name;

        $data['backid'] = $id;
        $data['jobpemohonid'] = '';
        $data['suamisitriid'] = '';
        $data['jobsuamiistriid'] = '';
        $data['keluargaid'] = '';
        $data['penghasilanid'] = '';
        $data['perbankanid'] = '';
        $data['applicationid'] = '';

        $jobpemohon = JobPemohon::where('pemohonid', '=', $id)->first();
        if (!is_null($jobpemohon)) {
        	$data['jobpemohonid'] = $jobpemohon->id;
        }
        $suamisitri = SuamiIstri::where('pemohonid', '=', $id)->first();
        if (!is_null($suamisitri)) {
        	$data['suamisitriid'] = $suamisitri->id;
        }
        $jobsuamiistri = JobSuamiIstri::where('pemohonid', '=', $id)->first();
        if (!is_null($jobsuamiistri)) {
        	$data['jobsuamiistriid'] = $jobsuamiistri->id;
        }
        $keluarga = Keluarga::where('pemohonid', '=', $id)->first();
        if (!is_null($keluarga)) {
        	$data['keluargaid'] = $keluarga->id;
        }
        $penghasilan = Penghasilan::where('pemohonid', '=', $id)->first();
        if (!is_null($penghasilan)) {
        	$data['penghasilanid'] = $penghasilan->id;
        }
        $perbankan = Perbankan::where('pemohonid', '=', $id)->first();
        if (!is_null($perbankan)) {
        	$data['perbankanid'] = $perbankan->id;
        }
        $application = Application::where('userid', '=', $data['detail']->userid)->get();
        $data['countApp'] = count($application);
        if (!is_null($application))
        {
            if($data['countApp'] == 1)
            {
               $data['applicationid'] = $application[0]->id;
            }
        }

        $data['pemohonid'] = '';

        return view('admin.pemohon.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $pem = Pemohon::find($id);

        $imgPath = Config::get('gradana.pemohon_img_path');
        $realPath = public_path().'/'.$imgPath;
        !file_exists($realPath) ? File::makeDirectory($realPath) : null ;

        if($request->hasFile('ktp_img')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('ktp_img')->getClientOriginalExtension();
            Image::make(Input::file('ktp_img'))->save($realPath.$filename);
            if (File::exists(public_path() .'/'. $pem->ktp_img)) {
                File::delete(public_path() .'/'. $pem->ktp_img);
            }

            $pem->ktp_img = $imgPath.$filename;
        }

        if($request->hasFile('npwp_img')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('npwp_img')->getClientOriginalExtension();
            Image::make($request->npwp_img)->save($realPath.$filename);
            if (File::exists(public_path() .'/'. $pem->npwp_img)) {
                File::delete(public_path() .'/'. $pem->npwp_img);
            }

            $pem->npwp_img = $imgPath.$filename;
        }

        if($request->hasFile('kk_img')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('kk_img')->getClientOriginalExtension();
            Image::make($request->kk_img)->save($realPath.$filename);
            if (File::exists(public_path() .'/'. $pem->kk_img)) {
                File::delete(public_path() .'/'. $pem->kk_img);
            }

            $pem->kk_img = $imgPath.$filename;
        }

        if($request->hasFile('tagihan_listrik')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('tagihan_listrik')->getClientOriginalExtension();
            Image::make($request->tagihan_listrik)->save($realPath.$filename);
            if (File::exists(public_path() .'/'. $pem->tagihan_listrik)) {
                File::delete(public_path() .'/'. $pem->tagihan_listrik);
            }

            $pem->tagihan_listrik = $imgPath.$filename;
        }

        if($request->hasFile('tagihan_air')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('tagihan_air')->getClientOriginalExtension();
            Image::make($request->tagihan_air)->save($realPath.$filename);
            if (File::exists(public_path() .'/'. $pem->tagihan_air)) {
                File::delete(public_path() .'/'. $pem->tagihan_air);
            }

            $pem->tagihan_air = $imgPath.$filename;
        }

        if($request->hasFile('tagihan_telp')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('tagihan_telp')->getClientOriginalExtension();
            Image::make($request->tagihan_telp)->save($realPath.$filename);
            if (File::exists(public_path() .'/'. $pem->tagihan_telp)) {
                File::delete(public_path() .'/'. $pem->tagihan_telp);
            }

            $pem->tagihan_telp = $imgPath.$filename;
        }

        if($request->hasFile('bi_checking')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('bi_checking')->getClientOriginalExtension();
            Image::make($request->bi_checking)->save($realPath.$filename);
            if (File::exists(public_path() .'/'. $pem->bi_checking)) {
                File::delete(public_path() .'/'. $pem->bi_checking);
            }

            $pem->bi_checking = $imgPath.$filename;
        }

        if($request->hasFile('akta_img')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('akta_img')->getClientOriginalExtension();
            Image::make($request->akta_img)->save($realPath.$filename);
            if (File::exists(public_path() .'/'. $pem->akta_img)) {
                File::delete(public_path() .'/'. $pem->akta_img);
            }

            $pem->akta_img = $imgPath.$filename;
        }

        if($request->hasFile('ssp')) {
            $filename = date("ymd").str_random(6).'.'.Input::file('ssp')->getClientOriginalExtension();
            Image::make($request->ssp)->save($realPath.$filename);
            if (File::exists(public_path() .'/'. $pem->ssp)) {
                File::delete(public_path() .'/'. $pem->ssp);
            }

            $pem->ssp = $imgPath.$filename;
        }

        $pem->save();

        $input = $request->except(['ktp_img', 'npwp_img', 'kk_img', 'tagihan_listrik', 'tagihan_air', 'tagihan_telp', 'bi_checking', 'akta_img', 'ssp']);
        $pem->fill($input)->save();

        alert()->success('Pemohon has been updated.');
        return back();
    }
	
	public function approved($id)
    {
        $app = Pemohon::find($id);
        $app->approved = 1;
        //$app->status = "Verified";
        $app->save();

        alert()->success('Pemohon ID '. $app->id .' has been approved.');
        return back();
    }
	
	public function unapproved($id)
    {
        $app = Pemohon::find($id);
        $app->approved = 0;
        //$app->status = "Verified";
        $app->save();

        alert()->success('Pemohon ID '. $app->id .' has been unapproved.');
        return back();
    }
	
	public function ragu($id)
    {
        $app = Pemohon::find($id);
        $app->approved = 2;
        //$app->status = "Verified";
        $app->save();

        alert()->success('Pemohon ID '. $app->id .' has been Updated.');
        return back();
    }

    public function destroy($id)
    {
        $del = Pemohon::find($id);
        JobPemohon::where('pemohonid', $del->id)->delete();
        SuamiIstri::where('pemohonid', $del->id)->delete();
        JobSuamiIstri::where('pemohonid', $del->id)->delete();
        Keluarga::where('pemohonid', $del->id)->delete();
        Penghasilan::where('pemohonid', $del->id)->delete();
        $pb = Perbankan::where('pemohonid', $del->id)->first();
        if (!is_null($pb)) {
            Perbankan_child::where('perbankanid', $pb->id)->delete();
            $pb->delete();
        }
        $del->delete();

        if (File::exists(public_path() .'/'. $del->ktp_img)) {
            File::delete(public_path() .'/'. $del->ktp_img);
        }

        if (File::exists(public_path() .'/'. $del->npwp_img)) {
            File::delete(public_path() .'/'. $del->npwp_img);
        }

        if (File::exists(public_path() .'/'. $del->kk_img)) {
            File::delete(public_path() .'/'. $del->kk_img);
        }

        if (File::exists(public_path() .'/'. $del->tagihan_listrik)) {
            File::delete(public_path() .'/'. $del->tagihan_listrik);
        }

        if (File::exists(public_path() .'/'. $del->tagihan_air)) {
            File::delete(public_path() .'/'. $del->tagihan_air);
        }

        if (File::exists(public_path() .'/'. $del->tagihan_telp)) {
            File::delete(public_path() .'/'. $del->tagihan_telp);
        }

        if (File::exists(public_path() .'/'. $del->bi_checking)) {
            File::delete(public_path() .'/'. $del->bi_checking);
        }

        if (File::exists(public_path() .'/'. $del->akta_img)) {
            File::delete(public_path() .'/'. $del->akta_img);
        }

        if (File::exists(public_path() .'/'. $del->ssp)) {
            File::delete(public_path() .'/'. $del->ssp);
        }

        alert()->success('Data Pemohon has been completely deleted.');
        return redirect('/admin/pemohon');
    }
}
