<?php

namespace App\Http\Controllers;

use App\Application;
use App\Assesment;
use App\Helpers\Score;
use App\Http\Requests;
use App\Scoring;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Input;
use Redirect;
use Sentinel;
use Session;

class AssesmentController extends JoshController
{
    public function index()
    {
        $data['asses'] = Assesment::with('application.user.penghasilan')->get();

        return view('admin.assesment.index', $data);
    }

    public function create()
    {
        $data['scoring'] = Scoring::whereNotExists(function ($query) {
                $query->select(DB::raw('id'))
                      ->from('assesments')
                      ->whereRaw('assesments.appid = scorings.appid');
        })->get();

        if (count($data['scoring']) == 0) {
            alert()->success('All Credit Scoring has been scored');
            return redirect('/admin/assesment');
        }

        $data['app'] = Application::all();
        $data['rating'] = $this->rating;

        return view('admin.assesment.create', $data);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        Assesment::create($input);

        $application = Application::where('id', $request->appid)->first();
        $detail = Scoring::where('appid', $application->id)->first();

        $ph = Scoring::with('application.user.penghasilan')->get();
        $app = $detail->application;
        $period = (int)$app->periode;
        $jangkaWaktu = ($period * 12) + (int)$app->jangka_waktu;
        $pinjaman = (int)$app->pinjaman;
        $appid = $app->id;

        $annumMax = $request->interest_annum_max;

        $thp = (int)$detail->application->user->penghasilan->first()->pemohon;
        $user = $detail->application->user;

        $netPlafon = Score::getPlafon($thp) - $detail->jml_hutang;
        $percentJmlHtg = ($detail->jml_hutang / $thp) * 100;
        $scoreJmlHtg = Score::getJmlHutang($percentJmlHtg);
        $cscoreJmlHtg = $scoreJmlHtg * 0.2;

        $first = (1 + $annumMax) / 100;
        $second = pow($first, $period);
        $third = $second * $pinjaman;
        $pinjHI = ($third) / $jangkaWaktu;
        $scoreHI = Score::getHI($netPlafon, $pinjHI);
        $cscoreHI  = $scoreHI * 0.3;

        $scoreCC = Score::getCC($detail->jml_cc);
        if($detail->jml_cc == "5 nilai - 0 kartu kredit") {
            $wscoreCC = "7.5%";
            $cscoreCC = $scoreCC * 0.075;;
        } else {
            $wscoreCC = "5.0%";
            $cscoreCC = $scoreCC * 0.05;
        }

        $scoreLimit = Score::getTotLimit($detail->limit_cc, $thp);
        if($detail->jml_cc == "5 nilai - 0 kartu kredit") {
            $wscoreLimit = "-";
            $cscoreLimit = "0";
            $scoreLimit = "-";
        } else {
            $wscoreLimit = "2.5%";
            $cscoreLimit = $scoreLimit * 0.025;
        }

        $scoreLamaKerja = Score::getLamaKerja($detail->lama_kerja);
        $cscoreLamaKerja = $scoreLamaKerja * 0.075;

        $scoreTotLamaKerja = Score::getTotLamaKerja($detail->total_lama_kerja);
        $cscoreTotLamaKerja = $scoreTotLamaKerja * 0.075;

        $scoreSaldo = Score::getSaldo3Bln($detail->saldo_3_bln, $thp);
        if($scoreSaldo >= 3) {
            $cscoreSaldo = $scoreSaldo * 0.175;
            $wscoreSaldo = "17.5%";
        } elseif ($scoreSaldo < 3) {
            $cscoreSaldo = $scoreSaldo * 0.125;
            $wscoreSaldo = "12.5%";
        }

        $scoreLengkap = Score::getKelengkapan($detail->kelengkapan_data);
        $cscoreLengkap = $scoreLengkap * 0.05;

        $scoreAkurasi = Score::getAkurasi($detail->akurasi_data);
        $cscoreAkurasi = $scoreAkurasi * 0.05;

        $scoreAssetBank = Score::getAsset($detail->jumlah_asset_bank, $pinjaman);
        if($scoreSaldo >= 3) {
            $scoreAssetBank = "-";
            $cscoreAssetBank = "0";
            $wscoreAssetBank = "-";
        } elseif ($scoreSaldo < 3) {
            $cscoreAssetBank = $scoreAssetBank * 0.05;
            $wscoreAssetBank = "5.0%";
        }

        $ave = $cscoreJmlHtg + $cscoreHI + $cscoreCC + $cscoreLimit + $cscoreLamaKerja + $cscoreTotLamaKerja + $cscoreSaldo + $cscoreLengkap + $cscoreAkurasi + $cscoreAssetBank;

        $detail->pinj_high_interest = $pinjHI;
        $detail->save();

        $asses = $detail->application->assesment;
        $asses->credit_rating = round($ave);
        $asses->save();
        // $input['credit_rating'] = round($ave);

        alert()->success('Credit Assesment has been created.');
        return redirect('/admin/assesment');
    }

    public function show($id)
    {
        $data['detail'] = Assesment::with('application.user', 'application.scoring')->find($id);
        // dd($data['detail']->application->scoring->id);
        $data['app'] = Application::all();
        $pinjaman = $data['detail']->application->pinjaman;
        $periode = $data['detail']->application->periode;
        $data['aInterest'] = ainterest($data['detail']);

        $data['user'] = $data['detail']->application->user;
        $data['app'] = $data['detail']->application;
        $data['thp'] = $data['detail']->application->user->penghasilan->first()->pemohon;
        $data['netPlafon'] = Score::getPlafon($data['thp']) - $data['detail']->application->scoring->jml_hutang;

        $satu = pow(1+$data['aInterest'], $periode + ((int)$data['detail']->jangka_waktu / 12))/100;
        $dua = $pinjaman * $satu;
        $tiga = 12 * $periode + ((int)$data['detail']->jangka_waktu / 12);
        $data['aInstallment'] = round($dua/$tiga);

        $data['detail']->actual_interest = $data['aInterest'];
        $data['detail']->installment =  $data['aInstallment'];
        $data['detail']->save();

        if($data['detail']->scoring) {
            $data['klik'] = '/admin/scoring/edit/'.$data['detail']->scoring->id;
        } else {
            $data['klik'] = '/admin/scoring/create';
        }

        return view('admin.assesment.show', $data);
    }

    public function edit($id)
    {
        $data['detail'] = Assesment::find($id);
        $data['app'] = Application::all();
        $data['rating'] = $this->rating;

        if($data['detail']->scoring) {
            $data['klik'] = '/admin/scoring/edit/'.$data['detail']->scoring->id;
        } else {
            $data['klik'] = '/admin/scoring/create';
        }

        return view('admin.assesment.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $detail = Assesment::find($id);

        $input = $request->all();
        $detail->fill($input)->save();

        alert()->success('Credit Assesment has been updated.');
        return back();//redirect('/admin/assesment');
    }

    public function destroy($id)
    {
        Assesment::find($id)->delete();

        alert()->success('Delete Credit Assesment success.');
        return redirect('/admin/assesment');
    }
}
