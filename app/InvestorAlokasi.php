<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvestorAlokasi extends Model
{
    protected $table = 'investoralokasi';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function investor()
    {
        return $this->belongsTo(Investor::class, 'investorid');
    }

    public function investorPayments()
    {
        return $this->hasMany(InvestorPayment::class, 'alokasiid');
    }

    public function detail()
    {
        return url("/admin/investor/alokasi/detail/{$this->id}");
    }

    public function detailInves()
    {
        return url("/admin/investor/detail/{$this->investor->id}");
    }

    public function editUrl()
    {
        return url("/admin/investor/alokasi/edit/{$this->id}");
    }

    public function delUrl()
    {
        return url("/admin/investor/alokasi/delete/{$this->id}");
    }

    public function paymentUrl()
    {
        return url("/admin/investorpayment/create/{$this->id}");
    }
}
