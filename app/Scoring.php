<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scoring extends Model
{
    protected $table = 'scorings';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function application()
    {
        return $this->belongsTo(Application::class, 'appid');
    }

    public function editUrl()
    {
        return url("/admin/scoring/edit/{$this->id}");
    }

    public function delUrl()
    {
        return url("/admin/scoring/delete/{$this->id}");
    }

    public function detailUrl()
    {
        return url("/admin/scoring/detail/{$this->id}");
    }

    public function assesUrl()
    {
        return url("/admin/assesment/detail/{$this->application->assesment->id}");
    }
}
