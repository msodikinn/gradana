<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sentinel;

class Application extends Model
{
    protected $dateFormat = 'U';

    protected $table = 'applications';

    protected $guarded = ['id'];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class, 'userid');
    }

    public function pemohon()
    {
        return $this->belongsTo(Pemohon::class, 'pemohonid');
    }

    public function pembayarans()
    {
        return $this->hasMany(Pembayaran::class, 'appid');
    }

    public function developer()
    {
        return $this->belongsTo(Developer::class, 'devid');
    }

    public function investor()
    {
        return $this->belongsTo(Investor::class, 'investorid');
    }

    public function property()
    {
        return $this->belongsTo(Property::class, 'propid');
    }

    public function scoring()
    {
        return $this->hasOne(Scoring::class, 'appid');
    }

    public function assesment()
    {
        return $this->hasOne(Assesment::class, 'appid');
    }

    public function penghasilan()
    {
        return $this->hasOne(Penghasilan::class, 'userid');
    }

    public function survey()
    {
        return $this->hasOne(Survey::class, 'appid');
    }

    public function createUrl()
    {
        return url("/admin/pembayaran/create/{$this->id}");
    }

    public function editUrl()
    {
        return url("/admin/application/edit/{$this->id}");
    }

    public function delUrl()
    {
        return url("/admin/application/delete/{$this->id}");
    }

    public function detailUrl()
    {
        return url("/admin/application/detail/{$this->id}");
    }

    public function payUrl()
    {
        return url("/admin/pembayaran/create/{$this->id}");
    }
	
	public function scoreUrl()
    {
        return url("/admin/application/scored/{$this->id}");
    }
	public function sendUrl()
    {
        return url("/admin/application/sends/{$this->id}");
    }

    public function userUrl()
    {
        return url("/admin/users/{$this->user->id}");
    }

    public function devUrl()
    {
        if ($this->developer) {
            return url("/admin/developer/detail/{$this->developer->id}");
        }
        return null;
    }

    public function propUrl()
    {
        if ($this->property) {
            return url("/admin/property/detail/{$this->property->id}");
        }
        return null;
    }

    public function invesUrl()
    {
        if ($this->investor) {
            return url("/admin/investor/detail/{$this->investor->id}");
        }
        return null;
    }

    public function approved()
    {
        return url("/admin/application/approved/{$this->id}");
    }

    public function unapproved()
    {
        return url("/admin/application/unapproved/{$this->id}");
    }

    public function borrDetail()
    {
        return url("/admin/investor/borrower/{$this->id}");
    }

    public function apl()
    {
        if(Sentinel::getUser()->status == "Admin"){
            $apl = Application::all();
        }else{
            $apl = Application::where('userid', Sentinel::getUser()->id)->get();
        }

        return $apl;
    }

    public function danaUrl()
    {
        return url("/admin/investorpayment/create/{$this->id}");
    }
}
