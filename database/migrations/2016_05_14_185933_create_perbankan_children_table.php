<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerbankanChildrenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perbankan_children', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('perbankanid');
            $table->integer('nasabah_bulan');
            $table->integer('nasabah_tahun');
            $table->string('jenis');
            $table->string('nama_bank');
            $table->string('rekening_kartu');
            $table->integer('saldo_limit_plafon');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('perbankan_children');
    }
}
