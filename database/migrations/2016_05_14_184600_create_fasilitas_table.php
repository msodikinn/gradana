<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFasilitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fasilitas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid');
            $table->string('jenis');
            $table->string('tujuan');
            $table->string('sis_bayar');
            $table->biginteger('pinjaman');
            $table->string('jangka_waktu');
            $table->biginteger('harga_beli');
            $table->biginteger('uang_muka');
            $table->string('renovasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fasilitas');
    }
}
