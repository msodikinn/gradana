<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemohonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemohons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid');
            $table->integer('pemohonid');
            $table->string('fullname_clean');
            $table->string('tmp_lahir');
            $table->string('ktp');
            $table->string('npwp');
            $table->string('nama_ibu');
            $table->string('status_kawin');
            $table->string('tanggungan');
            $table->string('pendidikan');
            $table->string('status_rumah');
            $table->string('alamat_ktp');
            $table->string('kelurahan_ktp');
            $table->string('kecamatan_ktp');
            $table->string('propinsi_ktp');
            $table->string('kota_ktp');
            $table->string('postal_ktp');
            $table->string('telp');
            $table->string('fax');
            $table->string('hp1');
            $table->string('hp2');
            $table->string('facebook_id');
            $table->string('linkedin_id');
            $table->biginteger('amount_balance');
            $table->string('ktp_img');
            $table->string('npwp_img');
            $table->string('kk_img');
            $table->string('tagihan_listrik');
            $table->string('tagihan_air');
            $table->string('tagihan_telp');
            $table->string('surat_ket_kerja');
            $table->string('bi_checking');
            $table->string('akta_img');
            $table->string('ssp');
            $table->boolean('cust_verified');
            $table->date('cust_verified_date');
            $table->integer('jml_anggota');
            $table->biginteger('simp_wajib');
            $table->biginteger('simp_pokok');
            $table->string('sumber_dana');
            $table->string('bentuk_usaha');
            $table->string('nomor_akta');
            $table->string('domisili_akta');
            $table->string('nama_penanggungjawab');
            $table->string('contact_person');
            $table->string('contact_position');
            $table->boolean('approved')->default(0);
            $table->timestamps();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pemohons');
    }
}
