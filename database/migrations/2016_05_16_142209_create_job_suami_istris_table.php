<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobSuamiIstrisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_suami_istris', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid');
            $table->integer('pemohonid');
            $table->string('jenis_pekerjaan');
            $table->string('nama_perusahaan');
            $table->string('bidang_usaha');
            $table->string('bidang_usaha_detail');
            $table->string('tanggal_pendirian');
            $table->string('alamat');
            $table->string('kelurahan');
            $table->string('kecamatan');
            $table->string('kota');
            $table->string('postal');
            $table->string('tlp_ktr_hunting');
            $table->string('tlp_ktr_direct');
            $table->string('fax');
            $table->string('unit_kerja');
            $table->string('jabatan');
            $table->string('bulan');
            $table->string('tahun');
            $table->biginteger('omset_bln');
            $table->string('kepemilikan');
            $table->biginteger('margin_untung');
            $table->string('nama_perusahaan_ago');
            $table->string('jenis_usaha_ago');
            $table->string('jabatan_ago');
            $table->string('unit_kerja_ago');
            $table->string('telp');
            $table->string('bulan_ago');
            $table->string('tahun_ago');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_suami_istris');
    }
}
