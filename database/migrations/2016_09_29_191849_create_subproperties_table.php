<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubpropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subproperties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('propid');
            $table->string('tipe');
            $table->string('luas_tanah');
            $table->string('luas_bangunan');
            $table->biginteger('range_harga_min');
            $table->biginteger('range_harga_max');
            $table->integer('jml_unit_available');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subproperties');
    }
}
