<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid');
            $table->integer('devid');
            $table->string('propid');
            $table->string('investorid');
            $table->string('tujuan');
            $table->string('jenis');
            $table->biginteger('uang_muka');
            $table->string('sis_bayar');
            $table->biginteger('pinjaman');
            $table->biginteger('pinjaman_bln');
            $table->integer('periode');
            $table->integer('jangka_waktu');
            $table->string('status');
            $table->biginteger('outstanding_loan');
            $table->date('installment_times');
            $table->string('renovasi');
            $table->string('jaminan');
            $table->integer('invoice_qty');
            $table->date('jatuh_tempo');
            $table->date('invoice_date');
            $table->date('invoice_client');
            $table->string('approved');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('applications');
    }
}
