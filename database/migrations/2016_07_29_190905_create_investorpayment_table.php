<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestorpaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investorpayments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('appid');
            $table->integer('invesid');
            $table->string('status_pembayaran');
            $table->date('tgl_pembayaran');
            $table->biginteger('amount');
            $table->text('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('investorpayments');
    }
}
