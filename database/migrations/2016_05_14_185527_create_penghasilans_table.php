<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenghasilansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penghasilans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid');
            $table->integer('pemohonid');
            $table->string('joint_income');
            $table->string('pemohon');
            $table->string('suami_istri');
            $table->string('tambahan');
            $table->string('biaya_rt');
            $table->string('angsuran_lain');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('penghasilans');
    }
}
