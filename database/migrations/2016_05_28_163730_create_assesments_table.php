<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssesmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assesments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('appid');
            $table->string('analisa');
            $table->string('borrower_summary'); //pake editor
            $table->integer('credit_rating');
            $table->decimal('interest_annum_max', 3, 1);
            $table->decimal('interest_annum_min', 3, 1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('assesments');
    }
}
