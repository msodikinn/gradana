<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoringsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scorings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assesid');
            $table->bigInteger('jml_hutang');
            $table->bigInteger('pinj_high_interest');
            $table->string('jml_cc');
            $table->bigInteger('limit_cc');
            $table->string('lama_kerja');
            $table->string('total_lama_kerja');
            $table->bigInteger('saldo_3_bln');
            $table->string('kelengkapan_data');
            $table->string('akurasi_data');
            $table->bigInteger('jumlah_asset_bank');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('scorings');
    }
}
