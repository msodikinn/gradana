<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid');
            $table->integer('devid');
            $table->string('nama');
            $table->string('alamat');
            $table->double('lat',20,10);
            $table->double('lng',20,10);
            $table->string('deskripsi');
            $table->string('tipe');
            $table->integer('luas_tanah');
            $table->integer('luas_bangunan');
            $table->string('status');
            $table->biginteger('harga_beli');
            $table->biginteger('harga_jual');
            $table->string('nomor_hgb');
            $table->string('pemilik_terdaftar');
            $table->integer('luas_lahan');
            $table->date('hgb_expire_date');
            $table->string('lokasi');
            $table->string('notes');
            $table->string('nomor_sph_aph');
            $table->date('tanggal_sph_aph');
            $table->string('pihak');
            $table->boolean('approved')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
    }
}
