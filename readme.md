# Gradana.com

## Configuration ##

Configuration file ada di `.env`. Bila file `.env` tidak ada, silahkan copy dari `.env.example`.

## Date Format ##
Untuk format tanggal gunakan helper `format($variable)`

## Email ##
* MAIL_DRIVER=smtp
* MAIL_HOST=mailtrap.io
* MAIL_PORT=2525
* MAIL_USERNAME=<username>
* MAIL_PASSWORD=<password>
* MAIL_ENCRYPTION=null

## VueJs ##
1. npm install vue
2. npm install vue-resource
3. npm install vue-router
4. npm install
5. gulp (optional)
6. tunggu proses gulp selesai

## Route path ##

Path route dibedakan menjadi dua:

* Route admin: `app/Http/routes/admin.php`
* Route frontend: `app/Http/routes/front.php`
