@extends('emails/layouts/default')

@section('content')
<p>Terima kasih atas registrasi Anda sebagai {{$register_as}} di Gradana.</p>

@if ($register_as == 'Investor')
<p>Terlampir adalah ketentuan perjanjian yang Anda perlu pahami dan setujui. Print dokumen terlampir, bubuhkan tanda tangan Anda lalu kirimkan scanned documents tersebut ke admin@gradana.com.</p>
<p>Klik <a href='http://www.gradana.com/my-account'>link</a> berikut untuk melengkapi form pemberian pinjaman anda.<br>admin kami akan segera memverifikasi dan menghubungi anda dalam kurun waktu 1x24 jam pada hari kerja.</p>
@else
<!--p> Klik <a href='http://www.gradana.com/my-account'> Link </a> Berikut untuk melengkapi form aplikasi anda. <br> 1.Anda dapat mengisi form singkat dan dihubungi oleh admin kami. <br> 2.mengisi form lengkap untuk langsung kami verifikasi.	</p-->

<p> Borrower Name: {{$register_as}} </p>
<p> Email: {{$email}} </p>
<p> Fullname: {{$fullname_clean}} </p>
<p> jenis Pekerjaan: {{$jenis_pekerjaan}} </p>
<p> kantor: {{$kantor}} </p>

@endif
<p>Best regards,</p>

<p>Gradana Team</p>
@stop
