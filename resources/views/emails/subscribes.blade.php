<body text="#000000">
<table width="700" cellspacing="0" cellpadding="10" align="center">
  <tr>
    <td>
      <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
        <img src="{{ asset('assets/img/gradana-logo-horizontal.jpg') }}" width="300" height="50">
      </font>
    </td>
  </tr>
  <tr>
    <td>
      <p>
      <font face="Verdana, Arial, Helvetica, sans-serif" size="2">
        Hai,<br><br>
        Terima kasih telah mendaftar di gradana.com, Kami akan menghubungi Anda setelah website ready untuk live.<br>
        <br>
        Thanks,<br>
        GRADANA.COM - New Mortgage Financing Solution
      </font>
      </p>
      <center>
        <font face="Verdana, Arial, Helvetica, sans-serif" size="4"><b>CONNECT WITH US</b></font><br><br>
        <a href="https://www.facebook.com/profile.php?id=100012728464456">
          <img src="{{ asset('assets/img/facebook.png') }}" width="40" height="40">
        </a>
        <a href="https://twitter.com/gradanaweb">
          <img src="{{ asset('assets/img/twitter.png') }}" width="40" height="40">
        </a>
        <a href="https://www.linkedin.com/in/gradana-property-8121a1124">
          <img src="{{ asset('assets/img/linkedin.png') }}" width="40" height="40">
        </a>
      </center>
    </td>
  </tr>
</table>
</body>
