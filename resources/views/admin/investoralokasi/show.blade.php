@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Detail Lender
@parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link href="{{ asset('assets/vendors/modal/css/component.css') }}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/modal/css/component.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/fancybox/jquery.fancybox.css') }}" media="screen" />
@stop

@section('content')
<section class="content-header">
    <h1>Detail Alokasi Dana</h1>
</section>

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <div class="panel-title pull-left">
                    <div class="caption">
                        <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Detail {{ $detail->nama }}
                    </div>
                </div>
                <div class="pull-right">
                    <a href="/admin/investor/alokasi" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Lender</a>
                    <a href="{{ $detail->editUrl() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-minus"></span> Edit Lender</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                {{-- <div class="col-md-12"> --}}
                    <table class="table">
                        <tr><td style="width: 150px">ID Alokasi</td><td>{{ $detail->id }}</td></tr>
                        <tr><td>Nama Lender</td><td><a href="{{ $detail->detailInves() }}">{{ $inves->nama }}</a></td></tr>
                        <tr><td>Alamat</td><td>{{ $inves->alamat }}</td></tr>
                        <tr><td>Email</td><td>{{ $inves->email }}</td></tr>
                        <tr><td>Telp. Rumah</td><td>{{ $inves->telp }}</td></tr>
                        <tr><td>Handphone</td><td>{{ $inves->hp }}</td></tr>
                        <tr><td>Alokasi Dana</td><td>{{ uang($detail->alokasi_dana) }}</td></tr>
                        <tr><td>Jumlah Alokasi</td><td>{{ $detail->jml_alokasi }}</td></tr>
                        <tr><td>Tenor</td><td>{{ $detail->tenor }}</td></tr>
                        <tr><td>Penempatan Dana</td><td>{{ $detail->penempatan_dana }}</td></tr>
                    </table>
                {{-- </div> --}}
            </div>
        </div>

        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <div class="panel-title pull-left">
                    <div class="caption">
                        <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Payment {{ $detail->nama }}
                    </div>
                </div>
            </div>
            <br />
            <div class="panel-body">
                {{-- <div class="col-md-12"> --}}
                <table class="table">
                    @if (!is_null($payment))
                        @foreach ($payment as $p)
                            <tr>
                                <td style="width: 150px">Payment ID</td>
                                <td>Application ID</td>
                                <td>Status Pembayaran</td>
                                <td>Tanggal</td>
                                <td>Amount</td>
                                <td>Keterangan</td>
                                <td>Approved</td>
                            </tr>
                            <tr><td><a href="{{ $p->detailUrl() }}">{{ $p->id }}</a></td>
                                <td><a href="{{ $p->appUrl() }}">{{ $p->appid }}</a></td>
                                <td>{{ $p->status_pembayaran }}</td>
                                <td>{{ $p->tgl_pembayaran }}</td>
                                <td>{{ uang($p->amount) }}</td>
                                <td>{{ $p->keterangan }}</td>
                                <td>{{ approve($p->approved) }}</td>
                            </tr>
                        @endforeach
                    @else
                        <h3 align="center">-- Empty Data --</h3>
                    @endif
                </table>


                {{-- </div> --}}
            </div>
        </div>
    </div>
</section>

@stop

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
    <script src="{{ asset('assets/vendors/modal/js/classie.js') }}" ></script>
    <script src="{{ asset('assets/vendors/fancybox/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-thumbs.js') }}" ></script>
    <script src="{{ asset('assets/js/pages/gallery.js') }}"  ></script>
    <script src="{{ asset('assets/vendors/modal/js/modalEffects.js') }}" ></script>
@stop
