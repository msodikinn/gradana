@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Alokasi Dana
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
@stop

@section('content')
    <section class="content-header">
        <h1>Manage Alokasi Dana</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary filterable">
                    <div class="panel-heading clearfix  ">
                      <div class="panel-title pull-left">
                        <div class="caption">
                          <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> List Alokasi Dana
                        </div>
                      </div>
                      <div class="pull-right">
                        <a href="/admin/investor/alokasi/create" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> Create Alokasi Dana</a>
                      </div>
                    </div>
                    <div class="panel-body table-responsive">
                        <p style="font-size:16px;margin-top:0px;margin-bottom:20px;">Berikut adalah list data keuangan lender yang telah dialokasikan untuk membiayai peminjaman.</p>
                        <table class="table table-striped table-bordered" id="table1">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama Lender</th>
                                    <th>Email</th>
                                    <th>Alokasi Dana</th>
                                    <th>Jumlah Alokasi</th>
                                    <th>Tenor</th>
                                    <th>Penempatan Dana</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($alokasi as $a)
                                    <tr role="row" class="odd">
                                        <td><a href="{{ $a->detail() }}">{{ $a->id }}</a></td>
                                        <td>{{ $a->investor->nama }}</td>
                                        <td>{{ $a->investor->email }}</td>
                                        <td>{{ uang($a->alokasi_dana) }}</td>
                                        <td>{{ $a->jml_alokasi }}</td>
                                        <td>{{ $a->tenor }}</td>
                                        <td>{{ $a->penempatan_dana }}</td>
                                        <td><a class="edit" href="{{ $a->editUrl() }}">Edit</a></td>
                                        <td><a class="edit" href="{{ $a->delUrl() }}">delete</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
@stop
