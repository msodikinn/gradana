@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Available Borrowers
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/theme.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/theme-elements.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/vendors/modal/css/component.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/fancybox/jquery.fancybox.css') }}" media="screen" />


    <link rel="stylesheet" href="{{ asset('assets/frontend/css/skins/default.css') }}">

    <script src="{{ asset('assets/frontend/vendor/modernizr/modernizr.min.js') }}"></script>
    <style>
      .arrow {
          background: "";
          width: 0px;
          height: 0px;
          display: table;
          position: relative;
      }
      .thumb-info .thumb-info-title {
          font-size: 14px;
      }

      .fancybox-effects-a {
          display: block;
          max-width:100%;
          width: auto;
          max-height:295px;
          height: auto;
      }
    </style>
@stop

@section('content')
        <section class="content-header">
            <h1>Available Borrowers</h1>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary filterable">
                        <div class="panel-heading clearfix  ">
                          <div class="panel-title pull-left">
                            <div class="caption">
                              <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Silahkan Pilih Borrower yang akan didanai
                            </div>
                          </div>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <p style="font-size:16px;margin-top:0px;margin-bottom:20px;">Berikut adalah list data peminjam yang sudah aktif dan belum dibiayai oleh lender.</p>
                            </div>
                            @if (count($app) > 0)
                                <?php $i=0; ?>
                                @foreach ($app as $a)
                                    <div class="col-md-4" style="margin-bottom: 15px">
                                        <span class="thumb-info thumb-info-hide-wrapper-bg">
                                            <span class="thumb-info-wrapper">
                                                @if ($a->pic == null)
                                                    <center><img src="{{ defaultpic() }}" class="img-responsive"></center>
                                                @else
                                                    <center><a href="{{'/'.$a->user->pic}}" class="fancybox-effects-a"><img src="{{'/'.$a->user->pic}}" class="img-responsive"></a></center>
                                                @endif
                                                <span class="thumb-info-title">
                                                    <span class="thumb-info-inner">{{$a->fullname}}</span>
                                                </span>
                                            </span>
                                            <span class="thumb-info-wrapper">
                                                <span class="thumb-info-caption-text">
                                                    <div class="table-responsive" style="max-height: 125px; overflow-y: scroll;">
                                                        <table border="0" width="100%">
                                                            @if (count($a->property))
                                                                <tr>
                                                                    <td><b>Nama Property:</b></td>
                                                                    <td><a href="{{ $a->property->detailUrl() }}" target="blank">{{$a->property->nama}}</a></td>
                                                                </tr>
                                                            @endif
                                                            <tr>
                                                                <td style="width:140px"><b>Pengajuan Pinjaman:</b></td>
                                                                <td>IDR {{ uang($a->pinjaman) }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Durasi Pinjaman:</b></td>
                                                                <td>{{ $durasi[$i] }} bulan</td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Total Bunga:</b></td>
                                                                {{-- if aplikasi belum dibuatkan assesment --}}
                                                                @if (isset($a->assesment))
                                                                    <td>{{ $a->assesment->actual_interest }} %</td>
                                                                @else
                                                                    <td>Not available </td>
                                                                @endif
                                                            </tr>
                                                            <tr>
                                                                <td><a href="{{ $a->borrDetail() }}" target="blank">Selengkapnya >></a></td>
                                                                <td align="right">
                                                                    <a
                                                                      data-toggle="modal"
                                                                      data-href="#alokasi{{$i}}"
                                                                      href="#alokasi{{$i}}"
                                                                      {{-- href="{{ $a->danaUrl() }}" --}} class="btn btn-primary btn-sm"
                                                                      id="dana">
                                                                        Danai
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </span>
                                            </span>
                                        </span>
                                    </div>

                                    {{-- CREATE LOOPING MODAL --}}
                                    <div class="modal fade in" id="alokasi{{$i}}" role="dialog" aria-hidden="false" style="display:none;">
                                        <?php $i++; ?>
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Available Alokasi Dana</h4>
                                                </div>
                                                <div class="modal-body">
                                                    {!! Form::open(array('url' => route('pChildCreate'), 'method' => 'post', 'class' => 'form-horizontal form-bordered')) !!}
                                                        <table class="table table-striped table-bordered" id="table2">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Investor ID</th>
                                                                    <th>Alokasi Dana</th>
                                                                    <th>Jumlah Alokasi</th>
                                                                    <th>Tenor</th>
                                                                    <th>Penempatan Dana</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($alokasi as $l)
                                                                    <tr role="row" class="odd">
                                                                        <td>{{ $l->id }}</a></td>
                                                                        <td>{{ $l->investor->nama }}</td>
                                                                        <td>{{ $l->alokasi_dana }}</td>
                                                                        <td>{{ $l->jml_alokasi }}</td>
                                                                        <td>{{ $l->tenor }}</td>
                                                                        <td>{{ $l->penempatan_dana }}</td>
                                                                        <td><a href="{{ $l->paymentUrl().'/'.$a->id }}" title="">wer</a></td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- END MODAL --}}

                                @endforeach
                            @else
                                <center><h3>Data Kosong</h3></center>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </section>
    @stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-thumbs.js') }}"></script>
    <script src="{{ asset('assets/js/pages/gallery.js') }}"></script>
    <script src="{{ asset('assets/vendors/modal/js/modalEffects.js') }}"></script>
    <script>
        $(document).ready(function() {
            $("#dana").click(function() {
                var appid=20;
                localStorage.setItem('appid', appid);
            });
        });
        // $(document).ready(function(){
        //     $("input[type='checkbox'],input[type='radio']").iCheck({
        //         checkboxClass: 'icheckbox_minimal-blue',
        //         radioClass: 'iradio_minimal-blue'
        //     });
        // });
    </script>
@stop
