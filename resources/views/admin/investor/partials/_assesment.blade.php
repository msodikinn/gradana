                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            Detail Assesment
                                        </h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table ">
                                                    <tr><td width="180">ID</td><td>{{ $asses->id }}</td></tr>
                                                    <tr><td>Application ID</td><td><a href="{{ $asses->application->detailUrl() }}">{{ $asses->application->id }}</a></td></tr>
                                                    <tr><td>Analisa</td><td>{{ $asses->analisa }}</td></tr>
                                                    <tr><td>Borrower Summary</td><td>{{ $asses->borrower_summary }}</td></tr>
                                                    <tr><td>Credit Rating</td><td>{{ $asses->credit_rating }}</td></tr>
                                                    <tr><td>Interest Annum Min</td><td>{{ $asses->interest_annum_min }}%</td></tr>
                                                    <tr><td>Interest Annum Max</td><td>{{ $asses->interest_annum_max }}%</td></tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
