                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            Detail Credit Scoring
                                        </h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table">
                                                    <tbody>
                                                        <tr>
                                                            <td width="200">ID</td>
                                                            <td>{{ $rating->id }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Assesment ID</td>
                                                            <td><a href="{{ $rating->assesUrl() }}">{{ $rating->assesid }}</a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Jumlah Hutang yang Sedang Berjalan</td>
                                                            <td>{{ uang($rating->jml_hutang) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Pinjaman Highest Interest</td>
                                                            <td>{{ uang($rating->pinj_high_interest )}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Jumlah Kartu Kredit</td>
                                                            <td>{{ $rating->jml_cc }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Limit Kartu Kredit</td>
                                                            <td>{{ uang($rating->limit_cc) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Lama Bekerja di pekerjaan sekarang</td>
                                                            <td>{{ $rating->lama_kerja }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Lama Bekerja</td>
                                                            <td>{{ $rating->total_lama_kerja }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Saldo 3 Bulan</td>
                                                            <td>{{ uang($rating->saldo_3_bln) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Kelengkapan Data</td>
                                                            <td>{{ $rating->kelengkapan_data }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Akurasi Data</td>
                                                            <td>{{ $rating->akurasi_data }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Jumlah Asset Bank</td>
                                                            <td>{{ uang($rating->jumlah_asset_bank) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Credit Rating</td>
                                                            <td>{{ $rating->assesment->credit_rating }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
