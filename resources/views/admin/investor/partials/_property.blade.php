                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            Detail Property
                                        </h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            {{-- <div id="map-canvas" style="width: 100%; height: 350px;"></div> --}}
                                            <div class="col-md-6" style="margin-top: 20px">
                                                <table class="table">
                                                    <tr><td>Nama</td><td>{{ $property->nama }}</td></tr>
                                                    <tr><td>Developer</td><td><a target="blank" href="{{ $property->devUrl() }}">{{ $property->developer->nama }}</a></td></tr>
                                                    <tr><td>Alamat</td><td>{{ $property->alamat }}</td></tr>
                                                    <tr><td>Latitude</td><td>{{ $property->lat }}</td></tr>
                                                    <tr><td>Longitude</td><td>{{ $property->lng }}</td></tr>
                                                    <tr><td>Deskripsi</td><td>{{ $property->deskripsi }}</td></tr>
                                                    <tr><td>Tipe</td><td>{{ $property->tipe }}</td></tr>
                                                    <tr><td>Luas Tanah</td><td>{{ $property->luas_tanah }}</td></tr>
                                                    <tr><td>Luas Bangunan</td><td>{{ $property->luas_bangunan }}</td></tr>
                                                    <tr><td>Status</td><td>{{ $property->status }}</td></tr>
                                                </table>
                                            </div>
                                            <div class="col-md-6" style="margin-top: 20px">
                                                <table class="table">
                                                    <tr><td style="width: 150px">Harga Beli</td><td>{{ $property->harga_beli }}</td></tr>
                                                    <tr><td>Harga Jual</td><td>{{ $property->harga_jual }}</td></tr>
                                                    <tr><td>No. HGB</td><td>{{ $property->nomor_hgb }}</td></tr>
                                                    <tr><td>Pemilik Terdaftar</td><td>{{ $property->pemilik_terdaftar }}</td></tr>
                                                    <tr><td>Luas Lahan</td><td>{{ $property->luas_lahan }}</td></tr>
                                                    <tr><td>HGB Expire Date</td><td>{{ format($property->hgb_expire_date) }}</td></tr>
                                                    <tr><td>Lokasi</td><td>{{ $property->lokasi }}</td></tr>
                                                    <tr><td>Notes</td><td>{{ $property->notes }}</td></tr>
                                                    <tr><td>No. SPH APH</td><td>{{ $property->nomor_sph_aph }}</td></tr>
                                                    <tr><td>Tanggal SPH APH</td><td>{{ format($property->tanggal_sph_aph) }}</td></tr>
                                                </table>
                                            </div>
                                            <div class="col-md-12">
                                                @if ($pics == "")
                                                    <div class="col-md-12" style="margin-top: 20px" >
                                                        <p align="center">{!! 'No Picture available, <a href="/admin/picture/create" title="">click here </a>to add picture ' !!}</p>
                                                    </div>
                                                @else
                                                  <?php $i=0; ?>
                                                    @foreach ($pics as $pic)
                                                        <?php $i++; ?>
                                                        @if ($i==3 || $i==6 || $i==9 || $i==12)
                                                            <div class="row">
                                                        @endif
                                                            <div class="col-md-4" style="margin-top: 20px" align="center">
                                                                <a class="fancybox-effects-a" href="{{ '/uploads/picture/'.$pic->img_name }}"><img src="{{ '/uploads/picture/'.$pic->img_name }}" class="img-responsive"></a>
                                                            </div>
                                                        @if ($i==3 || $i==6 || $i==9 || $i==12)
                                                            </div>
                                                        @endif

                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
