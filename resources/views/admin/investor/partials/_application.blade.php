                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            Detail Application
                                        </h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <table class="table ">
                                                    <tr><td>ID Application</td><td>{{ $app->id }}</td></tr>
                                                    <tr><td>Nama Pemohon</td><td><a href="{{ $app->userUrl() }}" target="_blank">{{ $app->user->fullname }}</a></td></tr>
                                                    <tr><td>Developer</td><td><a href="{{ $app->devUrl() }}" target="_blank">{{ $app->developer->nama }}</a></td></tr>
                                                    <tr><td>Property</td><td><a href="{{ $app->propUrl() }}" target="_blank">{{ $app->property->nama }}</a></td></tr>
                                                    <tr><td>Lender ID</td><td>{{ $app->investorid }}</td></tr>
                                                    <tr><td>Tujuan Kredit</td><td>{{ $app->tujuan }}</td></tr>
                                                    <tr><td>Jenis Application</td><td>{{ $app->jenis }}</td></tr>
                                                    <tr><td>Uang Muka</td><td>{{ uang($app->uang_muka) }}</td></tr>
                                                    <tr><td>Sistem Pembayaran</td><td>{{ $app->sis_bayar }}</td></tr>
                                                </table>
                                            </div>
                                            <div class="col-md-6">
                                                <table class="table ">
                                                    <tr><td>Jumlah Pinjaman</td><td>{{ uang($app->pinjaman) }}</td></tr>
                                                    <tr><td>Pinjaman bulanan yang diajukan</td><td>{{ uang($app->pinjaman_bln) }}</td></tr>
                                                    <tr><td>Pinjaman (tahun)</td><td>{{ $app->periode }} tahun</td></tr>
                                                    <tr><td>Pinjaman (bulan)</td><td>{{ $app->jangka_waktu }} bulan</td></tr>
                                                    <tr><td>Lama Angsuran</td><td>{{ $total }} bulan</td></tr>
                                                    <tr><td>Status</td><td>{{ $app->status }}</td></tr>
                                                    <tr><td>Outstanding Loan</td><td>{{ uang($app->outstanding_loan) }}</td></tr>
                                                    <tr><td>Installment Times</td><td>{{ format($app->installment_times) }}</td></tr>
                                                    <tr><td>Anggaran Renovasi</td><td>{{ uang($app->renovasi) }}</td></tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
