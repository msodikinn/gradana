@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
  Lender Edit Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/modal/css/component.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/fancybox/jquery.fancybox.css') }}" media="screen" />
@stop

@section('content')
        <section class="content-header">
            <h1>Lender Form</h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading clearfix">
                            <div class="panel-title pull-left">
                              <div class="caption">
                                <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Edit Lender
                              </div>
                            </div>
                            <div class="pull-right">
                              <a href="/admin/investor" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Lender</a>
                            </div>
                        </div>
                        <div class="panel-body border">
                          {!! Form::model($detail, ['method' => 'PUT', 'url' => $detail->editUrl(), 'class' => 'form-horizontal form-bordered', 'files' => true]) !!}
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Lender Image</label>
                                  <div class="col-md-6">
                                      <div style="margin-bottom: 10px">
                                          @if (count($detail->img) !== 0)
                                              <a class="fancybox-effects-a" href="{!! '/'.$detail->img !!}"><img src="{{ '/'.$detail->img }}" class="img-responsive" style="max-height: 350px;"></a>
                                          @else
                                              <img src="{!! defaultcompany() !!}" class="img-responsive" style="max-height: 350px;">
                                          @endif

                                      </div>
                                      <div>
                                          <input type="file" name="img" value="{!! old('img',$detail->img) !!}">
                                      </div>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Kategori</label>
                                  <div class="col-md-6">
                                      <select id="cat" name="catid" class="form-control" style="width: 100%">
                                        @foreach ($cats as $i)
                                          <option value="{{ $i->id }}" @if($i->id === $detail->catid) selected @endif> {{ $i->nama }} </option>
                                        @endforeach
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Nama Lender</label>
                                  <div class="col-md-6">
                                      <input type="text" name="nama" class="form-control" value="{!! old('nama',$detail->nama) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Tempat Lahir</label>
                                  <div class="col-md-6">
                                      <input type="text" class="form-control" value="{!! $detail->user->tempat_lahir !!}" disabled>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Tanggal Lahir</label>
                                  <div class="col-md-6">
                                      <input type="text" class="form-control" value="{!! format($detail->user->dob) !!}" disabled>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Alamat</label>
                                  <div class="col-md-6">
                                      <input type="text" name="alamat" class="form-control" value="{!! old('alamat',$detail->alamat) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Pekerjaan</label>
                                  <div class="col-md-6">
                                      <input type="text" name="jenis_usaha" class="form-control" value="{!! old('jenis_usaha',$detail->jenis_usaha) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Email</label>
                                  <div class="col-md-6">
                                      <input type="text" name="email" class="form-control" value="{!! old('email',$detail->email) !!}">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Telepon Rumah</label>
                                  <div class="col-md-6">
                                      <input type="text" name="telp" class="form-control" value="{!! old('telp',$detail->telp) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Handphone</label>
                                  <div class="col-md-6">
                                      <input type="text" name="hp" class="form-control" value="{!! old('hp',$detail->hp) !!}">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Alokasi Dana *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="alokasi_dana" id="autonum1" class="form-control" value="{!! old('alokasi_dana',$detail->alokasi_dana) !!}" required>
                                      <p class="help-block">Saya memilih untuk mengalokasikan total dana sebesar jumlah berikut dalam skema Gradana (Rp)</p>
                                  </div>
                              </div>

                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Jumlah Alokasi Dana *</label>
                                  <div class="col-md-6">
                                      <select id="jml_alokasi" name="jml_alokasi" class="form-control" style="width: 100%">
                                          @foreach($jmlAlokasi as $code=>$a)
                                            <option value="{{ $code }}" @if($code == $detail->jml_alokasi) selected @endif>
                                              {{$a}}
                                            </option>
                                          @endforeach
                                      </select>
                                      {{-- <input type="text" id="jml_alokasi" name="jml_alokasi" class="form-control" value="{!! old('jml_alokasi',$detail->jml_alokasi) !!}" required> --}}
                                      <p class="help-block">Saya memilih untuk mengalokasikan dana saya untuk 1 peminjam, 2, 3 atau >3</p>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Tenor *</label>
                                  <div class="col-md-6">
                                      <select id="tenor" name="tenor" class="form-control" style="width: 100%">
                                          @foreach($tenor as $code=>$t)
                                            <option value="{{ $code }}" @if($code === $detail->tenor) selected @endif>
                                              {{$t}}
                                            </option>
                                          @endforeach
                                      </select>
                                      {{-- <input type="text" id="tenor" name="tenor" class="form-control" value="{!! old('tenor',$detail->tenor) !!}" required> --}}
                                      <p class="help-block">Bila memungkinkan, saya memilih untuk menyalurkan pinjaman dengan tenor selama</p>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Penempatan Dana *</label>
                                  <div class="col-md-6">
                                      <select id="penempatan_dana" name="penempatan_dana" class="form-control" style="width: 100%">
                                          @foreach($penempatanDana as $code=>$p)
                                            <option value="{{ $code }}" @if($code === $detail->penempatan_dana) selected @endif>
                                              {{$p}}
                                            </option>
                                          @endforeach
                                      </select>
                                      {{-- <input type="text" id="penempatan_dana" name="penempatan_dana" class="form-control" value="{!! old('penempatan_dana',$detail->penempatan_dana) !!}" required> --}}
                                      <p class="help-block">Opsi penempatan dana yang ingin saya pilih adalah</p>
                                  </div>
                              </div>
                              <div class="form-group form-actions">
                                  <div class="col-md-9 col-md-offset-4">
                                      <input type="submit" class="btn btn-primary" value="Update Lender">
                                  </div>
                              </div>
                          {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-thumbs.js') }}" ></script>
    <script src="{{ asset('assets/js/pages/gallery.js') }}"  ></script>

    <script src="{{ asset('assets/vendors/modal/js/modalEffects.js') }}" ></script>
    <script>
      $(document).ready(function() {
          $('#cat').select2(),
          $('#jml_alokasi').select2()
          $('#tenor').select2()
          $('#penempatan_dana').select2()
      });
    </script>

@stop
