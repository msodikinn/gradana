@extends('admin/layouts/default')

@section('title')
Borrower Detail
@parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/modal/css/component.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/fancybox/jquery.fancybox.css') }}" media="screen" />
    {{-- <style>
      .arrow {
          background: "";
          width: 0px;
          height: 0px;
          display: table;
          position: relative;
      }
      .thumb-info .thumb-info-title {
          font-size: 14px;
      }
    </style> --}}
@stop

@section('content')
        <section class="content-header">
            <h1>Borrower Detail</h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab1" data-toggle="tab">
                                <i class="livicon" data-name="user" data-size="16" data-c="#000" data-hc="#000" data-loop="true"></i>
                                Profile</a>
                        </li>
                        <li>
                            <a href="#tab2" data-toggle="tab">
                                <i class="livicon" data-name="address-book" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                                Application</a>
                        </li>
                        <li>
                            <a href="#tab3" data-toggle="tab">
                                <i class="livicon" data-name="notebook" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                                Assesment</a>
                        </li>
                        <li>
                            <a href="#tab4" data-toggle="tab">
                                <i class="livicon" data-name="money" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                                Credit Scoring</a>
                        </li>
                        <li>
                            <a href="#tab5" data-toggle="tab">
                                <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                                Property</a>
                        </li>
                    </ul>
                    <div  class="tab-content mar-top">
                        <div id="tab1" class="tab-pane fade active in">
                            @include('admin.investor.partials._profile')
                        </div>
                        <div id="tab2" class="tab-pane fade">
                            @include('admin.investor.partials._application')
                        </div>
                        <div id="tab3" class="tab-pane fade">
                            @include('admin.investor.partials._assesment')
                        </div>
                        <div id="tab4" class="tab-pane fade">
                            @include('admin.investor.partials._scoring')
                        </div>
                        <div id="tab5" class="tab-pane fade">
                            @include('admin.investor.partials._property')
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @stop

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
    <script src="{{ asset('assets/vendors/fancybox/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-thumbs.js') }}" ></script>
    <script src="{{ asset('assets/js/pages/gallery.js') }}"  ></script>
    <script src="{{ asset('assets/vendors/modal/js/modalEffects.js') }}" ></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDljps3p6FQeNNLP44TOoq6wdA12ULvTnk&libraries=places" type="text/javascript"></script>
    <script>
        var lat = {{ $property->lat }};
        var lng = {{ $property->lng }};

        var map = new google.maps.Map(document.getElementById('map-canvas'), {
            center:{
              lat: lat,
              lng: lng,
            },
            zoom:15
        });

        var marker = new google.maps.Marker({
            position:{
              lat:lat,
              lng:lng
            },
            map:map
        });
    </script>
@stop
