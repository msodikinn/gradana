@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
  Lender Create Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
        <section class="content-header">
            <h1>Lender Form</h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="livicon" data-name="bell" data-loop="true" data-color="#fff" data-hovercolor="#fff" data-size="18"></i>
                                Create
                            </h3>
                        </div>
                        <div class="panel-body border">
                            {!! Form::open(array('url' => route('investorCreate'), 'method' => 'post', 'class' => 'form-horizontal form-bordered', 'files' => true)) !!}
                                <input type="hidden" name="userid" value="{{ Sentinel::getuser()->id }}">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Lender Image</label>
                                    <div class="col-md-6">
                                        <input type="file" name="img" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Kategori</label>
                                    <div class="col-md-6">
                                        <select id="cat" name="catid" class="form-control" style="width: 100%" required>
                                          @foreach ($cat as $c)
                                            <option value="{{ $c->id }}"> {{ $c->nama }} </option>
                                          @endforeach
                                        </select>
                                        {{-- <p class="help-block"><a href="/admin/investor_cat/create">klik disini</a> untuk create kategori Lender</p> --}}
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-md-4 control-label">Nama Lender</label>
                                  <div class="col-md-6">
                                      <input type="text" name="nama" class="form-control" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Alamat</label>
                                  <div class="col-md-6">
                                      <input type="text" name="alamat" class="form-control" required>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Pekerjaan</label>
                                  <div class="col-md-6">
                                      <input type="text" name="jenis_usaha" class="form-control" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Email</label>
                                  <div class="col-md-6">
                                      <input type="text" name="email" class="form-control">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Telepon Rumah</label>
                                  <div class="col-md-6">
                                      <input type="text" name="telp" class="form-control" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Handphone</label>
                                  <div class="col-md-6">
                                      <input type="text" name="hp" class="form-control">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Alokasi Dana *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="alokasi_dana" id="autonum1" class="form-control" required>
                                      <p class="help-block">Saya memilih untuk mengalokasikan total dana sebesar jumlah berikut dalam skema Gradana (Rp)</p>
                                  </div>
                              </div>
                              {{-- {{ dd($jmlAlokasi) }} --}}
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Jumlah Alokasi Dana *</label>
                                  <div class="col-md-6">
                                      <select id="jml_alokasi" name="jml_alokasi" class="form-control" style="width: 100%">
                                        @foreach($jmlAlokasi as $j)
                                          <option value="{{ $j }}">
                                            {{$j}}
                                          </option>
                                        @endforeach
                                      </select>
                                      {{-- <input type="text" id="jml_alokasi" name="jml_alokasi" class="form-control" required> --}}
                                      <p class="help-block">Saya memilih untuk mengalokasikan dana saya untuk 1 peminjam, 2, 3 atau >3</p>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Tenor *</label>
                                  <div class="col-md-6">
                                      <select id="tenor" name="tenor" class="form-control" style="width: 100%">
                                        @foreach($tenor as $t)
                                          <option value="{{ $t }}">
                                            {{$t}}
                                          </option>
                                        @endforeach
                                      </select>
                                      {{-- <input type="text" id="tenor" name="tenor" class="form-control" required> --}}
                                      <p class="help-block">Bila memungkinkan, saya memilih untuk menyalurkan pinjaman dengan tenor selama</p>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Penempatan Dana *</label>
                                  <div class="col-md-6">
                                      <select id="penempatan_dana" name="penempatan_dana" class="form-control" style="width: 100%">
                                        @foreach($penempatanDana as $p)
                                          <option value="{{ $p }}">
                                            {{$p}}
                                          </option>
                                        @endforeach
                                      </select>
                                      {{-- <input type="text" id="penempatan_dana" name="penempatan_dana" class="form-control" required> --}}
                                      <p class="help-block">Opsi penempatan dana yang ingin saya pilih adalah</p>
                                  </div>
                              </div>
                              <div class="form-group form-actions">
                                  <div class="col-md-9 col-md-offset-4">
                                      <input type="submit" class="btn btn-primary" value="Save Lender">
                                  </div>
                              </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script>
      $(document).ready(function() {
          $('#cat').select2()
          $('#tenor').select2()
          $('#jml_alokasi').select2()
          $('#penempatan_dana').select2()
      });
    </script>

@stop
