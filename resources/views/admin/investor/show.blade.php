@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Detail Lender
@parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link href="{{ asset('assets/vendors/modal/css/component.css') }}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/modal/css/component.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/fancybox/jquery.fancybox.css') }}" media="screen" />
@stop

@section('content')
<section class="content-header">
    <h1>Detail Lender</h1>
</section>

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <div class="panel-title pull-left">
                    <div class="caption">
                        <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Detail {{ $detail->nama }}
                    </div>
                </div>
                <div class="pull-right">
                    <a href="/admin/investor" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Lender</a>
                    <a href="{{ $detail->editUrl() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-minus"></span> Edit Lender</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="col-md-5">
                    <center><a class="fancybox-effects-a" href="{!! $detail->img == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detail->img !!}">
                        {!! $detail->img == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detail->img.'" class="img-responsive" style="max-height: 436px">' !!}
                    </a></center>
                </div>
                <div class="col-md-7">
                    <table class="table">
                        <tr><td style="width: 150px">ID</td><td>{{ $detail->id }}</td></tr>
                        <tr><td>Kategori</td><td>{{ $detail->investor_cat->nama }}</td></tr>
                        <tr><td>Keterangan</td><td>{{ $detail->investor_cat->keterangan }}</td></tr>
                        <tr><td>Jenis Usaha</td><td>{{ $detail->jenis_usaha }}</td></tr>
                        <tr><td>Nama Lender</td><td>{{ $detail->nama }}</td></tr>
                        <tr><td>Email</td><td>{{ $detail->email }}</td></tr>
                        <tr><td>Alamat</td><td>{{ $detail->alamat }}</td></tr>
                        <tr><td>No. Telepon</td><td>{{ $detail->telp }}</td></tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <div class="panel-title pull-left">
                    <div class="caption">
                        <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Alokasi Dana
                    </div>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="col-md-12">
                    <table class="table table-striped table-bordered" id="table2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Alokasi Dana</th>
                                <th>Jumlah Alokasi</th>
                                <th>Tenor</th>
                                <th>Penempatan Dana</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($alokasi as $a)
                                <tr role="row" class="odd">
                                    <td><a href="{{ $a->detail() }}">{{ $a->id }}</a></td>
                                    <td>{{ $a->alokasi_dana }}</td>
                                    <td>{{ $a->jml_alokasi }}</td>
                                    <td>{{ $a->tenor }}</td>
                                    <td>{{ $a->penempatan_dana }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

@stop

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
    <script src="{{ asset('assets/vendors/modal/js/classie.js') }}" ></script>
    <script src="{{ asset('assets/vendors/fancybox/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-thumbs.js') }}" ></script>
    <script src="{{ asset('assets/js/pages/gallery.js') }}"  ></script>
    <script src="{{ asset('assets/vendors/modal/js/modalEffects.js') }}" ></script>
@stop
