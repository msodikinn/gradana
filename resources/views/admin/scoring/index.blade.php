@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Credit Scoring Management
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/modal/css/component.css') }}" />
@stop

{{-- Page content --}}
@section('content')

        <section class="content-header">
                <!--section starts-->
                <h1>Manage Credit Scoring</h1>

            </section>
            <!--section ends-->
            <section class="content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary filterable">
                            <div class="panel-heading clearfix">
                              <div class="panel-title pull-left">
                                <div class="caption">
                                  <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> List Credit Scoring
                                </div>
                              </div>
                              <div class="pull-right">
                                <a href="#create" data-toggle="modal" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> Create Credit Scoring</a>
                              </div>
                            </div>
                            <div class="panel-body table-responsive">
                                <table class="table table-striped table-bordered" id="table1">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Application ID</th>
                                            <th>Jumlah Hutang per Bulan</th>
                                            <th>Pinjaman Highest Interest</th>
                                            <th>Jumlah Kartu Kredit</th>
                                            <th>Limit Kartu Kredit</th>
                                            <th>Lama Kerja</th>
                                            <th>Total Lama Kerja</th>
                                            <th>Saldo 3 Bulan</th>
                                            <th>Kelengkapan Data</th>
                                            <th>Akurasi Data</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($score as $s)
                                            <tr role="row" class="odd">
                                                <td><a href="{{ $s->detailUrl() }}">{{ $s->id }}</a></td>
                                                <td>{{ $s->appid }}</td>
                                                <td>{{ uang($s->jml_hutang) }}</td>
                                                <td>{{ $s->pinj_high_interest }}</td>
                                                <td>{{ $s->jml_cc }}</td>
                                                <td>{{ uang($s->limit_cc) }}</td>
                                                <td>{{ $s->lama_kerja }}</td>
                                                <td>{{ $s->total_lama_kerja }}</td>
                                                <td>{{ uang($s->saldo_3_bln) }}</td>
                                                <td>{{ $s->kelengkapan_data }}</td>
                                                <td>{{ $s->akurasi_data }}</td>
                                                <td><a class="edit" href="{{ $s->editUrl() }}">Edit</a></td>
                                                <td><a class="edit" href="{{ $s->delUrl() }}">delete</a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Third Basic Table Ends Here-->
            </section>
            <!-- content -->

    <!-- create modal-->
    <div class="modal fade in" id="create" role="dialog" aria-hidden="false" style="display:none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Credit Scoring -> Pilih Application</h4>
                </div>
                <div class="modal-body">
                    <table id="table2" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Pemohon</th>
                                <th>Tujuan</th>
                                <th>Sistem Pembayaran</th>
                                <th>Harga Property</th>
                                <th>Pinjaman</th>
                                <th>Pengajuan Angsuran</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=0; ?>
                            @foreach ($app as $a)
                                <?php $pem = $a->pemohonid; $app = App\Pemohon::find($pem); ?>
                                <tr role="row" class="odd">
                                    <td>{{ is_null($app) ? "" : $app->fullname_clean }}</td>
                                    <td>{{ $a->tujuan }}</td>
                                    <td>{{ $a->sis_bayar }}</td>
                                    <td>{{ uang($a->harga_property) }}</td>
                                    <td>{{ uang($a->pinjaman) }}</td>
                                    <td>{{ uang($a->pinjaman_bln) }}</td>
                                    <td>{{ $a->status }}</td>
                                    <td><a href="{{ '/admin/scoring/choose/'.$a->pemohonid.'/'.$a->id }}" class="btn btn-sm btn-primary">Select</a></td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END create modal-->
    @stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
    <script src="{{ asset('assets/vendors/modal/js/modalEffects.js') }}" ></script>
@stop
