@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Detail Credit Scoring
@parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link href="{{ asset('assets/vendors/modal/css/component.css') }}" rel="stylesheet" />
@stop

@section('content')
<section class="content-header">
    <h1>Detail Credit Scoring</h1>
</section>

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <div class="panel-title pull-left">
                    <div class="caption">
                        <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Detail {{ $detail->nama }}
                    </div>
                </div>
                <div class="pull-right">
                    <a href="/admin/scoring" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Credit Scoring</a>
                    <a href="{{ $detail->editUrl() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-minus"></span> Edit Credit Scoring</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                            <th>Applicant</th>
                            <th>App ID</th>
                            <th>Assesment ID</th>
                            <th>Net Plafon</th>
                            <th>Take Home Pay</th>
                            <th>Interest Annum</th>
                            <th>Pengajuan Pinjaman</th>
                            <th>Periode (tahun)</th>
                            <th>Jangka Waktu (total bulan)</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="{{ '/admin/users/'.$user->id }}" target="_blank">{{ $user->fullname }}</a></td>
                                <td><a href="{{ '/admin/application/detail/'.$appid }}" target="_blank">{{ $appid }}</a></td>
                                <td><a href="{{ '/admin/assesment/detail/'.$detail->application->assesment->id }}" target="_blank">{{ $detail->application->assesment->id }}</a></td>
                                <td>{{ uang($netPlafon) }}</td>
                                <td>{{ uang($thp) }}</td>
                                <td>{{ $annumMin }} - {{ $annumMax }}%</td>
                                <td>{{uang($pinjaman) }}</td>
                                <td>{{ $period }}</td>
                                <td>{{ $jangkaWaktu }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Data</th>
                                <th>Weight</th>
                                <th>Criteria</th>
                                <th>Credit Score</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>ID</td>
                                <td>{{ $detail->id }}</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td>Assesment ID</td>
                                <td><a href="{{ $detail->assesUrl() }}">{{ $detail->assesid }}</a></td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td>Jumlah Hutang per Bulan yang Sedang Berjalan</td>
                                <td>{{ uang($detail->jml_hutang) }}</td>
                                <td>20%</td>
                                <td>{{ $scoreJmlHtg }}</td>
                                <td>{{ $cscoreJmlHtg }}</td>
                            </tr>
                            <tr>
                                <td>Pinjaman Highest Interest</td>
                                <td>{{ uang($pinjHI )}}</td>
                                <td>30%</td>
                                <td>{{ $scoreHI }}</td>
                                <td>{{ $cscoreHI  }}</td>
                            </tr>
                            <tr>
                                <td>Jumlah Kartu Kredit</td>
                                <td>{{ $detail->jml_cc }}</td>
                                <td>{{ $wscoreCC }}</td>
                                <td>{{ $scoreCC }}</td>
                                <td>{{ $cscoreCC  }}</td>
                            </tr>
                            <tr>
                                <td>Total Limit Kartu Kredit</td>
                                <td>{{ uang($detail->limit_cc) }}</td>
                                <td>{{ $wscoreLimit }}</td>
                                <td>{{ $scoreLimit }}</td>
                                <td>{{ $cscoreLimit }}</td>
                            </tr>
                            <tr>
                                <td>Lama Bekerja di pekerjaan sekarang</td>
                                <td>{{ $detail->lama_kerja }}</td>
                                <td>7.5%</td>
                                <td>{{ $scoreLamaKerja }}</td>
                                <td>{{ $cscoreLamaKerja }}</td>
                            </tr>
                            <tr>
                                <td>Total Lama Bekerja</td>
                                <td>{{ $detail->total_lama_kerja }}</td>
                                <td>7.5%</td>
                                <td>{{ $scoreTotLamaKerja }}</td>
                                <td>{{ $cscoreTotLamaKerja }}</td>
                            </tr>
                            <tr>
                                <td>Saldo 3 Bulan</td>
                                <td>{{ uang($detail->saldo_3_bln) }}</td>
                                <td>{{ $wscoreSaldo }}</td>
                                <td>{{ $scoreSaldo }}</td>
                                <td>{{ $cscoreSaldo }}</td>
                            </tr>
                            <tr>
                                <td>Kelengkapan Data</td>
                                <td>{{ $detail->kelengkapan_data }}</td>
                                <td>5.0%</td>
                                <td>{{ $scoreLengkap }}</td>
                                <td>{{ $cscoreLengkap }}</td>
                            </tr>
                            <tr>
                                <td>Akurasi Data</td>
                                <td>{{ $detail->akurasi_data }}</td>
                                <td>5.0%</td>
                                <td>{{ $scoreAkurasi }}</td>
                                <td>{{ $cscoreAkurasi }}</td>
                            </tr>
                            <tr>
                                <td>Jumlah Asset Bank</td>
                                <td>{{ uang($detail->jumlah_asset_bank) }}</td>
                                <td>{{ $wscoreAssetBank }}</td>
                                <td>{{ $scoreAssetBank }}</td>
                                <td>{{ $cscoreAssetBank }}</td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right">Credit Rating</td>
                                <td>{{ $ave }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
    <script src="{{ asset('assets/vendors/modal/js/classie.js') }}" ></script>
    <script src="{{ asset('assets/vendors/modal/js/modalEffects.js') }}" ></script>
@stop
