@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Credit Scoring Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
        <section class="content-header">
            <h1>Edit Credit Scoring Form</h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading clearfix">
                            <div class="panel-title pull-left">
                              <div class="caption">
                                <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Edit Credit Scoring
                              </div>
                            </div>
                            <div class="pull-right">
                              <a href="/admin/scoring" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Credit Scoring</a>
                            </div>
                        </div>
                        <div class="panel-body border">
                          {!! Form::model($detail, ['method' => 'PUT', 'url' => $detail->editUrl(), 'class' => 'form-horizontal form-bordered']) !!}
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">ID</label>
                                  <div class="col-md-6">
                                      <input type="text" name="id" class="form-control" value="{!! old('id',$detail->id) !!}" readonly>
                                  </div>
                              </div>
                              <div class="form-group">
                                    <label class="col-md-4 control-label">Assesment ID</label>
                                    <div class="col-md-6">
                                        <select id="assess" name="assesid" class="form-control" style="width: 100%">
                                            @foreach($asses as $a)
                                                <option value="{{ $a->id }}" @if($a->id === $detail->assesid) selected @endif>
                                                    {{ $a->id }}. App ID: {{ $a->appid }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Jumlah Hutang per Bulan yang Sedang Berjalan *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="jml_hutang" id="autonum1" class="form-control" value="{!! old('jml_hutang',$detail->jml_hutang) !!}" required>
                                        <p class="help-block">jumlah semua hutang yang belum terbayar (format rupiah)</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Pinjaman Highest Interest</label>
                                    <div class="col-md-6">
                                        <input type="text" name="pinj_high_interest" class="form-control" value="{!! old('pinjHI',$detail->pinj_high_interest) !!}" readonly>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Jumlah Kartu Kredit</label>
                                    <div class="col-md-6">
                                        <select id="jml_cc" name="jml_cc" class="form-control">
                                            @foreach($ratingJmlCC as $code=>$c)
                                                <option value="{{ $code }}" @if($code === $detail->jml_cc) selected @endif>
                                                  {{$c}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Total Limit Kartu Kredit *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="limit_cc" id="autonum2" class="form-control" value="{!! old('limit_cc',$detail->limit_cc) !!}" required>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Lama Bekerja</label>
                                    <div class="col-md-6">
                                        <select id="lama_kerja" name="lama_kerja" class="form-control">
                                            @foreach($ratingLamaKerja as $code=>$c)
                                                <option value="{{ $code }}" @if($code === $detail->lama_kerja) selected @endif>
                                                  {{$c}}
                                                </option>
                                            @endforeach
                                        </select>
                                        <p class="help-block">lama bekerja di perusahaan / pekerjaan sekarang</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Total Lama Bekerja</label>
                                    <div class="col-md-6">
                                        <select id="total_lama_kerja" name="total_lama_kerja" class="form-control">
                                            @foreach($ratingTotalLamaKerja as $code=>$c)
                                                <option value="{{ $code }}" @if($code === $detail->total_lama_kerja) selected @endif>
                                                  {{$c}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Rata-rata Saldo 3 Bulan *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="saldo_3_bln" id="autonum3" class="form-control" value="{!! old('saldo_3_bln',$detail->saldo_3_bln) !!}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Kelengkapan Data</label>
                                    <div class="col-md-6">
                                        <select id="kelengkapan_data" name="kelengkapan_data" class="form-control">
                                            @foreach($ratingData as $code=>$c)
                                                <option value="{{ $code }}" @if($code === $detail->kelengkapan_data) selected @endif>
                                                  {{$c}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Akurasi Data</label>
                                    <div class="col-md-6">
                                        <select id="akurasi_data" name="akurasi_data" class="form-control">
                                            @foreach($ratingDataAkurasi as $code=>$c)
                                                <option value="{{ $code }}" @if($code === $detail->akurasi_data) selected @endif>
                                                  {{$c}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Jumlah Asset Bank</label>
                                    <div class="col-md-6">
                                        <input type="text" name="jumlah_asset_bank" id="autonum4" class="form-control" value="{!! old('jumlah_asset_bank',$detail->jumlah_asset_bank) !!}">
                                        <p class="help-block">Optional: diabaikan apabila rata-rata saldo tabungan 3 bulan &ge; 20% dari THP, total asset bank diluar permohonan (format rupiah) </p>
                                    </div>
                                </div>
                              <div class="form-group form-actions">
                                  <div class="col-md-9 col-md-offset-4">
                                      <input type="submit" class="btn btn-primary" value="Save Credit Scoring">
                                  </div>
                              </div>
                          {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#assess').select2()
            $('#jml_cc').select2()
            $('#lama_kerja').select2()
            $('#total_lama_kerja').select2()
            $('#kelengkapan_data').select2()
            $('#akurasi_data').select2()
        });
    </script>
@stop
