@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
  Penghasilan Create Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
        <section class="content-header">
            <h1>Penghasilan Form</h1>
        </section>
        <section class="content">
        	<div class="row">
				<div class="col-lg-12">
					<ul class="nav nav-tabs ">
						@if (!empty($pemohonid))
  						<li>
  							<a href="{{ URL::to('admin/pemohon/edit/'.$pemohonid) }}">Pemohon</a>
  						</li>
						@endif
						@if (!empty($jobpemohonid))
  						<li>
  							<a href="{{ URL::to('admin/jobpemohon/edit/'.$jobpemohonid) }}">Pekerjaan Pemohon</a>
  						</li>
						@endif
            @if (App\Pemohon::find($pemohonid)->status_kawin != "Lajang")
  						@if (!empty($suamisitriid))
    						<li>
    							<a href="{{ URL::to('admin/suamiistri/edit/'.$suamisitriid) }}">Pasangan</a>
    						</li>
  						@endif
  						@if (!empty($jobsuamiistriid))
    						<li>
    							<a href="{{ URL::to('admin/jobsuamiistri/edit/'.$jobsuamiistriid) }}">Pekerjaan Pasangan</a>
    						</li>
  						@endif
            @endif
						@if (!empty($keluargaid))
  						<li>
  							<a href="{{ URL::to('admin/keluarga/edit/'.$keluargaid) }}">Emergency Contact</a>
  						</li>
						@endif
						<li class="active">
							<a href="">Penghasilan</a>
						</li>
						@if (empty($perbankanid))
  						<li>
  							<a href="{{ URL::to('admin/perbankan/create?backid='.$backid) }}">Perbankan</a>
  						</li>
						@else
						<li>
							<a href="{{ URL::to('admin/perbankan/edit/'.$perbankanid) }}">Perbankan</a>
						</li>
						@endif
            @if ($countApp == 0)
              <li>
                <a href="{{ URL::to('admin/application/create?backid='.$backid) }}">Create Application</a>
              </li>
            @elseif($countApp == 1)
              <li>
                <a href="{{ URL::to('admin/application/edit/'.$applicationid) }}">Application</a>
              </li>
            @elseif($countApp > 1)
              <li>
                <a href="{{ URL::to('admin/application?iduser='.$detail->userid) }}">{{ $countApp }} Application</a>
              </li>
            @endif
					</ul>
				</div>
			</div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading clearfix">
                            <div class="panel-title pull-left">
                              <div class="caption">
                                <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Edit Penghasilan
                              </div>
                            </div>
                            <div class="pull-right">
                              <a href="/admin/penghasilan" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Penghasilan</a>
                            </div>
                        </div>
                        <div class="panel-body border">
                          {!! Form::model($detail, ['method' => 'PUT', 'url' => $detail->editUrl(), 'class' => 'form-horizontal form-bordered', 'files'=> true]) !!}
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Nama Pemohon</label>
                                  <div class="col-md-6">
                                      <input type="text" value="{{ $detail->user->fullname }}" class="form-control" readonly>
                                      <input type="hidden" name="userid" value="{{ $detail->user->id }}">
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Joint Income</label>
                                  <div class="col-md-6">
                                      <input type="text" name="joint_income" class="form-control" value="{!! old('joint_income',$detail->joint_income) !!}">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Penghasilan Pemohon (THP) *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="pemohon" class="form-control" value="{!! old('pemohon',$detail->pemohon) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Penghasilan Pasangan *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="suami_istri" class="form-control" value="{!! old('suami_istri',$detail->suami_istri) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Penghasilan Tambahan</label>
                                  <div class="col-md-6">
                                      <input type="text" name="tambahan" class="form-control" value="{!! old('tambahan',$detail->tambahan) !!}">
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Biaya Rumah Tangga *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="biaya_rt" class="form-control" value="{!! old('biaya_rt',$detail->biaya_rt) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Angsuran Lain</label>
                                  <div class="col-md-6">
                                      <input type="text" name="angsuran_lain" class="form-control" value="{!! old('angsuran_lain',$detail->angsuran_lain) !!}">
                                  </div>
                              </div>
                              <div class="form-group form-actions">
                                  <div class="col-md-9 col-md-offset-4">
                                      <input type="submit" class="btn btn-primary" value="Save Penghasilan">
                                  </div>
                              </div>
                          {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script>
      $(document).ready(function() {
          $('#userid').select2()
      });
    </script>

@stop
