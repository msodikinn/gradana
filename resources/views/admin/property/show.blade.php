@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Detail Property
@parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link href="{{ asset('assets/vendors/modal/css/component.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/fancybox/jquery.fancybox.css') }}" media="screen" />
    <style>
      .row-centered {
          text-align:center;
          padding-left: 0px;
          padding-right: 0px;
      }
      .col-centered {
          display:inline-block;
          float:none;
          text-align:left;
          /*margin-right:-4px;*/
          padding-left: 0px;
          padding-right: 0px;
      }
    </style>
@stop

@section('content')
    <section class="content-header">
        <h1>Detail Property</h1>
    </section>
    <section class="content paddingleft_right15">
        <div class="row">
            <div class="panel panel-primary ">
                <div class="panel-heading clearfix">
                    <div class="panel-title pull-left">
                        <div class="caption">
                            <i class="livicon" data-name="list-ul" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
                            <h3 style="display: inline-block;margin-top: 0px;">Detail {{ $detail->nama }}</h3>
                        </div>
                    </div>
                    <div class="pull-right">
                        <a href="/admin/property" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Property</a>
                        <a href="{{ $detail->editUrl() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-minus"></span> Edit Property</a>
                    </div>
                </div>
                <br />
                <div class="panel-body" style="margin-top: 0px; padding-top: 0px;">
                    <div class="col-md-12">
                      <div id="map-canvas" style="height: 350px;"></div>
                    </div>
                    <div class="col-md-6" style="margin-top: 20px">
                        <table class="table ">
                            <tr><td style="width: 150px">ID Property</td><td>{{ $detail->id }}</td></tr>
                            <tr><td>Nama Pemohon</td><td>{{ $detail->user->fullname }}</td></tr>
                            <tr><td>Developer</td><td><a target="blank" href="{{ $detail->devUrl() }}">{{ $detail->developer->nama }}</a></td></tr>
                            <tr><td>Nama Property</td><td>{{ $detail->nama }}</td></tr>
                            <tr><td>Alamat</td><td>{{ $detail->alamat }}</td></tr>
                            <tr><td>Latitude</td><td>{{ $detail->lat }}</td></tr>
                            <tr><td>Longitude</td><td>{{ $detail->lng }}</td></tr>
                            <tr><td>Deskripsi</td><td>{{ $detail->deskripsi }}</td></tr>
                        </table>
                    </div>
                    <div class="col-md-6" style="margin-top: 20px">
                        <table class="table ">
                            <tr><td>Property Type</td><td>{{ $detail->type }}</td></tr>
                            <tr><td>No. HGB</td><td>{{ $detail->nomor_hgb }}</td></tr>
                            <tr><td>Pemilik Terdaftar</td><td>{{ $detail->pemilik_terdaftar }}</td></tr>
                            <tr><td>Luas Lahan</td><td>{{ $detail->luas_lahan }} m<sup>2</sup></td></tr>
                            <tr><td>HGB Expire Date</td><td>{{ format($detail->hgb_expire_date) }}</td></tr>
                            <tr><td>Notes</td><td>{{ $detail->notes }}</td></tr>
                            <tr><td>No. SPH APH</td><td>{{ $detail->nomor_sph_aph }}</td></tr>
                            <tr><td>Tanggal SPH APH</td><td>{{ format($detail->tanggal_sph_aph) }}</td></tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content paddingleft_right15">
        <div class="row">
            <div class="panel panel-flat">
                <div class="panel-heading clearfix">
                    <div class="panel-title pull-left">
                      <caption><h3 style="margin-top:0px;">Products</h3></caption>
                    </div>
                    <div class="pull-right">
                      <a href="#create" class="btn btn-default" data-toggle="modal">Add Products</a>
                    </div>
                </div>
                <div class="panel-body" style="margin-top: 0px; padding-top: 0px;">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Tipe</th>
                          <th>Luas Tanah</th>
                          <th>Luas Bangunan</th>
                          <th>Unit Available</th>
                          <th>Range Harga Min</th>
                          <th>Range Harga Max</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if (count($detail->subproperties))
                          <?php $i=0; ?>
                          @foreach ($detail->subproperties as $s)
                            <tr>
                              <td>{{ $s->tipe }}</td>
                              <td>{{ $s->luas_tanah }} m<sup>2</sup></td>
                              <td>{{ $s->luas_bangunan }} m<sup>2</sup></td>
                              <td>{{ $s->jml_unit_available }}</td>
                              <td>{{ uang($s->range_harga_min)}}</td>
                              <td>{{ uang($s->range_harga_max) }}</td>
                              <td>
                                <a href="#edit{{$i}}" data-href="#edit{{$i}}" data-toggle="modal" class="btn btn-icon-only btn-info" title="edit"><i class="fa fa-edit"></i></a>
                                <a href="#delete{{$i}}" data-href="#delete{{$i}}" data-toggle="modal" class="btn btn-icon-only btn-danger" title="delete"><i class="fa fa-remove"></i></a>
                              </td>
                            </tr>
                            {{-- Looping Delete Modal --}}
                            <div id="delete{{$i}}" class="modal fade" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                            <h4 class="modal-title">Delete Product</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="scroller" style="height:30px" data-always-visible="1" data-rail-visible1="1">
                                                <div class="col-md-12">
                                                    <center><h4>Are you sure you want to delete {{ $s->tipe }}?</h4></center>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                                            <a href="{{ $s->delUrl() }}" class="btn btn-danger">Yes please!</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- Looping Edit Modal --}}
                            <div id="edit{{$i}}" class="modal fade bs-modal-lg" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                            <h4 class="modal-title">Edit Product</h4>
                                        </div>
                                        {!! Form::model($s, ['method' => 'PUT', 'url' => $s->editUrl(), 'class' => 'form-horizontal', 'files' => true]) !!}
                                            <div class="modal-body">
                                                <div class="col-md-12">
                                                    <input type="hidden" name="id" value="{{ $s->id }}">
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Tipe</label>
                                                        <div class="col-md-6" style="margin-bottom:15px;">
                                                            <input type="text" name="tipe" class="form-control" value="{{ $s->tipe }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Luas Tanah</label>
                                                        <div class="col-md-6">
                                                            <input type="text" name="luas_tanah" class="form-control" value="{{ $s->luas_tanah }}">
                                                            <p class="help-block">satuan m<sup>2</sup></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Luas Bangunan</label>
                                                        <div class="col-md-6">
                                                            <input type="text" name="luas_bangunan" class="form-control" value="{{ $s->luas_bangunan }}">
                                                            <p class="help-block">satuan m<sup>2</sup></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Range Harga</label>
                                                        <div class="col-md-6" style="margin-bottom:15px;">
                                                            <div class="row row-centered">
                                                                <div class="col-md-5 col-centered">
                                                                    <input type="text" name="range_harga_min" class="form-control" value="{{ $s->range_harga_min }}" id="autonum{{$i}}">
                                                                </div>
                                                                <div class="col-md-1 col-centered">
                                                                    <center>to</center>
                                                                </div>
                                                                <div class="col-md-5 col-centered">
                                                                    <input type="text" name="range_harga_max" class="form-control" value="{{ $s->range_harga_max }}" id="autonum{{$i=$i+1}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Unit Available</label>
                                                        <div class="col-md-6" style="margin-bottom:15px;">
                                                            <input type="text" name="jml_unit_available" class="form-control" value="{{ $s->jml_unit_available }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer" style="border-top: 0px;">
                                                  <div class="col-md-12">
                                                      <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                                                      <button type="submit" class="btn btn-info">Save Product</button>
                                                  </div>
                                            </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            <?php $i++; ?>
                          @endforeach
                        @else
                          <tr><td colspan="7"><center>No data available on this table</center></td></tr>
                        @endif
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <section class="content paddingleft_right15">
        <div class="row">
            <div class="panel panel-primary ">
                <div class="panel-body" style="margin-top: 0px; padding-top: 0px;">
                    @if ($pics == "")
                        <div class="col-md-12" style="margin-top: 20px">
                            <p align="center">{!! 'No Picture available, <a href="/admin/picture/create" title="">click here </a>to add picture ' !!}</p>
                        </div>
                    @else
                      <?php $i=0; ?>
                        @foreach ($pics as $pic)
                            <?php $i++; ?>
                            @if ($i==3 || $i==6 || $i==9 || $i==12)
                                <div class="row">
                            @endif
                                <div class="col-md-4" style="margin-top: 20px" align="center">
                                    <a class="fancybox-effects-a" href="{{ '/uploads/picture/'.$pic->img_name }}"><img src="{{ '/uploads/picture/'.$pic->img_name }}" class="img-responsive"></a>
                                </div>
                            @if ($i==3 || $i==6 || $i==9 || $i==12)
                                </div>
                            @endif

                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
{{-- Create Modal --}}
<div id="create" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Create New Product</h4>
            </div>
            {!! Form::open(array('url' => route('createsub'), 'method' => 'post', 'class' => 'form-horizontal', 'files' => true)) !!}
                <div class="modal-body">
                    <input type="hidden" name="propid" value="{{ $detail->id }}">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Tipe</label>
                            <div class="col-md-6" style="margin-bottom:15px;">
                                <input type="text" name="tipe" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Luas Tanah</label>
                            <div class="col-md-6">
                                <input type="text" name="luas_tanah" class="form-control">
                                <p class="help-block">satuan m<sup>2</sup></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Luas Bangunan</label>
                            <div class="col-md-6">
                                <input type="text" name="luas_bangunan" class="form-control">
                                <p class="help-block">satuan m<sup>2</sup></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Range Harga</label>
                            <div class="col-md-6" style="margin-bottom:15px;">
                                <div class="row row-centered">
                                    <div class="col-md-5 col-centered">
                                        <input type="text" name="range_harga_min" class="form-control" id="autonum31">
                                    </div>
                                    <div class="col-md-1 col-centered">
                                        <center>to</center>
                                    </div>
                                    <div class="col-md-5 col-centered">
                                        <input type="text" name="range_harga_max" class="form-control" id="autonum32">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Unit Available</label>
                            <div class="col-md-6" style="margin-bottom:15px;">
                                <input type="text" name="jml_unit_available" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="border-top: 0px;">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                    <button type="submit" class="btn btn-info">Create Product</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}"></script>
    <script src="{{ asset('assets/vendors/modal/js/classie.js') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-thumbs.js') }}"></script>
    <script src="{{ asset('assets/js/pages/gallery.js') }}" ></script>
    <script src="{{ asset('assets/vendors/modal/js/modalEffects.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDljps3p6FQeNNLP44TOoq6wdA12ULvTnk&libraries=places" type="text/javascript"></script>
    <script>
        var lat = {{ $detail->lat }};
        var lng = {{ $detail->lng }};

        var map = new google.maps.Map(document.getElementById('map-canvas'), {
            center:{
              lat: lat,
              lng: lng,
            },
            zoom:15
        });

        var marker = new google.maps.Marker({
            position:{
              lat:lat,
              lng:lng
            },
            map:map
        });
    </script>
@stop
