@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
  Property Create Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/pages/addproperty.css') }}" rel="stylesheet">
@stop

@section('content')
        <section class="content-header">
            <h1>Property Form</h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary filterable">
                        <div class="panel-heading clearfix">
                            <div class="panel-title pull-left">
                              <div class="caption">
                                <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Create Property
                              </div>
                            </div>
                            <div class="pull-right">
                              <a href="/admin/property" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Property</a>
                            </div>
                        </div>
                        <div class="panel-body border">
                            {!! Form::open(array('url' => route('propertyCreate'), 'method' => 'post', 'class' => 'form-horizontal form-bordered')) !!}
                                {{-- <div class="form-group">
                                    <label class="col-md-4 control-label">Nama Pemohon</label>
                                    <div class="col-md-6">
                                        <select id="userid" name="userid" class="form-control" style="width: 100%">
                                          @foreach($users as $u)
                                            <option value="{{ $u->id }}">
                                              {{$u->fullname}}
                                            </option>
                                          @endforeach
                                        </select>
                                    </div>
                                </div> --}}
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Developer</label>
                                    <div class="col-md-6">
                                        <select id="devid" name="devid" class="form-control" style="width: 100%">
                                            @foreach($dev as $d)
                                              <option value="{{ $d->id }}">
                                                {{$d->nama}}
                                              </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Nama Property *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="nama" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Property Type</label>
                                    <div class="col-md-6">
                                        <select id="status" name="type" class="form-control" style="width: 100%">
                                            @foreach($statusr as $code=>$s)
                                              <option value="{{ $code }}">
                                                {{$s}}
                                              </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Alamat *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="alamat" class="form-control" id="searchmap" required>
                                        <div id="map-canvas" class="form-control" style="margin-top: 10px; width: 100%; height: 300px; padding: 0px;"></div>
                                        <p class="help-block">silahkan set alamat dan lokasi marker</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Latitude</label>
                                    <div class="col-md-6">
                                        <input type="text" name="lat" class="form-control" id="lat" readonly>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Longitude</label>
                                    <div class="col-md-6">
                                        <input type="text" name="lng" class="form-control" id="lng" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Deskripsi</label>
                                    <div class="col-md-6">
                                        <textarea name="deskripsi" rows="5" class="form-control"></textarea>
                                    </div>
                                </div>
                                {{-- <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Tipe *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="tipe" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Luas Tanah *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="luas_tanah" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Luas Bangunan *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="luas_bangunan" class="form-control" required>
                                    </div>
                                </div> --}}
                                {{-- <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Harga Jual *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="harga_jual" id="autonum1" class="form-control" required>
                                    </div>
                                </div> --}}
                                {{-- <div class="form-group">
                                    <label class="col-md-4 control-label">Harga Beli *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="harga_beli" id="autonum2" class="form-control" required>
                                    </div>
                                </div> --}}
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">No. Hak Guna Bangunan (HGB)</label>
                                    <div class="col-md-6">
                                        <input type="text" name="nomor_hgb" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Pemilik Terdaftar *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="pemilik_terdaftar" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Luas Lahan *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="luas_lahan" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">HGB Expire Date</label>
                                    <div class="col-md-6">
                                        <input type="date" name="hgb_expire_date" class="form-control">
                                    </div>
                                </div>
                                {{-- <div class="form-group">
                                    <label class="col-md-4 control-label">Lokasi *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="lokasi" class="form-control" required>
                                    </div>
                                </div> --}}
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Notes</label>
                                    <div class="col-md-6">
                                        <input type="text" name="notes" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">No. SPH APH</label>
                                    <div class="col-md-6">
                                        <input type="text" name="nomor_sph_aph" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Tanggal SPH APH</label>
                                    <div class="col-md-6">
                                        <input type="date" name="tanggal_sph_aph" class="form-control">
                                    </div>
                                </div>
                                {{-- <div class="form-group">
                                    <label class="col-md-4 control-label">Pihak *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="pihak" class="form-control" required>
                                    </div>
                                </div> --}}
<div id="vuegallery" class="layoutgallery">
                                <button type=button class="btn buttonmargin" v-bind:class="[isaddphoto ? 'btn-warning' : 'btn-primary']" @click="viewaddphotogallery">@{{ textaddphotobutton }}</button>
						<div class="form-group" v-show="gallerychoose">
							<div class="col-md-12">
				                <div class="panel panel-info">
				                    <div class="panel-heading">
				                        <h3 class="panel-title">
				                            <i class="livicon" data-name="crop" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
				                            Gallery For Choose
				                        </h3>
				                                <span class="pull-right clickable">
				                                    <i class="glyphicon glyphicon-chevron-up"></i>
				                                </span>
				                    </div>
									<div class="panel-body">
										<span v-show="showlabelemptygalleryname">Semua Galeri telah di pakai Properti lain</span>
										<div v-for="galleryname in gallerynames" class="col-md-12" v-bind:class="[galleryname.isshow ? border1px : '']">
										<div v-show="galleryname.isshow"><div class="infolabel"><a class="btn btn-info btn-labeled marginbottom10" role="button" @click="addtothis(galleryname, $event)">
		                                            <span class="btn-label"><i class="livicon" data-name="checked-on" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i></span>
		                                            @{{ galleryname.named }}
		                                	</a></div>
												<div v-for="listchoose in galleryname.listchooses">
													<img :src="listchoose.img_name" alt="Gambar" :idimg="listchoose.id" class="col-sm-3 heightelement pointer">
												</div></div>
										</div>
									</div>
									<div class="panel-footer">
										<button type=button class="btn btn-primary buttonmargin" @click="viewaddphotogallery">@{{ textaddphotobutton }}</button>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12">
				                <div class="panel panel-success">
				                    <div class="panel-heading">
				                        <h3 class="panel-title">
				                            <i class="livicon" data-name="crop" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
				                            Property's Photo <input type="hidden" name="galleryidlist" :value="galleryidlist">
				                        </h3>
				                                <span class="pull-right clickable">
				                                    <i class="glyphicon glyphicon-chevron-up"></i>
				                                </span>
				                    </div>
									<div class="panel-body">
										<span v-show="showlabelemptylist">Galeri masih kosong</span>
										<div v-for="galleryname in listpictures" v-bind:class="[galleryname.isshow ? border1pxgreen : '']" class="col-md-12">
										<div v-show="galleryname.isshow"><div class="infolabelgreen"><a class="btn btn-success btn-labeled marginbottom10" role="button" @click="removefromthis(galleryname, $event)">
		                                            <span class="btn-label"><i class="livicon" data-name="remove-circle" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i></span>
		                                            @{{ galleryname.named }}
		                                	</a></div>
												<div v-for="listchoose in galleryname.listchooses">
													<img :src="listchoose.img_name" alt="Gambar" :idimg="listchoose.id" class="col-sm-3 heightelement pointer">
												</div></div>
										</div>
									</div>
								</div>
							</div>
						</div>
</div>
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-4">
                                        <input type="submit" class="btn btn-primary" value="Create Property">
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDljps3p6FQeNNLP44TOoq6wdA12ULvTnk&libraries=places" type="text/javascript"></script>
    <script>
        var map = new google.maps.Map(document.getElementById('map-canvas'),{
            center:{
                lat: -6.2,
                lng: 106.82
              },
            zoom:15
        });

        var marker = new google.maps.Marker({
            position:{
                lat: -6.2,
                lng: 106.82
            },
            map: map,
            draggable: true
        });

        var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));

        google.maps.event.addListener(searchBox, 'places_changed',function(){

            var places = searchBox.getPlaces();
            var bounds = new google.maps.LatLngBounds();
            var i, place;

            for(i=0;place=places[i];i++){
                bounds.extend(place.geometry.location);
                marker.setPosition(place.geometry.location); // set new marker position.....
            }

            map.fitBounds(bounds);
            map.setZoom(15);
        });

        google.maps.event.addListener(marker, 'position_changed', function() {

            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();

            $('#lat').val(lat);
            $('#lng').val(lng);
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#userid').select2()
            $('#devid').select2()
            $('#status').select2()
        });

(function() {
	function checkisempty(vuew){
		vuew.showlabelemptylist = true;
		for (i in vuew.listpictures) {
			if (vuew.listpictures[i].isshow) {
				vuew.showlabelemptylist = false;
				break;
			}
		}
		vuew.showlabelemptygalleryname = true;
		for (i in vuew.gallerynames) {
			if (vuew.gallerynames[i].isshow) {
				vuew.showlabelemptygalleryname = false;
				break;
			}
		}
	}
	var vm = new Vue({
    	el :'#vuegallery',
    	data : {
    		gallerychoose : false,
    		textaddphotobutton : 'Add Photo',
    		gallerynames : [],
    		listpictures : [],
    		isaddphoto: true,
    		galleryidlist : [],
    		showlabelemptylist:true,
    		showlabelemptygalleryname:false,
    	},
    	methods : {
    		viewaddphotogallery:function(){
    			this.gallerychoose = !this.gallerychoose;
    			if (this.isaddphoto) {
    				this.textaddphotobutton = 'Close Photo';
    			} else {
    				this.textaddphotobutton = 'Add Photo';
    			}
    			this.isaddphoto = !this.isaddphoto;
    		},
    		addpicturechoose : function(datalist){
    			for (x in datalist) {
        			pictures = [];
        			datagallery = datalist[x];
        			if (datagallery.pictures.length > 0) {
        				picturedatas = datagallery.pictures;
            			for (i in picturedatas) {
            				pictures.push({
            					img_name : picturedatas[i].img,
            					id : picturedatas[i].id,
            				});
            			}
        			}
    				this.gallerynames.push({
        				named : datagallery.galleryname,
        				listchooses : pictures,
        				id : datagallery.id,
        				isshow: datagallery.isshow
        			});
        			this.listpictures.push({
        				named : datagallery.galleryname,
        				listchooses : pictures,
        				id : datagallery.id,
        				isshow: false
        			});
    			}
    			checkisempty(this);
    		},
    		addtothis : function(gallery, e) {
    			gallery.isshow = false;
    			this.galleryidlist.push(gallery.id);
    			lists = this.listpictures;
    			for (i in lists) {
					if (lists[i].id == gallery.id) {
						lists[i].isshow = true;
						break;
					}
    			}
    			checkisempty(this);
    		},
    		removefromthis: function(gallery, e){
    			gallery.isshow = false;
    			lists = this.gallerynames;
    			id = gallery.id;
    			var index = this.galleryidlist.indexOf(id);
    			this.galleryidlist.splice(index, 1);
    			for (i in lists) {
					if (lists[i].id == id) {
						lists[i].isshow = true;
						break;
					}
    			}
    			checkisempty(this);
    		}
    	}
    	});
        	vm.addpicturechoose([
        <?php
        $isfirst = true;
        foreach ($galeries as $gallery) {
        	if ($isfirst) {
        		$isfirst = false;
        	} else {
        		echo ',';
        	}
        	echo '{galleryname:"'.$gallery->galleryname.'", id: '.$gallery->id.', isshow : true, pictures:[';
        	$ispicturefirst = true;
        	foreach ($gallery->properyimg as $imgshow) {
        		if ($ispicturefirst) {
        			echo '{id:'.$imgshow->id.', img:\''.URL::to('uploads/picture/'.$imgshow->img_name).'\'}';
        			$ispicturefirst = false;
        		} else {
        			echo ',{id:'.$imgshow->id.', img:\''.URL::to('uploads/picture/'.$imgshow->img_name).'\'}';
        		}
        	}
			echo ']}';
		}
                ?>
		]);
        })();
    </script>

@stop
