@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
  Edit Property Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/pages/addproperty.css') }}" rel="stylesheet">
@stop

@section('content')
        <section class="content-header">
            <h1>Edit Property Form</h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading clearfix">
                            <div class="panel-title pull-left">
                              <div class="caption">
                                <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Edit Property
                              </div>
                            </div>
                            <div class="pull-right">
                              <a href="/admin/property" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Property</a>
                            </div>
                        </div>
                        <div class="panel-body border">
                          {!! Form::model($detail, ['method' => 'PUT', 'url' => $detail->editUrl(), 'class' => 'form-horizontal form-bordered']) !!}
                            <div class="form-group">
                                <label class="col-md-4 control-label">ID</label>
                                <div class="col-md-6">
                                    <input type="text" name="id" class="form-control" value="{!! old('id',$detail->id) !!}" readonly>
                                </div>
                            </div>
                            {{-- <div class="form-group">
                                <label class="col-md-4 control-label">Nama Pemohon</label>
                                <div class="col-md-6">
                                    <select id="userid" name="userid" class="form-control" style="width: 100%">
                                      @foreach ($users as $u)
                                        <option value="{{ $u->id }}" @if($u->id === $detail->userid) selected @endif> {{ $u->fullname }} </option>
                                      @endforeach
                                    </select>
                                </div>
                            </div> --}}
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Developer</label>
                                <div class="col-md-6">
                                    <select id="devid" name="devid" class="form-control" style="width: 100%">
                                    @foreach ($dev as $d)
                                      <option value="{{ $d->id }}" @if($d->id === $detail->devid) selected @endif>
                                        {{ $d->nama }}
                                      </option>
                                    @endforeach
                                  </select>
                                </div>
                            </div>
                            <div class="form-group">
                                  <label class="col-md-4 control-label">Nama Property *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="nama" class="form-control" value="{!! old('nama',$detail->nama) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Property Type</label>
                                  <div class="col-md-6">
                                      <select id="status" name="status" class="form-control" style="width: 100%">
                                          @foreach($statusr as $code=>$s)
                                            <option value="{{ $code }}" @if($code === $detail->type) selected @endif>
                                              {{$s}}
                                            </option>
                                          @endforeach
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Alamat *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="alamat" class="form-control" value="{!! old('alamat',$detail->alamat) !!}" id="searchmap" required>
                                      <div id="map-canvas" class="form-control" style="margin-top: 10px; width: 100%; height: 300px; padding: 0px;"></div>
                                      <p class="help-block">silahkan set alamat dan lokasi marker</p>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Latitude</label>
                                  <div class="col-md-6">
                                      <input type="text" name="lat" class="form-control" id="lat" value="{!! old('lat',$detail->lat) !!}" readonly>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Longitude</label>
                                  <div class="col-md-6">
                                      <input type="text" name="lng" class="form-control" id="lng" value="{!! old('lng',$detail->lng) !!}" readonly>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Deskripsi</label>
                                  <div class="col-md-6">
                                      <textarea name="deskripsi" rows="5" class="form-control">{!! old('deskripsi',$detail->deskripsi) !!}</textarea>
                                  </div>
                              </div>
                              {{-- <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Tipe *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="tipe" class="form-control" value="{!! old('tipe',$detail->tipe) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Luas Tanah *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="luas_tanah" class="form-control" value="{!! old('luas_tanah',$detail->luas_tanah) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Luas Bangunan *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="luas_bangunan" class="form-control" value="{!! old('luas_bangunan',$detail->luas_bangunan) !!}" required>
                                  </div>
                              </div> --}}
                              {{-- <div class="form-group">
                                  <label class="col-md-4 control-label">Harga Jual *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="harga_jual" id="autonum1" class="form-control" value="{!! old('harga_jual',$detail->harga_jual) !!}" required>
                                  </div>
                              </div> --}}
                              {{-- <div class="form-group">
                                  <label class="col-md-4 control-label">Harga Beli *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="harga_beli" id="autonum2" class="form-control" value="{!! old('harga_beli',$detail->harga_beli) !!}" required>
                                  </div>
                              </div> --}}
                              <div class="form-group">
                                  <label class="col-md-4 control-label">No. Hak Guna Bangunan (HGB)</label>
                                  <div class="col-md-6">
                                      <input type="text" name="nomor_hgb" class="form-control" value="{!! old('nomor_hgb',$detail->nomor_hgb) !!}">
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Pemilik Terdaftar *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="pemilik_terdaftar" class="form-control" value="{!! old('pemilik_terdaftar',$detail->pemilik_terdaftar) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Luas Lahan *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="luas_lahan" class="form-control" value="{!! old('luas_lahan',$detail->luas_lahan) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">HGB Expire Date</label>
                                  <div class="col-md-6">
                                      <input type="date" name="hgb_expire_date" class="form-control" value="{!! old('hgb_expire_date',$detail->hgb_expire_date) !!}">
                                  </div>
                              </div>
                              {{-- <div class="form-group">
                                  <label class="col-md-4 control-label">Lokasi *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="lokasi" class="form-control" value="{!! old('lokasi',$detail->lokasi) !!}" required>
                                  </div>
                              </div> --}}
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Notes</label>
                                  <div class="col-md-6">
                                      <input type="text" name="notes" class="form-control" value="{!! old('notes',$detail->notes) !!}">
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">No. SPH APH</label>
                                  <div class="col-md-6">
                                      <input type="text" name="nomor_sph_aph" class="form-control" value="{!! old('nomor_sph_aph',$detail->nomor_sph_aph) !!}">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Tanggal SPH APH</label>
                                  <div class="col-md-6">
                                      <input type="date" name="tanggal_sph_aph" class="form-control" value="{!! old('tanggal_sph_aph',$detail->tanggal_sph_aph) !!}">
                                  </div>
                              </div>
                              {{-- <div class="form-group">
                                  <label class="col-md-4 control-label">Pihak *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="pihak" class="form-control" value="{!! old('pihak',$detail->pihak) !!}" required>
                                  </div>
                              </div> --}}
                              <div id="vuegallery" class="layoutgallery">
                                  <button type=button class="btn buttonmargin" v-bind:class="[isaddphoto ? 'btn-warning' : 'btn-primary']" @click="viewaddphotogallery">@{{ textaddphotobutton }}</button>
                      						<div class="form-group" v-show="gallerychoose">
                      							<div class="col-md-12">
                      				                <div class="panel panel-info">
                      				                    <div class="panel-heading">
                      				                        <h3 class="panel-title">
                      				                            <i class="livicon" data-name="crop" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                      				                            Gallery For Choose
                      				                        </h3>
                      				                                <span class="pull-right clickable">
                      				                                    <i class="glyphicon glyphicon-chevron-up"></i>
                      				                                </span>
                      				                    </div>
                      									<div class="panel-body">
                      										<span v-show="showlabelemptygalleryname">Semua Galeri telah di pakai Properti lain</span>
                      										<div v-for="galleryname in gallerynames" class="col-md-12" v-bind:class="[galleryname.isshow ? border1px : '']">
                      										<div v-show="galleryname.isshow"><div class="infolabel"><a class="btn btn-info btn-labeled marginbottom10" role="button" @click="addtothis(galleryname, $event)">
                      		                                            <span class="btn-label"><i class="livicon" data-name="checked-on" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i></span>
                      		                                            @{{ galleryname.named }}
                      		                                	</a></div>
                      												<div v-for="listchoose in galleryname.listchooses">
                      													<img :src="listchoose.img_name" alt="Gambar" :idimg="listchoose.id" class="col-sm-3 heightelement pointer">
                      												</div></div>
                      										</div>
                      									</div>
                      									<div class="panel-footer">
                      										<button type=button class="btn btn-primary buttonmargin" @click="viewaddphotogallery">@{{ textaddphotobutton }}</button>
                      									</div>
                      								</div>
                      							</div>
                      						</div>

                      						<div class="form-group">
                      							<div class="col-md-12">
                      				                <div class="panel panel-success">
                      				                    <div class="panel-heading">
                      				                        <h3 class="panel-title">
                      				                            <i class="livicon" data-name="crop" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                      				                            Property's Photo <input type="hidden" name="galleryidlist" :value="galleryidlist">
                      				                        </h3>
                      				                                <span class="pull-right clickable">
                      				                                    <i class="glyphicon glyphicon-chevron-up"></i>
                      				                                </span>
                      				                    </div>
                      									<div class="panel-body">
                      										<span v-show="showlabelemptylist">Galeri masih kosong</span>
                      										<div v-for="galleryname in listpictures" v-bind:class="[galleryname.isshow ? border1pxgreen : '']" class="col-md-12">
                      										<div v-show="galleryname.isshow"><div class="infolabelgreen"><a class="btn btn-success btn-labeled marginbottom10" role="button" @click="removefromthis(galleryname, $event)">
                      		                                            <span class="btn-label"><i class="livicon" data-name="remove-circle" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i></span>
                      		                                            @{{ galleryname.named }}
                      		                                	</a></div>
                      												<div v-for="listchoose in galleryname.listchooses">
                      													<img :src="listchoose.img_name" alt="Gambar" :idimg="listchoose.id" class="col-sm-3 heightelement pointer">
                      												</div></div>
                      										</div>
                      									</div>
                      								</div>
                      							</div>
                      						</div>
                              </div>
                              <div class="form-group form-actions">
                                  <div class="col-md-9 col-md-offset-4">
                                      <input type="submit" class="btn btn-primary" value="Save Property">
                                  </div>
                              </div>
                          {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDljps3p6FQeNNLP44TOoq6wdA12ULvTnk&libraries=places" type="text/javascript"></script>
    <script>
        var lat = {{ $detail->lat }};
        var lng = {{ $detail->lng }};

        var map = new google.maps.Map(document.getElementById('map-canvas'), {
            center:{
              lat: lat,
              lng: lng,
            },
            zoom:15
        });

        var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));

        google.maps.event.addListener(searchBox, 'places_changed',function(){

            var places = searchBox.getPlaces();
            var bounds = new google.maps.LatLngBounds();
            var i, place;

            for(i=0;place=places[i];i++){
                bounds.extend(place.geometry.location);
                marker.setPosition(place.geometry.location); // set baru untuk marker position.....
            }

            map.fitBounds(bounds);
            map.setZoom(15);
        });

        var marker = new google.maps.Marker({
            position:{
                lat: lat,
                lng: lng
            },
            map: map,
            draggable: true
        });

        google.maps.event.addListener(marker, 'position_changed', function() {

            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();

            $('#lat').val(lat);
            $('#lng').val(lng);
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#userid').select2()
            $('#devid').select2()
            $('#propid').select2()
            $('#investorid').select2()
            $('#jenis').select2()
            $('#tujuan').select2()
            $('#bayar').select2()
            $('#status').select2()
            $('#jaminan').select2()
        });
        (function() {
        	function checkisempty(vuew){
        		vuew.showlabelemptylist = true;
        		for (i in vuew.listpictures) {
        			if (vuew.listpictures[i].isshow) {
        				vuew.showlabelemptylist = false;
        				break;
        			}
        		}
        		vuew.showlabelemptygalleryname = true;
        		for (i in vuew.gallerynames) {
        			if (vuew.gallerynames[i].isshow) {
        				vuew.showlabelemptygalleryname = false;
        				break;
        			}
        		}
        	}
        	var vm = new Vue({
            	el :'#vuegallery',
            	data : {
            		gallerychoose : false,
            		textaddphotobutton : 'Add Photo',
            		gallerynames : [],
            		listpictures : [],
            		isaddphoto: true,
            		galleryidlist : [],
            		showlabelemptylist:true,
            		showlabelemptygalleryname:false,
            	},
            	methods : {
            		viewaddphotogallery:function(){
            			this.gallerychoose = !this.gallerychoose;
            			if (this.isaddphoto) {
            				this.textaddphotobutton = 'Close Photo';
            			} else {
            				this.textaddphotobutton = 'Add Photo';
            			}
            			this.isaddphoto = !this.isaddphoto;
            		},
            		addpicturechoose : function(ispicturechoose, datalist){
            			for (x in datalist) {
                			pictures = [];
                			datagallery = datalist[x];
                			if (datagallery.pictures.length > 0) {
                				picturedatas = datagallery.pictures;
                    			for (i in picturedatas) {
                    				pictures.push({
                    					img_name : picturedatas[i].img,
                    					id : picturedatas[i].id,
                    				});
                    			}
                			}
                			datapush = {
	                				named : datagallery.galleryname,
	                				listchooses : pictures,
	                				id : datagallery.id,
	                				isshow: datagallery.isshow
	                			};
                			if (ispicturechoose) {
	            				this.gallerynames.push(datapush);
                			} else {
                				this.listpictures.push(datapush);
                			}
            			}
            			checkisempty(this);
            		},
            		addtothis : function(gallery, e) {
            			gallery.isshow = false;
            			this.galleryidlist.push(gallery.id);
            			lists = this.listpictures;
            			for (i in lists) {
        					if (lists[i].id == gallery.id) {
        						lists[i].isshow = true;
        						break;
        					}
            			}
            			checkisempty(this);
            		},
            		removefromthis: function(gallery, e){
            			gallery.isshow = false;
            			lists = this.gallerynames;
            			id = gallery.id;
            			var index = this.galleryidlist.indexOf(id);
            			this.galleryidlist.splice(index, 1);
            			for (i in lists) {
        					if (lists[i].id == id) {
        						lists[i].isshow = true;
        						break;
        					}
            			}
            			checkisempty(this);
            		}
            	}
            	});
        	gallerychooser = [];
        	propertygallery = [];
                <?php
                foreach ($galeries as $gallery) {
                	echo 'gallerychooser.push({galleryname:"'.$gallery->galleryname.'", id: '.$gallery->id.', isshow : true, pictures:[';
                	$ispicturefirst = true;
                	foreach ($gallery->properyimg as $imgshow) {
                		if ($ispicturefirst) {
                			echo '{id:'.$imgshow->id.', img:\''.URL::to('uploads/picture/'.$imgshow->img_name).'\'}';
                			$ispicturefirst = false;
                		} else {
                			echo ',{id:'.$imgshow->id.', img:\''.URL::to('uploads/picture/'.$imgshow->img_name).'\'}';
                		}
                	}
                	echo ']});';
                	echo 'propertygallery.push({galleryname:"'.$gallery->galleryname.'", id: '.$gallery->id.', isshow : false, pictures:[';
                	$ispicturefirst = true;
                	foreach ($gallery->properyimg as $imgshow) {
                		if ($ispicturefirst) {
                			echo '{id:'.$imgshow->id.', img:\''.URL::to('uploads/picture/'.$imgshow->img_name).'\'}';
                			$ispicturefirst = false;
                		} else {
                			echo ',{id:'.$imgshow->id.', img:\''.URL::to('uploads/picture/'.$imgshow->img_name).'\'}';
                		}
                	}
                	echo ']});';
                }
                foreach ($propertyphotos as $gallery) {
                	echo 'gallerychooser.push({galleryname:"'.$gallery->galleryname.'", id: '.$gallery->id.', isshow : false, pictures:[';
                	$ispicturefirst = true;
                	foreach ($gallery->properyimg as $imgshow) {
                		if ($ispicturefirst) {
                			echo '{id:'.$imgshow->id.', img:\''.URL::to('uploads/picture/'.$imgshow->img_name).'\'}';
                			$ispicturefirst = false;
                		} else {
                			echo ',{id:'.$imgshow->id.', img:\''.URL::to('uploads/picture/'.$imgshow->img_name).'\'}';
                		}
                	}
                	echo ']});';
                	echo 'propertygallery.push({galleryname:"'.$gallery->galleryname.'", id: '.$gallery->id.', isshow : true, pictures:[';
                	$ispicturefirst = true;
                	foreach ($gallery->properyimg as $imgshow) {
                		if ($ispicturefirst) {
                			echo '{id:'.$imgshow->id.', img:\''.URL::to('uploads/picture/'.$imgshow->img_name).'\'}';
                			$ispicturefirst = false;
                		} else {
                			echo ',{id:'.$imgshow->id.', img:\''.URL::to('uploads/picture/'.$imgshow->img_name).'\'}';
                		}
                	}
                	echo ']});';
                	echo 'vm.galleryidlist.push('.$gallery->id.');';
                }
                ?>
                vm.addpicturechoose(true, gallerychooser);
                vm.addpicturechoose(false, propertygallery);
        })();
    </script>
@stop
