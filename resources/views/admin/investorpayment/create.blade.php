@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
  Create Investor Payment Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <style>
        .form-bordered .form-group > div {
            border-left: 0px;
        }
    </style>
@stop

@section('content')
        <section class="content-header">
            <h1>Investor Payment Form</h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary filterable">
                        <div class="panel-heading clearfix">
                            <div class="panel-title pull-left">
                              <div class="caption">
                                <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Create Investor Payment
                              </div>
                            </div>
                            <div class="pull-right">
                              <a href="/admin/investorpayment" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Pembayaran</a>
                            </div>
                        </div>
                        <div class="panel-body border">
                            {!! Form::open(array('url' => $app->danaUrl(), 'method' => 'post', 'class' => 'form-horizontal form-bordered')) !!}
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Application ID</label>
                                    <div class="col-md-4">
                                        <input type="text" name="appid" class="form-control" value="{{ $app->id }}" readonly>
                                    </div>
                                    <div class="col-md-1">
                                        <a href="#app" class="btn btn-sm btn-default" data-toggle="modal" data-href="#app">lihat detail</a>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Alokasi ID</label>
                                    <div class="col-md-4">
                                        <input type="text" name="alokasiid" class="form-control" value="{{ $alokasi->id }}" readonly>
                                    </div>
                                    <div class="col-md-1">
                                        <a href="#alokasi" class="btn btn-sm btn-default" data-toggle="modal" data-href="#alokasi">lihat detail</a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Status *</label>
                                    <div class="col-md-6">
                                        <select id="status_pembayaran" name="status_pembayaran" class="form-control" style="width: 100%">
                                          @foreach($status as $code=>$s)
                                            <option value="{{ $code }}">
                                              {{$s}}
                                            </option>
                                          @endforeach
                                        </select>
                                        {{-- <input type="text" name="status_pembayaran" class="form-control" value="Pendanaan untuk" readonly> --}}
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Tanggal Payment *</label>
                                    <div class="col-md-6">
                                        <input type="date" name="tgl_pembayaran" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Amount *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="amount" id="autonum1" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Keterangan</label>
                                    <div class="col-md-6">
                                        <input type="text" name="keterangan" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-4">
                                        <input type="submit" class="btn btn-primary" value="Create">
                                    </div>
                                </div>
                            {!! Form::close() !!}

                            {{-- APPLICATION MODAL --}}
                            <div class="modal fade in" id="app" role="dialog" aria-hidden="false" style="display:none;">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Detail App</h4>
                                        </div>
                                        <div class="modal-body">
                                            <table class="table table-striped table-bordered" id="table2">
                                                <tbody>
                                                    <tr><td>#</td><td>{{ $app->id }}</a></td></tr>
                                                    <tr><td>User ID</td><td>{{ $app->userid }}</td></tr>
                                                    <tr><td>Dev ID</td><td>{{ $app->devid }}</td></tr>
                                                    <tr><td>Prop ID</td><td>{{ $app->propid }}</td></tr>
                                                    <tr><td>Investor ID</td><td>{{ $app->investorid }}</td></tr>
                                                    <tr><td>Tujuan</td><td>{{ $app->tujuan }}</td></tr>
                                                    <tr><td>Jenis Application</td><td>{{ $app->jenis }}</td></tr>
                                                    <tr><td>Uang Muka</td><td>{{ uang($app->uang_muka) }}</td></tr>
                                                    <tr><td>Sistem Pembayaran</td><td>{{ $app->sis_bayar }}</td></tr>
                                                    <tr><td>Jumlah Pinjaman</td><td>{{ uang($app->pinjaman) }}</td></tr>
                                                    <tr><td>Pinjaman bulanan yang diajukan</td><td>{{ uang($app->pinjaman_bln) }}</td></tr>
                                                    <tr><td>Pinjaman (tahun)</td><td>{{ $app->periode }} tahun</td></tr>
                                                    <tr><td>Pinjaman (bulan)</td><td>{{ $app->jangka_waktu }} bulan</td></tr>
                                                    <tr><td>Status</td><td>{{ $app->status }}</td></tr>
                                                    <tr><td>Outstanding Loan</td><td>{{ uang($app->outstanding_loan) }}</td></tr>
                                                    <tr><td>Installment Times</td><td>{{ format($app->installment_times) }}</td></tr>
                                                    <tr><td>Anggaran Renovasi</td><td>{{ uang($app->renovasi) }}</td></tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- END MODAL --}}

                            {{-- ALOKASI MODAL --}}
                            <div class="modal fade in" id="alokasi" role="dialog" aria-hidden="false" style="display:none;">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Detail Alokasi Dana</h4>
                                        </div>
                                        <div class="modal-body">
                                            <table class="table table-striped table-bordered" id="table2">
                                                <tbody>
                                                    <tr><td>#</td><td>{{ $alokasi->id }}</a></td></tr>
                                                    <tr><td>Investor</td><td>{{ $alokasi->investor->nama }}</td></tr>
                                                    <tr><td>Alokasi Dana</td><td>{{ $alokasi->alokasi_dana }}</td></tr>
                                                    <tr><td>Jumlah Alokasi</td><td>{{ $alokasi->jml_alokasi }}</td></tr>
                                                    <tr><td>Tenor</td><td>{{ $alokasi->tenor }}</td></tr>
                                                    <tr><td>Penempatan Dana</td><td>{{ $alokasi->penempatan_dana }}</td></tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- END MODAL --}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#appid').select2()
            $('#status_pembayaran').select2()
        });
    </script>

@stop
