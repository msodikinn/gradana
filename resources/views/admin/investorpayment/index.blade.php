@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Investor Payment Management
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
@stop

{{-- Page content --}}
@section('content')

        <section class="content-header">
                <h1>Manage Payment</h1>
            </section>
            <!--section ends-->
            <section class="content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary filterable">
                            <div class="panel-heading clearfix">
                              <div class="panel-title pull-left">
                                <div class="caption">
                                  <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> List Payment
                                </div>
                              </div>
                              <div class="panel-title pull-right">
                                <div class="caption">
                                  <a href="/admin/investor/borrower" class="btn btn-default">Danai Peminjam</a>
                                </div>
                              </div>
                            </div>
                            <div class="panel-body table-responsive">
                                <p style="font-size:16px;margin-top:0px;margin-bottom:20px;">Berikut adalah list data pemberian pinjaman kepada peminjam, data akan dapat diproses setelah diapproved oleh admin.</p>
                                <table class="table table-striped table-bordered" id="table1">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Alokasi ID</th>
                                            <th>Application ID</th>
                                            <th>Status</th>
                                            <th>Tanggal Payment</th>
                                            <th>Amount</th>
                                            <th>Keterangan</th>
                                            <th style="width: 100px">Approved</th>
                                            <th>Detail</th>
                                            {{-- <th>Delete</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($invespay as $i)
                                            <tr role="row" class="odd">
                                                <td><a href="{{ $i->detailUrl() }}">{{ $i->id }}</a></td>
                                                <td><a href="{{ $i->appUrl() }}">{{ $i->alokasiid }}</a></td>
                                                <td><a href="{{ $i->appUrl() }}">{{ $i->appid }}</a></td>
                                                <td>{{ $i->status_pembayaran }}</td>
                                                <td>{{ format($i->tgl_pembayaran) }}</td>
                                                <td>{{ uang($i->amount) }}</td>
                                                <td>{{ $i->keterangan }}</td>
                                                @if($i->approved == 0)
                                                    @if(Sentinel::getuser()->status == "Admin")
                                                      <td><a href="{{ $i->approved() }}" class="btn btn-sm btn-primary">Approved</a></td>
                                                    @else
                                                      <td><i class="fa fa-fw fa-times"></i> Unapproved</td>
                                                    @endif
                                                @else
                                                    @if(Sentinel::getuser()->status == "Admin")
                                                        <td><a href="{{ $i->unapproved() }}" class="btn btn-sm btn-primary">Unapproved</a></td>
                                                    @else
                                                        <td><i class="fa fa-fw fa-check"></i> Approved</td>
                                                    @endif
                                                @endif
                                                <td><a class="edit" href="{{ $i->detailUrl() }}">Detail</a></td>
                                                {{-- <td><a class="edit" href="{{ $b->delUrl() }}">delete</a></td> --}}
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Third Basic Table Ends Here-->
            </section>
            <!-- content -->

    @stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
@stop
