@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
  Investor Category Edit Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
        <section class="content-header">
            <h1>Investor Category Form</h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="livicon" data-name="bell" data-loop="true" data-color="#fff" data-hovercolor="#fff" data-size="18"></i>
                                Edit
                            </h3>
                        </div>
                        <div class="panel-body border">
                          {!! Form::model($cat, ['method' => 'PUT', 'url' => $cat->editUrl(), 'class' => 'form-horizontal form-bordered']) !!}
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Nama</label>
                                  <div class="col-md-6">
                                      <input type="text" name="nama" class="form-control" value="{{ old('nama', $cat->nama) }}" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Keterangan</label>
                                  <div class="col-md-6">
                                      <input type="text" name="keterangan" class="form-control" value="{{ old('nama', $cat->keterangan) }}" required>
                                  </div>
                              </div>
                              <div class="form-group form-actions">
                                  <div class="col-md-9 col-md-offset-4">
                                      <input type="submit" class="btn btn-primary" value="Update Investor Category">
                                  </div>
                              </div>
                          {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script>
      $(document).ready(function() {
          $('#name').select2()
          $('#perlu').select2()
          $('#jenis').select2()
          $('#nama_dev').select2()
      });
    </script>

@stop
