@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
  Pemohon Create Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
    <section class="content-header">
        <h1>Pemohon Form</h1>
    </section>
    <section class="content">
    <div class="row">
		<div class="col-lg-12">
			<ul class="nav nav-tabs ">
				<li class="active">
					<a href="">Pemohon</a>
				</li>
			</ul>
		</div>
	</div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary filterable">
                    <div class="panel-heading clearfix">
                        <div class="panel-title pull-left">
                          <div class="caption">
                            <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Create Pemohon
                          </div>
                        </div>
                        {{-- <div class="pull-right">
                          <a href="/admin/pemohon" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Pemohon</a>
                        </div> --}}
                    </div>
                    <div class="panel-body border">
                        {!! Form::open(array('url' => route('pemohonCreate'), 'method' => 'post', 'class' => 'form-horizontal form-bordered', 'files'=> true)) !!}
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nama Pemohon</label>
                                <div class="col-md-6">
                                    @if (permission() == "Applicant")
                                      <input type="hidden" name="userid" value="{{ Sentinel::getuser()->id }}">
                                      <input type="text" value="{{ Sentinel::getuser()->fullname }}" class="form-control" readonly>
                                    @elseif(permission() == "Admin")
                                      <select id="userid" name="userid" class="form-control" style="width: 100%">
                                        @foreach($users as $u)
                                          <option value="{{ $u->id }}">
                                            {{$u->fullname}}
                                          </option>
                                        @endforeach
                                      </select>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Full Name Clean *</label>
                                <div class="col-md-6">
                                    <input type="text" name="fullname_clean" class="form-control" required>
                                    <p class="help-block">tanpa title dan gelar</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tempat Lahir *</label>
                                <div class="col-md-6">
                                    <input type="text" name="tmp_lahir" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">NO KTP *</label>
                                <div class="col-md-6">
                                    <input type="text" name="ktp" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">NPWP *</label>
                                <div class="col-md-6">
                                    <input type="text" name="npwp" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Nama lengkap Ibu kandung sesuai Kartu KK *</label>
                                <div class="col-md-6">
                                    <input type="text" name="nama_ibu" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Status Kawin</label>
                                <div class="col-md-6">
                                    <select id="status_kawin" name="status_kawin" class="form-control" style="width: 100%">
                                      @foreach($kawin as $code=>$s)
                                        <option value="{{ $code }}">
                                          {{$s}}
                                        </option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Jumlah Tanggungan *</label>
                                <div class="col-md-2">
                                    <input type="text" name="tanggungan" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Pendidikan Terakhir</label>
                                <div class="col-md-6">
                                    <select id="pendidikan" name="pendidikan" class="form-control" style="width: 100%">
                                      @foreach($pendidikan as $code=>$p)
                                        <option value="{{ $code }}">
                                          {{$p}}
                                        </option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Status rumah atau bangungan yang saat ini ditempati</label>
                                <div class="col-md-6">
                                    <select id="status_rumah" name="status_rumah" class="form-control" style="width: 100%">
                                      @foreach($rumah as $code=>$r)
                                        <option value="{{ $code }}">
                                          {{$r}}
                                        </option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Alamat KTP</label>
                                <div class="col-md-6">
                                    <input type="text" name="alamat_ktp" class="form-control">
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label for="propinsi_ktp" class="col-md-4 control-label">Propinsi</label>
                                <div class="col-md-6">
                                    {{ Form::select('propinsi_ktp', $propinsi, null, array('id' => 'sPropinsi', 'style'=>'width: 100%'))  }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="kota_ktp" class="col-md-4 control-label">Kabupaten / Kota</label>
                                <div class="col-md-6">
                                    {{ Form::select('kota_ktp', array(), null, array('id' => 'sKota', 'style'=>'width: 100%')) }}
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label for="kecamatan_ktp" class="col-md-4 control-label">Kecamatan</label>
                                <div class="col-md-6">
                                    {{ Form::select('kecamatan_ktp', array(), null, array('id' => 'sKecamatan', 'style'=>'width: 100%')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="kelurahan_ktp" class="col-md-4 control-label">Desa / Kelurahan</label>
                                <div class="col-md-6">
                                    {{ Form::select('kelurahan_ktp', array(), null, array('id' => 'sDesa', 'style'=>'width: 100%')) }}
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Kodepos</label>
                                <div class="col-md-6">
                                    <input type="text" name="postal_ktp" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Telepon *</label>
                                <div class="col-md-6">
                                    <input type="text" name="telp" class="form-control" required>
                                </div>
                            </div>
                            {{-- <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Fax</label>
                                <div class="col-md-6">
                                    <input type="text" name="fax" class="form-control">
                                </div>
                            </div> --}}
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Handphone 1</label>
                                <div class="col-md-6">
                                    <input type="text" name="hp1" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Handphone 2</label>
                                <div class="col-md-6">
                                    <input type="text" name="hp2" class="form-control">
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Facebook ID</label>
                                <div class="col-md-6">
                                    <input type="text" name="facebook_id" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Linkedin ID</label>
                                <div class="col-md-6">
                                    <input type="text" name="linkedin_id" class="form-control">
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Jumlah pinjaman di bank (Rp) *</label>
                                <div class="col-md-6">
                                    <input type="text" name="amount_balance" id="autonum1" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">KTP Image *</label>
                                <div class="col-md-6">
                                    <input type="file" name="ktp_img" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">NPWP Image *</label>
                                <div class="col-md-6">
                                    <input type="file" name="npwp_img" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Kartu Keluarga Image *</label>
                                <div class="col-md-6">
                                    <input type="file" name="kk_img" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Tagihan Listrik Image</label>
                                <div class="col-md-6">
                                    <input type="file" name="tagihan_listrik" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tagihan Air Image</label>
                                <div class="col-md-6">
                                    <input type="file" name="tagihan_air" class="form-control">
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Tagihan Telp Image</label>
                                <div class="col-md-6">
                                    <input type="file" name="tagihan_telp" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">BI Checking Report</label>
                                <div class="col-md-6">
                                    <input type="file" name="bi_checking" class="form-control">
                                </div>
                            </div>
                            {{-- <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Akta Image *</label>
                                <div class="col-md-6">
                                    <input type="file" name="akta_img" class="form-control" required>
                                </div>
                            </div> --}}
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">SPT</label>
                                <div class="col-md-6">
                                    <input type="file" name="ssp" class="form-control">
                                </div>
                            </div>
                            {{-- <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Customer Verified</label>
                                <div class="col-md-6">
                                    <input type="text" name="cust_verified" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Customer Verified Date</label>
                                <div class="col-md-6">
                                    <input type="text" name="cust_verified_date" class="form-control">
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Jumlah Anggota</label>
                                <div class="col-md-6">
                                    <input type="text" name="jml_anggota" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Simpanan Wajib</label>
                                <div class="col-md-6">
                                    <input type="text" name="simp_wajib" class="form-control">
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Simpanan Pokok</label>
                                <div class="col-md-6">
                                    <input type="text" name="simp_pokok" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Sumber Dana</label>
                                <div class="col-md-6">
                                    <input type="text" name="sumber_dana" class="form-control">
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Bentuk Usaha</label>
                                <div class="col-md-6">
                                    <input type="text" name="bentuk_usaha" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nomor Akta</label>
                                <div class="col-md-6">
                                    <input type="text" name="nomor_akta" class="form-control">
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Domisili Akta</label>
                                <div class="col-md-6">
                                    <input type="text" name="domisili_akta" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nama Penanggung Jawab</label>
                                <div class="col-md-6">
                                    <input type="text" name="nama_penanggungjawab" class="form-control">
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Contact Person</label>
                                <div class="col-md-6">
                                    <input type="text" name="contact_person" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Contact Person Position</label>
                                <div class="col-md-6">
                                    <input type="text" name="contact_position" class="form-control">
                                </div>
                            </div> --}}
                            <div class="form-group form-actions">
                                <div class="col-md-9 col-md-offset-4">
                                    <input type="submit" class="btn btn-primary" value="Step 2: Create Job Pemohon">
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script>
      $(document).ready(function() {
          $('#status_kawin').select2()
          $('#pendidikan').select2()
          $('#status_rumah').select2()
          $('#userid').select2()
          $('#sPropinsi').select2()
          $('#sDesa').select2()
          $('#sKecamatan').select2()
          $('#sKota').select2()
      });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#sPropinsi').on('change', function(){
                $.post('{{ URL::to('admin/site/data') }}',
                {
                    '_token': '{{ csrf_token() }}',
                    type: 'kota_ktp',
                    id: $('#sPropinsi').val(),
                }, function(e){
                    $('#sKota').html(e);
                });
                $('#sKecamatan').html('');
                $('#sDesa').html('');

            });
            $('#sKota').on('change', function(){
                $.post('{{ URL::to('admin/site/data') }}',
                {
                    '_token': '{{ csrf_token() }}',
                    type: 'kecamatan_ktp',
                    id: $('#sKota').val()
                }, function(e){
                    $('#sKecamatan').html(e);
                });
                $('#sDesa').html('');
            });
            $('#sKecamatan').on('change', function(){
                $.post('{{ URL::to('admin/site/data') }}',
                {
                    '_token': '{{ csrf_token() }}',
                    type: 'kelurahan_ktp',
                    id: $('#sKecamatan').val()
                }, function(e){
                    $('#sDesa').html(e);
                });
            });
        });
    </script>
@stop
