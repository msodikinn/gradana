@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Detail Pemohon
@parent
@stop

@section('header_styles')
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/modal/css/component.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/fancybox/jquery.fancybox.css') }}" media="screen">
@stop

@section('content')
<section class="content-header">
    <h1>Detail Pemohon</h1>
</section>

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <div class="panel-title pull-left">
                    <div class="caption">
                        <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Detail {{ $detailPemohon->users->fullname }}
                    </div>
                </div>
                <div class="pull-right">
                    <a href="/admin/pemohon" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Pemohon</a>
                    <a href="{{ $detailPemohon->editUrl() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-minus"></span> Edit Pemohon</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <ul class="nav nav-pills">
                    <li class="active">
                        <a href="#tab6" data-toggle="tab">Data Pemohon</a>
                    </li>
                    <li>
                        <a href="#tab7" data-toggle="tab">Pekerjaan Pemohon</a>
                    </li>
                    @if (App\Pemohon::find($detailPemohon->id)->status_kawin != "Lajang")
                        <li>
                            <a href="#tab8" data-toggle="tab">Data Pasangan</a>
                        </li>
                        <li>
                            <a href="#tab9" data-toggle="tab">Pekerjaan Pasangan</a>
                        </li>
                    @endif
                    <li>
                        <a href="#tab10" data-toggle="tab">Keluarga</a>
                    </li>
                    <li>
                        <a href="#tab11" data-toggle="tab">Penghasilan</a>
                    </li>
                    <li>
                        <a href="#tab12" data-toggle="tab">Perbankan</a>
                    </li>
                </ul>
                <div  class="tab-content mar-top">
                    <div id="tab6" class="tab-pane fade active in">
                        @include('admin.users.partials._pemohon')
                    </div>
                    <div id="tab7" class="tab-pane fade">
                        @include('admin.users.partials._jobPemohon')
                    </div>
                    @if (App\Pemohon::find($detailPemohon->id)->status_kawin != "Lajang")
                        <div id="tab8" class="tab-pane fade">
                            @include('admin.users.partials._suamiIstri')
                        </div>
                        <div id="tab9" class="tab-pane fade">
                            @include('admin.users.partials._jobSuamiIstri')
                        </div>
                    @endif
                    <div id="tab10" class="tab-pane fade">
                        @include('admin.users.partials._keluarga')
                    </div>
                    <div id="tab11" class="tab-pane fade">
                        @include('admin.users.partials._penghasilan')
                    </div>
                    <div id="tab12" class="tab-pane fade">
                        @include('admin.users.partials._perbankan')
                    </div>
                </div>
                {{-- <div class="row">
                    <div class="col-md-6">
                        <table class="table ">
                            <tr><td style="width: 180px">ID</td><td>{{ $detail->id }}</td></tr>
                            <tr><td>Nama Pemohon</td><td>{{ $detail->users->fullname }}</td></tr>
                            <tr><td>Full Name Clean</td><td>{{ $detail->fullname_clean }}</td></tr>
                            <tr><td>Tempat Lahir</td><td>{{ $detail->tmp_lahir }}</td></tr>
                            <tr><td>NO KTP</td><td>{{ $detail->ktp }}</td></tr>
                            <tr><td>NPWP</td><td>{{ $detail->npwp }}</td></tr>
                            <tr><td>Nama lengkap Ibu kandung sesuai Kartu KK</td><td>{{ $detail->nama_ibu }}</td></tr>
                            <tr><td>Status Kawin</td><td>{{ $detail->status_kawin }}</td></tr>
                            <tr><td>Tanggungan</td><td>{{ $detail->tanggungan }}</td></tr>
                            <tr><td>Pendidikan</td><td>{{ $detail->pendidikan }}</td></tr>
                            <tr><td>Status rumah atau bangungan yang saat ini ditempati</td><td>{{ $detail->status_rumah }}</td></tr>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table class="table ">
                            <tr><td>Alamat KTP</td><td>{{ $detail->alamat_ktp }}</td></tr>
                            <tr><td>Kelurahan</td><td>{{ $detail->kelurahan_ktp }}</td></tr>
                            <tr><td>Kecamatan</td><td>{{ $detail->kecamatan_ktp }}</td></tr>
                            <tr><td>Propinsi</td><td>{{ $detail->propinsi_ktp }}</td></tr>
                            <tr><td>Kota</td><td>{{ $detail->kota_ktp }}</td></tr>
                            <tr><td>Kodepos</td><td>{{ $detail->postal_ktp }}</td></tr>
                            <tr><td>Telepon</td><td>{{ $detail->telp }}</td></tr>
                            <tr><td>Handphone 1</td><td>{{ $detail->hp1 }}</td></tr>
                            <tr><td>Handphone 2</td><td>{{ $detail->hp2 }}</td></tr>
                            <tr><td>Facebook id</td><td>{{ $detail->facebook_id }}</td></tr>
                            <tr><td>Linkedin id</td><td>{{ $detail->linkedin_id }}</td></tr>
                            <tr><td>Jumlah pinjaman di bank (Rp)</td><td>{{ $detail->amount_balance }}</td></tr>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <table class="table">
                            <tr>
                                <td style="width: 180px; height: 350px;">KTP img</td>
                                <td>
                                    <a class="fancybox-effects-a" href="{!! $detail->ktp_img == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detail->ktp_img !!}">
                                        {!! $detail->ktp_img == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detail->ktp_img.'" class="img-responsive" style="max-height: 350px">' !!}
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>NPWP img</td>
                                <td>
                                    <a class="fancybox-effects-a" href="{!! $detail->npwp_img == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detail->npwp_img !!}">
                                        {!! $detail->npwp_img == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detail->npwp_img.'" class="img-responsive" style="max-height: 350px">' !!}
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>Akta img</td>
                                <td>
                                    <a class="fancybox-effects-a" href="{!! $detail->akta_img == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detail->akta_img !!}">
                                        {!! $detail->akta_img == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detail->akta_img.'" class="img-responsive" style="max-height: 350px">' !!}
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>KK img</td>
                                <td>
                                    <a class="fancybox-effects-a" href="{!! $detail->kk_img == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detail->kk_img !!}">
                                        {!! $detail->kk_img == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detail->kk_img.'" class="img-responsive" style="max-height: 350px">' !!}
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>BI Checking Report</td>
                                <td>
                                    <a class="fancybox-effects-a" href="{!! $detail->bi_checking == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detail->bi_checking !!}">
                                        {!! $detail->bi_checking == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detail->bi_checking.'" class="img-responsive" style="max-height: 350px">' !!}
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table class="table">
                            <tr>
                                <td style="width: 180px; height: 350px;">Tagihan listrik</td>
                                <td>
                                    <a class="fancybox-effects-a" href="{!! $detail->tagihan_listrik == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detail->tagihan_listrik !!}">
                                        {!! $detail->tagihan_listrik == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detail->tagihan_listrik.'" class="img-responsive" style="max-height: 350px">' !!}
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>Tagihan air</td>
                                <td>
                                    <a class="fancybox-effects-a" href="{!! $detail->tagihan_air == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detail->tagihan_air !!}">
                                        {!! $detail->tagihan_air == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detail->tagihan_air.'" class="img-responsive" style="max-height: 350px">' !!}
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>Tagihan telp</td>
                                <td>
                                    <a class="fancybox-effects-a" href="{!! $detail->tagihan_telp == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detail->tagihan_telp !!}">
                                        {!! $detail->tagihan_telp == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detail->tagihan_telp.'" class="img-responsive" style="max-height: 350px">' !!}
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>Surat ket kerja</td>
                                <td>
                                    <a class="fancybox-effects-a" href="{!! $detail->surat_ket_kerja == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detail->surat_ket_kerja !!}">
                                        {!! $detail->surat_ket_kerja == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detail->surat_ket_kerja.'" class="img-responsive" style="max-height: 350px">' !!}
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>SPT</td>
                                <td>
                                    <a class="fancybox-effects-a" href="{!! $detail->ssp == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detail->ssp !!}">
                                        {!! $detail->ssp == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detail->ssp.'" class="img-responsive" style="max-height: 350px">' !!}
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
</section>
@stop

@section('footer_scripts')

    <script src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
    <script src="{{ asset('assets/vendors/modal/js/classie.js') }}" ></script>
    <script src="{{ asset('assets/vendors/modal/js/modalEffects.js') }}" ></script>
    <script src="{{ asset('assets/vendors/fancybox/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-thumbs.js') }}" ></script>
    <script src="{{ asset('assets/js/pages/gallery.js') }}"  ></script>
@stop
