@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
  Pemohon Edit Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/fancybox/jquery.fancybox.css') }}" media="screen" rel="stylesheet">
@stop

@section('content')
        <section class="content-header">
            <h1>Pemohon Form</h1>
        </section>

        <section class="content">
        	<div class="row">
				<div class="col-lg-12">
					<ul class="nav nav-tabs ">
						<li class="active">
							<a href="">Pemohon</a>
						</li>
						@if (empty($jobpemohonid))
  						<li>
  							<a href="{{ URL::to('admin/jobpemohon/create?backid='.$backid) }}">Pekerjaan Pemohon</a>
  						</li>
						@else
  						<li>
  							<a href="{{ URL::to('admin/jobpemohon/edit/'.$jobpemohonid) }}">Pekerjaan Pemohon</a>
  						</li>
						@endif
            @if (App\Pemohon::find($backid)->status_kawin != "Lajang")
  						@if (!empty($suamisitriid))
    						<li>
    							<a href="{{ URL::to('admin/suamiistri/edit/'.$suamisitriid) }}">Pasangan</a>
    						</li>
  						@endif
  						@if (!empty($jobsuamiistriid))
    						<li>
    							<a href="{{ URL::to('admin/jobsuamiistri/edit/'.$jobsuamiistriid) }}">Pekerjaan Pasangan</a>
    						</li>
  						@endif
            @endif
						@if (!empty($keluargaid))
  						<li>
  							<a href="{{ URL::to('admin/keluarga/edit/'.$keluargaid) }}">Emergency Contact</a>
  						</li>
						@endif
						{{-- @if (!empty($penghasilanid))
  						<li>
  							<a href="{{ URL::to('admin/penghasilan/edit/'.$penghasilanid) }}">Penghasilan</a>
  						</li>
						@endif --}}
						@if (!empty($perbankanid))
  						<li>
  							<a href="{{ URL::to('admin/perbankan/edit/'.$perbankanid) }}">Perbankan</a>
  						</li>
						@endif
            @if ($countApp == 0)
              <li>
                <a href="{{ URL::to('admin/application/create?backid='.$backid) }}">Create Application</a>
              </li>
            @elseif($countApp == 1)
              <li>
                <a href="{{ URL::to('admin/application/edit/'.$applicationid) }}">Application</a>
              </li>
            @elseif($countApp > 1)
              <li>
                <a href="{{ URL::to('admin/application?iduser='.$detail->userid) }}">{{ $countApp }} Application</a>
              </li>
            @endif
					</ul>
				</div>
			</div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading clearfix">
                            <div class="panel-title pull-left">
                              <div class="caption">
                                <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Edit Pemohon
                              </div>
                            </div>
                            <div class="pull-right">
                              <a href="/admin/pemohon" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Pemohon</a>
                            </div>
                        </div>
                        <div class="panel-body border">
                            {!! Form::model($detail, ['method' => 'PUT', 'url' => $detail->editUrl(), 'class' => 'form-horizontal form-bordered', 'files'=> true]) !!}
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">ID</label>
                                    <div class="col-md-6">
                                        <input type="text" name="id" class="form-control" value="{!! old('id',$detail->id) !!}" readonly>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-4 control-label">Nama Pemohon</label>
                                    <div class="col-md-6">
                                        <input type="text" value="{{ $detail->users->fullname }}" class="form-control" readonly>
                                        <input type="hidden" name="userid" value="{{ $detail->users->id }}">
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Full Name Clean *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="fullname_clean" class="form-control" value="{!! old('fullname_clean',$detail->fullname_clean) !!}">
                                        <p class="help-block">tanpa title dan gelar</p>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-4 control-label">Tempat Lahir *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="tmp_lahir" class="form-control" value="{!! old('tmp_lahir',$detail->tmp_lahir) !!}">
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">NO KTP *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="ktp" class="form-control" value="{!! old('ktp',$detail->ktp) !!}">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-4 control-label">NPWP *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="npwp" class="form-control" value="{!! old('npwp',$detail->npwp) !!}">
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Nama lengkap Ibu kandung sesuai Kartu KK *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="nama_ibu" class="form-control" value="{!! old('nama_ibu',$detail->nama_ibu) !!}">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-4 control-label">Status Kawin</label>
                                    <div class="col-md-6">
                                        <select id="status_kawin" name="status_kawin" class="form-control" style="width: 100%">
                                          @foreach($kawin as $code=>$s)
                                            <option value="{{ $code }}" @if($code === $detail->status_kawin) selected @endif>
                                              {{$s}}
                                            </option>
                                          @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Jumlah Tanggungan *</label>
                                    <div class="col-md-2">
                                        <input type="text" name="tanggungan" class="form-control" value="{!! old('tanggungan',$detail->tanggungan) !!}">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-4 control-label">Pendidikan Terakhir</label>
                                    <div class="col-md-6">
                                        <select id="pendidikan" name="pendidikan" class="form-control" style="width: 100%">
                                            @foreach($pendidikan as $code=>$p)
                                              <option value="{{ $code }}" @if($code === $detail->pendidikan) selected @endif>
                                                {{$p}}
                                              </option>
                                            @endforeach
                                          </select>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Status rumah atau bangungan yang saat ini ditempati</label>
                                    <div class="col-md-6">
                                        <select id="status_rumah" name="status_rumah" class="form-control" style="width: 100%">
                                            @foreach($rumah as $code=>$r)
                                              <option value="{{ $code }}" @if($code === $detail->status_rumah) selected @endif>
                                                {{$r}}
                                              </option>
                                            @endforeach
                                          </select>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-4 control-label">Alamat KTP</label>
                                    <div class="col-md-6">
                                        <input type="text" name="alamat_ktp" class="form-control" value="{!! old('alamat_ktp',$detail->alamat_ktp) !!}">
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label for="propinsi_ktp" class="col-md-4 control-label">Propinsi</label>
                                    <div class="col-md-6">
                                        {{ Form::select('propinsi_ktp', $propinsi, $detail->propinsi_ktp, array('id' => 'sPropinsi', 'style'=>'width: 100%'))  }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kota_ktp" class="col-md-4 control-label">Kabupaten / Kota</label>
                                    <div class="col-md-6">
                                        {{ Form::select('kota_ktp', $kota, $detail->kota_ktp, array('id' => 'sKota', 'style'=>'width: 100%')) }}
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label for="kecamatan_ktp" class="col-md-4 control-label">Kecamatan</label>
                                    <div class="col-md-6">
                                        {{ Form::select('kecamatan_ktp', $kecamatan, $detail->kecamatan_ktp, array('id' => 'sKecamatan', 'style'=>'width: 100%')) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kelurahan_ktp" class="col-md-4 control-label">Desa / Kelurahan</label>
                                    <div class="col-md-6">
                                        {{ Form::select('kelurahan_ktp', $desa, $detail->kelurahan_ktp, array('id' => 'sDesa', 'style'=>'width: 100%')) }}
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Jumlah pinjaman di bank (Rp) *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="amount_balance" id="autonum1" class="form-control" value="{!! old('amount_balance',$detail->amount_balance) !!}">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-4 control-label">KTP Image *</label>
                                    <div class="col-md-6">
                                        <div style="margin-bottom: 10px">
                                            <a class="fancybox-effects-a" href="{!! $detail->ktp_img == '' ? '' : '/'.$detail->ktp_img !!}">
                                                {!! $detail->ktp_img == '' ? '' : '<img src="'.'/'.$detail->ktp_img.'" class="img-responsive" style="max-height: 350px">' !!}
                                            </a>
                                        </div>
                                        <div>
                                            <input type="file" name="ktp_img" value="{!! old('ktp_img',$detail->ktp_img) !!}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">NPWP Image *</label>
                                    <div class="col-md-6">
                                        <div style="margin-bottom: 10px">
                                            <a class="fancybox-effects-a" href="{!! $detail->npwp_img == '' ? '' : '/'.$detail->npwp_img !!}">
                                                {!! $detail->npwp_img == '' ? '' : '<img src="'.'/'.$detail->npwp_img.'" class="img-responsive" style="max-height: 350px">' !!}
                                            </a>
                                        </div>
                                        <div>
                                            <input type="file" name="npwp_img" value="{!! old('npwp_img',$detail->npwp_img) !!}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-4 control-label">Kartu Keluarga Image *</label>
                                    <div class="col-md-6">
                                        <div style="margin-bottom: 10px">
                                            <a class="fancybox-effects-a" href="{!! $detail->kk_img == '' ? '' : '/'.$detail->kk_img !!}">
                                                {!! $detail->kk_img == '' ? '' : '<img src="'.'/'.$detail->kk_img.'" class="img-responsive" style="max-height: 350px">' !!}
                                            </a>
                                        </div>
                                        <div>
                                            <input type="file" name="kk_img" value="{!! old('kk_img',$detail->kk_img) !!}">
                                    </div>
                                </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Tagihan Listrik Image</label>
                                    <div class="col-md-6">
                                        <div style="margin-bottom: 10px">
                                            <a class="fancybox-effects-a" href="{!! $detail->tagihan_listrik == '' ? '' : '/'.$detail->tagihan_listrik !!}">
                                                {!! $detail->tagihan_listrik == '' ? '' : '<img src="'.'/'.$detail->tagihan_listrik.'" class="img-responsive" style="max-height: 350px">' !!}
                                            </a>
                                        </div>
                                        <div>
                                            <input type="file" name="tagihan_listrik" value="{!! old('tagihan_listrik',$detail->tagihan_listrik) !!}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-4 control-label">Tagihan Air Image</label>
                                    <div class="col-md-6">
                                        <div style="margin-bottom: 10px">
                                            <a class="fancybox-effects-a" href="{!! $detail->tagihan_air == '' ? '' : '/'.$detail->tagihan_air !!}">
                                                {!! $detail->tagihan_air == '' ? '' : '<img src="'.'/'.$detail->tagihan_air.'" class="img-responsive" style="max-height: 350px">' !!}
                                            </a>
                                        </div>
                                        <div>
                                            <input type="file" name="tagihan_air" value="{!! old('tagihan_air',$detail->tagihan_air) !!}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Tagihan Telp Image</label>
                                    <div class="col-md-6">
                                        <div style="margin-bottom: 10px">
                                            <a class="fancybox-effects-a" href="{!! $detail->tagihan_telp == '' ? '' : '/'.$detail->tagihan_telp !!}">
                                                {!! $detail->tagihan_telp == '' ? '' : '<img src="'.'/'.$detail->tagihan_telp.'" class="img-responsive" style="max-height: 350px">' !!}
                                            </a>
                                        </div>
                                        <div>
                                            <input type="file" name="tagihan_telp" value="{!! old('tagihan_telp',$detail->tagihan_telp) !!}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">BI Checking Report</label>
                                    <div class="col-md-6">
                                        <div style="margin-bottom: 10px">
                                            <a class="fancybox-effects-a" href="{!! $detail->bi_checking == '' ? '' : '/'.$detail->bi_checking !!}">
                                                {!! $detail->bi_checking == '' ? '' : '<img src="'.'/'.$detail->bi_checking.'" class="img-responsive" style="max-height: 350px">' !!}
                                            </a>
                                        </div>
                                        <div>
                                            <input type="file" name="bi_checking" value="{!! old('bi_checking',$detail->bi_checking) !!}">
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="form-group ">
                                    <label class="col-md-4 control-label">Akta Image *</label>
                                    <div class="col-md-6">
                                        <div style="margin-bottom: 10px">
                                            <a class="fancybox-effects-a" href="{!! $detail->akta_img == '' ? '' : '/'.$detail->akta_img !!}">
                                                {!! $detail->akta_img == '' ? '' : '<img src="'.'/'.$detail->akta_img.'" class="img-responsive" style="max-height: 350px">' !!}
                                            </a>
                                        </div>
                                        <div>
                                            <input type="file" name="akta_img" value="{!! old('akta_img',$detail->akta_img) !!}">
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="form-group">
                                    <label class="col-md-4 control-label">SPT</label>
                                    <div class="col-md-6">
                                        <div style="margin-bottom: 10px">
                                            <a class="fancybox-effects-a" href="{!! $detail->ssp == '' ? '' : '/'.$detail->ssp !!}">
                                                {!! $detail->ssp == '' ? '' : '<img src="'.'/'.$detail->ssp.'" class="img-responsive" style="max-height: 350px">' !!}
                                            </a>
                                        </div>
                                        <div>
                                            <input type="file" name="ssp" value="{!! old('ssp',$detail->ssp) !!}">
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="form-group ">
                                    <label class="col-md-4 control-label">Customer Verified</label>
                                    <div class="col-md-6">
                                        <div>
                                            <label class="radio-inline">
                                            {{ Form::radio('cust_verified', '0', $detail->cust_verified == 1, ['id' => 'optionsRadiosInline1']) }}not verified</label>

                                            <label class="radio-inline">
                                            {{ Form::radio('cust_verified', '1', $detail->cust_verified == 1) }}verified</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Customer Verified Date</label>
                                    <div class="col-md-6">
                                        <div style="margin-bottom: 10px">
                                            <a class="fancybox-effects-a" href="{!! $detail->rekening_koran2 == '' ? '' : $detail->rekening_koran2 !!}">
                                                {!! $detail->rekening_koran2 == '' ? '' : '<img src="'.$detail->rekening_koran2.'" class="img-responsive" style="max-height: 350px">' !!}
                                            </a>
                                        </div>
                                        <div>
                                            <input type="date" name="cust_verified_date" class="form-control" value="{!! old('cust_verified_date',$detail->cust_verified_date) !!}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-4 control-label">Jumlah Anggota</label>
                                    <div class="col-md-6">
                                        <input type="text" name="jml_anggota" class="form-control" value="{!! old('jml_anggota',$detail->jml_anggota) !!}">
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Simpanan Wajib</label>
                                    <div class="col-md-6">
                                        <input type="text" name="simp_wajib" class="form-control" value="{!! old('simp_wajib',$detail->simp_wajib) !!}">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-4 control-label">Simpanan Pokok</label>
                                    <div class="col-md-6">
                                        <input type="text" name="simp_pokok" class="form-control" value="{!! old('simp_pokok',$detail->simp_pokok) !!}">
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Sumber Dana</label>
                                    <div class="col-md-6">
                                        <input type="text" name="sumber_dana" class="form-control" value="{!! old('sumber_dana',$detail->sumber_dana) !!}">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-4 control-label">Bentuk Usaha</label>
                                    <div class="col-md-6">
                                        <input type="text" name="bentuk_usaha" class="form-control" value="{!! old('bentuk_usaha',$detail->bentuk_usaha) !!}">
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Nomor Akta</label>
                                    <div class="col-md-6">
                                        <input type="text" name="nomor_akta" class="form-control" value="{!! old('nomor_akta',$detail->nomor_akta) !!}">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-4 control-label">Domisili Akta</label>
                                    <div class="col-md-6">
                                        <input type="text" name="domisili_akta" class="form-control" value="{!! old('domisili_akta',$detail->domisili_akta) !!}">
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Nama Penanggung Jawab</label>
                                    <div class="col-md-6">
                                        <input type="text" name="nama_penanggungjawab" class="form-control" value="{!! old('nama_penanggungjawab',$detail->nama_penanggungjawab) !!}">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-4 control-label">Contact Person</label>
                                    <div class="col-md-6">
                                        <input type="text" name="contact_person" class="form-control" value="{!! old('contact_person',$detail->contact_person) !!}">
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Contact Person Position</label>
                                    <div class="col-md-6">
                                        <input type="text" name="contact_position" class="form-control" value="{!! old('contact_position',$detail->contact_position) !!}">
                                    </div>
                                </div> --}}
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-4">
                                        <input type="submit" class="btn btn-primary" value="Save Pemohon">
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-thumbs.js') }}" ></script>
    <script src="{{ asset('assets/js/pages/gallery.js') }}"  ></script>
    <script>
      $(document).ready(function() {
          $('#status_kawin').select2()
          $('#pendidikan').select2()
          $('#status_rumah').select2()
          $('#userid').select2()
          $('#sPropinsi').select2()
          $('#sDesa').select2()
          $('#sKecamatan').select2()
          $('#sKota').select2()
      });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#sPropinsi').on('change', function(){
                $.post('{{ URL::to('admin/site/data') }}',
                {
                    '_token': '{{ csrf_token() }}',
                    type: 'kota_ktp',
                    id: $('#sPropinsi').val(),
                }, function(e){
                    $('#sKota').html(e);
                });
                $('#sKecamatan').html('');
                $('#sDesa').html('');

            });
            $('#sKota').on('change', function(){
                $.post('{{ URL::to('admin/site/data') }}',
                {
                    '_token': '{{ csrf_token() }}',
                    type: 'kecamatan_ktp',
                    id: $('#sKota').val()
                }, function(e){
                    $('#sKecamatan').html(e);
                });
                $('#sDesa').html('');
            });
            $('#sKecamatan').on('change', function(){
                $.post('{{ URL::to('admin/site/data') }}',
                {
                    '_token': '{{ csrf_token() }}',
                    type: 'kelurahan_ktp',
                    id: $('#sKecamatan').val()
                }, function(e){
                    $('#sDesa').html(e);
                });
            });
        });
    </script>
@stop
