@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Pemohon Management
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <!--section starts-->
        <h1>Manage Pemohon</h1>

    </section>
    <!--section ends-->
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary filterable">
                    <div class="panel-heading clearfix">
                      <div class="panel-title pull-left">
                        <div class="caption">
                          <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> List Pemohon
                        </div>
                      </div>
                      <div class="pull-right">
                        <a href="/admin/pemohon/create" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> Create Data Permohonan</a>
                      </div>
                    </div>
                    <div class="panel-body table-responsive">
                        <table class="table table-striped table-bordered" id="table1">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Pemohon</th>
                                    <th>Tempat Lahir</th>
                                    <th>Nama Ibu Kandung</th>
                                    <th>Tanggungan</th>
                                    <th>Pendidikan</th>
                                    <!--th>Status rumah atau bangunan</th>
                                    <th>Approved</th>
                                    <th>Ragu-Ragu</th>
                                    <th>Unapproved</th-->
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pem as $p)
                                    <tr role="row" class="odd">
                                        <td><a href="{{ $p->detailUrl() }}">{{ $p->id }}</a></td>
                                        <td><a href="{{ $p->userUrl() }}">{{ $p->users->fullname }}</a></td>
                                        <td>{{ $p->tmp_lahir }}</td>
                                        <td>{{ $p->nama_ibu }}</td>
                                        <td>{{ $p->tanggungan }}</td>
                                        <td>{{ $p->pendidikan }}</td>
                                        <!--td>{{ $p->status_rumah }}</td>
										@if($p->approved == 1)
                                        <td> <button class="btn btn-sm btn-primary"> V </button> </td>
										@else
										<td><a href="{{ $p->approved() }}" class="btn btn-sm btn-primary"> Approved </a></td>
					
										@endif
										@if($p->approved == 2)
										<td> <button class="btn btn-sm btn-primary">V</button> </td>
            
										@else
										<td><a href="{{ $p->ragu() }}" class="btn btn-sm btn-primary"> Ragu-Ragu </a></td>
            							@endif
                                        
										@if($p->approved == 3)
			                            <td <button class="btn btn-sm btn-primary">V</button> </td>
										@else
			                            <td><a href="{{ $p->unapproved() }}" class="btn btn-sm btn-primary"> Unapproved </a></td>	
										@endif
										-->
                                        <td><a class="edit" href="{{ $p->editUrl() }}">Edit</a></td>
                                        <td><a class="edit" href="{{ $p->delUrl() }}">delete</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Third Basic Table Ends Here-->
    </section>
    <!-- content -->
  @stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
@stop
