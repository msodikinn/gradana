@extends('admin/layouts/default')

@section('title')
  Developer Create Form
@parent
@stop

@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
        <section class="content-header">
            <h1>Developer Form</h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="livicon" data-name="bell" data-loop="true" data-color="#fff" data-hovercolor="#fff" data-size="18"></i>
                                Create
                            </h3>
                        </div>
                        <div class="panel-body border">
                            {!! Form::open(array('url' => route('developerCreate'), 'method' => 'post', 'class' => 'form-horizontal form-bordered', 'files' => true)) !!}
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Developer Image</label>
                                    <div class="col-md-6">
                                        <input type="file" name="img" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Nama Developer *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="nama" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Alamat *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="alamat" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">No. Telepon *</label required>
                                    <div class="col-md-6">
                                        <input type="text" name="telp" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-4">
                                        <input type="submit" class="btn btn-primary" value="Create Developer">
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script>
      $(document).ready(function() {
          $('#cat').select2()
      });
    </script>

@stop
