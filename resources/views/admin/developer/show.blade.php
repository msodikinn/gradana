@extends('admin/layouts/default')

@section('title')
    Detail Developer
@parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link href="{{ asset('assets/vendors/modal/css/component.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/modal/css/component.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/fancybox/jquery.fancybox.css') }}" media="screen" />
@stop

@section('content')
    <section class="content-header">
        <h1>Detail Developer</h1>
    </section>

    <section class="content paddingleft_right15">
        <div class="row">
            <div class="panel panel-primary ">
                <div class="panel-heading clearfix">
                    <div class="panel-title pull-left">
                        <div class="caption">
                            <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                            Detail {{ $detail->nama }}
                        </div>
                    </div>
                    <div class="pull-right">
                        <a href="/admin/developer" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Developer</a>
                        <a href="{{ $detail->editUrl() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-minus"></span> Edit Developer</a>
                    </div>
                </div>
                <br />
                <div class="panel-body">
                    <div class="col-md-5">
                        <center><a class="fancybox-effects-a" href="{!! $detail->img == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detail->img !!}">
                            {!! $detail->img == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detail->img.'" class="img-responsive" style="max-height: 436px">' !!}
                        </a></center>
                    </div>
                    <div class="col-md-7">
                        <table class="table">
                            <tr><td style="width: 150px">ID</td><td>{{ $detail->id }}</td></tr>
                            <tr><td>Nama Developer</td><td>{{ $detail->nama }}</td></tr>
                            <tr><td>Alamat</td><td>{{ $detail->alamat }}</td></tr>
                            <tr><td>No. Telepon</td><td>{{ $detail->telp }}</td></tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading clearfix">
                    <div class="panel-title pull-left">
                        <div class="caption">
                            <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Data Property
                        </div>
                    </div>
                    <div class="panel-title pull-right">
                        <a class="btn btn-primary btn-sm" data-toggle="modal" data-href="#create" href="#create"><span class="glyphicon glyphicon-plus"></span> Add Property...</a>
                    </div>
                </div>
                <div class="panel-body table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>ID Property</th>
                                <th>Pemohon</th>
                                <th>Nama Property</th>
                                <th>Deskripsi</th>
                                <th>Alamat</th>
                                <th>Status</th>
                                <th>Pemilik</th>
                                <th>Harga Beli</th>
                                <th>Harga Jual</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($prop as $p)
                                <tr role="row" class="odd">
                                    <td><a href="{{ $p->detailUrl() }}">{{ $p->id }}</a></td>
                                    <td><a href="{{ '/admin/users/'.$p->user->id }}">{{ $p->user->fullname }}</a></td>
                                    <td>{{ $p->nama }}</td>
                                    <td>{{ $p->alamat }}</td>
                                    <td>{{ $p->deskripsi }}</td>
                                    <td>{{ $p->status }}</td>
                                    <td>{{ $p->pemilik_terdaftar }}</td>
                                    <td>{{ $p->harga_beli }}</td>
                                    <td>{{ $p->harga_jual }}</td>
                                    <td><a class="edit" href="{{ $p->editUrl() }}">Edit</a></td>
                                    <td><a class="edit" href="{{ $p->delUrl() }}">delete</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        {{-- <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading clearfix">
                  <div class="panel-title pull-left">
                      <div class="caption">
                          <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Data Member
                      </div>
                  </div>
                </div>
                <div class="panel-body table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>ID User</th>
                                <th>ID Pemohon</th>
                                <th>Fullname Clean</th>
                                <th>KTP</th>
                                <th>Alamat KTP</th>
                                <th>Telepon</th>
                                <th>Customer Verified</th>
                                <th>Verified Date</th>
                            </tr>
                        </thead>
                        @foreach ($detail->applications as $app)
                            @forelse ($app->user->pemohon as $p)
                                <tbody>
                                    <tr role="row">
                                        <td><a href="{{ '/admin/users/'.$p->userid }}">{{ $p->userid }}</a></td>
                                        <td><a href="{{ $p->detailUrl() }}">{{ $p->id }}</a></td>
                                        <td>{{ $p->fullname_clean }}</td>
                                        <td>{{ $p->ktp }}</td>
                                        <td>{{ $p->alamat_ktp }}</td>
                                        <td>{{ $p->telp }}</a></td>
                                        <td>{{ $p->cust_verified }}</td>
                                        <td>{{ $p->cust_verified_date }}</td>
                                    </tr>
                                </tbody>
                            @empty
                                Data Empty
                            @endforelse
                        @endforeach
                    </table>
                </div>
            </div>
        </div> --}}
    </section>

    <!-- create modal-->
    <div class="modal fade in" id="create" role="dialog" aria-hidden="false" style="display:none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Create Property</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(array('url' => route('propertyCreate'), 'method' => 'post', 'class' => 'form-horizontal form-bordered')) !!}
                        <input type="hidden" name="statProp" value="true">
                        <input type="hidden" name="idDev" value="{{ $detail->id }}">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Nama Pemohon</label>
                            <div class="col-md-6">
                                <select id="userid" name="userid" class="form-control" style="width: 100%">
                                  @foreach($users as $u)
                                    <option value="{{ $u->id }}">
                                      {{$u->fullname}}
                                    </option>
                                  @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group striped-col">
                            <label class="col-md-4 control-label">Developer</label>
                            <div class="col-md-6">
                                <select id="devid" name="devid" class="form-control" style="width: 100%">
                                    @foreach($dev as $d)
                                      <option value="{{ $d->id }}">
                                        {{$d->nama}}
                                      </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Nama Property</label>
                            <div class="col-md-6">
                                <input type="text" name="nama" class="form-control">
                            </div>
                        </div>
                        <div class="form-group striped-col">
                            <label class="col-md-4 control-label">Alamat</label>
                            <div class="col-md-6">
                                <input type="text" name="alamat" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Deskripsi</label>
                            <div class="col-md-6">
                                <input type="text" name="deskripsi" class="form-control">
                            </div>
                        </div>
                        <div class="form-group striped-col">
                            <label class="col-md-4 control-label">Tipe</label>
                            <div class="col-md-6">
                                <input type="text" name="tipe" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Luas Tanah</label>
                            <div class="col-md-6">
                                <input type="text" name="luas_tanah" class="form-control">
                            </div>
                        </div>
                        <div class="form-group striped-col">
                            <label class="col-md-4 control-label">Luas Bangunan</label>
                            <div class="col-md-6">
                                <input type="text" name="luas_bangunan" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Status</label>
                            <div class="col-md-6">
                                <select id="status" name="status" class="form-control" style="width: 100%">
                                    @foreach($statusr as $code=>$s)
                                      <option value="{{ $code }}">
                                        {{$s}}
                                      </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group striped-col">
                            <label class="col-md-4 control-label">Harga Jual</label>
                            <div class="col-md-6">
                                <input type="text" name="harga_jual" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Harga Beli</label>
                            <div class="col-md-6">
                                <input type="text" name="harga_beli" class="form-control">
                            </div>
                        </div>
                        <div class="form-group striped-col">
                            <label class="col-md-4 control-label">No. Hak Guna Bangunan (HGB)</label>
                            <div class="col-md-6">
                                <input type="text" name="nomor_hgb" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Pemilik Terdaftar</label>
                            <div class="col-md-6">
                                <input type="text" name="pemilik_terdaftar" class="form-control">
                            </div>
                        </div>
                        <div class="form-group striped-col">
                            <label class="col-md-4 control-label">Luas Lahan</label>
                            <div class="col-md-6">
                                <input type="text" name="luas_lahan" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">HGB Expire Date</label>
                            <div class="col-md-6">
                                <input type="date" name="hgb_expire_date" class="form-control">
                            </div>
                        </div>
                        <div class="form-group striped-col">
                            <label class="col-md-4 control-label">Lokasi</label>
                            <div class="col-md-6">
                                <input type="text" name="lokasi" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Notes</label>
                            <div class="col-md-6">
                                <input type="text" name="notes" class="form-control">
                            </div>
                        </div>
                        <div class="form-group striped-col">
                            <label class="col-md-4 control-label">No. SPH APH</label>
                            <div class="col-md-6">
                                <input type="text" name="nomor_sph_aph" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Tanggal SPH APH</label>
                            <div class="col-md-6">
                                <input type="date" name="tanggal_sph_aph" class="form-control">
                            </div>
                        </div>
                        <div class="form-group striped-col">
                            <label class="col-md-4 control-label">Pihak</label>
                            <div class="col-md-6">
                                <input type="text" name="pihak" class="form-control">
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-4">
                                <input type="submit" class="btn btn-primary" value="Create Property">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- END create modal-->
@stop

@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
    <script src="{{ asset('assets/vendors/modal/js/classie.js') }}" ></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-thumbs.js') }}" ></script>
    <script src="{{ asset('assets/js/pages/gallery.js') }}"  ></script>

    <script src="{{ asset('assets/vendors/modal/js/modalEffects.js') }}" ></script>
    <script>
        $(document).ready(function() {
            $('#userid').select2()
            $('#devid').select2()
            $('#status').select2()
        });
    </script>
    <script>
      // swal({
      //   title: "Error!",
      //   text: "Here's my error message!",
      //   type: "error",
      //   confirmButtonText: "Cool"
      // });
    </script>
@stop
