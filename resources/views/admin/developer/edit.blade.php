@extends('admin/layouts/default')

@section('title')
  Developer Edit Form
@parent
@stop

@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/modal/css/component.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/fancybox/jquery.fancybox.css') }}" media="screen" />
@stop

@section('content')
        <section class="content-header">
            <h1>Developer Form</h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading clearfix">
                            <div class="panel-title pull-left">
                              <div class="caption">
                                <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Edit Developer
                              </div>
                            </div>
                            <div class="pull-right">
                              <a href="/admin/developer" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Developer</a>
                            </div>
                        </div>
                        <div class="panel-body border">
                          {!! Form::model($detail, ['method' => 'PUT', 'url' => $detail->editUrl(), 'class' => 'form-horizontal form-bordered', 'files' => true]) !!}
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Developer Image</label>
                                  <div class="col-md-6">
                                      <div style="margin-bottom: 10px">
                                          <a class="fancybox-effects-a" href="{!! $detail->img == '' ? '' : '/'.$detail->img !!}">
                                              {!! $detail->img == '' ? '' : '<img src="'.'/'.$detail->img.'" class="img-responsive" style="max-height: 350px">' !!}
                                          </a>
                                      </div>
                                      <div>
                                          <input type="file" name="img" value="{!! old('img',$detail->img) !!}">
                                      </div>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Nama Developer *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="nama" class="form-control" value="{!! old('nama',$detail->nama) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Alamat *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="alamat" class="form-control" value="{!! old('alamat',$detail->alamat) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">No. Telepon *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="telp" class="form-control" value="{!! old('telp',$detail->telp) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group form-actions">
                                  <div class="col-md-9 col-md-offset-4">
                                      <input type="submit" class="btn btn-primary" value="Update Developer">
                                  </div>
                              </div>
                          {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-thumbs.js') }}" ></script>
    <script src="{{ asset('assets/js/pages/gallery.js') }}"  ></script>

    <script src="{{ asset('assets/vendors/modal/js/modalEffects.js') }}" ></script>
    <script>
      $(document).ready(function() {
          $('#cat').select2()
      });
    </script>

@stop
