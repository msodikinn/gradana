@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
  Newsletters for Investor
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}"  rel="stylesheet" media="screen">
    <link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet">

@stop

  @section('content')
      <section class="content-header">
          <h1>Newsletters for Investor</h1>
      </section>
      <div class="col-md-12">
          <div class="panel panel-primary filterable">
              <div class="panel-heading clearfix">
                  <div class="panel-title pull-left">
                      <div class="caption">
                          <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff"></i> Send Newsletter
                      </div>
                  </div>
                  <div class="pull-right">
                      <a href="/admin/newsletter/manage" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-hamburger"></span> Manage Newsletter</a>
                  </div>
              </div>
              <div class="panel-body">
                  <div class="col-md-7">
                      <h3>Quick Create Newsletter</h3>
                      <p>Anda dapat membuat Newsletter disini. Anda juga dapat menggunakan <u>custom shortcode</u> untuk menambahkan data dinamis.</p>
                      <p>Total {{ $countInvestor }} investor available for newsletter</p>
                      {!! Form::open(array('url' => route('newsletterCreate'), 'class' => 'form-vertical', 'method' => 'post')) !!}
                          <div class="form-group">
                              <label class="control-label"><b>Subject</b></label>
                              {!! Form::text('subject', null, ['class'=>'form-control']) !!}
                          </div>
                          <div class="form-group">
                              <label class="control-label"><b>Message</b></label>
                              {!! Form::textarea('message', null, ['class' => 'form-control typeahead-basic', 'id' => 'tinymce_full']) !!}
                          </div>
                          <div class="form-group">
                              <button type="submit" class="btn btn-primary">Quick Create <i class="icon-arrow-right14 position-right"></i></button>
                          </div>
                      {!! Form::close() !!}
                      <h3>Custom Shortcode: </h3>
                      <ul>
                          <li>Name: <b>[name]</b></li>
                          <li>Email: <b>[email]</b></li>
                          <li>Jenis Usaha: <b>[jenisusaha]</b></li>
                          <li>Alamat: <b>[alamat]</b></li>
                          <li>No. Telp: <b>[telp]</b></li>
                      </ul>
                  </div>
                  <div class="col-md-5">
                      <h3>List Newsletters</h3>
                      <div class="list-group">
                          @foreach ($news as $n)
                              <?php $title = str_limit($n->subject,60); ?>
                              <a href="{{ $n->detail() }}" class="list-group-item">{{ $title }}</a>
                          @endforeach
                      </div>
                      <div style="padding-top: 5px;">
                          <a href="/admin/newsletter/manage" class="text-size-large">View More...</a>
                      </div>
                  </div>
              </div>
          </div>
      </div>

  @stop

  @section('footer_scripts')
      <script src="{{ asset('assets/vendors/tinymce/tinymce.min.js') }}"></script>
      <script src="{{ asset('http://josh.tes/assets/vendors/ckeditor/js/ckeditor.js') }}"></script>
      <script src="{{ asset('http://josh.tes/assets/vendors/ckeditor/js/jquery.js') }}" ></script>
      <script src="{{ asset('http://josh.tes/assets/vendors/ckeditor/js/config.js') }}"></script>
      <script src="{{ asset('assets/js/pages/editor.js') }}"></script>
  @stop
