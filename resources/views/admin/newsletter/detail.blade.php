@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Detail Investor
@parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link href="{{ asset('assets/vendors/modal/css/component.css') }}" rel="stylesheet" />
@stop

    @section('content')
        <section class="content-header">
            <h1>Detail Newsletters</h1>
        </section>
        <div class="col-md-12">
            <div class="panel panel-primary filterable">
                <div class="panel-heading clearfix">
                    <div class="panel-title pull-left">
                        <div class="caption">
                            <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff"></i> Newsletter to be Send
                        </div>
                    </div>
                    <div class="pull-right">
                        <a href="{{ $detail->edit() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-edit"></span> Edit Newsletter</a>
                        <a href="/admin/newsletter/manage" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-hamburger"></span> Manage Newsletter</a>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-borderless table-hover">
                        <tbody>
                            <tr><td width="160px">From:</td><td>admin@gradana.com</td></tr>
                            <tr><td>To:</td><td>(All active investor)</td></tr>
                            <tr><td>Subject:</td><td><b>{{ $detail->subject }}</b></td></tr>
                            <tr><td>Message:</td><td><?php echo $detail->message ?></td></tr>
                        </tbody>
                    </table>
                    <div class="col-md-12" style="margin-top: 20px;">
                        <a href="{{ $detail->send() }}" class="btn btn-primary">Send Now <span class="glyphicon glyphicon-share-alt"></span></a>
                    </div>
                </div>
            </div>
        </div>
    @stop

    @section('footer_scripts')
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
        <script src="{{ asset('assets/vendors/modal/js/classie.js') }}" ></script>
        <script src="{{ asset('assets/vendors/modal/js/modalEffects.js') }}" ></script>
    @stop

