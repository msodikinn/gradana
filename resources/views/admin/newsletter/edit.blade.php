@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
  Newsletter Edit Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}"  rel="stylesheet" media="screen">
    <link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet">
@stop

    @section('content')
        <section class="content-header">
          <h1>Newsletter Edit Form</h1>
        </section>
        <div class="col-md-12">
            <div class="panel panel-primary filterable">
                <div class="panel-heading clearfix">
                    <div class="panel-title pull-left">
                        <div class="caption">
                            <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff"></i> Edit Form
                        </div>
                    </div>
                    <div class="pull-right">
                        <a href="/admin/newsletter/manage" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-hamburger"></span> Manage Newsletter</a>
                    </div>
                </div>
                    <div class="panel-body">
                        <div class="col-md-9">
                            {!! Form::model($newsletter, ['method' => 'PUT', 'url' => '/admin/newsletter/edit/'.$newsletter->id, 'class' => 'form-horizontal']) !!}

                                @include('admin.newsletter._form')

                                <button type="submit" class="btn btn-primary">Update Newsletter <i class="icon-floppy-disk position-right"></i></button>
                                <button type="submit" class="btn bg-warning-700">Delete Newsletter <i class="icon-trash-alt position-right"></i></button>

                            {!! Form::close() !!}
                        </div>

                        <div class="col-md-3">
                            <p>Total {{ $countInvestor }} investor available for newsletter</p>
                            <b class="text-size-large">Shortcode:</b>
                            <ul>
                                <li>Name: <b>[name]</b></li>
                                <li>Email: <b>[email]</b></li>
                                <li>Jenis Usaha: <b>[jenisusaha]</b></li>
                                <li>Alamat: <b>[alamat]</b></li>
                                <li>No. Telp: <b>[telp]</b></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @stop

    @section('footer_scripts')
      <script src="{{ asset('assets/vendors/tinymce/tinymce.min.js') }}"></script>
      <script src="{{ asset('http://josh.tes/assets/vendors/ckeditor/js/ckeditor.js') }}"></script>
      <script src="{{ asset('http://josh.tes/assets/vendors/ckeditor/js/jquery.js') }}" ></script>
      <script src="{{ asset('http://josh.tes/assets/vendors/ckeditor/js/config.js') }}"></script>
      <script src="{{ asset('assets/js/pages/editor.js') }}"></script>
    @stop
