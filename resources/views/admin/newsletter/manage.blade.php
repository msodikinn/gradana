@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Manage Newsletter
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
@stop

    @section('content')
        <section class="content-header">
            <h1>Manage Newsletter</h1>
        </section>
        <div class="col-md-12">
            <div class="panel panel-primary filterable">
                <div class="panel-heading clearfix">
                    <div class="panel-title pull-left">
                        <div class="caption">
                            <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff"></i> Total {{ $totalNewsletter }} Newsletters
                        </div>
                    </div>
                    <div class="pull-right">
                        <a href="/admin/newsletter/create" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> Create Newsletter</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover" id="table1">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Subject</th>
                                    <th>Message</th>
                                    <th>Last Send</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($listNewsletter as $ln)
                                    <?php
                                      $msg = str_limit($ln->message, 60);
                                      $title = str_limit($ln->subject, 60);
                                    ?>
                                    <tr>
                                        <td>{{ $ln->id }}</td>
                                        <td><a href="{{ $ln->detail() }}">{{ $title }}</a></td>
                                        <td><?php echo $msg; ?></td>
                                        <td>{!! Carbon\Carbon::parse($ln->last_send)->diffForHumans() !!}</td>
                                        <td>
                                            <a href="{{ $ln->detail() }}">
                                                <i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="View Detail" style="width: 18px; height: 18px;"></i>
                                            </a>
                                            <a href="{{ $ln->edit() }}">
                                                <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update" style="width: 18px; height: 18px;"></i>
                                            </a>
                                            <a href="{{ $ln->del() }}">
                                                <i class="livicon" data-name="remove-circle" data-size="18" data-loop="true" data-c="red" data-hc="red" title="Delete" style="width: 18px; height: 18px;"></i>
                                            </a>
                                            <a href="{{ $ln->send() }}">
                                                <i class="livicon" data-name="redo" data-size="18" data-loop="true" data-c="green" data-hc="green" title="Send Now" style="width: 18px; height: 18px;"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @stop

    @section('footer_scripts')

        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
    @stop
