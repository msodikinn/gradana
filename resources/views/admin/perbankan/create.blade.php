@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
  Perbankan Create Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
    <section class="content-header">
        <h1>Perbankan Form</h1>
    </section>
    <section class="content">
    <div class="row">
  		<div class="col-lg-12">
  			<ul class="nav nav-tabs ">
  				<li>
  					<a href="{{ URL::to('admin/pemohon/edit/'.$pemohonid) }}">Pemohon</a>
  				</li>
  				<li>
            {{-- {{ dd($jobpemohonid) }} --}}
  					<a href="{{ URL::to('admin/jobpemohon/edit/'.$jobpemohonid) }}">Pekerjaan Pemohon</a>
  				</li>
          @if (App\Pemohon::find($pemohonid)->status_kawin != "Lajang")
    				<li>
    					<a href="{{ URL::to('admin/suamiistri/edit/'.$suamisitriid) }}">Pasangan</a>
    				</li>
    				<li>
    					<a href="{{ URL::to('admin/jobsuamiistri/edit/'.$jobsuamiistriid) }}">Pekerjaan Pasangan</a>
    				</li>
          @endif
  				<li>
  					<a href="{{ URL::to('admin/keluarga/edit/'.$keluargaid) }}">Emergency Contact</a>
  				</li>
  				{{-- <li>
  					<a href="{{ URL::to('admin/penghasilan/edit/'.$penghasilanid) }}">Penghasilan</a>
  				</li> --}}
  				<li class="active">
  					<a href="">Perbankan</a>
  				</li>
  			</ul>
  		</div>
  	</div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary filterable">
                    <div class="panel-heading clearfix">
                        <div class="panel-title pull-left">
                          <div class="caption">
                            <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Create Perbankan
                          </div>
                        </div>
                        {{-- <div class="pull-right">
                          <a href="/admin/perbankan" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Perbankan</a>
                        </div> --}}
                    </div>
                    <div class="panel-body border">
                        {!! Form::open(array('url' => route('perbankanCreate'), 'method' => 'post', 'class' => 'form-horizontal form-bordered', 'files' => 'true')) !!}
                        {!! Form::hidden('pemohonid', $pemohonid) !!}
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Nama Pemohon</label>
                                <div class="col-md-6">
                                    <input type="hidden" name="userid" value="{{ $users->id }}">
                                    <input type="text" name="pem" value="{{ $users->fullname }}" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Sumber Informasi *</label>
                                <div class="col-md-6">
                                    <input type="text" name="sumber_info" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Rekening Koran 1 *</label>
                                <div class="col-md-6">
                                    <input type="file" name="rekening_koran" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Rekening Koran 2</label>
                                <div class="col-md-6">
                                    <input type="file" name="rekening_koran2" class="form-control">
                                </div>
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-9 col-md-offset-4">
                                    <input type="submit" class="btn btn-primary" value="Save">
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script>
      $(document).ready(function() {
          $('#userid').select2()
      });
    </script>

@stop
