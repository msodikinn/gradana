@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Detail Perbankan
@parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/modal/css/component.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/fancybox/jquery.fancybox.css') }}" media="screen" />
    <style>
      input[type="date"]:before {
        content: attr(placeholder) !important;
        color: #aaa;
        margin-right: 0.5em;
      }
      input[type="date"]:focus:before,
      input[type="date"]:valid:before {
        content: "";
      }
    </style>
@stop

@section('content')
<div id="EditController" class="col-lg-12">

    <section class="content-header">
        <h1>Detail Perbankan</h1>
    </section>

    <section class="content paddingleft_right15">
        <div class="row">
            <div class="panel panel-primary ">
                <div class="panel-heading clearfix">
                    <div class="panel-title pull-left">
                        <div class="caption">
                            <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                            Detail {{ $detail->nama }}
                        </div>
                    </div>
                    <div class="pull-right">
                        <a href="/admin/perbankan" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Perbankan</a>
                        <a href="{{ $detail->editUrl() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-minus"></span> Edit Perbankan</a>
                    </div>
                </div>
                <br />
                <div class="panel-body">
                    <div class="col-md-12">
                        <table class="table ">
                            <tr><td style="width: 150px">ID</td><td style="width: 40%;">{{ $detail->id }}</td><td></td><td></td></tr>
                            <tr><td>Nama Pemohon</td><td>{{ $detail->user->fullname }}</td><td></td><td></td></tr>
                            <tr><td>Sumber Informasi</td><td>{{ $detail->sumber_info }}</td><td></td><td></td></tr>
                            <tr>
                                <td>Rekening Koran 1</td>
                                <td>
                                    <a class="fancybox-effects-a" href="{!! $detail->rekening_koran == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detail->rekening_koran !!}">
                                        {!! $detail->rekening_koran == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detail->rekening_koran.'" class="img-responsive" style="max-height: 436px">' !!}
                                    </a>
                                </td>
                                <td style="width: 10%;">Rekening Koran 2</td>
                                <td style="width: 40%;">
                                    <a class="fancybox-effects-a" href="{!! $detail->rekening_koran2 == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detail->rekening_koran2 !!}">
                                        {!! $detail->rekening_koran2 == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detail->rekening_koran2.'" class="img-responsive" style="max-height: 436px">' !!}
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading clearfix  ">
                    <div class="panel-title pull-left">
                        <div class="caption">
                            <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Data Perbankan {{ $detail->user->fullname }}
                        </div>
                    </div>
                    <div class="panel-title pull-right">
                        <a class="btn btn-sm btn-primary" data-toggle="modal" data-href="#create" href="#create"><span class="glyphicon glyphicon-plus"></span> Add More...</a>
                    </div>
                </div>
                <div class="panel-body table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Lama Tahun</th>
                                <th>Lama Bulan</th>
                                <th>Jenis</th>
                                <th>Nama Bank</th>
                                <th>Rekening</th>
                                <th>Limit Kartu</th>
                                <th>Jenis Cicilan</th>
                                <th>Jumlah Cicilan per Bulan</th>
                                <th>Jatuh Tempo</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <linkid></linkid>
                            @foreach ($child as $c)
                                <tr role="row">
                                    <td>{{ $c->nasabah_tahun }}</td>
                                    <td>{{ $c->nasabah_bulan }}</td>
                                    <td>{{ $c->jenis }}</td>
                                    <td>{{ $c->nama_bank }}</a></td>
                                    <td>{{ $c->rekening }}</td>
                                    <td>{{ uang($c->limit_kartu) }}</td>
                                    <td>{{ $c->jenis_cicilan }}</td>
                                    <td>{{ uang($c->jml_cicilan_bln) }}</td>
                                    <td>{{ format($c->jatuh_tempo) }}</td>
                                    <td><a class="edit" href="{{ $c->editChild() }}">Edit</a></td>
                                    <td><a class="edit" href="{{ $c->delChild() }}">delete</a></td>
                                </tr>
                            @endforeach
                            {{-- <template v-for="child in childs">
                                <tr role="row">
                                    <td>@{{ child.id }}</td>
                                    <td>@{{ child.nasabah_tahun }}</td>
                                    <td>@{{ child.nasabah_bulan }}</td>
                                    <td>@{{ child.jenis }}</td>
                                    <td>@{{ child.nama_bank }}</a></td>
                                    <td>@{{ child.rekening }}</td>
                                    <td>@{{ child.limit_kartu }}</td>
                                    <td>
                                        <button type="button" class="btn btn-link btn-sm" data-toggle="modal" data-target="#edit" @click="showChild(child.id)">Edit</button>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-link btn-sm" @click="delChild(child.id)">Delete</button>
                                    </td>
                                </tr>
                            </template> --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- create modal-->
    <div class="modal fade in" id="create" role="dialog" aria-hidden="false" style="display:none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Create Data Keuangan</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(array('url' => route('pChildCreate'), 'method' => 'post', 'class' => 'form-horizontal form-bordered')) !!}
                        <div class="form-group">
                            <label class="col-md-4 control-label">Id Perbankan</label>
                            <div class="col-md-6">
                                <input type="text" name="perbankanid" class="form-control"  value="{!! old('perbankanid',$detail->id) !!}" readonly>
                            </div>
                        </div>
                        <div class="form-group striped-col">
                            <label class="col-md-4 control-label">Lama nasabah (tahun) *</label>
                            <div class="col-md-6">
                                <input type="text" name="nasabah_tahun" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Lama nasabah (bulan) *</label>
                            <div class="col-md-6">
                                <input type="text" name="nasabah_bulan" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group striped-col">
                            <label class="col-md-4 control-label">Jenis</label>
                            <div class="col-md-6">
                                <select id="jenis" name="jenis" class="form-control" style="width: 100%">
                                    @foreach($jenis as $code=>$j)
                                        <option value="{{ $code }}">
                                            {{$j}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div id="holder"></div>
                        {{-- <div class="form-group">
                            <label class="col-md-4 control-label">Nama Bank *</label>
                            <div class="col-md-6">
                                <input type="text" name="nama_bank" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group striped-col">
                            <label class="col-md-4 control-label">Rekening *</label>
                            <div class="col-md-6">
                                <input type="text" name="rekening" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Limit Kartu *</label>
                            <div class="col-md-6">
                                <input type="text" name="limit_kartu" class="form-control" required>
                            </div>
                        </div> --}}
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-4">
                                <input type="submit" class="btn btn-primary" value="Save Perbankan">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- END create modal-->

    <!-- edit modal-->
    <div class="modal fade in" id="edit" role="dialog" aria-hidden="false" style="display:none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Edit Data Perbankan</h4>
                </div>
                <div class="modal-body">
                    {!! Form::model($detail, ['url' => '#', 'method' => 'put', 'class' => 'form-horizontal form-bordered', '@submit.prevent' => 'addChild']) !!}
                        <div class="form-group">
                            <label class="col-md-4 control-label">ID</label>
                            <div class="col-md-6">
                                <input v-model="pchilds.id" type="text" name="id" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Lama nasabah (tahun)</label>
                            <div class="col-md-6">
                                <input v-model="pchilds.nasabah_tahun" type="text" name="nasabah_tahun" class="form-control">
                            </div>
                        </div>
                        <div class="form-group striped-col">
                            <label class="col-md-4 control-label">Lama nasabah (bulan)</label>
                            <div class="col-md-6">
                                <input v-model="pchilds.nasabah_bulan" type="text" name="nasabah_bulan" class="form-control">
                            </div>
                        </div>
                        <div class="form-group striped-col">
                            <label class="col-md-4 control-label">Jenis</label>
                            <div class="col-md-6">
                                <select name="jenis" id="jenis" v-model="selected" class="form-control">
                                    <option v-for="wer in jenisN">@{{ wer.text }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Nama Bank</label>
                            <div class="col-md-6">
                                <input v-model="pchilds.nama_bank" type="text" name="nama_bank" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Rekening</label>
                            <div class="col-md-6">
                                <input v-model="pchilds.rekening" type="text" name="rekening" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Limit Kartu</label>
                            <div class="col-md-6">
                                <input v-model="pchilds.limit_kartu" type="text" name="limit_kartu" class="form-control">
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" @click="editChild(pchilds.id)">Save Perbankan</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- END edit modal-->

</div>
@stop

@section('footer_scripts')

    <script src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script src="{{ asset('assets/vendors/fancybox/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-thumbs.js') }}" ></script>
    <script src="{{ asset('assets/js/pages/gallery.js') }}"  ></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/modal/js/modalEffects.js') }}" ></script>
    <script>
      $(document).ready(function() {
          // $('#userid').select2()
          $('#jenis').select2()
      });
    </script>
    @push('scripts')
        <script src="/assets/js/edit-pchild.js"></script>

        <style>
            .success-transition {
                transition: all .5s ease-out;
            }
            .success.enter, .success.leave {
                opacity: 0;
            }
        </style>
    @endpush
    <script>
      // jQuery('.wer').click(function(){
      //     var id = $(this).attr('id');
      //     $.ajax( {
      //         type : 'GET',
      //         url  : '/admin/perbankan/pchild/edit/'+id,
      //         data : { 'id' : id },
      //         success: function(response) {
      //             var Vals = JSON.parse(per);
      //             // alert("Vals");
      //         }
      //     });
      // })
    </script>
    <script>
      $("#jenis").change(function(){
        val = $(this).val();

        if(val == "Tabungan / Giro / Deposito"){
          $("#holder").find("*").remove();
          var ctrl = '<div class="form-group"><label class="col-md-4 control-label">Nama Bank *</label><div class="col-md-6"><input type="text" name="nama_bank" class="form-control" placeholder="Nama Bank" required></div></div>';
            ctrl += '<div class="form-group striped-col"><label class="col-md-4 control-label">No. Rekening *</label><div class="col-md-6"><input type="text" name="rekening" class="form-control" placeholder="No. Rekening" required></div></div>';
            ctrl += '<div class="form-group"><label class="col-md-4 control-label">Saldo *</label><div class="col-md-6"><input type="text" name="saldo" class="form-control" placeholder="Saldo" required></div></div>';
          $("#holder").append(ctrl);
          $('#autonum1').autoNumeric('init');
        }else if (val == "Kartu Kredit"){
          $("#holder").find("*").remove();
          var ctrl = '<div class="form-group"><label class="col-md-4 control-label">Nama Bank *</label><div class="col-md-6"><input type="text" name="nama_bank" class="form-control" placeholder="Nama Bank" required></div></div>';
            ctrl += '<div class="form-group striped-col"><label class="col-md-4 control-label">Limit Kartu Kredit *</label><div class="col-md-6"><input type="text" name="limit_kartu" class="form-control" placeholder="Limit Kartu Kredit" required></div></div>';
          $("#holder").append(ctrl);
          $('#autonum2').autoNumeric('init');
        }else if (val == "Angsuran yang sedang berjalan"){
          $("#holder").find("*").remove();
          var ctrl = '<div class="form-group"><label class="col-md-4 control-label">Jenis Cicilan *</label><div class="col-md-6"><input type="text" name="jenis_cicilan" class="form-control" placeholder="Jenis Cicilan" required></div></div>';
            ctrl += '<div class="form-group striped-col"><label class="col-md-4 control-label">Jumlah Cicilan per Bulan *</label><div class="col-md-6"><input type="text" name="jml_cicilan_bln" class="form-control" placeholder="Jumlah Cicilan per Bulan" required></div></div>';
            ctrl += '<div class="form-group"><label class="col-md-4 control-label">Jatuh Tempo Cicilan *</label><div class="col-md-6"><input type="date" name="jatuh_tempo" class="form-control" placeholder="Jatuh Tempo Cicilan" required></div></div>';
          $("#holder").append(ctrl);
          $('#autonum3').autoNumeric('init');
        }else{
          $("#holder").find("*").remove();
        }
      });
    </script>
@stop
