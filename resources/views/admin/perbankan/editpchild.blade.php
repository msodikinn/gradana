@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
  Perbankan Child Edit Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/fancybox/jquery.fancybox.css') }}" media="screen" rel="stylesheet">
@stop

@section('content')
        <section class="content-header">
            <h1>Edit Keuangan</h1>
        </section>
        <section class="content">
          <div class="row">
              <div class="col-lg-12">
                  <div class="panel panel-primary">
                      <div class="panel-heading clearfix">
                          <div class="panel-title pull-left">
                            <div class="caption">
                              <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Edit Keuangan Form
                            </div>
                          </div>
                          <div class="pull-right">
                            <a href="{{ URL::previous() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> Back</a>
                          </div>
                      </div>
                      <div class="panel-body border">
                        {!! Form::model($pchild, ['url' => $pchild->editChild(), 'method' => 'put', 'class' => 'form-horizontal form-bordered', '@submit.prevent' => 'addChild']) !!}
                      <div class="form-group">
                          <label class="col-md-4 control-label">Perbankan Child ID</label>
                          <div class="col-md-6">
                              <input type="text" name="id" class="form-control" value="{!! old('id',$pchild->id) !!}" readonly>
                          </div>
                      </div>
                      <div class="form-group striped-col">
                          <label class="col-md-4 control-label">Lama Bulan *</label>
                          <div class="col-md-6">
                              <input type="text" name="nasabah_bulan" class="form-control" value="{!! old('nasabah_bulan',$pchild->nasabah_bulan) !!}" required>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-md-4 control-label">Lama Tahun *</label>
                          <div class="col-md-6">
                              <input type="text" name="nasabah_tahun" class="form-control" value="{!! old('nasabah_tahun',$pchild->nasabah_tahun) !!}" required>
                          </div>
                      </div>
                      <div class="form-group striped-col">
                          <label class="col-md-4 control-label">Jenis *</label>
                          <div class="col-md-6">
                              <select id="jenis" name="jenis" class="form-control" style="width: 100%">
                                @foreach($jenis as $code=>$j)
                                  <option value="{{ $code }}" @if($code === $pchild->jenis) selected @endif>
                                    {{$j}}
                                  </option>
                                @endforeach
                              </select>
                          </div>
                      </div>
                      <div id="holder">
                          @if ($pchild->jenis == "Tabungan / Giro / Deposito ")
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Nama Bank *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="nama_bank" class="form-control" value="{{ $pchild->nama_bank }}" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">No. Rekening *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="rekening" class="form-control" value="{{ $pchild->rekening }}" required>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Saldo *</label>
                                  <div class="col-md-6">
                                      <input id="autonum11" type="text" name="saldo" class="form-control" value="{{ $pchild->saldo }}" required>
                                  </div>
                              </div>
                          @elseif($pchild->jenis == "Kartu Kredit")
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Nama Bank *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="nama_bank" class="form-control" value="{{ $pchild->nama_bank }}" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Limit Kartu Kredit *</label>
                                  <div class="col-md-6">
                                      <input id="autonum12" type="text" name="limit_kartu" class="form-control" value="{{ $pchild->limit_kartu }}" required>
                                  </div>
                              </div>
                          @elseif($pchild->jenis == "Angsuran yang sedang berjalan")
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Jenis Cicilan *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="jenis_cicilan" class="form-control" value="{{ $pchild->jenis_cicilan }}" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Jumlah Cicilan per Bulan *</label>
                                  <div class="col-md-6">
                                      <input id=autonum13 type="text" name="jml_cicilan_bln" class="form-control" value="{{ $pchild->jml_cicilan_bln }}" required>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Jatuh Tempo Cicilan *</label>
                                  <div class="col-md-6">
                                      <input type="date" name="jatuh_tempo" class="form-control" value="{{ $pchild->jatuh_tempo }}" required>
                                  </div>
                              </div>
                          @endif
                      </div>
                      <div class="form-group form-actions">
                          <div class="col-md-9 col-md-offset-4">
                              <input type="submit" class="btn btn-primary" value="Save Perbankan Child">
                          </div>
                      </div>
                  {!! Form::close() !!}
              </div>
          </div>
        </section>
    @stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-thumbs.js') }}" ></script>
    <script src="{{ asset('assets/js/pages/gallery.js') }}"  ></script>
    <script>
      $(document).ready(function() {
          $('#jenis').select2()
      });
    </script>
    <script>
      $("#jenis").change(function(){
        val = $(this).val();

        if(val == "Tabungan / Giro / Deposito"){
          $("#holder").find("*").remove();
          var ctrl = '<div class="form-group"><label class="col-md-4 control-label">Nama Bank *</label><div class="col-md-6"><input type="text" name="nama_bank" class="form-control" placeholder="Nama Bank" required></div></div>';
            ctrl += '<div class="form-group striped-col"><label class="col-md-4 control-label">No. Rekening *</label><div class="col-md-6"><input type="text" name="rekening" class="form-control" placeholder="No. Rekening" required></div></div>';
            ctrl += '<div class="form-group"><label class="col-md-4 control-label">Saldo *</label><div class="col-md-6"><input id="autonum1" type="text" name="saldo" class="form-control" placeholder="Saldo" required></div></div>';
          $("#holder").append(ctrl);
          $('#autonum1').autoNumeric('init');
        }else if (val == "Kartu Kredit"){
          $("#holder").find("*").remove();
          var ctrl = '<div class="form-group"><label class="col-md-4 control-label">Nama Bank *</label><div class="col-md-6"><input type="text" name="nama_bank" class="form-control" placeholder="Nama Bank" required></div></div>';
            ctrl += '<div class="form-group striped-col"><label class="col-md-4 control-label">Limit Kartu Kredit *</label><div class="col-md-6"><input id="autonum2" type="text" name="limit_kartu" class="form-control" placeholder="Limit Kartu Kredit" required></div></div>';
          $("#holder").append(ctrl);
          $('#autonum2').autoNumeric('init');
        }else if (val == "Angsuran yang sedang berjalan"){
          $("#holder").find("*").remove();
          var ctrl = '<div class="form-group"><label class="col-md-4 control-label">Jenis Cicilan *</label><div class="col-md-6"><input type="text" name="jenis_cicilan" class="form-control" placeholder="Jenis Cicilan" required></div></div>';
            ctrl += '<div class="form-group striped-col"><label class="col-md-4 control-label">Jumlah Cicilan per Bulan *</label><div class="col-md-6"><input id="autonum3" type="text" name="jml_cicilan_bln" class="form-control" placeholder="Jumlah Cicilan per Bulan" required></div></div>';
            ctrl += '<div class="form-group"><label class="col-md-4 control-label">Jatuh Tempo Cicilan *</label><div class="col-md-6"><input type="date" name="jatuh_tempo" class="form-control" placeholder="Jatuh Tempo Cicilan" required></div></div>';
          $("#holder").append(ctrl);
          $('#autonum3').autoNumeric('init');
        }else{
          $("#holder").find("*").remove();
        }
      });
    </script>
@stop
