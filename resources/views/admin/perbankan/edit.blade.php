@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
  Perbankan Edit Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/fancybox/jquery.fancybox.css') }}" media="screen" rel="stylesheet">
    <style>
      input[type="date"]:before {
        content: attr(placeholder) !important;
        color: #aaa;
        margin-right: 0.5em;
      }
      input[type="date"]:focus:before,
      input[type="date"]:valid:before {
        content: "";
      }
    </style>
@stop

@section('content')
        <section class="content-header">
            <h1>Perbankan Form</h1>
        </section>
        <section class="content">
          	<div class="row">
      				<div class="col-lg-12">
      					<ul class="nav nav-tabs ">
      						@if (!empty($pemohonid))
        						<li>
        							<a href="{{ URL::to('admin/pemohon/edit/'.$pemohonid) }}">Pemohon</a>
        						</li>
      						@endif
      						@if (!empty($jobpemohonid))
        						<li>
        							<a href="{{ URL::to('admin/jobpemohon/edit/'.$jobpemohonid) }}">Pekerjaan Pemohon</a>
        						</li>
      						@endif
                  @if (App\Pemohon::find($pemohonid)->status_kawin != "Lajang")
        						@if (!empty($suamisitriid))
          						<li>
          							<a href="{{ URL::to('admin/suamiistri/edit/'.$suamisitriid) }}">Pasangan</a>
          						</li>
        						@endif
        						@if (!empty($jobsuamiistriid))
          						<li>
          							<a href="{{ URL::to('admin/jobsuamiistri/edit/'.$jobsuamiistriid) }}">Pekerjaan Pasangan</a>
          						</li>
        						@endif
                  @endif
      						@if (!empty($keluargaid))
        						<li>
        							<a href="{{ URL::to('admin/keluarga/edit/'.$keluargaid) }}">Emergency Contact</a>
        						</li>
      						@endif
      						{{-- @if (!empty($penghasilanid))
        						<li>
        							<a href="{{ URL::to('admin/penghasilan/edit/'.$penghasilanid) }}">Penghasilan</a>

        						</li>
      						@endif --}}
      						<li class="active">
      							<a href="">Perbankan</a>
      						</li>
                  @if ($countApp == 0)
                    <li>
                      <a href="{{ URL::to('admin/application/create?backid='.$backid) }}">Create Application</a>
                    </li>
                  @elseif($countApp == 1)
                    <li>
                      <a href="{{ URL::to('admin/application/edit/'.$applicationid) }}">Application</a>
                    </li>
                  @elseif($countApp > 1)
                    <li>
                      <a href="{{ URL::to('admin/application?iduser='.$detail->userid) }}">{{ $countApp }} Application</a>
                    </li>
                  @endif
      					</ul>
      				</div>
      			</div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading clearfix">
                            <div class="panel-title pull-left">
                              <div class="caption">
                                <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Edit Perbankan
                              </div>
                            </div>
                            <div class="pull-right">
                              <a href="/admin/perbankan" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Perbankan</a>
                            </div>
                        </div>
                        <div class="panel-body border">
                          {!! Form::model($detail, ['method' => 'PUT', 'url' => $detail->editUrl(), 'class' => 'form-horizontal form-bordered', 'files'=> true]) !!}
                              <div class="form-group">
                                  <label class="col-md-4 control-label">ID</label>
                                  <div class="col-md-6">
                                      <input type="text" name="id" class="form-control" value="{!! old('id',$detail->id) !!}" readonly>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Nama Pemohon</label>
                                  <div class="col-md-6">
                                      {{-- <input type='text' id="autonum1"/> --}}
                                      <input type="text" value="{{ $detail->user->fullname }}" class="form-control" readonly>
                                      <input type="hidden" name="userid" value="{{ $detail->user->id }}">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Sumber Informasi *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="sumber_info" class="form-control" value="{!! old('sumber_info',$detail->sumber_info) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Rekening Koran 1 *</label>
                                  <div class="col-md-6">
                                      <div style="margin-bottom: 10px">
                                          <a class="fancybox-effects-a" href="{!! $detail->rekening_koran == '' ? '' : '/'.$detail->rekening_koran !!}">
                                              {!! $detail->rekening_koran == '' ? '' : '<img src="'.'/'.$detail->rekening_koran.'" class="img-responsive" style="max-height: 350px">' !!}
                                          </a>
                                      </div>
                                      <div>
                                          <input type="file" name="rekening_koran" class="form-control" required>
                                      </div>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Rekening Koran 2</label>
                                  <div class="col-md-6">
                                      <div style="margin-bottom: 10px">
                                          <a class="fancybox-effects-a" href="{!! $detail->rekening_koran2 == '' ? '' : '/'.$detail->rekening_koran2 !!}">
                                              {!! $detail->rekening_koran2 == '' ? '' : '<img src="'.'/'.$detail->rekening_koran2.'" class="img-responsive" style="max-height: 350px">' !!}
                                          </a>
                                      </div>
                                      <div>
                                          <input type="file" name="rekening_koran2" class="form-control">
                                      </div>
                                  </div>
                              </div>
                              <div class="form-group form-actions">
                                  <div class="col-md-9 col-md-offset-4">
                                      <input type="submit" class="btn btn-primary" value="Save Perbankan">
                                  </div>
                              </div>
                          {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading clearfix  ">
                            <div class="panel-title pull-left">
                                <div class="caption">
                                    <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Data Keuangan {{ $detail->user->fullname }}
                                </div>
                            </div>
                            <div class="panel-title pull-right">
                                <a class="btn btn-sm btn-primary" data-toggle="modal" data-href="#create" href="#create"><span class="glyphicon glyphicon-plus"></span> Add More...</a>
                            </div>
                        </div>
                        <div class="panel-body table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Lama Tahun</th>
                                        <th>Lama Bulan</th>
                                        <th>Jenis</th>
                                        <th>Nama Bank</th>
                                        <th>Rekening</th>
                                        <th>Limit Kartu</th>
                                        <th>Jenis Cicilan</th>
                                        <th>Jumlah Cicilan per Bulan</th>
                                        <th>Jatuh Tempo</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <linkid></linkid>
                                    @foreach ($child as $c)
                                        <tr role="row">
                                            <td>{{ $c->nasabah_tahun }}</td>
                                            <td>{{ $c->nasabah_bulan }}</td>
                                            <td>{{ $c->jenis }}</td>
                                            <td>{{ $c->nama_bank }}</a></td>
                                            <td>{{ $c->rekening }}</td>
                                            <td>{{ uang($c->limit_kartu) }}</td>
                                            <td>{{ $c->jenis_cicilan }}</td>
                                            <td>{{ uang($c->jml_cicilan_bln) }}</td>
                                            <td>{{ format($c->jatuh_tempo) }}</td>
                                            <td><a class="edit" href="{{ $c->editChild() }}">Edit</a></td>
                                            <td><a class="edit" href="{{ $c->delChild() }}">delete</a></td>
                                        </tr>
                                    @endforeach
                                    {{-- <template v-for="child in childs">
                                        <tr role="row">
                                            <td>@{{ child.id }}</td>
                                            <td>@{{ child.nasabah_tahun }}</td>
                                            <td>@{{ child.nasabah_bulan }}</td>
                                            <td>@{{ child.jenis }}</td>
                                            <td>@{{ child.nama_bank }}</a></td>
                                            <td>@{{ child.rekening }}</td>
                                            <td>@{{ child.limit_kartu }}</td>
                                            <td>
                                                <button type="button" class="btn btn-link btn-sm" data-toggle="modal" data-target="#edit" @click="showChild(child.id)">Edit</button>
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-link btn-sm" @click="delChild(child.id)">Delete</button>
                                            </td>
                                        </tr>
                                    </template> --}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- create modal-->
        <div class="modal fade in" id="create" role="dialog" aria-hidden="false" style="display:none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Create Data Keuangan</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(array('url' => route('pChildCreate'), 'method' => 'post', 'class' => 'form-horizontal form-bordered')) !!}
                            <div class="form-group">
                                <label class="col-md-4 control-label">Id Perbankan</label>
                                <div class="col-md-6">
                                    <input type="text" name="perbankanid" class="form-control"  value="{!! old('perbankanid',$detail->id) !!}" readonly>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Lama nasabah (tahun) *</label>
                                <div class="col-md-6">
                                    <input type="text" name="nasabah_tahun" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Lama nasabah (bulan) *</label>
                                <div class="col-md-6">
                                    <input type="text" name="nasabah_bulan" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Jenis</label>
                                <div class="col-md-6">
                                    <select id="jenis" name="jenis" class="form-control" style="width: 100%">
                                        @foreach($jenis as $code=>$j)
                                            <option value="{{ $code }}">
                                                {{$j}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div id="holder"></div>
                            {{-- <div class="form-group">
                                <label class="col-md-4 control-label">Nama Bank *</label>
                                <div class="col-md-6">
                                    <input type="text" name="nama_bank" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Rekening *</label>
                                <div class="col-md-6">
                                    <input type="text" name="rekening" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Limit Kartu *</label>
                                <div class="col-md-6">
                                    <input type="text" name="limit_kartu" class="form-control" required>
                                </div>
                            </div> --}}
                            <div class="form-group form-actions">
                                <div class="col-md-9 col-md-offset-4">
                                    <input type="submit" class="btn btn-primary" value="Save Perbankan">
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- END create modal-->
    @stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
    <script src="{{ asset('assets/vendors/fancybox/helpers/jquery.fancybox-thumbs.js') }}" ></script>
    <script src="{{ asset('assets/js/pages/gallery.js') }}"  ></script>
    <script>
      $(document).ready(function() {
          $('#userid').select2()
          $('#jenis').select2()
      });
    </script>
    <script>
      $("#jenis").change(function(){
        val = $(this).val();

        if(val == "Tabungan / Giro / Deposito"){
          $("#holder").find("*").remove();
          var ctrl = '<div class="form-group"><label class="col-md-4 control-label">Nama Bank *</label><div class="col-md-6"><input type="text" name="nama_bank" class="form-control" placeholder="Nama Bank" required></div></div>';
            ctrl += '<div class="form-group striped-col"><label class="col-md-4 control-label">No. Rekening *</label><div class="col-md-6"><input type="text" name="rekening" class="form-control" placeholder="No. Rekening" required></div></div>';
            ctrl += '<div class="form-group"><label class="col-md-4 control-label">Saldo *</label><div class="col-md-6"><input type="text" id="autonum1" name="saldo" class="form-control" placeholder="Saldo" required></div></div>';
          $("#holder").append(ctrl);
          $('#autonum1').autoNumeric('init');
        }else if (val == "Kartu Kredit"){
          $("#holder").find("*").remove();
          var ctrl = '<div class="form-group"><label class="col-md-4 control-label">Nama Bank *</label><div class="col-md-6"><input type="text" name="nama_bank" class="form-control" placeholder="Nama Bank" required></div></div>';
            ctrl += '<div class="form-group striped-col"><label class="col-md-4 control-label">Limit Kartu Kredit *</label><div class="col-md-6"><input id="autonum2" type="text" name="limit_kartu" class="form-control" placeholder="Limit Kartu Kredit" required></div></div>';
          $("#holder").append(ctrl);
          $('#autonum2').autoNumeric('init');
        }else if (val == "Angsuran yang sedang berjalan"){
          $("#holder").find("*").remove();
          var ctrl = '<div class="form-group"><label class="col-md-4 control-label">Jenis Cicilan *</label><div class="col-md-6"><input type="text" name="jenis_cicilan" class="form-control" placeholder="Jenis Cicilan" required></div></div>';
            ctrl += '<div class="form-group striped-col"><label class="col-md-4 control-label">Jumlah Cicilan per Bulan *</label><div class="col-md-6"><input id="autonum3" type="text" name="jml_cicilan_bln" class="form-control" placeholder="Jumlah Cicilan per Bulan" required></div></div>';
            ctrl += '<div class="form-group"><label class="col-md-4 control-label">Jatuh Tempo Cicilan *</label><div class="col-md-6"><input type="date" name="jatuh_tempo" class="form-control" placeholder="Jatuh Tempo Cicilan" required></div></div>';
          $("#holder").append(ctrl);
          $('#autonum3').autoNumeric('init');
        }else{
          $("#holder").find("*").remove();
        }
      });
    </script>
@stop
