@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
  Pekerjaan Pemohon Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
        <section class="content-header">
            <h1>Pekerjaan Pemohon Form</h1>
        </section>
        <section class="content">
	        <div class="row">
				<div class="col-lg-12">
					<ul class="nav nav-tabs ">
						@if (!empty($pemohonid))
  						<li>
  							<a href="{{ URL::to('admin/pemohon/edit/'.$pemohonid) }}">Pemohon</a>
  						</li>
						@endif
						<li class="active">
							<a href="">Pekerjaan Pemohon</a>
						</li>
            @if (App\Pemohon::find($pemohonid)->status_kawin != "Lajang")
  						@if (empty($suamisitriid))
    						<li>
    							<a href="{{ URL::to('admin/suamiistri/create?backid='.$backid) }}">Pasangan</a>
    						</li>
  						@else
    						<li>
    							<a href="{{ URL::to('admin/suamiistri/edit/'.$suamisitriid) }}">Pasangan</a>
    						</li>
  						@endif
  						@if (!empty($jobsuamiistriid))
    						<li>
    							<a href="{{ URL::to('admin/jobsuamiistri/edit/'.$jobsuamiistriid) }}">Pekerjaan Pasangan</a>
    						</li>
  						@endif
            @endif
						@if (!empty($keluargaid))
  						<li>
  							<a href="{{ URL::to('admin/keluarga/edit/'.$keluargaid) }}">Emergency Contact</a>
  						</li>
						@endif
						{{-- @if (!empty($penghasilanid))
  						<li>
  							<a href="{{ URL::to('admin/penghasilan/edit/'.$penghasilanid) }}">Penghasilan</a>
  						</li>
						@endif --}}
						@if (!empty($perbankanid))
  						<li>
  							<a href="{{ URL::to('admin/perbankan/edit/'.$perbankanid) }}">Perbankan</a>
  						</li>
						@endif
            @if ($countApp == 0)
              <li>
                <a href="{{ URL::to('admin/application/create?backid='.$backid) }}">Create Application</a>
              </li>
            @elseif($countApp == 1)
              <li>
                <a href="{{ URL::to('admin/application/edit/'.$applicationid) }}">Application</a>
              </li>
            @elseif($countApp > 1)
              <li>
                <a href="{{ URL::to('admin/application?iduser='.$detail->userid) }}">{{ $countApp }} Application</a>
              </li>
            @endif
					</ul>
				</div>
			</div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading clearfix">
                            <div class="panel-title pull-left">
                              <div class="caption">
                                <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Edit Pekerjaan Pemohon
                              </div>
                            </div>
                            <div class="pull-right">
                              <a href="/admin/jobpemohon" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Pekerjaan Pemohon</a>
                            </div>
                        </div>
                        <div class="panel-body border">
                          {!! Form::model($detail, ['method' => 'PUT', 'url' => $detail->editUrl(), 'class' => 'form-horizontal form-bordered', 'files'=> true]) !!}
                              <div class="form-group">
                                  <label class="col-md-4 control-label">ID</label>
                                  <div class="col-md-6">
                                      <input type="text" name="id" class="form-control" value="{!! old('id',$detail->id) !!}" readonly>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Nama Pemohon</label>
                                  <div class="col-md-6">
                                      <input type="text" value="{{ $detail->user->fullname }}" class="form-control" readonly>
                                      <input type="hidden" name="userid" value="{{ $detail->user->id }}">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Jenis Pekerjaan</label>
                                  <div class="col-md-6">
                                      <select id="jenis_pekerjaan" name="jenis_pekerjaan" class="form-control" style="width: 100%">
                                        @foreach($jenis as $code=>$j)
                                          <option value="{{ $code }}" @if($code === $detail->jenis_pekerjaan) selected @endif>
                                            {{$j}}
                                          </option>
                                        @endforeach
                                      </select>
                                      <div id="holder"></div>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Nama Perusahaan *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="nama_perusahaan" class="form-control" value="{!! old('nama_perusahaan',$detail->nama_perusahaan) !!}" required>
                                  </div>
                              </div>
                              {{-- <div class="form-group">
                                  <label class="col-md-4 control-label">Bidang Usaha</label>
                                  <div class="col-md-6">
                                      <select id="bidang_usaha" name="bidang_usaha" class="form-control" style="width: 100%">
                                        @foreach($usaha as $code=>$j)
                                          <option value="{{ $code }}" @if($code === $detail->bidang_usaha) selected @endif>
                                            {{$j}}
                                          </option>
                                        @endforeach
                                      </select>
                                  </div>
                              </div> --}}
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Bidang Usaha Detail *</label>
                                  <div class="col-md-6">
                                      <select id="bidang_usaha_detail" name="bidang_usaha_detail" class="form-control" style="width: 100%">
                                        @foreach($usaha as $code=>$j)
                                          <option value="{{ $code }}" @if ($code === "$detail->bidang_usaha_detail") selected @endif>
                                            {{$j}}
                                          </option>
                                        @endforeach
                                      </select>
                                      {{-- <input type="text" name="bidang_usaha_detail" class="form-control" value="{!! old('bidang_usaha_detail',$detail->bidang_usaha_detail) !!}" required> --}}
                                  </div>
                              </div>
                              {{-- <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Tanggal Pendirian *</label>
                                  <div class="col-md-3">
                                      <input type="date" name="tanggal_pendirian" class="form-control" value="{!! old('tanggal_pendirian',$detail->tanggal_pendirian) !!}" required>
                                  </div>
                              </div> --}}
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Alamat Kantor *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="alamat" class="form-control" value="{!! old('alamat',$detail->alamat) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label for="propinsi" class="col-md-4 control-label">Propinsi</label>
                                  <div class="col-md-6">
                                      {{ Form::select('propinsi', $propinsi, $detail->propinsi, array('id' => 'sPropinsi', 'style'=>'width: 100%'))  }}
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label for="kota" class="col-md-4 control-label">Kabupaten / Kota</label>
                                  <div class="col-md-6">
                                      {{ Form::select('kota', $kota, $detail->kota, array('id' => 'sKota', 'style'=>'width: 100%')) }}
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label for="kecamatan" class="col-md-4 control-label">Kecamatan</label>
                                  <div class="col-md-6">
                                      {{ Form::select('kecamatan', $kecamatan, $detail->kecamatan, array('id' => 'sKecamatan', 'style'=>'width: 100%')) }}
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label for="kelurahan" class="col-md-4 control-label">Desa / Kelurahan</label>
                                  <div class="col-md-6">
                                      {{ Form::select('kelurahan', $desa, $detail->kelurahan, array('id' => 'sDesa', 'style'=>'width: 100%')) }}
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Postal</label>
                                  <div class="col-md-6">
                                      <input type="text" name="postal" class="form-control" value="{!! old('postal',$detail->postal) !!}">
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Telepon Kantor *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="tlp_ktr_direct" class="form-control" value="{!! old('tlp_ktr_direct',$detail->tlp_ktr_direct) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Extension</label>
                                  <div class="col-md-6">
                                      <input type="text" name="tlp_ktr_hunting" class="form-control" value="{!! old('tlp_ktr_hunting',$detail->tlp_ktr_hunting) !!}">
                                  </div>
                              </div>
                              {{-- <div class="form-group">
                                  <label class="col-md-4 control-label">Fax</label>
                                  <div class="col-md-6">
                                      <input type="text" name="fax" class="form-control" value="{!! old('fax',$detail->fax) !!}">
                                  </div>
                              </div> --}}
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Unit Kerja</label>
                                  <div class="col-md-6">
                                      <input type="text" name="unit_kerja" class="form-control" value="{!! old('unit_kerja',$detail->unit_kerja) !!}">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Jabatan *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="jabatan" class="form-control" value="{!! old('jabatan',$detail->jabatan) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Gaji Bersih per Bulan *</label>
                                  <div class="col-md-6">
                                      <input id="autonum2" type="text" name="pemohon" class="form-control" value="{!! old('pemohon',$detailPenghasilan->pemohon) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Bekerja sejak Bulan *</label>
                                  <div class="col-md-6">
                                      {{ Form::selectMonth('month', $detail->bulan, ['class' => 'form-control', 'name' => 'bulan', 'id' => 'bulan']) }}
                                  </div>
                              </div>

                              <div class="form-group">
                                  <label class="col-md-4 control-label">Bekerja sejak Tahun *</label>
                                  <div class="col-md-6">
                                      <select id="tahun" name="tahun" class="form-control" style="width: 100%">
                                          @for ($awal = 1930; $awal <= date('Y'); $awal++)
                                            <option value="{{$awal}}" @if($awal == $detail->tahun) selected @endif>
                                                {{$awal}}
                                            </option>}
                                          @endfor
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group ">
                                  <label class="col-md-4 control-label">Surat Keterangan Kerja Image *</label>
                                  <div class="col-md-6">
                                      <div style="margin-bottom: 10px">
                                          <a class="fancybox-effects-a" href="{!! $detail->surat_ket_kerja == '' ? '' : '/'.$detail->surat_ket_kerja !!}">
                                              {!! $detail->surat_ket_kerja == '' ? '' : '<img src="'.'/'.$detail->surat_ket_kerja.'" class="img-responsive" style="max-height: 350px">' !!}
                                          </a>
                                      </div>
                                      <div>
                                          <input type="file" name="surat_ket_kerja" value="{!! old('surat_ket_kerja',$detail->surat_ket_kerja) !!}">
                                      </div>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <h4 class="col-md-4  control-label" style="margin-bottom: 20px;">Untuk wiraswasta/profesi</h4>
                                </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Omset Kotor per Bulan</label>
                                  <div class="col-md-6">
                                      <input type="text" name="omset_bln" id="autonum1" class="form-control" value="{!! old('omset_bln',$detail->omset_bln) !!}">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Nama Perusahaan Ago</label>
                                  <div class="col-md-6">
                                      <input type="text" name="nama_perusahaan_ago" class="form-control" value="{!! old('nama_perusahaan_ago',$detail->nama_perusahaan_ago) !!}">
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Jenis Usaha Ago</label>
                                  <div class="col-md-6">
                                      <input type="text" name="jenis_usaha_ago" class="form-control" value="{!! old('jenis_usaha_ago',$detail->jenis_usaha_ago) !!}">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Jabatan Ago</label>
                                  <div class="col-md-6">
                                      <input type="text" name="jabatan_ago" class="form-control" value="{!! old('jabatan_ago',$detail->jabatan_ago) !!}">
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Unit Kerja Ago</label>
                                  <div class="col-md-6">
                                      <input type="text" name="unit_kerja_ago" class="form-control" value="{!! old('unit_kerja_ago',$detail->unit_kerja_ago) !!}">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Telepon</label>
                                  <div class="col-md-6">
                                      <input type="text" name="telp" class="form-control" value="{!! old('telp',$detail->telp) !!}">
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Bulan Ago</label>
                                  <div class="col-md-6">
                                      <input type="text" name="bulan_ago" class="form-control" value="{!! old('bulan_ago',$detail->bulan_ago) !!}">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Tahun Ago</label>
                                  <div class="col-md-6">
                                      <input type="text" name="tahun_ago" class="form-control" value="{!! old('tahun_ago',$detail->tahun_ago) !!}">
                                  </div>
                              </div>
                              <div class="form-group form-actions">
                                  <div class="col-md-9 col-md-offset-4">
                                      <input type="submit" class="btn btn-primary" value="Save Pekerjaan Pemohon">
                                  </div>
                              </div>
                          {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script>
      $(document).ready(function() {
          $('#jenis_pekerjaan').select2()
          $('#userid').select2()
          $('#bidang_usaha_detail').select2()
          $('#tahun').select2()
          $('#bulan').select2()
          $('#sPropinsi').select2()
          $('#sDesa').select2()
          $('#sKecamatan').select2()
          $('#sKota').select2()
      });
    </script>
    <script>
      $("#jenis_pekerjaan").change(function(){
        val = $(this).val();

        if(val == "Lainnya"){
          var ctrl = '<input type="text" name="nama_pekerjaan_lain" class="form-control" style="margin-top:20px" placeholder="Masukkan jenis pekerjaan lainnya" required>';
          $("#holder").append(ctrl);
        }else if (val == "Wiraswasta"){
          $("#holder input").remove();
          var ctrl = '<input type="date" name="tanggal_pendirian" class="form-control" style="margin-top:20px" placeholder="Tanggal Pendirian">';
          $("#holder").append(ctrl);
        }else{
          $("#holder input").remove();
        }
      });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#sPropinsi').on('change', function(){
                $.post('{{ URL::to('admin/site/data') }}',
                {
                    '_token': '{{ csrf_token() }}',
                    type: 'kota',
                    id: $('#sPropinsi').val(),
                }, function(e){
                    $('#sKota').html(e);
                });
                $('#sKecamatan').html('');
                $('#sDesa').html('');

            });
            $('#sKota').on('change', function(){
                $.post('{{ URL::to('admin/site/data') }}',
                {
                    '_token': '{{ csrf_token() }}',
                    type: 'kecamatan',
                    id: $('#sKota').val()
                }, function(e){
                    $('#sKecamatan').html(e);
                });
                $('#sDesa').html('');
            });
            $('#sKecamatan').on('change', function(){
                $.post('{{ URL::to('admin/site/data') }}',
                {
                    '_token': '{{ csrf_token() }}',
                    type: 'kelurahan',
                    id: $('#sKecamatan').val()
                }, function(e){
                    $('#sDesa').html(e);
                });
            });
        });
    </script>
@stop
