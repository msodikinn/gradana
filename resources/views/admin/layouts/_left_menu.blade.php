<ul id="menu" class="page-sidebar-menu">
    <li {!! (Request::is('admin') ? 'class="active"' : '') !!}>
        <a href="{{ route('dashboard') }}">
            <i class="livicon" data-name="presentation" data-size="18" data-c="#418BCA" data-hc="#418BCA"
               data-loop="true"></i>
            <span class="title">Dashboard</span>
        </a>
    </li>
    @if (permission() == "Admin")
        <li {!! (Request::is('admin/developer*') ? 'class="active"' : '') !!}>
            <a href="{{ route('developer') }}">
                <i class="livicon" data-name="hammer" data-size="18" data-c="#418BCA" data-hc="#418BCA" data-loop="true"></i>
                <span class="title">Developer</span>
            </a>
        </li>
        <li {!! (Request::is('admin/property*') || Request::is('admin/gallery*') || Request::is('admin/picture*') ? 'class="active"' : '') !!}>
            <a href="#">
                <i class="livicon" data-name="image" data-c="#418BCA" data-hc="#418BCA" data-size="18"
                   data-loop="true"></i>
                <span class="title">Property</span>
                <span class="fa arrow"></span>
            </a>
            <ul class="sub-menu">
                <li {!! (Request::is('admin/property*') ? 'class="active"' : '') !!}>
                    <a href="{{ route('property') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Property
                    </a>
                </li>
                <li {!! (Request::is('admin/gallery*') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/gallery') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Gallery Property
                    </a>
                </li>
                <li {!! (Request::is('admin/picture*') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/picture') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Picture
                    </a>
                </li>
            </ul>
        </li>
    @endif
    <li {!! (Request::is('admin/pembayaran*') || Request::is('admin/groups*') || Request::is('admin/users*') || Request::is('admin/user_profile') || Request::is('admin/users/*') || Request::is('admin/deleted_users*') || Request::is('admin/application*') || Request::is('admin/pemohon*') || Request::is('admin/job*') || Request::is('admin/keluarga*') || Request::is('admin/suami*') || Request::is('admin/pemohon*') || Request::is('admin/penghasilan*') || Request::is('admin/perbankan*') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="user" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
               data-loop="true"></i>
            <span class="title">Users</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/users') || Request::is('admin/users/create') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/users') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Users
                </a>
            </li>
            @if (permission() == "Admin")
              <li {!! (Request::is('admin/groups*') ? 'class="active" id="active"' : '') !!}>
                  <a href="{{ URL::to('admin/groups') }}">
                      <i class="fa fa-angle-double-right"></i>
                      User Groups
                  </a>
              </li>
            @endif
            <li {!! (Request::is('admin/pemohon*') || Request::is('admin/job*') || Request::is('admin/keluarga*') || Request::is('admin/suami*') || Request::is('admin/pemohon*') || Request::is('admin/penghasilan*') || Request::is('admin/perbankan*') ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/pemohon') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Data Pemohon
                </a>
            </li>
            {{-- <li {!! ((Request::is('admin/users/')) ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::route('users.show',Sentinel::getUser()->id) }}">
                    <i class="fa fa-angle-double-right"></i>
                    View Profile
                </a>
            </li> --}}
            @if (permission() == "Applicant" || permission() == "Admin")
              <li {!! (Request::is('admin/application*') ? 'class="active"' : '') !!}>
                  <a href="{{ URL::to('admin/application') }}">
                      <i class="fa fa-angle-double-right"></i>
                      <!--Application Form-->
					  Scheme 
                  </a>
              </li>
              <li {!! (Request::is('admin/pembayaran*') ? 'class="active"' : '') !!}>
                  <a href="{{ URL::to('admin/pembayaran') }}">
                      <i class="fa fa-angle-double-right"></i>
                      Loan Payment
                  </a>
              </li>
            @endif
        </ul>
    </li>
    {{-- @if (permission() == "Applicant" || permission() == "Admin")
        <li {!! (Request::is('admin/pemohon*') || Request::is('admin/suamiistri*') || Request::is('admin/keluarga*') || Request::is('admin/jobpemohon*') || Request::is('admin/jobsuamiistri*') || Request::is('admin/penghasilan*') || Request::is('admin/perbankan*') ? 'class="active"' : '') !!}>
            <a href="#"><i class="livicon" data-name="notebook" data-c="#418BCA" data-hc="#418BCA" data-size="18" data-loop="true"></i>
                <span class="title">Data Pemohon</span>
                <span class="fa arrow"></span>
            </a>
            <ul class="sub-menu">
                <li {!! (Request::is('admin/pemohon*') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/pemohon') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Pemohon
                    </a>
                </li>
                <li {!! (Request::is('admin/jobpemohon*') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/jobpemohon') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Pekerjaan Pemohon
                    </a>
                </li>
                <li {!! (Request::is('admin/suamiistri*') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/suamiistri') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Pasangan
                    </a>
                </li>
                <li {!! (Request::is('admin/jobsuamiistri*') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/jobsuamiistri') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Pekerjaan Pasangan
                    </a>
                </li>
                <li {!! (Request::is('admin/keluarga*') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/keluarga') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Keluarga Terdekat
                    </a>
                </li>
                <li {!! (Request::is('admin/penghasilan*') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/penghasilan') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Data Penghasilan
                    </a>
                </li>
                <li {!! (Request::is('admin/perbankan*') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/perbankan') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Data Perbankan/Lainnya
                    </a>
                </li>
            </ul>
        </li>
    @endif --}}
    @if (permission() == "Investor" || permission() == "Admin")
        <li {!! (Request::is('admin/investorpayment*') || Request::is('admin/investor') || Request::is('admin/investor/alokasi*') || Request::is('admin/investor/create*') || Request::is('admin/investor/edit*') || Request::is('admin/investor/detail*') || Request::is('admin/investor_cat*') || Request::is('admin/investor/borrower*') ? 'class="active"' : '') !!}>
            <a href="#"><i class="livicon" data-name="money" data-c="#418BCA" data-hc="#418BCA" data-size="18" data-loop="true"></i>
                <span class="title">Lender</span>
                <span class="fa arrow"></span>
            </a>
            <ul class="sub-menu">
                <li {!! (Request::is('admin/investor') || Request::is('admin/investor/create*') ||Request::is('admin/investor/edit*') || Request::is('admin/investor/detail*') ? 'class="active"' : '') !!}>
                    <a href="{{ route('investor') }}">
                        <i class="fa fa-angle-double-right"></i> Lender
                    </a>
                </li>
                {{-- <li {!! (Request::is('admin/investor_cat*') ? 'class="active"' : '') !!}>
                    <a href="{{ route('investor_cat') }}">
                        <i class="fa fa-angle-double-right"></i> Category
                    </a>
                </li> --}}
                <li {!! (Request::is('admin/investor/alokasi*') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/investor/alokasi') }}">
                        <i class="fa fa-angle-double-right"></i> Alokasi Dana
                    </a>
                </li>
                <li {!! (Request::is('admin/investor/borrower*') ? 'class="active"' : '') !!}>
                    <a href="/admin/investor/borrower">
                        <i class="fa fa-angle-double-right"></i> Available Borrowers
                    </a>
                </li>
                <li {!! (Request::is('admin/investorpayment*') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/investorpayment') }}">
                        <i class="fa fa-angle-double-right"></i> Lender Payment
                    </a>
                </li>
            </ul>
        </li>
    @endif
    @if (permission() == "Admin")
        <li {!! (Request::is('admin/assesment*') || Request::is('admin/survey*') || Request::is('admin/scoring*') ? 'class="active"' : '') !!}>
            <a href="#">
                <i class="livicon" data-name="archive-add" data-c="#418BCA" data-hc="#418BCA" data-size="18"
                   data-loop="true"></i>
                <span class="title">Manage Credit</span>
                <span class="fa arrow"></span>
            </a>
            <ul class="sub-menu">
                <li {!! (Request::is('admin/survey*') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/survey') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Application Survey
                    </a>
                </li>
                <li {!! (Request::is('admin/scoring*') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/scoring') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Credit Scoring
                    </a>
                </li>
                <li {!! (Request::is('admin/assesment*') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/assesment') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Credit Assesment
                    </a>
                </li>
            </ul>
        </li>
        <li {!! ((Request::is('admin/blogcategory*') || Request::is('admin/blogcategory/create*') || Request::is('admin/blog*') ||  Request::is('admin/blog/create*')) || Request::is('admin/blog/*') || Request::is('admin/blogcategory/*') ? 'class="active"' : '') !!}>
            <a href="#">
                <i class="livicon" data-name="comment" data-c="#F89A14" data-hc="#F89A14" data-size="18"
                   data-loop="true"></i>
                <span class="title">Blog</span>
                <span class="fa arrow"></span>
            </a>
            <ul class="sub-menu">
                <li {!! (Request::is('admin/blogcategory*') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/blogcategory') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Kategori
                    </a>
                </li>
                <li {!! (Request::is('admin/blog') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/blog') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Blog List
                    </a>
                </li>
                <li {!! (Request::is('admin/blog/create*') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/blog/create') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Create Blog
                    </a>
                </li>
            </ul>
        </li>
        <li {!! (Request::is('admin/newsletter*') ? 'class="active"' : '') !!}>
            <a href="{{ route('newsletter') }}">
                <i class="livicon" data-name="mail-alt" data-size="18" data-c="#418BCA" data-hc="#418BCA" data-loop="true"></i>
                <span class="title">Send Newsletter</span>
            </a>
        </li>
    @endif
    <!-- Menus generated by CRUD generator -->
    @include('admin/layouts/menu')
</ul>
