<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="token" id="token" value="{{ csrf_token() }}">
    <title>
        @section('title')
            | Gradana Admin Page
        @show
    </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/lib/sweetalert-master/dist/sweetalert.css') }}" rel="stylesheet"></script>

        @yield('header_styles')
</head>
<body class="skin-josh">
<header class="header">
    <a href="/index" class="logo">
        <img src="{{ asset('assets/img/gradana-logo-temp.png') }}" alt="logo">
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <div>
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <div class="responsive_nav"></div>
            </a>
        </div>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        @if(Sentinel::getUser()->pic)
                            <img src="{!! '/'.Sentinel::getUser()->pic !!}" alt="img"
                                 class="img-circle img-responsive pull-left" height="35px" width="35px"/>
                        @else
                            <img src="{!! defaultpic() !!} " width="35"
                                 class="img-circle img-responsive pull-left" height="35" alt="riot">
                        @endif
                        <div class="riot">
                            <div>
                                {{ Sentinel::getUser()->first_name }} {{ Sentinel::getUser()->last_name }}
                                <span>
                                        <i class="caret"></i>
                                    </span>
                            </div>
                        </div>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header bg-light-blue">
                            @if(Sentinel::getUser()->pic)
                                <img src="{!! '/'.Sentinel::getUser()->pic !!}" alt="img"
                                     class="img-circle img-bor"/>
                            @else
                                <img src="{!! defaultpic() !!}"
                                     class="img-responsive img-circle" alt="User Image">
                            @endif
                            <p class="topprofiletext">{{ Sentinel::getUser()->fullname }}</p>
                        </li>
                        <!-- Menu Body -->
                        <li>
                            <a href="{{ URL::route('users.show',Sentinel::getUser()->id) }}">
                                <i class="livicon" data-name="user" data-s="18"></i>
                                My Profile
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.users.edit', Sentinel::getUser()->id) }}">
                                <i class="livicon" data-name="gears" data-s="18"></i>
                                Account Settings
                            </a>
                        </li>
                        <!-- Menu Footer-->
                        <li>
                            <a href="{{ URL::to('admin/logout') }}">
                                <i class="livicon" data-name="sign-out" data-s="18"></i>
                                Logout
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <section class="sidebar ">
            <div class="page-sidebar  sidebar-nav">
            <div class="clearfix"></div>
            <!-- BEGIN SIDEBAR MENU -->
            @include('admin.layouts._left_menu')
            <!-- END SIDEBAR MENU -->
            </div>
        </section>
    </aside>
    <aside class="right-side">

        <!-- Notifications -->
        @include('notifications')

                <!-- Content -->
        @yield('content')

    </aside>
    <!-- right-side -->
</div>
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top"
   data-toggle="tooltip" data-placement="left">
    <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
</a>
<!-- global js -->
<script src="{{ asset('assets/js/app.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/lib/vue-lib.js') }}"></script>
<script src="{{ asset('assets/lib/auto-numeric/autoNumeric.js') }}"></script>
<script src="{{ asset('assets/lib/sweetalert-master/dist/sweetalert-dev.js') }}"></script>
@stack('scripts')

<!-- end of global js -->
    @include('sweet::alert')
<script>
  $(document).ready(function(){
    $('#autonum0').autoNumeric('init');
    $('#autonum1').autoNumeric('init');
    $('#autonum2').autoNumeric('init');
    $('#autonum3').autoNumeric('init');
    $('#autonum4').autoNumeric('init');
    $('#autonum5').autoNumeric('init');
    $('#autonum6').autoNumeric('init');
    $('#autonum7').autoNumeric('init');
    $('#autonum8').autoNumeric('init');
    $('#autonum9').autoNumeric('init');
    $('#autonum10').autoNumeric('init');
    $('#autonum11').autoNumeric('init');
    $('#autonum12').autoNumeric('init');
    $('#autonum13').autoNumeric('init');
    $('#autonum14').autoNumeric('init');
    $('#autonum15').autoNumeric('init');
    $('#autonum16').autoNumeric('init');
    $('#autonum17').autoNumeric('init');
    $('#autonum18').autoNumeric('init');
    $('#autonum19').autoNumeric('init');
    $('#autonum20').autoNumeric('init');
    $('#autonum21').autoNumeric('init');
    $('#autonum22').autoNumeric('init');
    $('#autonum23').autoNumeric('init');
    $('#autonum24').autoNumeric('init');
    $('#autonum25').autoNumeric('init');
    $('#autonum26').autoNumeric('init');
    $('#autonum27').autoNumeric('init');
    $('#autonum28').autoNumeric('init');
    $('#autonum29').autoNumeric('init');
    $('#autonum30').autoNumeric('init');
    $('#autonum31').autoNumeric('init');
    $('#autonum32').autoNumeric('init');
  });
</script>

<!-- begin page level js -->
    @yield('footer_scripts')
<!-- end page level js -->
</body>
</html>
