@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Application Management
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
@stop

{{-- Page content --}}
@section('content')

        <section class="content-header">
                <!--section starts-->
                <h1>Manage Application</h1>

            </section>
            <!--section ends-->
            <section class="content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary filterable">
                            <div class="panel-heading clearfix">
                              <div class="panel-title pull-left">
                                <div class="caption">
                                  <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> List Application
                                </div>
                              </div>
                              <div class="pull-right">
                                <a href="/admin/application/create" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> Create Application</a>
                              </div>
                            </div>
                            <div class="panel-body table-responsive">
                                <h4>
                                    @if (permission() == "Admin")
                                        <p>Anda Login sebagai: <a href="{{ '/admin/users/'.Sentinel::getuser()->id }}">{{ Sentinel::getuser()->fullname }}</a></p>
                                        @if (!empty($fromPemohon))
                                            <p>Data application milik:
                                              <a href="{{ '/admin/pemohon/detail/'.$apl->first()->user->pemohon->id }}">
                                                {{ $apl->first()->user->fullname }}
                                              </a>
                                            </p>
                                            <p><a href="/admin/application" class="btn btn-sm btn-primary pull-right">Browse All Application</a></p>
                                        @endif
                                    @elseif(permission() == "Applicant")
                                        Nama Pemohon: <a href="{{ '/admin/users/'.$apl[0]->user->id }}">{{ $apl[0]->user->fullname }}</a>
                                    @endif

                                </h4><br>
                                <table class="table table-striped table-bordered" id="table1">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Tujuan</th>
                                            <th>Sistem Pembayaran</th>
                                            <th>Harga Property</th>
                                            <th>Pinjaman</th>
                                            <th>Pengajuan Angsuran</th>
                                            <th>Lama Angsuran</th>
                                            <th>Status</th>
                                            <th style="width: 100px">Approval</th>
                                            <th>Payment </th>
											<!--th>Scoring </th>
											<th>Send To Lender </th-->
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=0; ?>
                                        @foreach ($apl as $a)
                                            <tr role="row" class="odd">
                                                <td><a href="{{ $a->detailUrl() }}">{{ $a->id }}</a></td>
                                                <td>{{ $a->tujuan }}</td>
                                                <td>{{ $a->sis_bayar }}</td>
                                                <td>{{ uang($a->harga_property) }}</td>
                                                <td>{{ uang($a->pinjaman) }}</td>
                                                <td>{{ uang($a->pinjaman_bln) }}</td>
                                                <td>{{ $total[$i] }} bulan</td>
                                                <td>{{ $a->status }}</td>
                                                @if($a->approved == 0)
                                                    @if(Sentinel::getuser()->status == "Admin")
                                                      <td><a href="{{ $a->approved() }}" class="btn btn-sm btn-primary">Approved</a></td>
                                                    @else
                                                      <td><i class="fa fa-fw fa-times"></i> Unapproved</td>
                                                    @endif
                                                    <td></td>
                                                @else
                                                    @if(Sentinel::getuser()->status == "Admin")
                                                        <td><a href="{{ $a->unapproved() }}" class="btn btn-sm btn-primary">Unapproved</a></td>
                                                    @else
                                                        <td><i class="fa fa-fw fa-check"></i> Approved</td>
                                                    @endif
                                                    <td><a href="{{ $a->payUrl() }}" class="btn btn-primary">Bayar</a></td>
													
                                                @endif
												
											<!--	@if($a->approved == 0)
													<td> </td>
												@else
													<td><a href="{{ $a->scoreUrl() }}" class="btn btn-primary">Score</a></td>
												@endif
												
												@if($a->score == 0)
													<td> </td>
												@else
													<td><a href="{{ $a->sendUrl() }}" class="btn btn-primary">Send</a></td>
												@endif -->
												
												
                                                <td><a class="edit" href="{{ $a->editUrl() }}">Edit</a></td>
                                                <td><a class="edit" href="{{ $a->delUrl() }}">delete</a></td>
                                            </tr>
                                            <?php $i++; ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Third Basic Table Ends Here-->
            </section>
            <!-- content -->

    @stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
@stop
