@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Application Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
        <section class="content-header">
            <h1>Edit Application Form</h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading clearfix">
                            <div class="panel-title pull-left">
                              <div class="caption">
                                <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Edit Application
                              </div>
                            </div>
                            <div class="pull-right">
                              <a href="/admin/application" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Application</a>
                            </div>
                        </div>
                        <div class="panel-body border">
                          {!! Form::model($detail, ['method' => 'PUT', 'url' => $detail->editUrl(), 'class' => 'form-horizontal form-bordered']) !!}
                            <div class="form-group">
                                <label class="col-md-4 control-label">ID</label>
                                <div class="col-md-6">
                                    <input type="text" name="id" class="form-control" value="{!! old('id',$detail->id) !!}" readonly>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Nama Pemohon</label>
                                <div class="col-md-6">
                                    <select id="userid" name="userid" class="form-control" style="width: 100%">
                                      @foreach ($users as $u)
                                        <option value="{{ $u->id }}" @if($u->id === $detail->userid) selected @endif> {{ $u->fullname }} </option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                            @if (Sentinel::getuser()->status == "Admin")
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Developer</label>
                                    <div class="col-md-6">
                                        <select id="devid" name="devid" class="form-control" style="width: 100%">
                                        @foreach ($dev as $d)
                                          <option value="{{ $d->id }}" @if($d->id === $detail->devid) selected @endif>
                                            {{ $d->nama }}
                                          </option>
                                        @endforeach
                                      </select>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Property</label>
                                <div class="col-md-6">
                                    <select id="propid" name="propid" class="form-control" style="width: 100%">
                                      @foreach($prop as $p)
                                        <option value="{{ $p->id }}" @if($p->id === $detail->propid) selected @endif>
                                          {{$p->nama}}
                                        </option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                            @if (Sentinel::getuser()->status == "Admin")
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Lender</label>
                                    <div class="col-md-6">
                                        <select id="investorid" name="investorid" class="form-control" style="width: 100%">
                                          @foreach($inves as $i)
                                            <option value="{{ $i->id }}" @if($i->id === $detail->investorid) selected @endif>
                                              {{$i->nama}}
                                            </option>
                                          @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Tujuan / Keperluan</label>
                                  <div class="col-md-6">
                                      <select id="tujuan" name="tujuan" class="form-control" style="width: 100%">
                                        @foreach($tujuan as $code=>$t)
                                          <option value="{{ old('tujuan', $code) }}" @if($code === $detail->tujuan) selected @endif>
                                            {{$t}}
                                          </option>
                                        @endforeach
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Jenis Aplikasi</label>
                                  <div class="col-md-6">
                                      <select id="jenis" name="jenis" class="form-control" style="width: 100%">
                                        @foreach($jenis as $code=>$j)
                                          <option value="{{ old('jenis', $code) }}" @if($code === $detail->jenis) selected @endif>
                                            {{$j}}
                                          </option>
                                        @endforeach
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Uang Muka *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="uang_muka" id="autonum1" class="form-control" value="{!! old('uang_muka',$detail->uang_muka) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Harga Property *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="harga_property" id="autonum6" class="form-control" value="{!! old('harga_property',$detail->harga_property) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Sistem Pembayaran</label>
                                  <div class="col-md-6">
                                      <select id="bayar" name="sis_bayar" class="form-control" style="width: 100%">
                                        @foreach($bayar as $code=>$b)
                                          <option value="{{ $code }}" @if($code === $detail->sis_bayar) selected @endif>
                                            {{$b}}
                                          </option>
                                        @endforeach
                                      </select>
                                  </div>
                              </div>
							  <div class="form-group">
                                    <label class="col-md-4 control-label">Skema </label>
                                    <div class="col-md-6">
                                        <select id="scheme" name="scheme" class="form-control" style="width: 100%">
                                         
                                            <option value="skema_a">
                                            Skema A: Investor Membayar FULL kas keras ke developer  
                                            </option>
											<option value="skema_b">
                                            Skema B: Investor Hanya Membayar DP ke Developer Mengikuti harga DP  
                                            </option>
										 
                                        </select>
                                    </div>
                                </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Jumlah Pinjaman *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="pinjaman" id="autonum2" class="form-control" value="{!! old('pinjaman',$detail->pinjaman) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Pinjaman Bulanan *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="pinjaman_bln" id="autonum3" class="form-control" value="{!! old('pinjaman_bln',$detail->pinjaman_bln) !!}" required>
                                      <p class="help-block">pinjaman bulanan yang diajukan</p>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Pinjaman (tahun) *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="periode" class="form-control" value="{!! old('periode',$detail->periode) !!}" required>
                                      <p class="help-block">dalam tahun</p>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Pinjaman (bulan) *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="jangka_waktu" class="form-control" value="{!! old('jangka_waktu',$detail->jangka_waktu) !!}" required>
                                      <p class="help-block">dalam bulan</p>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Status</label>
                                  <div class="col-md-6">
                                      <select id="status" name="status" class="form-control" style="width: 100%">
                                        @foreach($status as $code=>$s)
                                          <option value="{{ $code }}" @if($code === $detail->status) selected @endif>
                                            {{$s}}
                                          </option>
                                        @endforeach
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Outstanding Loan *</label>
                                    <div class="col-md-6">
                                        <input type="text" name="outstanding_loan" id="autonum4" class="form-control" value="{!! old('outstanding_loan',$detail->outstanding_loan) !!}" required>
                                    </div>
                                </div>
                                {{-- <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Installment Times *</label>
                                    <div class="col-md-3">
                                        <input type="date" name="installment_times" class="form-control" value="{!! old('installment_times',$detail->installment_times) !!}" required>
                                    </div>
                                </div> --}}
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Anggaran Renovasi</label>
                                    <div class="col-md-6">
                                        <input type="text" name="renovasi" id="autonum5" class="form-control" value="{!! old('renovasi',$detail->renovasi) !!}">
                                        <p class="help-block">optional: khusus untuk renovasi</p>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label" style="padding-top:15px !important;">Approval</label>
                                    <div class="col-md-6">
                                        {{ Form::radio('approved', 0, ['class' => 'radio-inline form-control']) }} &nbsp;Unapproved &nbsp;&nbsp;&nbsp;&nbsp;
                                        {{ Form::radio('approved', 1, ['class' => 'radio-inline form-control']) }} &nbsp;Approved
                                    </div>
                                </div>
                                {{-- <div class="form-group">
                                    <label class="col-md-4 control-label"><h3>A/R Factoring</h3></label>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Jaminan</label>
                                    <div class="col-md-6">
                                        <select id="jaminan" name="jaminan" class="form-control" style="width: 100%" disabled>
                                          @foreach($jaminan as $code=>$j)
                                            <option value="{{ $code }}">
                                              {{$j}}
                                            </option>
                                          @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Jatuh Tempo</label>
                                    <div class="col-md-3">
                                        <input type="date" name="jatuh_tempo" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Invoice Quantity</label>
                                    <div class="col-md-6">
                                        <input type="text" name="invoice_qty" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Invoice Date</label>
                                    <div class="col-md-3">
                                        <input type="date" name="invoice_date" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">Invoice Client</label>
                                    <div class="col-md-3">
                                        <input type="date" name="invoice_client" class="form-control" disabled>
                                    </div>
                                </div> --}}
                              <div class="form-group form-actions">
                                  <div class="col-md-9 col-md-offset-4">
                                      <input type="submit" class="btn btn-primary" value="Save Application">
                                  </div>
                              </div>
                          {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#userid').select2()
            $('#devid').select2()
            $('#propid').select2()
            $('#investorid').select2()
            $('#jenis').select2()
            $('#tujuan').select2()
            $('#bayar').select2()
            $('#status').select2()
            $('#jaminan').select2()
        });
    </script>

@stop
