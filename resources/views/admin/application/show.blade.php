@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Detail Application
@parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link href="{{ asset('assets/vendors/modal/css/component.css') }}" rel="stylesheet" />
@stop

@section('content')
<section class="content-header">
    <h1>Detail Application</h1>
</section>

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <div class="panel-title pull-left">
                    <div class="caption">
                        <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Detail {{ $detail->nama }}
                    </div>
                </div>
                <div class="pull-right">
                    <a href="/admin/application" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Application</a>
                    <a href="{{ $detail->editUrl() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-minus"></span> Edit Application</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="col-md-6">
                    <table class="table ">
                        <tr><td>ID Application</td><td>{{ $detail->id }}</td></tr>
                        <tr><td>Nama Pemohon</td>
                            <td>
                                @if (count($detail->user))
                                    <a href="{{ '/admin/users/'.$detail->user->id }}" target="_blank">{{ $detail->user->fullname }}</a>
                                @endif
                            </td>
                        </tr>
                        <tr><td>Developer</td>
                            <td>
                                @if (count($detail->developer))
                                    <a href="{{ '/admin/developer/detail/'.$detail->developer->id }}" target="_blank">{{ $detail->developer->nama }}</a>
                                @endif
                            </td>
                        </tr>
                        <tr><td>Property</td>
                            <td>
                                @if (count($detail->property))
                                    <a href="{{ '/admin/property/detail/'.$detail->property->id }}" target="_blank">{{ $detail->property->nama }}</a>
                                @endif
                            </td>
                        </tr>
                        <tr><td>Investor</td>
                            <td>
                                @if (count($detail->investor))
                                    <a href="{{ '/admin/investor/detail/'.$detail->investor->id }}" target="_blank">{{ $detail->investor->nama }}</a>
                                @endif
                            </td>
                        </tr>
                        <tr><td>Tujuan Kredit</td><td>{{ $detail->tujuan }}</td></tr>
                        <tr><td>Jenis Application</td><td>{{ $detail->jenis }}</td></tr>
                        <tr><td>Uang Muka</td><td>{{ uang($detail->uang_muka) }}</td></tr>
                        <tr><td>Sistem Pembayaran</td><td>{{ $detail->sis_bayar }}</td></tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table ">
                        <tr><td>Jumlah Pinjaman</td><td>{{ uang($detail->pinjaman) }}</td></tr>
                        <tr><td>Pinjaman bulanan yang diajukan</td><td>{{ uang($detail->pinjaman_bln) }}</td></tr>
                        <tr><td>Pinjaman (tahun)</td><td>{{ $detail->periode }} tahun</td></tr>
                        <tr><td>Pinjaman (bulan)</td><td>{{ $detail->jangka_waktu }} bulan</td></tr>
                        <tr><td>Lama Angsuran</td><td>{{ $total }} bulan</td></tr>
                        <tr><td>Status</td><td>{{ $detail->status }}</td></tr>
                        <tr><td>Outstanding Loan</td><td>{{ uang($detail->outstanding_loan) }}</td></tr>
                        <tr><td>Installment Times</td><td>{{ format($detail->installment_times) }}</td></tr>
                        <tr><td>Anggaran Renovasi</td><td>{{ uang($detail->renovasi) }}</td></tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
    <script src="{{ asset('assets/vendors/modal/js/classie.js') }}" ></script>
    <script src="{{ asset('assets/vendors/modal/js/modalEffects.js') }}" ></script>
@stop
