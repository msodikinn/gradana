
            <div class="col-lg-12">
                <h2>Profile Pemohon</h2><br>
                <ul class="nav nav-pills">
                    <li class="active">
                        <a href="#tab6" data-toggle="tab">Data Pemohon</a>
                    </li>
                    <li>
                        <a href="#tab7" data-toggle="tab">Pekerjaan Pemohon</a>
                    </li>
                    <li>
                        <a href="#tab8" data-toggle="tab">Data Pasangan</a>
                    </li>
                    <li>
                        <a href="#tab9" data-toggle="tab">Pekerjaan Pasangan</a>
                    </li>
                    <li>
                        <a href="#tab10" data-toggle="tab">Keluarga</a>
                    </li>
                    <li>
                        <a href="#tab11" data-toggle="tab">Penghasilan</a>
                    </li>
                    <li>
                        <a href="#tab12" data-toggle="tab">Perbankan</a>
                    </li>
                </ul>
                <div  class="tab-content mar-top">
                    <div id="tab6" class="tab-pane fade active in">
                        @include('admin.users.partials._pemohon')
                    </div>
                    <div id="tab7" class="tab-pane fade">
                        @include('admin.users.partials._jobPemohon')
                    </div>
                    <div id="tab8" class="tab-pane fade">
                        @include('admin.users.partials._suamiIstri')
                    </div>
                    <div id="tab9" class="tab-pane fade">
                        @include('admin.users.partials._jobSuamiIstri')
                    </div>
                    <div id="tab10" class="tab-pane fade">
                        @include('admin.users.partials._keluarga')
                    </div>
                    <div id="tab11" class="tab-pane fade">
                        @include('admin.users.partials._penghasilan')
                    </div>
                    <div id="tab12" class="tab-pane fade">
                        @include('admin.users.partials._perbankan')
                    </div>
                </div>
            </div>
&nbsp;
