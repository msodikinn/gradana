                    <div class="col-md-12">
                        <table class="table ">
                            <tr><td width="200">ID</td><td style="width: 40%;">{{ $detailPerbankan->id }}</td><td></td><td></td><td> <button value="Approve" class="btn btn-sm btn-primary"> Benar </button> </td> <td> <button value="Approve" class="btn btn-sm btn-primary">Ragu-Ragu</button> </td> <td> <button value="Approve" class="btn btn-sm btn-primary">Salah</button></td></tr>
                            <tr><td>Nama Pemohon</td><td>{{ $detailPerbankan->user->fullname }}</td><td></td><td></td> <td> <button value="Approve" class="btn btn-sm btn-primary"> Benar </button> </td> <td> <button value="Approve" class="btn btn-sm btn-primary">Ragu-Ragu</button> </td> <td> <button value="Approve" class="btn btn-sm btn-primary">Salah</button></td> </tr>
                            <tr><td>Sumber Informasi</td><td>{{ $detailPerbankan->sumber_info }}</td><td></td><td></td> <td> <button value="Approve" class="btn btn-sm btn-primary"> Benar </button> </td> <td> <button value="Approve" class="btn btn-sm btn-primary">Ragu-Ragu</button> </td> <td> <button value="Approve" class="btn btn-sm btn-primary">Salah</button></td></tr>
                            <tr>
                                <td>Rekening Koran 1</td>
                                <td>
                                    <a class="fancybox-effects-a" href="{!! $detailPerbankan->rekening_koran == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detailPerbankan->rekening_koran !!}">
                                        {!! $detailPerbankan->rekening_koran == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detailPerbankan->rekening_koran.'" class="img-responsive" style="max-height: 436px">' !!}
                                    </a>
                                </td>
                                <td style="width: 10%;">Rekening Koran 2</td>
                                <td style="width: 40%;">
                                    <a class="fancybox-effects-a" href="{!! $detailPerbankan->rekening_koran2 == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detailPerbankan->rekening_koran2 !!}">
                                        {!! $detailPerbankan->rekening_koran2 == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detailPerbankan->rekening_koran2.'" class="img-responsive" style="max-height: 436px">' !!}
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Nama Bank</th>
                                    <th>Tahun</th>
                                    <th>Bulan</th>
                                    <th>Jenis</th>
                                    <th>Rekening Kartu</th>
                                    <th>Saldo Limit Plafon</th>
                                </tr>
                            </thead>
                            <tbody>
                                <linkid></linkid>
                                @foreach ($detailPerbankanChild as $c)
                                    <tr role="row">
                                        <td>{{ $c->nama_bank }}</a></td>
                                        <td>{{ $c->nasabah_tahun }}</td>
                                        <td>{{ $c->nasabah_bulan }}</td>
                                        <td>{{ $c->jenis }}</td>
                                        <td>{{ $c->rekening_kartu }}</td>
                                        <td>{{ $c->saldo_limit_plafon }}</td>
                                    </tr>
                                @endforeach
                                {{-- <template v-for="child in childs">
                                    <tr role="row">
                                        <td>@{{ child.id }}</td>
                                        <td>@{{ child.nasabah_tahun }}</td>
                                        <td>@{{ child.nasabah_bulan }}</td>
                                        <td>@{{ child.jenis }}</td>
                                        <td>@{{ child.nama_bank }}</a></td>
                                        <td>@{{ child.rekening_kartu }}</td>
                                        <td>@{{ child.saldo_limit_plafon }}</td>
                                        <td>
                                            <button type="button" class="btn btn-link btn-sm" data-toggle="modal" data-target="#edit" @click="showChild(child.id)">Edit</button>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-link btn-sm" @click="delChild(child.id)">Delete</button>
                                        </td>
                                    </tr>
                                </template> --}}
                            </tbody>
                        </table>
