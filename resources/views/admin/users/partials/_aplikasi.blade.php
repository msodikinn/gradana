            <div class="row">
                <div class="col-md-12">
                    <h2>Data Application</h2><br>
                    <table class="table table-striped table-bordered" id="table3">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tujuan</th>
                                <th>Sistem Pembayaran</th>
                                <th>Pinjaman</th>
                                <th>Angsuran</th>
                                <th>Lama Angsuran</th>
                                <th>Installment Times</th>
                                <th>Status</th>
                                <th style="width: 100px">Approved</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=0; ?>
                            @foreach ($detailApplication as $a)
                                <tr role="row" class="odd">
                                    <td><a href="{{ $a->detailUrl() }}">{{ $a->id }}</a></td>
                                    <td>{{ $a->tujuan }}</td>
                                    <td>{{ $a->sis_bayar }}</td>
                                    <td>{{ $a->pinjaman }}</td>
                                    <td>{{ $a->pinjaman_bln }}</td>
                                    <td>{{ $total[$i] }} bulan</td>
                                    <td>{{ format($a->installment_times) }}</td>
                                    <td>{{ $a->status }}</td>
                                    @if($a->approved == 0)
                                        @if(Sentinel::getuser()->status == "Admin")
                                          <td><a href="{{ $a->approved() }}" class="btn btn-sm btn-primary">Approve</a></td>
                                        @else
                                          <td><i class="fa fa-fw fa-times"></i> Unapproved</td>
                                        @endif
                                    @else
                                        <td><i class="fa fa-fw fa-check"></i> Approved</td>
                                    @endif
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
