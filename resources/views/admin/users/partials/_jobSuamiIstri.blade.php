                <div class="col-md-6">
                    <table class="table ">
                        <tr><td>Id</td><td>{{ $detailJobPasangan->id }}</td></tr>
                        <tr><td>Userid</td><td>{{ $detailJobPasangan->user->fullname }}</td></tr>
                        <tr><td>Jenis</td><td>{{ $detailJobPasangan->jenis_pekerjaan }}</td></tr>
                        <tr><td>Nama Perusahaan</td><td>{{ $detailJobPasangan->nama_perusahaan }}</td></tr>
                        {{-- <tr><td>Bidang Usaha</td><td>{{ $detailJobPasangan->bidang_usaha }}</td></tr> --}}
                        <tr><td>Bidang Usaha Detail</td><td>{{ $detailJobPasangan->bidang_usaha_detail }}</td></tr>
                        <tr><td>Tanggal Pendirian</td><td>{{ format($detailJobPasangan->tanggal_pendirian) }}</td></tr>
                        <tr><td>Alamat</td><td>{{ $detailJobPasangan->alamat }}</td></tr>
                        <tr><td>Propinsi</td><td>{{ $propinsi_jobpasangan?$propinsi_jobpasangan->name:'-' }}</td></tr>
                        <tr><td>Kabupaten / Kota</td><td>{{ $kota_jobpasangan?$kota_jobpasangan->name:'-' }}</td></tr>
                        <tr><td>Kecamatan</td><td>{{ $kec_jobpasangan?$kec_jobpasangan->name:'-' }}</td></tr>
                        <tr><td>Kelurahan / Desa</td><td>{{ $kel_jobpasangan?$kel_jobpasangan->name:'-' }}</td></tr>
                        <tr><td>Postal</td><td>{{ $detailJobPasangan->postal }}</td></tr>
                        <tr><td>Tlp Ktr Hunting</td><td>{{ $detailJobPasangan->tlp_ktr_hunting }}</td></tr>
                        <tr><td>Tlp Ktr Direct</td><td>{{ $detailJobPasangan->tlp_ktr_direct }}</td></tr>
                        {{-- <tr><td>Fax</td><td>{{ $detailJobPasangan->fax }}</td></tr> --}}
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table ">
                        <tr><td>Unit Kerja</td><td>{{ $detailJobPasangan->unit_kerja }}</td></tr>
                        <tr><td>Jabatan</td><td>{{ $detailJobPasangan->jabatan }}</td></tr>
                        <tr><td>Bulan</td><td>{{ $detailJobPasangan->bulan }}</td></tr>
                        <tr><td>Tahun</td><td>{{ $detailJobPasangan->tahun }}</td></tr>
                        <tr><td>Omset Bln</td><td>{{ $detailJobPasangan->omset_bln }}</td></tr>
                        <tr><td>Kepemilikan</td><td>{{ $detailJobPasangan->kepemilikan }}</td></tr>
                        <tr><td>Margin Untung</td><td>{{ $detailJobPasangan->margin_untung }}</td></tr>
                        <tr><td>Nama Perusahaan Lama</td><td>{{ $detailJobPasangan->nama_perusahaan_ago }}</td></tr>
                        <tr><td>Bidang Usaha</td><td>{{ $detailJobPasangan->jenis_usaha_ago }}</td></tr>
                        <tr><td>Jabatan</td><td>{{ $detailJobPasangan->jabatan_ago }}</td></tr>
                        <tr><td>Unit Kerja</td><td>{{ $detailJobPasangan->unit_kerja_ago }}</td></tr>
                        <tr><td>Telp</td><td>{{ $detailJobPasangan->telp }}</td></tr>
                        <tr><td>Lama Bekerja (Bulan)</td><td>{{ $detailJobPasangan->bulan_ago }}</td></tr>
                        <tr><td>Lama Bekerja (Tahun)</td><td>{{ $detailJobPasangan->tahun_ago }}</td></tr>
                    </table>
                </div>
