                      <div class="row">
                            <div class="col-md-12">
                                <h2>Data Payment</h2><br>
                                @if (Sentinel::getuser()->status != "Investor")
                                  <p>Pembayaran Terlambat: {{ badPay($detailPayment) }}</p>
                                  <br>
                                @endif

                                <table class="table table-striped table-bordered" id="table2">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Application ID</th>
                                            <th>Status Pembayaran</th>
                                            <th>Plan</th>
                                            <th>Angsuran</th>
                                            <th>Dibayar</th>
                                            <th>Tanggal Pembayaran</th>
                                            <th>Status</th>
                                            <th>Approved</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($detailPayment as $b)
                                            <tr role="row" class="odd">
                                                <td><a href="{{ $b->detailUrl() }}">{{ $b->id }}</a></td>
                                                <td><a href="{{ $b->appUrl() }}">{{ $b->application->id }}</a></td>
                                                <td>{{ $b->status_pembayaran }}</td>
                                                <td>{{ $b->plan }}</td>
                                                <td>{{ $b->application->pinjaman_bln }}</td>
                                                <td>{{ $b->amount }}</td>
                                                <td>{{ format($b->tgl_pembayaran) }}</td>
                                                <td>{{ statusPay(tanggal($b->tgl_pembayaran)) }}</td>
                                                @if($b->approved == 0)
                                                    @if(Sentinel::getuser()->status == "Admin")
                                                      <td><a href="{{ $b->approved() }}" class="btn btn-sm btn-primary">Approve</a></td>
                                                    @else
                                                      <td><i class="fa fa-fw fa-times"></i> Unapproved</td>
                                                    @endif
                                                @else
                                                    <td><i class="fa fa-fw fa-check"></i> Approved</td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
