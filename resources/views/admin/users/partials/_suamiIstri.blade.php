                <div class="col-md-12">
                    <table class="table ">
                        <tr><td style="width: 200px;">ID</td><td>{{ $detailPasangan->id }}</td></tr>
                        <tr><td>Nama Pemohon</td><td>{{ $detailPasangan->user->fullname }}</td></tr>
                        <tr><td>Nama Pasangan</td><td>{{ $detailPasangan->nama }}</td></tr>
                        <tr><td>Status</td><td>{{ $detailPasangan->status }}</td></tr>
                        <tr><td>Tempat Lahir</td><td>{{ $detailPasangan->tempat_lahir }}</td></tr>
                        <tr><td>Tanggal Lahir</td><td>{{ format($detailPasangan->tgl_lahir) }}</td></tr>
                        <tr><td>KTP</td><td>{{ $detailPasangan->ktp }}</td></tr>
                        <tr><td>NPWP</td><td>{{ $detailPasangan->npwp }}</td></tr>
                        <tr><td>Telepon</td><td>{{ $detailPasangan->telp }}</td></tr>
                        <tr><td>Handphone</td><td>{{ $detailPasangan->hp }}</td></tr>
                    </table>
                </div>
