@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Detail Credit Assesment
@parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link href="{{ asset('assets/vendors/modal/css/component.css') }}" rel="stylesheet" />
@stop

@section('content')
<section class="content-header">
    <h1>Detail Credit Assesment</h1>
</section>

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <div class="panel-title pull-left">
                    <div class="caption">
                        <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Detail {{ $detail->nama }}
                    </div>
                </div>
                <div class="pull-right">
                    <a href="{{ $klik }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-ok"></span> Update Credit Score</a>
                    <a href="/admin/assesment" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Credit Assesment</a>
                    <a href="{{ $detail->editUrl() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-minus"></span> Edit Credit Assesment</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                            <th>Applicant</th>
                            <th>App ID</th>
                            <th>Scoring ID</th>
                            <th>Net Plafon</th>
                            <th>Take Home Pay</th>
                            <th>Interest Annum</th>
                            <th>Pengajuan Pinjaman</th>
                            <th>Periode (tahun)</th>
                            <th>Jangka Waktu (total bulan)</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="{{ '/admin/users/'.$user->id }}" target="_blank">{{ $user->fullname }}</a></td>
                                <td><a href="{{ '/admin/application/detail/'.$detail->application->id }}" target="_blank">{{ $detail->application->id }}</a></td>
                                <td><a href="{{ '/admin/scoring/detail/'.$detail->application->scoring->id }}" target="_blank">{{ $detail->application->scoring->id }}</a></td>
                                <td>{{ uang($netPlafon) }}</td>
                                <td>{{ uang($detail->application->user->penghasilan->first()->pemohon) }}</td>
                                <td>{{ $detail->interest_annum_min }} - {{ $detail->interest_annum_max }}%</td>
                                <td>{{uang($detail->application->pinjaman) }}</td>
                                <td>{{ $detail->application->periode }}</td>
                                <td>{{ ($detail->application->periode * 12) + $detail->application->jangka_waktu }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <table class="table ">
                      <thead>
                        <th>Name</th>
                        <th>Data</th>
                      </thead>
                        <tr>
                            <td width="40%">Assesment ID</td>
                            <td>{{ $detail->id }}</td>
                        </tr>
                        <tr>
                            <td>Analisa</td>
                            <td>{{ $detail->analisa }}</td>
                        </tr>
                        <tr>
                            <td>Borrower Summary</td>
                            <td>{{ $detail->borrower_summary }}</td>
                        </tr>
                        <tr>
                            <td>Credit Rating</td>
                            <td>{{ $detail->credit_rating }}</td>
                        </tr>
                        <tr>
                            <td>Interest Annum Min</td>
                            <td>{{ $detail->interest_annum_min }}%</td>
                        </tr>
                        <tr>
                            <td>Interest Annum Max</td>
                            <td>{{ $detail->interest_annum_max }}%</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                          <tr>
                              <th width="33%">Actual Interest</th>
                              <th width="33%">Actual Installment per bulan</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                              <td>{{ $aInterest.'%' }}</td>
                              <td>IDR {{ uang($aInstallment) }}</td>
                          </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
    <script src="{{ asset('assets/vendors/modal/js/classie.js') }}" ></script>
    <script src="{{ asset('assets/vendors/modal/js/modalEffects.js') }}" ></script>
@stop
