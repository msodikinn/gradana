@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
  Edit Credit Assesment Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
        <section class="content-header">
            <h1>Edit Credit Assesment Form</h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading clearfix">
                            <div class="panel-title pull-left">
                              <div class="caption">
                                <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Edit Credit Assesment
                              </div>
                            </div>
                            <div class="pull-right">
                              <a href="/admin/assesment" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Credit Assesment</a>
                            </div>
                        </div>
                        <div class="panel-body border">
                          {!! Form::model($detail, ['method' => 'PUT', 'url' => $detail->editUrl(), 'class' => 'form-horizontal form-bordered']) !!}
                              <div class="form-group">
                                  <label class="col-md-4 control-label">ID</label>
                                  <div class="col-md-6">
                                      <input type="text" name="id" class="form-control" value="{!! old('id',$detail->id) !!}" readonly>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Application ID</label>
                                  <div class="col-md-6">
                                      <select id="appid" name="appid" class="form-control" style="width: 100%">
                                        @foreach($app as $a)
                                          <option value="{{ $a->id }}" @if($a->id === $detail->appid) selected @endif>
                                            {{$a->tujuan}}
                                          </option>
                                        @endforeach
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Analisa *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="analisa" class="form-control" value="{!! old('analisa',$detail->analisa) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Borrower Summary *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="borrower_summary" class="form-control" value="{!! old('borrower_summary',$detail->borrower_summary) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Credit Rating</label>
                                  <div class="col-md-6">
                                      <input type="text" name="credit_rating" class="form-control" value="{!! old('credit_rating',$detail->credit_rating) !!}" readonly>
                                      <p class="help-block"><a href="{{ $klik }}">klik disini</a> untuk update credit scoring</p>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Interest Annum Min (%) *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="interest_annum_min" class="form-control" value="{!! old('interest_annum_min',$detail->interest_annum_min) !!}" required>
                                      <p class="help-block">koma dengan . (titik)</p>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-4 control-label">Interest Annum Max (%) *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="interest_annum_max" class="form-control" value="{!! old('interest_annum_max',$detail->interest_annum_max) !!}" required>
                                      <p class="help-block">koma dengan . (titik)</p>
                                  </div>
                              </div>
                              <div class="form-group form-actions">
                                  <div class="col-md-9 col-md-offset-4">
                                      <input type="submit" class="btn btn-primary" value="Save Credit Assesment">
                                  </div>
                              </div>
                          {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#appid').select2()
            $('#credit_rating').select2()
        });
    </script>

@stop
