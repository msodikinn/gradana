@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Gradana Dashboard Page
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
	<link type="text/css" rel="stylesheet" href="{{ asset('assets/css/pages/flot.css') }}" />
	<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('assets/css/pages/only_dashboard.css') }}"/>
  <meta name="_token" content="{{ csrf_token() }}">
@stop

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Welcome to Gradana.com</h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="#">
                    <i class="livicon" data-name="home" data-size="16" data-color="#333" data-hovercolor="#333"></i>
                    Dashboard
                </a>
            </li>
        </ol>
    </section>
    <section class="content">
      <div class="row">
      	<div class="col-lg-12">
      		<div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="livicon" data-name="key" data-size="16" data-c="#fff" data-hc="#fff" data-loop="true"></i>
                    Grafik Type
                </h3>
                <span class="pull-right">
                    <i class="glyphicon glyphicon-chevron-up showhide clickable"></i>
                    <i class="glyphicon glyphicon-remove removepanel clickable"></i>
                </span>
            </div>
            <div class="panel-body" id="optionchart">
            	<form role="form">
              	<div class="row">
                  <div class="form-group">
                    <div class="col-md-2">
                      <div class="radio">
                        <label>
                          <input name="modechart" type="radio" @click="setbulanan" checked><div class="labelcheckbox">Bulanan</div>
                        </label>
                      </div>
                    </div>
                    <div class="col-md-4">
                    	{!! Form::select('bulanan', ['01' => 'Januari', '02' =>'Februari', '03' =>'Maret', '04' =>'April', '05' =>'Mei', '06' =>'Juni', '07' =>'Juli', '08' =>'Agustus', '09' =>'September', '10' =>'Oktober', '11' =>'November', '12' =>'Desember'], null, ['class' => 'form-control', 'id' => 'bulanan', 'v-bind:value' => 'monthselected', '@change' => 'changemonth']) !!}
  	                </div>
                  </div>
                </div>
                <div class="row">
                    <div class="form-group">
                      <div class="col-md-2">
                        <div class="radio">
                          <label>
                            <input  name="modechart"  type="radio" @click="settahunan">
                            <div class="labelcheckbox">Tahunan</div>
                          </label>
                        </div>
                      </div>
                      <div class="col-md-4">
                        {!! Form::select('tahun', $yearoptions, null, ['class' => 'form-control', 'id' => 'tahunan', 'v-bind:value' => 'yearselected', '@change' => 'changeyear']) !!}
                      </div>
                    </div>
                </div>
                <div class="row">
                  <div class="form-group">
                    <div class="col-md-2">{!! Form::Label('jenisreport', 'Jenis Report') !!}
                    </div>
                    <div class="col-md-4">
                      {!! Form::select('jenisreport', ['loan' => 'Loan',
                      'loanapproved' => 'Loan Approved',
                      'loanrejected' => 'Loan Rejected',
                      ], null, ['class' => 'form-control', 'id' => 'jenisreport', 'v-bind:value' => 'graphtype', '@change' => 'graphtypechange']) !!}
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
      	</div>
      </div>
      <div class="row">
          <div class="col-lg-12">
              <div class="panel panel-success">
                  <div class="panel-heading">
                      <h3 class="panel-title">
                          <i class="livicon" data-name="linechart" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                          Grafik
                      </h3>
                      <span class="pull-right">
                          <i class="glyphicon glyphicon-chevron-up showhide clickable"></i>
                          <i class="glyphicon glyphicon-remove removepanel clickable"></i>
                      </span>
                  </div>
                  <div class="panel-body">
                      <div id="basicFlotLegend" class="flotLegend"></div>
                      <div id="line-chart" class="flotChart1"></div>
                  </div>
              </div>
          </div>
      </div>
      <div class="row">
    		<div class="col-md-6">
    			<div class="portlet box info">
              <div class="portlet-title">
                  <div class="caption">
                      <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                      Top 5 Developer
                  </div>
              </div>
              <div class="portlet-body">
                  <div class="table-scrollable">
                      <table class="table table-striped table-hover">
                          <tbody>
                              <?php $i = 1;?>
                              @foreach($top5developers as $top5developer)
                              		<tr>
                                      <td><?php echo $i;$i++;?></td>
                                      <td><a href="{{ URL::to('admin/developer/detail/'.$top5developer->id) }}">{{ $top5developer->nama }}</a></td>
                                      <td align="right">{{ number_format($top5developer->countdata,0,",",".") }}</td>
                                  </tr>
                              @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
    		</div>
    		<div class="col-md-6">
    			<div class="portlet box info">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Top 5 Investors
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover">
                            <tbody>
                                <?php $i = 1;?>
                                @foreach($top5investors as $top5investor)
                                		<tr>
                                        <td><?php echo $i;$i++;?></td>
                                        <td><a href="{{ URL::to('admin/investor/detail/'.$top5investor->id) }}">{{ $top5investor->nama }}</a></td>
                                        <td align="right">{{ number_format($top5investor->countdata,0,",",".") }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    		</div>
      </div>
      <div class="row">
    		<div class="col-md-6">
    			<div class="portlet box info">
              <div class="portlet-title">
                  <div class="caption">
                      <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                      Top 5 Property
                  </div>
              </div>
              <div class="portlet-body">
                  <div class="table-scrollable">
                      <table class="table table-striped table-hover">
                          <tbody>
                            <?php $i = 1;?>
                            @foreach($top5properties as $top5property)
                            	<tr>
                                  <td><?php echo $i;$i++;?></td>
                                  <td><a href="{{ URL::to('admin/property/detail/'.$top5property->id) }}">{{ $top5property->nama }}</a></td>
                                  <td align="right">{{ number_format($top5property->countdata,0,",",".") }}</td>
                              </tr>
                            @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
    		</div>
    		<div class="col-md-6">
      			<div class="portlet box info">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Last Blog
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover">
                            <tbody>
                                <?php $i = 1;?>
                                @foreach($blogs as $blog)
            											<tr>
                                      <td><?php echo $i;$i++;?></td>
                                      <td width="30%">@if (!is_null($blog->image) && !empty($blog->image))
                                      <img src="{{ URL::to('/uploads/blog/'.$blog->image) }}" height="100px">
                                      @endif</td>
                                      <td><a href="{{ URL::to('admin/blog/'.$blog->id.'/edit') }}">{{ $blog->title }}</a>
                                      <br/>
                                      {{ $blog->updated_at->diffForHumans() }}
                                      </td>
                                  </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    		</div>
    	</div>
    	<div class="row">
  		<div class="col-md-6">
  			<div class="portlet box primary">
            <div class="portlet-title">
                <div class="caption">
                    <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Skor Transaksi
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-striped table-hover">
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Loan</td>
                                <td align="right">{{ $loancount }}</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Loan Approved</td>
                                <td align="right">{{ $loanapprovedcount }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
  		</div>
  		<div class="col-md-6">
  			<div class="portlet box success">
          <div class="portlet-title">
              <div class="caption">
                  <i class="livicon" data-name="info" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                  PESAN
              </div>
          </div>
          <div class="portlet-body">
              pesan pesan
          </div>
        </div>
  		</div>
      </div>
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
	<script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/flotchart/js/jquery.flot.js') }}" ></script>
	<script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/flotchart/js/jquery.flot.categories.js') }}" ></script>
  <script type="text/javascript">
    var holder = $('#line-chart');
    $(function () {
    	$("<div class='tooltip-inner' id='line-chart-tooltip'></div>").css({
    	    position: "absolute",
    	    display: "none",
    	    opacity: 0.8
    	}).appendTo("body");
    	holder.bind("plothover", function(event, pos, item) {
    	    if (item) {
    			$("#line-chart-tooltip").html(item.series.data[item.dataIndex][2])
    	        .css({top: item.pageY + 5, left: item.pageX + 5, 'font-weight':'bold', 'font-size' : '1.3em'})
    	        .fadeIn(200);
    	    } else {
    	        $("#line-chart-tooltip").hide();
    	    }

    	});
		var params = [];
    	var d = new Date();
    	var vm = new Vue({
    		el :'#optionchart',
    		data : {
				modechart: 'monthly',
				monthselected: ("0" + (d.getMonth() + 1)).slice(-2),
				yearselected: d.getFullYear(),
				graphtype:'loan',
    		},
    		methods: {
				settahunan:function(e) {
					this.modechart = 'yearly';
					this.refreshchart();
				},
				setbulanan:function(e) {
					this.modechart = 'monthly';
					this.refreshchart();
				},
				changemonth: function(e) {
					this.monthselected = e.target.value;
					this.refreshchart();
				},
				changeyear: function(e) {
					this.yearselected = e.target.value;
					this.refreshchart();
				},
				graphtypechange: function(e) {
					this.graphtype = e.target.value;
					this.refreshchart();
				},
				refreshchart: function() {
					this.$http.get('{{ URL::to('admin/dashboard/applicationgraph') }}', {
						modechart: this.modechart,
						monthselected: this.monthselected,
						yearselected: this.yearselected,
						graphtype: this.graphtype,
					}).then(function(response) {
						result = response.data;
						$.plot(holder, result.grafikdata, result.options);
					});
				}
    		}
    	});
    	vm.refreshchart();
    });
  </script>
@stop
