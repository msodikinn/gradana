@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Detail Pekerjaan Pasangan
@parent
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link href="{{ asset('assets/vendors/modal/css/component.css') }}" rel="stylesheet" />
@stop

@section('content')
<section class="content-header">
    <h1>Detail Pekerjaan Pasangan</h1>
</section>

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <div class="panel-title pull-left">
                    <div class="caption">
                        <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Detail {{ $detail->nama }}
                    </div>
                </div>
                <div class="pull-right">
                    <a href="/admin/jobsuamiistri" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Pekerjaan Pasangan</a>
                    <a href="{{ $detail->editUrl() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-minus"></span> Edit Pekerjaan Pasangan</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="col-md-6">
                    <table class="table ">
                        <tr><td>Id</td><td>{{ $detail->id }}</td></tr>
                        <tr><td>Userid</td><td>{{ $detail->user->fullname }}</td></tr>
                        <tr><td>Jenis</td><td>{{ $detail->jenis_pekerjaan }}</td></tr>
                        <tr><td>Nama Perusahaan</td><td>{{ $detail->nama_perusahaan }}</td></tr>
                        {{-- <tr><td>Bidang Usaha</td><td>{{ $detail->bidang_usaha }}</td></tr> --}}
                        <tr><td>Bidang Usaha Detail</td><td>{{ $detail->bidang_usaha_detail }}</td></tr>
                        <tr><td>Tanggal Pendirian</td><td>{{ format($detail->tanggal_pendirian) }}</td></tr>
                        <tr><td>Alamat</td><td>{{ $detail->alamat }}</td></tr>
                        <tr><td>Propinsi</td><td>{{ $detail->propinsis->name }}</td></tr>
                        <tr><td>Kelurahan</td><td>{{ $detail->desas->name }}</td></tr>
                        <tr><td>Kecamatan</td><td>{{ $detail->kecamatans->name }}</td></tr>
                        <tr><td>Kota</td><td>{{ $detail->kotas->name }}</td></tr>
                        <tr><td>Postal</td><td>{{ $detail->postal }}</td></tr>
                        <tr><td>Tlp Ktr Hunting</td><td>{{ $detail->tlp_ktr_hunting }}</td></tr>
                        <tr><td>Tlp Ktr Direct</td><td>{{ $detail->tlp_ktr_direct }}</td></tr>
                        {{-- <tr><td>Fax</td><td>{{ $detail->fax }}</td></tr> --}}
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table ">
                        <tr><td>Unit Kerja</td><td>{{ $detail->unit_kerja }}</td></tr>
                        <tr><td>Jabatan</td><td>{{ $detail->jabatan }}</td></tr>
                        <tr><td>Penghasilan Pasangan</td><td>{{ $detailPasangan->suami_istri }}</td></tr>
                        <tr><td>Bulan</td><td>{{ $detail->bulan }}</td></tr>
                        <tr><td>Tahun</td><td>{{ $detail->tahun }}</td></tr>
                        <tr><td>Omset Bln</td><td>{{ $detail->omset_bln }}</td></tr>
                        <tr><td>Kepemilikan</td><td>{{ $detail->kepemilikan }}</td></tr>
                        <tr><td>Margin Untung</td><td>{{ $detail->margin_untung }}</td></tr>
                        <tr><td>Nama Perusahaan Lama</td><td>{{ $detail->nama_perusahaan_ago }}</td></tr>
                        <tr><td>Bidang Usaha</td><td>{{ $detail->jenis_usaha_ago }}</td></tr>
                        <tr><td>Jabatan</td><td>{{ $detail->jabatan_ago }}</td></tr>
                        <tr><td>Unit Kerja</td><td>{{ $detail->unit_kerja_ago }}</td></tr>
                        <tr><td>Telp</td><td>{{ $detail->telp }}</td></tr>
                        <tr><td>Lama Bekerja (Bulan)</td><td>{{ $detail->bulan_ago }}</td></tr>
                        <tr><td>Lama Bekerja (Tahun)</td><td>{{ $detail->tahun_ago }}</td></tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
    <script src="{{ asset('assets/vendors/modal/js/classie.js') }}" ></script>
    <script src="{{ asset('assets/vendors/modal/js/modalEffects.js') }}" ></script>
@stop
