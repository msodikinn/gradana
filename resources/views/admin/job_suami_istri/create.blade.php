@extends('admin/layouts/default')

@section('title')
  Pekerjaan Pasangan Create Form
@parent
@stop

@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
    <section class="content-header">
        <h1>Pekerjaan Pasangan Form</h1>
    </section>
    <section class="content">
    <div class="row">
		<div class="col-lg-12">
			<ul class="nav nav-tabs ">
				<li>
					<a href="{{ URL::to('admin/pemohon/edit/'.$pemohonid) }}">Pemohon</a>
				</li>
				<li>
					<a href="{{ URL::to('admin/jobpemohon/edit/'.$jobpemohonid) }}">Pekerjaan Pemohon</a>
				</li>
				<li>
					<a href="{{ URL::to('admin/suamiistri/edit/'.$suamisitriid) }}">Pasangan</a>
				</li>
				<li class="active">
					<a href="">Pekerjaan Pasangan</a>
				</li>
			</ul>
		</div>
	</div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary filterable">
                    <div class="panel-heading clearfix">
                        <div class="panel-title pull-left">
                          <div class="caption">
                            <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Create Pekerjaan Pasangan
                          </div>
                        </div>
                        {{-- <div class="pull-right">
                          <a href="/admin/jobsuamiistri" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Pekerjaan Pasangan</a>
                        </div> --}}
                    </div>
                    <div class="panel-body border">
                        {!! Form::open(array('url' => route('jobSuamiIstriCreate'), 'method' => 'post', 'class' => 'form-horizontal form-bordered', 'files' => 'true')) !!}
                            {!! Form::hidden('pemohonid', $pemohonid) !!}
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Nama Pemohon</label>
                                <div class="col-md-6">
                                    <input type="hidden" name="userid" value="{{ $users->id }}">
                                    <input type="text" name="pem" value="{{ $users->fullname }}" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Jenis Pekerjaan</label>
                                <div class="col-md-6">
                                    <select id="jenis_pekerjaan" name="jenis_pekerjaan" class="form-control" style="width: 100%">
                                      @foreach($jenis as $code=>$j)
                                        <option value="{{ $code }}">
                                          {{$j}}
                                        </option>
                                      @endforeach
                                    </select>
                                    <div id="holder"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nama Perusahaan *</label>
                                <div class="col-md-6">
                                    <input type="text" name="nama_perusahaan" class="form-control" required>
                                </div>
                            </div>
                            {{-- <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Bidang Usaha</label>
                                <div class="col-md-6">
                                    <select id="bidang_usaha" name="bidang_usaha" class="form-control" style="width: 100%">
                                      @foreach($usaha as $code=>$j)
                                        <option value="{{ $code }}">
                                          {{$j}}
                                        </option>
                                      @endforeach
                                    </select>
                                </div>
                            </div> --}}
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Bidang Usaha Detail *</label>
                                <div class="col-md-6">
                                    <select id="bidang_usaha_detail" name="bidang_usaha_detail" class="form-control" style="width: 100%">
                                      @foreach($usaha as $code=>$j)
                                        <option value="{{ $code }}">
                                          {{$j}}
                                        </option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Tanggal Pendirian *</label>
                                <div class="col-md-3">
                                    <input type="date" name="tanggal_pendirian" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Alamat Kantor *</label>
                                <div class="col-md-6">
                                    <input type="text" name="alamat" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label for="propinsi" class="col-md-4 control-label">Propinsi</label>
                                <div class="col-md-6">
                                    {{ Form::select('propinsi', $propinsi, null, array('id' => 'sPropinsi', 'style'=>'width: 100%'))  }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="kota" class="col-md-4 control-label">Kabupaten / Kota</label>
                                <div class="col-md-6">
                                    {{ Form::select('kota', array(), null, array('id' => 'sKota', 'style'=>'width: 100%')) }}
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label for="kecamatan" class="col-md-4 control-label">Kecamatan</label>
                                <div class="col-md-6">
                                    {{ Form::select('kecamatan', array(), null, array('id' => 'sKecamatan', 'style'=>'width: 100%')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="kelurahan" class="col-md-4 control-label">Desa / Kelurahan</label>
                                <div class="col-md-6">
                                    {{ Form::select('kelurahan', array(), null, array('id' => 'sDesa', 'style'=>'width: 100%')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Postal</label>
                                <div class="col-md-6">
                                    <input type="text" name="postal" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tlp Kantor *</label>
                                <div class="col-md-6">
                                    <input type="text" name="tlp_ktr_direct" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Extension</label>
                                <div class="col-md-6">
                                    <input type="text" name="tlp_ktr_hunting" class="form-control">
                                </div>
                            </div>
                            {{-- <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Fax</label>
                                <div class="col-md-6">
                                    <input type="text" name="fax" class="form-control">
                                </div>
                            </div> --}}
                            <div class="form-group">
                                <label class="col-md-4 control-label">Unit Kerja</label>
                                <div class="col-md-6">
                                    <input type="text" name="unit_kerja" class="form-control">
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Jabatan *</label>
                                <div class="col-md-6">
                                    <input type="text" name="jabatan" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Penghasilan Pasangan *</label>
                                <div class="col-md-6">
                                    <input id="autonum2" type="text" name="suami_istri" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Bekerja sejak Bulan *</label>
                                <div class="col-md-6">
                                    {{ Form::selectMonth('month', 7, ['class' => 'form-control', 'name' => 'bulan', 'id' => 'bulan']) }}
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Bekerja sejak Tahun *</label>
                                <div class="col-md-6">
                                    <select id="tahun" name="tahun" class="form-control" style="width: 100%">
                                        @for ($awal = 1930; $awal <= date('Y'); $awal++)
                                          <option value="{{ $awal }}">{{ $awal }}</option>}
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Surat Keterangan Kerja Image *</label>
                                <div class="col-md-6">
                                    <input type="file" name="surat_ket_kerja" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                              <h4 class="col-md-4  control-label" style="margin-bottom: 20px;">Untuk wiraswasta/profesi</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Omset Kotor per Bulan</label>
                                <div class="col-md-6">
                                    <input type="text" name="omset_bln" id="autonum1" class="form-control">
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Nama Perusahaan Ago</label>
                                <div class="col-md-6">
                                    <input type="text" name="nama_perusahaan_ago" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Jenis Usaha Ago</label>
                                <div class="col-md-6">
                                    <input type="text" name="jenis_usaha_ago" class="form-control">
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Jabatan Ago</label>
                                <div class="col-md-6">
                                    <input type="text" name="jabatan_ago" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Unit Kerja Ago</label>
                                <div class="col-md-6">
                                    <input type="text" name="unit_kerja_ago" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Telepon</label>
                                <div class="col-md-6">
                                    <input type="text" name="telp" class="form-control">
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Bulan Ago</label>
                                <div class="col-md-6">
                                    <input type="text" name="bulan_ago" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tahun Ago</label>
                                <div class="col-md-6">
                                    <input type="text" name="tahun_ago" class="form-control">
                                </div>
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-9 col-md-offset-4">
                                    <input type="submit" class="btn btn-primary" value="Step 5: Create Emergency Contact">
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script>
      $(document).ready(function() {
          $('#jenis_pekerjaan').select2()
          $('#userid').select2()
          $('#bidang_usaha_detail').select2()
          $('#tahun').select2()
          $('#bulan').select2()
          $('#sPropinsi').select2()
          $('#sDesa').select2()
          $('#sKecamatan').select2()
          $('#sKota').select2()
      });
    </script>
    <script>
      $("#jenis_pekerjaan").change(function(){
        val = $(this).val();

        if(val == "Lainnya"){
          $("#holder input").remove();
          var ctrl = '<input type="text" name="nama_pekerjaan_lain" class="form-control" style="margin-top:20px" placeholder="Masukkan jenis pekerjaan lainnya" required>';
          $("#holder").append(ctrl);
        }else if (val == "Wiraswasta"){
          $("#holder input").remove();
          var ctrl = '<input type="date" name="tanggal_pendirian" class="form-control" style="margin-top:20px" placeholder="Tanggal Pendirian">';
          $("#holder").append(ctrl);
        }else{
          $("#holder input").remove();
        }
      });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#sPropinsi').on('change', function(){
                $.post('{{ URL::to('admin/site/data') }}',
                {
                    '_token': '{{ csrf_token() }}',
                    type: 'kota',
                    id: $('#sPropinsi').val(),
                }, function(e){
                    $('#sKota').html(e);
                });
                $('#sKecamatan').html('');
                $('#sDesa').html('');

            });
            $('#sKota').on('change', function(){
                $.post('{{ URL::to('admin/site/data') }}',
                {
                    '_token': '{{ csrf_token() }}',
                    type: 'kecamatan',
                    id: $('#sKota').val()
                }, function(e){
                    $('#sKecamatan').html(e);
                });
                $('#sDesa').html('');
            });
            $('#sKecamatan').on('change', function(){
                $.post('{{ URL::to('admin/site/data') }}',
                {
                    '_token': '{{ csrf_token() }}',
                    type: 'kelurahan',
                    id: $('#sKecamatan').val()
                }, function(e){
                    $('#sDesa').html(e);
                });
            });
        });
    </script>
@stop
