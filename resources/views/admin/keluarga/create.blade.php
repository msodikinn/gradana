@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
  Emergency Contact Create Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
        <section class="content-header">
            <h1>Emergency Contact Form</h1>
        </section>
        <section class="content">
        <div class="row">
      		<div class="col-lg-12">
      			<ul class="nav nav-tabs ">
      				<li>
      					<a href="{{ URL::to('admin/pemohon/edit/'.$pemohonid) }}">Pemohon</a>
      				</li>
      				<li>
      					<a href="{{ URL::to('admin/jobpemohon/edit/'.$jobpemohonid) }}">Pekerjaan Pemohon</a>
      				</li>
              @if (App\Pemohon::find($pemohonid)->status_kawin != "Lajang")
                <li>
                  <a href="{{ URL::to('admin/suamiistri/edit/'.$suamisitriid) }}">Pasangan</a>
                </li>
                <li>
                  <a href="{{ URL::to('admin/jobsuamiistri/edit/'.$jobsuamiistriid) }}">Pekerjaan Pasangan</a>
                </li>
              @endif
      				<li class="active">
      					<a href="">Emergency Contact Form</a>
      				</li>
      			</ul>
      		</div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary filterable">
                    <div class="panel-heading clearfix">
                        <div class="panel-title pull-left">
                          <div class="caption">
                            <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Create Emergency Contact
                          </div>
                        </div>
                    </div>
                    <div class="panel-body border">
                        {!! Form::open(array('url' => route('keluargaCreate'), 'method' => 'post', 'class' => 'form-horizontal form-bordered')) !!}
                        {!! Form::hidden('pemohonid', $pemohonid) !!}
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Nama Pemohon</label>
                                <div class="col-md-6">
                                    <input type="hidden" name="userid" value="{{ $users->id }}">
                                    <input type="text" name="pem" value="{{ $users->fullname }}" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nama Contact *</label>
                                <div class="col-md-6">
                                    <input type="text" name="nama" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Hubungan *</label>
                                <div class="col-md-6">
                                    <select id="hub" name="hub" class="form-control" style="width: 100%">
                                      @foreach($hub as $u)
                                        <option value="{{ $u }}">
                                          {{$u}}
                                        </option>
                                      @endforeach
                                    </select>
                                    <div id="holder"></div>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Alamat *</label>
                                <div class="col-md-6">
                                    <input type="text" name="alamat" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">No Telepon *</label>
                                <div class="col-md-6">
                                    <input type="text" name="telp" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">No Handphone</label>
                                <div class="col-md-6">
                                    <input type="text" name="hp" class="form-control">
                                </div>
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-9 col-md-offset-4">
                                    <input type="submit" class="btn btn-primary" value="Step 6: Create Data Perbankan">
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script>
      $(document).ready(function() {
          $('#userid').select2()
          $('#hub').select2()
      });
    </script>
    <script>
      $("#hub").change(function(){
        val = $(this).val();

        if(val == "Lainnya"){
          var ctrl = '<input type="text" name="hub_lain" class="form-control" style="margin-top:20px" placeholder="Masukkan hubungan lain" required>';
          $("#holder").append(ctrl);
        }else{
          $("#holder input").remove();
        }
      });
    </script>
@stop
