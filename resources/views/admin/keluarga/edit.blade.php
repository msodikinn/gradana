@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
  Emergency Contact Create Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
        <section class="content-header">
            <h1>Emergency Contact Form</h1>
        </section>
        <section class="content">
        	<div class="row">
				<div class="col-lg-12">
					<ul class="nav nav-tabs ">
						@if (!empty($pemohonid))
  						<li>
  							<a href="{{ URL::to('admin/pemohon/edit/'.$pemohonid) }}">Pemohon</a>
  						</li>
						@endif
						@if (!empty($jobpemohonid))
  						<li>
  							<a href="{{ URL::to('admin/jobpemohon/edit/'.$jobpemohonid) }}">Pekerjaan Pemohon</a>
  						</li>
						@endif
						@if (!empty($suamisitriid))
  						<li>
  							<a href="{{ URL::to('admin/suamiistri/edit/'.$suamisitriid) }}">Pasangan</a>
  						</li>
						@endif
						@if (!empty($jobsuamiistriid))
  						<li>
  							<a href="{{ URL::to('admin/jobsuamiistri/edit/'.$jobsuamiistriid) }}">Pekerjaan Pasangan</a>
  						</li>
						@endif
						<li class="active">
							<a href="">Emergency Contact</a>
						</li>
						{{-- @if (empty($penghasilanid))
  						<li>
  							<a href="{{ URL::to('admin/penghasilan/create?backid='.$backid) }}">Create Penghasilan</a>
  						</li>
						@else
  						<li>
  							<a href="{{ URL::to('admin/penghasilan/edit/'.$penghasilanid) }}">Penghasilan</a>
  						</li>
						@endif --}}
						@if (!empty($perbankanid))
  						<li>
  							<a href="{{ URL::to('admin/perbankan/edit/'.$perbankanid) }}">Perbankan</a>
  						</li>
						@endif
            @if ($countApp == 0)
              <li>
                <a href="{{ URL::to('admin/application/create?backid='.$backid) }}">Create Application</a>
              </li>
            @elseif($countApp == 1)
              <li>
                <a href="{{ URL::to('admin/application/edit/'.$applicationid) }}">Application</a>
              </li>
            @elseif($countApp > 1)
              <li>
                <a href="{{ URL::to('admin/application?iduser='.$detail->userid) }}">{{ $countApp }} Application</a>
              </li>
            @endif
					</ul>
				</div>
			</div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading clearfix">
                            <div class="panel-title pull-left">
                              <div class="caption">
                                <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Edit Emergency Contact
                              </div>
                            </div>
                            <div class="pull-right">
                              <a href="/admin/keluarga" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Emergency Contact</a>
                            </div>
                        </div>
                        <div class="panel-body border">
                          {!! Form::model($detail, ['method' => 'PUT', 'url' => $detail->editUrl(), 'class' => 'form-horizontal form-bordered', 'files'=> true]) !!}
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Nama Pemohon</label>
                                  <div class="col-md-6">
                                      <input type="text" value="{{ $detail->user->fullname }}" class="form-control" readonly>
                                      <input type="hidden" name="userid" value="{{ $detail->user->id }}">
                                  </div>
                              </div>
                              <div class="form-group ">
                                  <label class="col-md-4 control-label">Nama Contact *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="nama" class="form-control" value="{!! old('nama',$detail->nama) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group ">
                                  <label class="col-md-4 control-label">Hubungan *</label>
                                  <div class="col-md-6">
                                      <select id="hub" name="hub" class="form-control" style="width: 100%">
                                        @foreach($hub as $u)
                                          <option value="{{ $u }}" @if($u === $detail->hub) selected @endif>
                                            {{$u}}
                                          </option>
                                        @endforeach
                                      </select>
                                      <div id="holder"></div>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">Alamat *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="alamat" class="form-control" value="{!! old('alamat',$detail->alamat) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group ">
                                  <label class="col-md-4 control-label">No Telepon *</label>
                                  <div class="col-md-6">
                                      <input type="text" name="telp" class="form-control" value="{!! old('telp',$detail->telp) !!}" required>
                                  </div>
                              </div>
                              <div class="form-group striped-col">
                                  <label class="col-md-4 control-label">No Handphone</label>
                                  <div class="col-md-6">
                                      <input type="text" name="hp" class="form-control" value="{!! old('hp',$detail->hp) !!}">
                                  </div>
                              </div>
                              <div class="form-group form-actions">
                                  <div class="col-md-9 col-md-offset-4">
                                      <input type="submit" class="btn btn-primary" value="Save Emergency Contact">
                                  </div>
                              </div>
                          {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script>
      $(document).ready(function() {
          $('#userid').select2()
          $('#hub').select2()
      });
    </script>
    <script>
      $("#hub").change(function(){
        val = $(this).val();

        if(val == "Lainnya"){
          var ctrl = '<input type="text" name="hub_lain" class="form-control" style="margin-top:20px" placeholder="Masukkan hubungan lain" required>';
          $("#holder").append(ctrl);
        }else{
          $("#holder input").remove();
        }
      });
    </script>
@stop
