                <div class="modal fade in" id="responsive" tabindex="-1" role="dialog" aria-hidden="false" style="display:none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Data Application</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-striped table-bordered" id="table2">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Tujuan</th>
                                                    <th>Sistem Pembayaran</th>
                                                    <th>Pinjaman</th>
                                                    <th>Angsuran</th>
                                                    <th>Lama Angsuran</th>
                                                    <th>Status</th>
                                                    <th style="width: 100px">Approved</th>
                                                    <th>Payment </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i=0; ?>
                                                @foreach ($apl as $a)
                                                    <tr role="row" class="odd">
                                                        <td><a href="{{ $a->detailUrl() }}">{{ $a->id }}</a></td>
                                                        <td>{{ $a->tujuan }}</td>
                                                        <td>{{ $a->sis_bayar }}</td>
                                                        <td>{{ uang($a->pinjaman) }}</td>
                                                        <td>{{ uang($a->pinjaman_bln) }}</td>
                                                        <td>{{ $total[$i] }} bulan</td>
                                                        <td>{{ $a->status }}</td>
                                                        @if($a->approved == 0)
                                                            @if(Sentinel::getuser()->status == "Admin")
                                                              <td><a href="{{ $a->approved() }}" class="btn btn-sm btn-primary">Approve</a></td>
                                                            @else
                                                              <td><i class="fa fa-fw fa-times"></i> Unapproved</td>
                                                            @endif
                                                            <td></td>
                                                        @else
                                                            <td><i class="fa fa-fw fa-check"></i> Approved</td>
                                                            <td><a href="{{ $a->payUrl() }}" class="btn btn-primary">Bayar</a></td>
                                                        @endif
                                                    </tr>
                                                    <?php $i++; ?>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <p align="left">* Pilih aplikasi yang akan dibayar</p>
                            </div>
                        </div>
                    </div>
                </div>
