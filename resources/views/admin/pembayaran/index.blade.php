@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Pembayaran Management
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/modal/css/component.css') }}"/>

@stop

{{-- Page content --}}
@section('content')

        <section class="content-header">
                <!--section starts-->
                <h1>Manage Pembayaran</h1>

            </section>
            <section class="content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary filterable">
                            <div class="panel-heading clearfix">
                              <div class="panel-title pull-left">
                                <div class="caption">
                                  <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> List Pembayaran
                                </div>
                              </div>
                              <div class="pull-right">
                                <a href="#responsive" data-toggle="modal" data-href="#responsive" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> Pay Installment</a>
                              </div>
                            </div>
                            <div class="panel-body table-responsive">
                                <h4>Pembayaran yang telah dilakukan oleh: <a href="{{ '/admin/users/'.Sentinel::getuser()->id }}">{{ Sentinel::getuser()->fullname }}</a></h4><br>
                                <table class="table table-striped table-bordered" id="table1">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Application ID</th>
                                            <th>Status Pembayaran</th>
                                            <th>Plan</th>
                                            <th>Angsuran</th>
                                            <th>Dibayar</th>
                                            <th>Tanggal Pembayaran</th>
                                            <th>Approved</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($byr as $b)
                                            <tr role="row" class="odd">
                                                <td><a href="{{ $b->detailUrl() }}">{{ $b->id }}</a></td>
                                                <td><a href="{{ $b->appUrl() }}">{{ $b->application->id }}</a></td>
                                                <td>{{ $b->status_pembayaran }}</td>
                                                <td>{{ $b->plan }}</td>
                                                <td>{{ uang($b->application->pinjaman_bln) }}</td>
                                                <td>{{ uang($b->amount) }}</td>
                                                <td>{{ format($b->tgl_pembayaran) }}</td>
                                                @if($b->approved == 0)
                                                    @if(Sentinel::getuser()->status == "Admin")
                                                      <td><a href="{{ $b->approved() }}" class="btn btn-sm btn-primary">Approve</a></td>
                                                    @else
                                                      <td><i class="fa fa-fw fa-times"></i> Unapproved</td>
                                                    @endif
                                                @else
                                                    <td><i class="fa fa-fw fa-check"></i> Approved</td>
                                                @endif
                                                <td><a class="edit" href="{{ $b->editUrl() }}">Edit</a></td>
                                                <td><a class="edit" href="{{ $b->delUrl() }}">delete</a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @include('admin.pembayaran._payApp')

    @stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/modal/js/classie.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/modal/js/modalEffects.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
@stop
