@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
  Create Pembayaran Form
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
    <section class="content-header">
        <h1>Pembayaran Form</h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary filterable">
                    <div class="panel-heading clearfix">
                        <div class="panel-title pull-left">
                          <div class="caption">
                            <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Create Pembayaran
                          </div>
                        </div>
                        <div class="pull-right">
                          <a href="/admin/pembayaran" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> List Pembayaran</a>
                        </div>
                    </div>
                    <div class="panel-body border">
                        {!! Form::open(array('url' => $appCreate->createUrl(), 'method' => 'post', 'class' => 'form-horizontal form-bordered')) !!}
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Application ID</label>
                                <div class="col-md-6">
                                    <input type="text" name="appid" value="{{ $appCreate->id }}" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nasabah</label>
                                <div class="col-md-6">
                                    <input type="text" name="nasabahname" value="{{ \Sentinel::getuser()->fullname }}" class="form-control" disabled>
                                    <input type="hidden" name="nasabah" value="{{ \Sentinel::getuser()->id }}">
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Status Pembayaran</label>
                                <div class="col-md-6">
                                    <input type="text" name="status_pembayaran" class="form-control" value="Angsuran ke {{ $totalBayar + 1 }} dari {{ ($appCreate->periode * 12) + $appCreate->jangka_waktu }}" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Plan</label>
                                <div class="col-md-6">
                                    <input type="text" name="plan" class="form-control" value="Monthly" readonly>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Tanggal Pembayaran</label>
                                <div class="col-md-6">
                                    <input type="date" name="tgl_pembayaran" class="form-control" value="{{ date("Y-m-d") }}" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Jumlah Angsuran</label>
                                <div class="col-md-6">
                                    <input type="text" name="angsuran" class="form-control" value="{{ $appCreate->pinjaman_bln }}" readonly>
                                </div>
                            </div>
                            <div class="form-group striped-col">
                                <label class="col-md-4 control-label">Pembayaran *</label>
                                <div class="col-md-6">
                                    <input type="text" name="amount" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Keterangan</label>
                                <div class="col-md-6">
                                    <input type="text" name="keterangan" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-9 col-md-offset-4">
                                    <input type="submit" class="btn btn-primary" value="Create Pembayaran">
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#appid').select2()
        });
    </script>
@stop
