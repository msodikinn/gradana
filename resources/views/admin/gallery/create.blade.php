@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Add Gallery
    @parent
    @stop

    {{-- page level styles --}}
    @section('header_styles')
            <!--page level css -->
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/pages/wizard.css') }}" rel="stylesheet">
    {{-- <link href="{{ asset('assets/css/kustom.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    {{-- <link href="{{ asset('assets/css/pages/addgallery.css') }}" rel="stylesheet"> --}}
    <!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Add Gallery</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
            </li>
            <li><a href="{{ route('gallery') }}">Gallery</a></li>
            <li class="active">Add Gallery</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="crop" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                            Add Gallery
                        </h3>
                        <span class="pull-right clickable">
                            <i class="glyphicon glyphicon-chevron-up"></i>
                        </span>
                    </div>
                    <div class="panel-body">
                        <!-- errors -->
                        <div class="has-error">
                            {!! $errors->first('galleryname', '<span class="help-block">:message</span>') !!}
                        </div>
                        <!--main content-->
    				{!! Form::open(array('method' => 'post', 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'formdata')) !!}
    						<div class="form-group">
                    {!! Form::label('galleryname', 'Gallery Name *', ['class' => 'col-sm-2 control-label']) !!}
                    <div class="col-sm-10">
                        <select id="galleryname" name="galleryname" class="form-control" style="width: 100%">
                            @foreach ($property as $p)
                              <option value="{{ $p->id }}"> {{ $p->nama }} </option>
                            @endforeach
                        </select>
                        {{-- <input type="text" name="galleryname" class="form-control" required> --}}
                        {{-- {!! Form::text('galleryname', null, ['class' => 'form-control required']) !!} --}}
    						    </div>
    						</div>
    						<button type=button class="btn buttonmargin" v-bind:class="[isaddphoto ? 'btn-warning' : 'btn-primary']" @click="viewaddphotogallery">@{{ textaddphotobutton }}</button>
    						<div class="form-group" v-show="gallerychoose">
    							<div class="col-md-12">
    	                <div class="panel panel-info">
    		                    <div class="panel-heading">
    		                        <h3 class="panel-title">
    		                            <i class="livicon" data-name="crop" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
    		                            Photo For Choose
    		                        </h3>
                                <span class="pull-right clickable">
                                    <i class="glyphicon glyphicon-chevron-up"></i>
                                </span>
    		                    </div>
      									<div class="panel-body">
          									<div v-for="listchoose in listchooses">
          										  <img :src="listchoose.img_name" alt="Gambar" :idimg="listchoose.id" class="col-sm-3 heightelement pointer" @click="addtothis(listchoose, $event)">
          									</div>
      									</div>
    									<div class="panel-footer">
    										<button type=button class="btn btn-primary buttonmargin" @click="viewaddphotogallery">@{{ textaddphotobutton }}</button>
    									</div>
    								</div>
    							</div>
    						</div>

    						<div class="form-group">
    							<div class="col-md-12">
                    <div class="panel panel-success">
                      <div class="panel-heading">
                          <h3 class="panel-title">
                              <i class="livicon" data-name="crop" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Gallery Photo
                              <input type="hidden" name="photolist" :value="listphotoidvalue">
                          </h3>
                          <span class="pull-right clickable">
                              <i class="glyphicon glyphicon-chevron-up"></i>
                          </span>
                      </div>
    									<div class="panel-body">
          								<div v-for="listpicture in listpictures">
          								    <img :src="listpicture.img_name" alt="Gambar" :idimg="listpicture.id" class="col-sm-3 heightelement pointer" @click="removefromthis(listpicture, $event)">
          								</div>
    		              </div>
    								</div>
    							</div>
    						</div>


    						<div class="form-group">
                    <div class="col-sm-offset-2 col-sm-4">
                        <a class="btn btn-danger" href="{{ route('gallery') }}">
                            @lang('button.cancel')
                        </a>
                        <button type="submit" class="btn btn-success">
                            @lang('button.save')
                        </button>
                    </div>
                </div>
    				{!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--row end-->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/add_gallery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
<script>
    $('#galleryname').select2({
        allowClear: true,
        placeholder: 'Search property',
    });
    (function() {
    	vm.addpicturechoose([
    <?php
    $isfirst = true;
    foreach ($imgshows as $imgshow) {
    	if ($isfirst) {
    		echo '{id:'.$imgshow->id.', img:\''.URL::to('uploads/picture/'.$imgshow->img_name).'\'}';
    		$isfirst = false;
    	} else {
    		echo ',{id:'.$imgshow->id.', img:\''.URL::to('uploads/picture/'.$imgshow->img_name).'\'}';
    	}
    }
    ?>
    	]);
    })();
</script>
@stop
