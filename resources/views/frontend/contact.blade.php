@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
Contact
@parent
@stop

@section('header_styles')
    <!-- Current Page CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">

    <!-- Demo CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
@stop

@section('content')
		<div class="body">
      @include('frontend.layouts._header')
			<div role="main" class="main">
				<section class="section section-text-light section-default section-default-scale-8 section-center mt-none">
					<div class="container">
						<div class="row mt-xl">
							<div class="col-md-8 col-md-offset-2 mt-xlg">
								<h1 class="mt-xlg font-weight-semibold text-uppercase">{{ trans('contact.title') }}</h1>
								<p class="mb-none lead">{{ trans('contact.desc') }}</p>
							</div>
						</div>
						<div class="row mt-lg">
							<div class="col-md-10 col-md-offset-1">
								<div class="row mt-lg align-center">
									<div style="width: 40%; margin:0 auto;">
										<h3 class="mt-md mb-none"><center>Gradana</center></h3>
										<ul class="list list-icons mt-xlg align-center">
                      <li>{{--<i class="fa fa-map-marker "></i>--}} {!! trans('contact.address') !!}</li>
                      {{-- <li><i class="fa fa-phone"></i> <strong>Phone:</strong> (123) 456-789</li> --}}
											<li>{{--<i class="fa fa-envelope"></i>--}} <strong>Email:</strong> <a href="mailto:mail@example.com">admin@gradana.com</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="row mt-lg">
								<div class="col-md-10 col-md-offset-1 center">
									{{-- <div class="divider divider-primary divider-small divider-small-center mb-xl">
										<hr>
									</div>
									<h2 class="mb-none mt-xl font-weight-semibold">Send us a Message</h2>
									<p class="lead mb-none">Kami siap melayani Anda</p> --}}
									<div class="divider divider-style-4 divider-primary divider-top-section-custom taller">
										{{--<i class="fa fa-chevron-down"></i>--}}
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

					<div class="row mt-xl">
						<div class="col-md-10 col-md-offset-1">

							<div class="alert alert-success hidden mt-lg" id="contactSuccess">
								<strong>Success!</strong> Your message has been sent to us.
							</div>

							<div class="alert alert-danger hidden mt-lg" id="contactError">
								<strong>Error!</strong> There was an error sending your message.
								<span class="font-size-xs mt-sm display-block" id="mailErrorMessage"></span>
							</div>

							<form id="contactForm" action="php/contact-form.php" method="POST">
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<input type="text" placeholder="{{ trans('contact.name') }}" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control input-lg" name="name" id="name" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<input type="email" placeholder="{{ trans('contact.email') }}" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control input-lg" name="email" id="email" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<input type="text" placeholder="{{ trans('contact.subject') }}" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control input-lg" name="subject" id="subject" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<textarea maxlength="5000" placeholder="{{ trans('contact.message') }}" data-msg-required="Please enter your message." rows="10" class="form-control input-lg" name="message" id="message" required></textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input type="submit" value="{{ trans('contact.send') }}" class="btn btn-primary btn-lg mb-xs">
									</div>
								</div>
							</form>
						</div>

					</div>

				</div>

				<!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
				{{--<div id="googlemaps" class="google-map"></div>--}}
			</div>
      @include('frontend.layouts._footer')
		</div>
@stop

@section('footer_styles')
		<!-- Current Page Vendor and Views -->
		<script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>

		<!-- Current Page Vendor and Views -->
		<script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
		<script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>

		<script src="http://maps.google.com/maps/api/js"></script>
		<script>
			/*
			Map Settings

				Find the Latitude and Longitude of your address:
					- http://universimmedia.pagesperso-orange.fr/geo/loc.htm
					- http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude/

			*/

			// Map Markers
			var mapMarkers = [{
				address: "Palem Street, Jakarta",
				html: "<strong>Jakarta Office</strong><br>New York, NY 10017<br><br><a href='#' onclick='mapCenterAt({latitude: -6.1820687, longitude: 106.7736218, zoom: 7}, event)'>[+] zoom here</a>",
				icon: {
					image: "{{ asset('assets/img/pin.png') }}",
					iconsize: [26, 46],
					iconanchor: [12, 46]
				}
			},{
				latitude: "-6.1820687",
				longitude: "106.7736218",
				html: "<strong>Los Angeles Office</strong><br>Los Angeles<br><br><a href='#' onclick='mapCenterAt({latitude: -6.1820687, longitude: 106.7736218, zoom: 7}, event)'>[+] zoom here</a>",
				icon: {
					image: "{{ asset('assets/img/pin.png') }}",
					iconsize: [26, 46],
					iconanchor: [12, 46]
				}
			}];

			// Map Initial Location
			var initLatitude = -6.1820687;
			var initLongitude = 106.7736218;

			// Map Extended Settings
			var mapSettings = {
				controls: {
					draggable: (($.browser.mobile) ? false : true),
					panControl: true,
					zoomControl: true,
					mapTypeControl: true,
					scaleControl: true,
					streetViewControl: true,
					overviewMapControl: true
				},
				scrollwheel: false,
				markers: mapMarkers,
				latitude: initLatitude,
				longitude: initLongitude,
				zoom: 16
			};

			var map = $("#googlemaps").gMap(mapSettings),
				mapRef = $("#googlemaps").data('gMap.reference');

			// Map Center At
			var mapCenterAt = function(options, e) {
				e.preventDefault();
				$("#googlemaps").gMap("centerAt", options);
			}

			// Create an array of styles.
			var mapColor = "#567522";

			var styles = [{
				stylers: [{
					hue: mapColor
				}]
			}, {
				featureType: "road",
				elementType: "geometry",
				stylers: [{
					lightness: 0
				}, {
					visibility: "simplified"
				}]
			}, {
				featureType: "road",
				elementType: "labels",
				stylers: [{
					visibility: "off"
				}]
			}];

			var styledMap = new google.maps.StyledMapType(styles, {
				name: "Styled Map"
			});

			mapRef.mapTypes.set('map_style', styledMap);
			mapRef.setMapTypeId('map_style');

		</script>
@stop
