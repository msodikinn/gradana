<style>
  #header .header-nav-main nav > ul > li.dropdown-mega .dropdown-mega-content {
      padding: 20px 10px;
  }
</style>
<header id="header" class="header-narrow header-semi-transparent" data-plugin-options='{"stickyEnabled": true, "stickyEnableOnBoxed": true, "stickyEnableOnMobile": true, "stickyStartAt": 1, "stickySetTop": "1"}'>
  <div class="header-body">
    <div class="header-container container">
      <div class="header-row">
        <div class="header-column">
          <div class="header-logo">
            <a href="/index">
              <img alt="gradana" height="50" src="{{ asset('assets/img/logo_web_gradana.png') }}">
              <div style="position: relative; right: -59px; top: -28px; font-weight: 600;">Beta</div>
            </a>
          </div>
        </div>
        <div class="header-column">
          <div class="header-row">
            <div class="header-nav header-nav-stripe">
              <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
                <i class="fa fa-bars"></i>
              </button>
              {{-- <ul class="header-social-icons social-icons hidden-xs">
                <li class="social-icons-facebook"><a href="https://www.facebook.com/profile.php?id=100012728464456" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                <li class="social-icons-twitter"><a href="http://www.twitter.com/gradanaweb" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                <li class="social-icons-linkedin"><a href="https://www.linkedin.com/in/gradana-property-8121a1124" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
              </ul> --}}
              <div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1 collapse">
                <nav>
                <?php $a=Route::current()->geturi() ?>
                  {{-- {{ dd($a) }} --}}
                  <ul class="nav nav-pills" id="mainNav">
                    <li{{ ( $a == "") ? 'class=active' : '' }}>
                      <a href="/">
                        Home
                      </a>
                    </li>
                    <li {{ ( $a == 'lender') ? 'class=active' : '' }}>
                      <a href="/lender">
                        Lender
                      </a>
                    </li>
                    <li {{ ( $a == 'borrower') ? 'class=active' : '' }}>
                      <a href="/borrower">
                        Borrower
                      </a>
                    </li>
                    {{--<li {{ ( $a == 'property') ? 'class=active' : '' }}>
                      <a href="/property">
                        {!! trans('header.property') !!}
                      </a>
                    </li>--}}
                    <li {{ ( $a == 'about') ? 'class=active' : '' }}>
                      <a href="/about">
                        {!! trans('header.about') !!}
                      </a>
                    </li>
                    <li {{ ( $a == 'contact') ? 'class=active' : '' }}>
                      <a href="/contact">
                        {!! trans('header.contact') !!}
                      </a>
                    </li>
                    {{-- <li {{ ( $a == 'faq') ? 'class=active' : '' }}>
                      <a href="/faq">
                        FAQ
                      </a>
                    </li> --}}
                    {{--<li {{ ( $a == 'blog') ? 'class=active' : '' }}>
                      <a href="/blog">
                        Blog
                      </a>
                    </li>--}}
                    @if (Sentinel::check())
                      <li class="dropdown dropdown-mega dropdown-mega-signin signin logged" id="headerAccount">
                        <a class="dropdown-toggle" href="#">
                          <i class="fa fa-user"></i>{{ Sentinel::getuser()->fullname }}
                        </a>
                        <ul class="dropdown-menu">
                          <li>
                            <div class="dropdown-mega-content">
                              <div class="row">
                                <div class="col-md-7">
                                  <div class="user-avatar">
                                    <div class="img-thumbnail">
                                      <img src="{{ '/'.Sentinel::getuser()->pic }}" alt="">
                                    </div>
                                    <p><strong>{{ Sentinel::getuser()->fullname }}</strong><span>{{ Sentinel::getuser()->status }}</span></p>
                                  </div>
                                </div>
                                <div class="col-md-5">
                                  <ul class="list-account-options">
                                    @if (Sentinel::getuser()->status == 'Applicant')
                                      <li>
                                        <a href="/applicant">Application</a>
                                      </li>
                                      <li>
                                        <a href="/my-account">My Profile</a>
                                      </li>
									  <li>
                                        <a href="/applicant/dashboard">Dashboard</a>
                                      </li>
                                    @endif
                                    @if (Sentinel::getuser()->status == 'Investor')
                                      
                                      <li>
                                        <a href="/my-account">Profile</a>
                                      </li>
									  <li>
                                        <a href="/lender/dashboard">Dashboard</a>
                                      </li>
                                      <!--li>
                                        <a href="/lender/history">History</a>
                                      </li-->
                                      <li>
                                        <a href="/lender/borrower-card">Borrower Details</a>
                                      </li>
                                      <li>
                                        <a href="/lender/available-loans">Available Loans</a>
                                      </li>
									  <li>
                                        <a href="/lender/investor-form">Investor Form</a>
                                      </li>
                                    @endif
                                    <li>
                                      <a href="/logout">Log Out</a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </li>
                    @else
                      <li {{ ( $a == 'login') ? 'class=active' : '' }}>
                        <a href="/login">
                          Login
                        </a>
                      </li>
                    @endif
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                          {{ Config::get('language')[App::getLocale()] }}
                        </a>
                        <ul class="dropdown-menu">
                          @foreach (Config::get('language') as $lang => $language)
                            @if ($lang != App::getLocale())
                              <li>
                                <a href="{{ route('lang.switch', $lang) }}">{!! $language !!}</a>
                              </li>
                            @endif
                          @endforeach
                        </ul>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
