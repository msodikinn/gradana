<!DOCTYPE html>
<html>
  <head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @section('title')
            | Gradana - New Mortgage Financing Solution to Live A Dream Home
        @show
    </title>

    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Porto - Responsive HTML5 Template">
    <meta name="author" content="gradana.com">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon-32x32.png') }}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="{{ asset('assets/img/apple-icon-152x152.png') }}">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{asset('assets/frontend/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/vendor/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/vendor/simple-line-icons/css/simple-line-icons.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/vendor/owl.carousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/vendor/owl.carousel/assets/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/vendor/magnific-popup/magnific-popup.min.css')}}">
    <link href="{{ asset('assets/lib/sweetalert-master/dist/sweetalert.css') }}" rel="stylesheet"></script>

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/theme.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/theme-elements.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/theme-blog.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/theme-shop.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/theme-animate.css') }}">

    <!-- Skin CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/skins/default.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('assets/frontend/css/skins/skin-digital-agency.css') }}"> --}}

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/lib/sweetalert-master/dist/sweetalert.css') }}">


    <!-- Head Libs -->
    <script src="{{ asset('assets/frontend/vendor/modernizr/modernizr.min.js') }}"></script>


    @yield('header_styles')
  </head>
  <body data-spy="scroll" data-target="#sidebar" data-offset="120">

        @yield('content')

    <!-- Vendor -->
    <script src="{{asset('assets/frontend/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('assets/frontend/vendor/jquery.appear/jquery.appear.min.js')}}"></script>
    <script src="{{asset('assets/frontend/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
    <script src="{{asset('assets/frontend/vendor/jquery-cookie/jquery-cookie.min.js')}}"></script>
    <script src="{{asset('assets/frontend/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/frontend/vendor/common/common.min.js')}}"></script>
    <script src="{{asset('assets/frontend/vendor/jquery.validation/jquery.validation.min.js')}}"></script>
    <script src="{{asset('assets/frontend/vendor/jquery.stellar/jquery.stellar.min.js')}}"></script>
    <script src="{{asset('assets/frontend/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js')}}"></script>
    <script src="{{asset('assets/frontend/vendor/jquery.gmap/jquery.gmap.min.js')}}"></script>
    <script src="{{asset('assets/frontend/vendor/jquery.lazyload/jquery.lazyload.min.js')}}"></script>
    <script src="{{asset('assets/frontend/vendor/isotope/jquery.isotope.min.js')}}"></script>
    <script src="{{asset('assets/frontend/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/frontend/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('assets/frontend/vendor/vide/vide.min.js')}}"></script>

    <!-- Theme Base, Components and Settings -->
    <script src="{{ asset('assets/frontend/js/theme.js') }}"></script>

    <!-- Theme Custom -->
    <script src="{{ asset('assets/frontend/js/custom.js') }}"></script>

    <!-- Theme Initialization Files -->
    <script src="{{ asset('assets/frontend/js/theme.init.js') }}"></script>
    <script src="{{ asset('assets/lib/sweetalert-master/dist/sweetalert.min.js') }}"></script>
    @stack('scripts')
    @include('sweet::alert')

    @yield('footer_styles')

    <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-12345678-1', 'auto');
      ga('send', 'pageview');
    </script>
     -->

  </body>
</html>
