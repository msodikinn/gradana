<footer class="short" id="footer">
        <div class="container">
          <div class="row">
            <div class="col-md-8">
              <h4 class="heading-primary">{!! trans('footer.title') !!}</h4>
              <p>{!! trans('footer.desc') !!}</p>
              <hr class="light">
            </div>
            <div class="col-md-3 col-md-offset-1">
              <h5 class="mb-sm">{!! trans('footer.contact') !!}</h5>
              <p class="mb-none">{!! trans('footer.alamat') !!}</p>
              <ul class="list list-icons list-icons-sm">
                <li><i class="fa fa-envelope"></i> <a href="mailto:admin@gradana.com">admin@gradana.com</a></li>
              </ul>
              <ul class="social-icons">
                <li class="social-icons-facebook"><a href="https://www.facebook.com/profile.php?id=100012728464456" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                <li class="social-icons-twitter"><a href="http://www.twitter.com/gradanaweb" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                <li class="social-icons-linkedin"><a href="https://www.linkedin.com/in/gradana-property-8121a1124" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="footer-copyright">
          <div class="container">
            <div class="row">
              <div class="col-md-1" style="margin-right: 0px; padding-right: 0px;">
                <a href="index.html" class="logo">
                  <img alt="Porto Website Template" class="img-responsive" src="{{ asset('assets/img/gradana-trans-small-gray.png') }}">
                </a>
              </div>
              <div class="col-md-11" style="margin-top: 30px;">
                <p>Gradana.com © Copyright 2016. All Rights Reserved.</p>
              </div>
            </div>
          </div>
        </div>
      </footer>
