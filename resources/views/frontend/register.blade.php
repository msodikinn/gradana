@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
  Register
@parent
@stop

@section('header_styles')
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Register</title>
  <link rel="stylesheet" href="{{asset('assets/vendors/iCheck/css/all.css')}}" />
  {{-- <link rel="stylesheet" href="{{ asset('assets/css/frontend/register.css') }}"> --}}
  <link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet"/>

  <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
  {{-- <link href="{{ asset('assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/> --}}
  <link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
  <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/pages/wizard.css') }}" rel="stylesheet">

  <!--end of page level css-->
@stop

@section('content')
    <div class="body">
        @include('frontend.layouts._header')
        <div role="main" class="main">
          <section class="page-header">
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <ul class="breadcrumb">
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12" style="margin-top: 45px">
                </div>
              </div>
            </div>
          </section>
          <div class="container">
              <div class="row">
                  <div class="box animation flipInX">
                      <h3 class="heading-primary">Register</h3>
                      <!-- Notifications -->
                      <p>{{ trans('register.regdesc') }}</p>
                      @include('notifications')
                      @if($errors->has())
                          @foreach ($errors->all() as $error)
                              <div class="text-danger">{{ $error }}</div>
                          @endforeach
                      @endif

                      {!! Form::open(array('url' => route('register'), 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true, 'id' => 'commentForm')) !!}
                          <!-- CSRF Token -->
                          <div id="rootwizard">
                              <ul>
                                  <lsi><a href="#tab1" data-toggle="tab">{{--{{ trans('register.profil') }}--}}</a></li>
                                  {{-- <li><a href="#tab2" data-toggle="tab">{{ trans('register.data') }}</a></li> --}}
                              </ul>
                              <div class="tab-content">
                                  <div class="tab-pane" id="tab1">
                                      {{--<div class="form-group">
                                          <label class="col-md-2">Avatar:</label>
                                          <div class="col-md-10">
                                              <div class="fileinput fileinput-new" data-provides="fileinput">
                                                  <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 150px;">
                                                      <img src="http://placehold.it/200x150" alt="..." class="img-responsive"/>
                                                  </div>
                                                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                                  <div>
                                                      <span class="btn btn-primary btn-file">
                                                          <span class="fileinput-new">{{ trans('register.image') }}</span>
                                                          <span class="fileinput-exists">{{ trans('register.change') }} </span>
                                                          <input type="file" name="pic" id="pic" />
                                                      </span>
                                                      <span class="btn btn-primary fileinput-exists" data-dismiss="fileinput">{{ trans('register.remove') }}</span>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>--}}
                                      <div class="form-group {{ $errors->first('fullname', 'has-error') }}">
                                          <div class="col-md-2">
                                            <label> {{ trans('register.fullname') }}</label>
                                          </div>
                                          <div class="col-md-10">
                                            <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Full Name" required>
                                            {!! $errors->first('fullname', '<span class="help-block">:message</span>') !!}
                                          </div>
                                      </div>
                                      <div class="form-group {{ $errors->first('email', 'has-error') }}">
                                          <div class="col-md-2">
                                            <label> Email</label>
                                          </div>
                                          <div class="col-md-10">
                                            <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                                            {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                          </div>
                                      </div>
                                      <div class="form-group {{ $errors->first('password', 'has-error') }}">
                                          <div class="col-md-2">
                                            <label> Password</label>
                                          </div>
                                          <div class="col-md-10">
                                            <input type="password" class="form-control" id="Password1" name="password" placeholder="Password">
                                            {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                                          </div>
                                      </div>
                                      <div class="form-group {{ $errors->first('password_confirm', 'has-error') }}">
                                          <div class="col-md-2">
                                            <label> {{ trans('register.confirm') }}</label>
                                          </div>
                                          <div class="col-md-10">
                                            <input type="password" class="form-control" id="Password2" name="password_confirm"
                                                 placeholder="Konfirmasi">
                                            {!! $errors->first('password_confirm', '<span class="help-block">:message</span>') !!}
                                          </div>
                                      </div>
                                      <div class="form-group {{ $errors->first('status', 'has-error') }}">
                                          <div class="col-md-2">
                                            <label>{{ trans('register.as') }}</label>
                                          </div>
                                          <div class="col-md-10">
                                            <label class="radio-inline" style="padding-left: 0px;">
                                              <input type="radio" name="status" id="inlineRadio1" value="Applicant" checked="checked"> {{ trans('register.borrow') }}
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="status" id="inlineRadio2" value="Investor"> {{ trans('register.investor') }}
                                            </label>

                                            {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                          </div>
                                      </div>

                                  </div>

                                  {{-- <div class="tab-pane" id="tab2" disabled="disabled">
                                      <div class="form-group {{ $errors->first('dob', 'has-error') }}">
                                          <div class="col-md-2">
                                            <label> {{ trans('register.tgl') }}</label>
                                          </div>
                                          <div class="col-md-5">
                                            <input type="date" class="form-control" name="dob" placeholder="Tanggal Lahir" required>
                                            {!! $errors->first('dob', '<span class="help-block">:message</span>') !!}
                                          </div>
                                      </div>
                                      <div class="form-group {{ $errors->first('country', 'has-error') }}">
                                          <div class="col-md-2">
                                            <label> {{ trans('register.negara') }}</label>
                                          </div>
                                          <div class="col-md-5">
                                            <select id="country" name="country" class="form-control" style="width: 100%">
                                              @foreach($negara as $code=>$n)
                                                <option value="{{ $code }}">
                                                  {{$n}}
                                                </option>
                                              @endforeach
                                            </select>
                                          </div>
                                      </div>
                                      <div class="form-group {{ $errors->first('address', 'has-error') }}">
                                          <div class="col-md-2">
                                            <label> {{ trans('register.address') }}</label>
                                          </div>
                                          <div class="col-md-10">
                                            <input type="text" class="form-control" id="address" name="address" placeholder="address" required>
                                            {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                                          </div>
                                      </div>
                                      <div class="form-group {{ $errors->first('state', 'has-error') }}">
                                          <div class="col-md-2">
                                            <label> {{ trans('register.prop') }}</label>
                                          </div>
                                          <div class="col-md-10">
                                            <input type="text" class="form-control" name="state" placeholder="state" required>
                                            {!! $errors->first('state', '<span class="help-block">:message</span>') !!}
                                          </div>
                                      </div>
                                      <div class="form-group {{ $errors->first('city', 'has-error') }}">
                                          <div class="col-md-2">
                                            <label> {{ trans('register.kota') }}</label>
                                          </div>
                                          <div class="col-md-10">
                                            <input type="text" class="form-control" id="city" name="city" placeholder="Telepon" required>
                                            {!! $errors->first('city', '<span class="help-block">:message</span>') !!}
                                          </div>
                                      </div>
                                      <div class="form-group {{ $errors->first('postal', 'has-error') }}">
                                          <div class="col-md-2">
                                            <label> {{ trans('register.postal') }}</label>
                                          </div>
                                          <div class="col-md-10">
                                            <input type="text" class="form-control" id="postal" name="postal" placeholder="Kode Pos" required>
                                            {!! $errors->first('postal', '<span class="help-block">:message</span>') !!}
                                          </div>
                                      </div>

                                  </div> --}}
                                  <div class="form-group">

                                      <div class="col-md-2">
                                        <label></label>
                                      </div>
                                      <div class="col-md-10">
                                        {{-- <input id="agree" name="agree" type="checkbox">
                                        <label for="agree">&nbsp;By checking, I agree to the <a id="term" href="#" data-toggle="modal" data-target="#largeModal">terms and conditions.</a></label> --}}

                                        <a id="term" href="#" class="btn btn-primary" data-toggle="modal" data-target="#largeModal">Sign Up</a>
                                      </div>
                                  </div>
                                  <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true" style="width:90%; margin:0 auto;">
                                    <div class="modal-dialog modal-lg">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                          <h4 class="modal-title" id="largeModalLabel">Terms and Conditions</h4>
                                        </div>
                                        <div id="StatusApplicant" class="desc">
                                            @include('frontend.partials._termBorrower')
                                        </div>
                                        <div id="StatusInvestor" class="desc" style="display: none;">
                                            @include('frontend.partials._termInvestor')
                                        </div>
                                        <div class="modal-footer">
                                            <div class="checkbox text-left">
                                                <label>
                                                    <input type="checkbox" name="tnc" value="yes"> <div class="text-left">{{ trans('investorform.tnt') }}</div> <br>
                                                </label>
                                            </div>
                                            <a href="javascript: submitform()" class="btn btn-primary">{{ trans('basicform.submit') }}</a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  {{-- <ul class="pager wizard">
                                      <li class="previous"><a href="#">{{ trans('register.prev') }}</a></li>
                                      <li class="next"><a href="#">{{ trans('register.next') }}</a></li>
                                      <input type="submit" class="btn btn-block btn-primary" value="Sign up" name="submit">
                                      <li class="next finish" style="display:none;">
                                      <a href="javascript: submitform()">Sign Up</a>

                                      </li>
                                  </ul> --}}
                              </div>
                          </div>
                      {!! Form::close() !!}
                      {{ trans('login.return') }}  {{ trans('register.silahkan') }} <a href="{{ route('login') }}"> {{ trans('register.login') }}</a>
                  </div>
              </div>
          </div>
        </div>
        @include('frontend.layouts._footer')
    </div>
@stop

@section('footer_styles')
  <!--global js starts-->
  <script type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

  <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
  <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"  type="text/javascript"></script>
  <script src="{{ asset('assets/js/pages/datepicker.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/vendors/intl-tel-input/js/intlTelInput.min.js') }}" type="text/javascript"></script> --}}
  <script src="{{ asset('assets/js/pages/validation.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/pages/adduser.js') }}"></script>

  <script type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>

  <!--global js end-->
  <script>
    $(document).ready(function() {
        $("input[name$='status']").click(function() {
            var test = $(this).val();
            console.log(test);

            $("div.desc").hide();
            $("#Status" + test).show();
        });
    });
  </script>
  <script>
    $(document).ready(function() {
        $('#country').select2()
    });
  </script>
  <script>
      // $(document).ready(function(){
      //     $("input[type='checkbox'],input[type='radio']").iCheck({
      //         checkboxClass: 'icheckbox_minimal-blue',
      //         radioClass: 'iradio_minimal-blue'
      //     });
      // });
  </script>
  <script>
      function submitform()
      {
        document.getElementById("commentForm").submit();
      }
  </script>

@stop
