@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
  Login / Register
@parent
@stop

@section('header_styles')
    <!-- Current Page CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">
    <!-- Demo CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">

    <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/vendors/iCheck/css/all.css')}}" />
    <link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/pages/wizard.css') }}" rel="stylesheet">
    <style>
      label {
        display: block;
        font-weight: 100;
        font-size: 25px;
        line-height: 20px;
      }
    </style>
@stop

@section('content')
    <div class="body">
      @include('frontend.layouts._header')
      <div role="main" class="main">
        <section class="page-header">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <ul class="breadcrumb">
                </ul>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12" style="margin-top: 45px">
              </div>
            </div>
          </div>
        </section>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="featured-boxes">
                <div class="row">
                  <div style="width: 50%; margin:0 auto;">
                    <div class="featured-box featured-box-primary align-left mt-xlg">
                      <div class="box-content">
                        <h4 class="heading-primary text-uppercase mb-md">{{ trans('login.return') }}</h4>
                        {!! Form::open(array('url' => '/login', 'method' => 'post')) !!}
                          <div class="row">
                            <div class="form-group">
                              <div class="col-md-12">
                                <label>{{ trans('login.email') }}</label>
                                <input type="text" name="email" value="" class="form-control input-lg">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="form-group">
                              <div class="col-md-12">
                                <a class="pull-right" href="/forgot-password">{{ trans('login.lost') }}</a>
                                <label>Password</label>
                                <input type="password" name="password" value="" class="form-control input-lg">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              {{ trans('login.noaccount') }} <a href="/register">{{ trans('login.register') }}</a>
                            </div>
                            <div class="col-md-6">
                              <input type="submit" value="Login" class="btn btn-primary pull-right mb-xl">
                            </div>
                          </div>

                        {{-- <div class="row">
                            <h4 align="center">{{ trans('login.or') }}</h4> <br>
                            <div class="col-md-12">
                                <p align="center">
                                    <a href="/auth/facebook" title="Facebook Login"><img src="/assets/img/fb-logo.png" alt="Facebook Login"></a> &nbsp;
                                    <a href="/auth/google"><img src="/assets/img/google-logo.png" alt="Google Login"></a> &nbsp;
                                    <a href="/auth/twitter" title="Twitter Login"><img src="/assets/img/twitter-logo.png" alt="Twitter Login"></a> &nbsp;
                                    <a href="/auth/linkedin" title="LinkedIn Login"><img src="/assets/img/linkedin-logo.png" alt="LinkedIn Login"></a>
                                </p>
                            </div>
                        </div> --}}
                        {{ Form::close() }}
                      </div>
                    </div>
                  </div>
                  {{-- <div align="center">
                    <label>OR</label>
                  </div>
                  <div style="width: 50%; margin:0 auto;">
                    <div class="featured-box featured-box-secondary align-left mt-xlg">
                      <div class="box-content">
                        <h4 class="heading-primary text-uppercase mb-md">Register An Account</h4>

                        @include('notifications')
                        @if($errors->has())
                            @foreach ($errors->all() as $error)
                                <div class="text-danger">{{ $error }}</div>
                            @endforeach
                        @endif

                        {!! Form::open(array('url' => route('register'), 'method' => 'post', 'id' => 'commentForm')) !!}
                          <div class="row">
                            <div class="form-group">
                              <div class="col-md-12">
                                <label>Nama Lengkap</label>
                                <input type="text"  name="fullname" value="" class="form-control input-lg">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="form-group">
                              <div class="col-md-12">
                                <label>E-mail Address</label>
                                <input type="text" id="email" name="email" value="" class="form-control input-lg">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="form-group">
                              <div class="col-md-6">
                                <label>Password</label>
                                <input type="password" id="password1" name="password" value="" class="form-control input-lg">
                              </div>
                              <div class="col-md-6">
                                <label>Re-enter Password</label>
                                <input type="password" id="password2" name="password-confirmation" class="form-control input-lg">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <input type="submit" value="Register" class="btn btn-primary pull-right mb-xl">
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div> --}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      @include('frontend.layouts._footer')
    </div>
@stop

@section('footer_styles')
    <script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>

    <!-- Current Page Vendor and Views -->
    <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
    {{-- <script src="{{ asset('assets/vendors/intl-tel-input/js/intlTelInput.min.js') }}" type="text/javascript"></script> --}}
    <script src="{{ asset('assets/js/pages/validation.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/adduser.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>

@stop
