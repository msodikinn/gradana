                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.jenis_pekerjaan_jobpm') }}</label>
                                <div class="col-md-7">
                                    <select id="jenis_pekerjaan_jobpm" name="jenis_pekerjaan_jobpm" class="form-control" style="width: 100%">
                                      @foreach($jenis as $code=>$j)
                                        <option value="{{ $code }}">
                                          {{$j}}
                                        </option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.nama_perusahaan_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="nama_perusahaan_jobpm" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.bidang_usaha_jobpm') }}</label>
                                <div class="col-md-7">
                                    <select id="bidang_usaha_jobpm" name="bidang_usaha_jobpm" class="form-control" style="width: 100%">
                                      @foreach($usaha as $code=>$j)
                                        <option value="{{ $code }}">
                                          {{$j}}
                                        </option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.bidang_usaha_detail_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="bidang_usaha_detail_jobpm" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.tanggal_pendirian_jobpm') }}</label>
                                <div class="col-md-3">
                                    <input type="date" name="tanggal_pendirian_jobpm" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.alamat_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="alamat_jobpm" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.kelurahan_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="kelurahan_jobpm" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.kecamatan_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="kecamatan_jobpm" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.kota_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="kota_jobpm" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.postal_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="postal_jobpm" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.tlp_ktr_hunting_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="tlp_ktr_hunting_jobpm" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.tlp_ktr_direct_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="tlp_ktr_direct_jobpm" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.fax_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="fax_jobpm" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.unit_kerja_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="unit_kerja_jobpm" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.jabatan_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="jabatan_jobpm" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.bulan_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="bulan_jobpm" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.tahun_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="tahun_jobpm" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                              <h4 class="col-md-3  control-label" style="margin-bottom: 20px;">Untuk wiraswasta/profesi lainnya</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.omset_bln_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="omset_bln_jobpm" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.kepemilikan_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="kepemilikan_jobpm" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.margin_untung_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="margin_untung_jobpm" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.nama_perusahaan_ago_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="nama_perusahaan_ago_jobpm" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.jenis_usaha_ago_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="jenis_usaha_ago_jobpm" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.jabatan_ago_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="jabatan_ago_jobpm" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.unit_kerja_ago_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="unit_kerja_ago_jobpm" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.telp_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="telp_jobpm" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.bulan_ago_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="bulan_ago_jobpm" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpemohon.tahun_ago_jobpm') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="tahun_ago_jobpm" class="form-control">
                                </div>
                            </div>
