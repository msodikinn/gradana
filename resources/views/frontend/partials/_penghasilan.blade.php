                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/penghasilan.nama_keluarga') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="joint_income_penghasilan" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/penghasilan.alamat_keluarga') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="pemohon_penghasilan" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/penghasilan.kelurahan_keluarga') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="suami_istri_penghasilan" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/penghasilan.kecamatan_keluarga') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="tambahan_penghasilan" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/penghasilan.propinsi_keluarga') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="biaya_rt_penghasilan" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/penghasilan.kota_keluarga') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="angsuran_lain_penghasilan" class="form-control">
                                </div>
                            </div>
