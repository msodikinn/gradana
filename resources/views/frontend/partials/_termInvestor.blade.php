{{-- <style>
    OL {
      counter-reset: item
    }
    LI {
      display: block;
    }
    LI:before {
      content: counters(item, ".") ". ";
      counter-increment: item;
    }
</style> --}}
<center><h2>SYARAT DAN KETENTUAN GRADANA <br>
BAGI PEMBERI PINJAMAN</h2></center>
<p>www.Gradana.com (“Situs”) adalah Situs yang dikelola oleh PT Gradana Indonesia (“Gradana”) yang untuk selanjutnya disebut “Kami”. Syarat dan ketentuan berikut merupakan perjanjian hukum (“PERJANJIAN ”) antara Lender dengan Gradana yang mengatur penggunaan layanan Gradana Indonesia.</p>
<p>Lender harus membaca dan memahami semua persyaratan dan ketentuan dalam Perjanjian ini sebelum menggunakan fitur layanan dan/atau menerima konten yang terdapat di dalam Situs. Apabila Lender menekan tombol “Setuju” pada kolom yang disediakan, maka Lender telah menerima dan menyetujui seluruh persyaratan dan ketentuan dalam Perjanjian untuk menggunakan dan menerima layanan dan akses atas seluruh konten atau fitur layanan yang terdapat dalam Situs.</p>

<strong>A.  PENDAHULUAN</strong>
<ol>
  <li><span>Perjanjian ini diatur dan diinterpretasikan berdasarkan Hukum Republik Indonesia.</span></li>
  <li>Kami dapat mengubah atau memperbarui Perjanjian ini setiap waktu dengan mencantumkan Perjanjian yang telah diubah atau diperbarui di dalam Situs dan/atau media lain. Perjanjian yang telah diubah dan diperbarui tersebut akan berlaku setelah 3 (tiga) hari kalender terhitung sejak tanggal diumumkan di Situs. Lender dianggap mengetahui dan menyetujui perubahan atau pembaruan atas Perjanjian layanan Situs apabila Lender terus menggunakan Situs setelah perubahan atau pembaruan tersebut berlaku dan tercantum pada Situs. Oleh karenanya Lender diharapkan untuk memeriksa halaman ini secara berkala agar mengetahui perubahan-perubahan tersebut.</li>
  <li>Kami juga dapat mengubah, menghapus, menambah atau memperbarui fitur atau fasilitas dalam Situs ini setiap waktu tanpa mengumumkan terlebih dahulu mengenai hal tersebut.</li>
</ol>

<strong>B.  LATAR BELAKANG KERJASAMA</strong>
<ol>
  <li>Situs yang Kami kelola adalah platform pendanaan point to point dimana Gradana akan mempertemukan Lender dan Investee melalui platform ini dimana Lender dapat dapat membiayai pembelian properti untuk kepentingan Investee berdasarkan Skema A atau Skema B (“Platform”).</li>
  <li>Dalam Skema A, Lender akan membiayai penuh harga properti kepada pengembang dan memperoleh properti tersebut untuk kepentingan Investee berdasarkan harga khusus yang disepakati pengembang dan Lender. Investee akan membayar cicilan dana talangan kepada Lender berdasarkan harga KPA / KPR. Pada saat cicilan Investee mencapai sedikitnya 20% dari harga KPA/ KPR, Investee akan mengajukan dan memperoleh pinjaman dari bank untuk melunasi sisa dari harga KPA/KPR tersebut kepada Lender dan melanjutkan cicilan dengan bank.</li>
  <li>Dalam Skema B, Lender akan membayar sebagian dari harga properti sebagai dana talangan kepada pengembang dan memperoleh properti tersebut untuk kepentingan Investee berdasarkan harga khusus yang disepakati pengembang dan Lender. Sama halnya dengan Skema A, Investee akan membayar cicilan terhadap dana talangan kepada Lender berdasarkan harga KPA / KPR. Pada saat Investee melunasi cicilan dana talangan tersebut, maka Investee akan mengajukan dan memperoleh pinjaman dari bank untuk melanjutkan cicilan dengan bank.</li>
  <li>Investee dapat membayar cicilan dana talangan setiap bulannya kepada Lender.</li>
  <li>Dana talangan sedikitnya senilai 20% dari harga properti atau nilai yang ditentukan oleh pengembang dan Lender atau nilai lain yang disepakati antara para pihak.</li>
  <li>Setelah Investee membayar lunas dana talangan kepada Lender berdasarkan Harga Properti yang disebutkan dalam paragraf D2 , Investee dapat melanjutkan cicilan pembelian properti kepada Bank atau Lender (yang sama atau lainnya) apabila Investee tidak mendapatkan persetujuan dari Bank.</li>
</ol>

<strong>C.  MEKANISME KERJASAMA:</strong>
<ol>
  <li>Harga Properti
    <ol type="a">
      <li>Untuk Skema A, harga properti yang dibayarkan oleh Lender kepada pengembang adalah harga Unit Properti adalah harga cash keras atau harga khusus yang disepakati antara Pengembang dan Gradana.</li>
      <li>Untuk Skema B, harga properti yang dibayarkan oleh Lender kepada pengembang adalah harga Unit Properti yang ditawarkan oleh Pengembang kepada Lender Gradana dengan nilai [*] % diatas harga property Skema A.</li>
      <li>Harga yang ditawarkan kepada Investee dalam Skema A dan Skema B adalah harga properti berdasarkan pembayaran kredit pemilikan apartemen (KPA).</li>
    </ol>
  </li>
  <li>Dokumentasi dan Pencairan Dana
    <ol type="a">
      <li>Investee akan membayar Booking Fee kepada pengembang pada saat Investee menandatangani Form Penjualan.</li>
      <li>Setelah Lender menentukan Investee pilihannya, pada hari yang disepakati oleh Investee, Lender, pengembang dan Gradana:
        <ol type="i">
          <li>Lender akan membayar lunas seluruh (untuk Skema A) atau sebagian (untuk Skema B) dari harga property yang telah ditentukan oleh Pembeli kepada pengembang dikurangi dengan Booking Fee,</li>
          <li>Pengembang  dan Lender menandatangani Surat Pesanan dan / atau PPJB terkait dengan Unit Properti tersebut, dimana didalamnya tercantum bahwa (i) perjanjian ini dapat dinovasikan kepada Pembeli (tanpa biaya administrasi apapun) setelah segala kewajiban Pembeli kepada Lender telah dipenuhi dan (ii) Lender memberikan kuasa kepada Pengembang untuk mengalihkan kepemilikan Unit Properti yang dibiayai oleh Lender kepada Pembeli pada saat Pembeli telah memenuhi seluruh kewajiban pembayarannya kepada Lender dan/atau Pengembang.</li>
        </ol>
      <li>Investee akan membayar cicilan bulanan kepada Lender sesuai perjanjian yang telah ditandatangani antara Lender dan Investee.</li>
      <li>Setelah Investee melunasi dana talangan,  Investee akan mengajukan permohonan pinjaman kepada bank untuk melunasi seluruh harga property kepada Lender (untuk Skema A) atau pengembang (untuk Skema B).</li>
      <li>Dana pinjaman dari bank akan disetor secara langsung ke rekening bank yang ditentukan oleh Lender (untuk Skema A) atau pengembang (untuk Skema B).</li>
      <li>Pada hari yang sama dimana
        <ol type="i">
          <li>Lender menerima pengembalian dana dari Pembeli berdasarkan pencairan dana dari bank (untuk Skema A),</li>
          <li>Lender menerima pengembalian dana talangan, dan Pengembang  telah menerima pembayaran penuh atas unit properti (untuk Skema B), atau</li>
          <li>hari lain yang disetujui oleh Lender dan Pembeli, Pembeli akan menandatangani akta jual beli dengan Pengembang dan perjanjian pengikatan jual beli (PPJB) akan berakhir dengan ditandatanganinya akta jual beli antara Pengembang dan Pembeli.</li>
        </ol>
      </li>
    </ol>
  </li>
  <li>Penempatan Dana
    <ol type="a">
      <li>Lender dapat memilih untuk menempatkan dana yang ingin diinvestasikan dalam suatu rekening bank yang ditentukan oleh Gradana (“Rekening Lender”)
        <ol type="i">
          <li>pada saat Lender melakukan pendaftaran dalam Situs (“Penempatan Dana Opsi 1”), atau</li>
          <li>pada saat Lender telah menentukan Investee yang akan dibiayainya (“Penempatan Dana Opsi 2”).</li>
        </ol>
      </li>
      <li>Dalam Penempatan Dana Opsi 1:
        <ol type="i">
          <li>Lender akan menaruh dana yang dialokasikan untuk tujuan investasi di rekening bank yang ditentukan oleh Gradana dalam bentuk escrow account / virtual account;</li>
          <li>seluruh bunga yang diperoleh berdasarkan dana yang ditempatkan Lender dalam Rekening Lender merupakan hak dan sepenuhnya milik Investror sampai dengan dana tersbut digunakan untuk Investasi;</li>
          <li>setelah Lender menerima pemberitahuan dari Gradana terkait Investee dan  properti yang telah ditentukan Investee,Lender dapat menentukan Investee yang ingin dibiayai berdasarkan diskresi penuh dan keputusan Lender;</li>
          <li>setelah Lender menentukan Investee pilihannya dan Surat Pesanan dan/atau PPJB ditandatangani, dana yang ditempatkan oleh Lender dapat secara langsung ditransfer kepada pengembang dalam rangka memperoleh properti tersebut untuk kepentingan Investee.</li>
        </ol>
      </li>
      <li>Dalam Penempatan Dana Opsi 2:
        <ol type="i">
          <li>setelah Lender menentukan Investee pilihannya, Lender kemudian baru akan menempatkan dana ke Rekening yang dialokasika oleh Gradana yang kemudian akan ditransfer kepada pengembang setelah Surat Pesanan dan / atau PPJB ditandatangani dalam rangka memperoleh properti tersebut untuk kepentingan Investee.</li>
        </ol>
      </li>
    </ol>
  </li>
</ol>

<strong>D.  HAMBATAN PEMBAYARAN DARI INVESTEE</strong>
<ol>
  <li>Lender mengerti terdapat kemungkinan dimana Investee:
      <ol>
        <li>tidak berhasil dalam mendapatkan fasilitas pinjaman dari bank atau bentuk pinjaman atau saluran dana lainnya untuk melanjutkan angsuran terhadap properti dalam waktu 3 bulan sejak pembayaran cicilan dana talangan terakhir oleh Investee kepada Lender, atau</li>
        <li>tidak melakukan pembayaran angsuran terhadap dana talangan kepada Lender selama 3 bulan yang cukup dibuktikan oleh Lender dengan surat peringatan pembayaran dari Gradana kepada Investee.</li>
      </ol>
  </li>
  <li>Apabila salah satu dari peristiwa dalam Klausul D.1.1 atau D.1.2 terjadi, maka:
      <ol>
        <li>Untuk Skema A,  Investee dapat melanjutkan cicilan terhadap properti kepada Lender berdasarkan kesepakatan antara Lender dan Investee, atau Investee dapat memilih pengembalian uang ; atau</li>
        <li>Untuk Skema B, Investee dapat melakukan pembayaran cicilan terhadap properti kepada Lender apabila Lender setuju untuk membayar lunas sisa dari Harga Tunai Skema B kepada pengembang, atau Investee dapat memilih pengembalian uang.</li>
        <li>Pengaturan lebih lanjut terkait hal diatas tercantum dalam perjanjian antara Investee dan Lender.</li>
      </ol>
  </li>
</ol>

<strong>E.  BIAYA</strong>
<ol>
  <li>Gradana akan memungut Service Fee sebesar 2.5% dari nilai transaksi berdasarkan harga KPA yang akan dibayarkan oleh Investee. Sebagai ilustrasi, Lender setuju untuk memberikan pinjaman berdasarkan Skema A. Apabila nilai properti yang harus dibayar oleh Lender senilai IDR 1 milyar, sementara harga KPA untuk property yang sama adalah IDR 1,2 milyar, maka Service Fee yang akan diterima Gradana adalah sejumlah IDR 30 juta (2.5% dari IDR 1,2 milyar).</li>
  <li>Service Fee tersebut akan langsung dipotong oleh Gradana dari setiap cicilan yang dibayarkan oleh Investee setiap bulannya ke virtual account yang dibuat oleh Gradana untuk masing-masing Investee. Gradana akan menyetorkan cicilan net setelah potongan Service Fee kepada Lender paling lambat 2 hari kerja setelah pembayaran cicilan oleh Investee diterima Gradana.</li>
</ol>

<strong>F.  ITIKAD BAIK</strong>
<ol>
  <li>Lender mengerti bahwa tujuan dari kerjasama ini adalah untuk memberikan dana talangan / pendanaan kepada Investee.</li>
  <li>Apabila Lender sewaktu-waktu melanggar maksud dan tujuan tersebut dan tidak menepati ketentuan yang telah disepakati, termasuk namun tidak terbatas pada kondisi dimana Lender tidak mau bekerjasama dalam pengalihan properti kepada Investee karena sebab apapun, maka Lender wajib mengembalikan uang yang telah diterima dari Investee ditambah memberikan kompensasi sejumlah harga properti berdasarkan nilai pasar wajar yang ditentukan oleh Gradana.</li>
</ol>

<strong>G.  INFORMASI LENDER</strong>
<ol>
  <li>Lender diwajibkan memberi data informasi pribadi yang sebenarnya. Selain itu Lender juga diwajibkan memberi keterangan kontak yang sebenarnya.</li>
  <li>Kami berhak untuk tidak memproses registrasi Lender yang tidak memenuhi persyaratan atau yang dicurigai tidak memberikan informasi yang sebenarnya.</li>
  <li>Kami berhak untuk membatasi atau menghentikan proses penghubungan Lender dengan Investee jika kami memiliki alasan untuk mencurigai bahwa Lender telah melanggar ketentuan dari Perjanjian ini atau peraturan perundang-undangan yang berlaku.</li>
  <li>Sanksi pidana atas pemalsuan atau pemberian keterangan palsu berlaku bagi Investee yang memberikan informasi atau data tidak benar atau palsu.</li>
</ol>
