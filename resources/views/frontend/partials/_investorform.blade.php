                        <div class="col-md-12">
                            {!! Form::open(array('url' => route('investorCreate'), 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}
                                <input type="hidden" id="userid" name="userid" value="{{ $user->id }}">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{ trans('investorform.image') }} *</label>
                                    <div class="col-md-5">
                                        <input type="file" id="img" name="img" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">{{ trans('investorform.kategori') }}</label>
                                    <div class="col-md-5">
                                        <select id="catid" name="catid" class="form-control" style="width: 100%" required>
                                          @foreach ($cat as $c)
                                            <option value="{{ $c->id }}"> {{ $c->nama }} </option>
                                          @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama" class="col-md-4 control-label">{{ trans('investorform.nama') }}</label>
                                    <div class="col-md-5">
                                        <input type="text" id="nama" name="nama" class="form-control" value="{{ $user->fullname }}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="alamat" class="col-md-4 control-label">{{ trans('investorform.alamat') }} *</label>
                                    <div class="col-md-5">
                                        <input type="text" id="alamat" name="alamat" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{ trans('investorform.usaha') }}</label>
                                    <div class="col-md-5">
                                        <input type="text" id="jenis_usaha" name="jenis_usaha" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">{{ trans('investorform.email') }} *</label>
                                    <div class="col-md-5">
                                        <input type="text" id="email" name="email" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">{{ trans('investorform.telp') }}</label>
                                    <div class="col-md-5">
                                        <input type="text" id="telp" name="telp" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">{{ trans('investorform.hp') }}</label>
                                    <div class="col-md-5">
                                        <input type="text" id="hp" name="hp" class="form-control">
                                    </div>
                                </div>
                                {{-- <div class="form-group">
                                    <label class="col-md-4 control-label">{{ trans('investorform.alokasi') }} *</label>
                                    <div class="col-md-5">
                                        <input type="text" id="alokasi_dana" name="alokasi_dana" class="form-control" required>
                                        <p class="help-block">{{ trans('investorform.alokasidesc') }}</p>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">{{ trans('investorform.jml_alokasi') }} *</label>
                                    <div class="col-md-5">
                                        <select id="jml_alokasi" id="jml_alokasi" name="jml_alokasi" class="form-control" style="width: 100%">
                                          @foreach($jmlAlokasi as $j)
                                            <option value="{{ $j }}">
                                              {{$j}}
                                            </option>
                                          @endforeach
                                        </select>
                                        <p class="help-block">{{ trans('investorform.jml_alokasidesc') }}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{ trans('investorform.tenor') }} *</label>
                                    <div class="col-md-5">
                                        <select id="tenor" name="tenor" class="form-control" style="width: 100%">
                                          @foreach($tenor as $t)
                                            <option value="{{ $t }}">
                                              {{$t}}
                                            </option>
                                          @endforeach
                                        </select>
                                        <p class="help-block">{{ trans('investorform.tenordesc') }}</p>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-4 control-label">{{ trans('investorform.penempatan') }} *</label>
                                    <div class="col-md-5">
                                        <select id="penempatan_dana" name="penempatan_dana" class="form-control" style="width: 100%">
                                          @foreach($penempatanDana as $p)
                                            <option value="{{ $p }}">
                                              {{$p}}
                                            </option>
                                          @endforeach
                                        </select>
                                        <p class="help-block">{{ trans('investorform.penempatandesc') }}</p>
                                    </div>
                                </div> --}}
                                <hr class="short">
                                <div class="form-group">
                                    <label class="col-md-4 control-label"></label>
                                    <div class="col-md-5">
                                        <input id="agree" name="agree" type="checkbox">
                                        <label for="agree">&nbsp;By checking, I agree to the <a id="term" href="#" data-toggle="modal" data-target="#largeModal">terms and conditions.</a></label>
                                    </div>
                                </div>
                                {{-- <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-4">
                                        <input type="submit" class="btn btn-primary" value="{{ trans('investorform.save') }}">
                                    </div>
                                </div> --}}

                                <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true" style="width:90%; margin:0 auto;">
                                  <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="largeModalLabel">Terms and Conditions for Lender</h4>
                                      </div>
                                      <div class="modal-body">
                                        @include('frontend.partials._termInvestor')
                                      </div>
                                      <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('investorform.cancel') }}</button>
                                          <button type="submit" class="btn btn-primary">{{ trans('investorform.save') }}</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                            {!! Form::close() !!}
                        </div>
