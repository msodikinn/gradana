                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.nama') }}</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control" value="{{ $user->fullname }}" readonly>
                                    <input type="hidden" name="userid" value="{{ $user->id }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.namaclean') }} *</label>
                                <div class="col-md-7">
                                    <input type="text" name="fullname_clean" class="form-control" >
                                    <p class="help-block">{{ trans('pemohon/pemohon.namacleandesc') }}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.lahir') }} *</label>
                                <div class="col-md-7">
                                    <input type="text" name="tmp_lahir" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">KTP *</label>
                                <div class="col-md-7">
                                    <input type="text" name="ktp" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">NPWP *</label>
                                <div class="col-md-7">
                                    <input type="text" name="npwp" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.ibu') }} *</label>
                                <div class="col-md-7">
                                    <input type="text" name="nama_ibu" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.statuskawin') }}</label>
                                <div class="col-md-7">
                                    <select id="status_kawin" name="status_kawin" class="form-control" style="width: 100%">
                                      @foreach($kawin as $code=>$s)
                                        <option value="{{ $code }}">
                                          {{$s}}
                                        </option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.tanggungan') }} *</label>
                                <div class="col-md-2">
                                    <input type="text" name="tanggungan" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.pendidikan') }}</label>
                                <div class="col-md-7">
                                    <select id="pendidikan" name="pendidikan" class="form-control" style="width: 100%">
                                      @foreach($pendidikan as $code=>$p)
                                        <option value="{{ $code }}">
                                          {{$p}}
                                        </option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.statusrumah') }}</label>
                                <div class="col-md-7">
                                    <select id="status_rumah" name="status_rumah" class="form-control" style="width: 100%">
                                      @foreach($rumah as $code=>$r)
                                        <option value="{{ $code }}">
                                          {{$r}}
                                        </option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.alamatktp') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="alamat_ktp" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.kelurahan') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="kelurahan_ktp" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.kecamatan') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="kecamatan_ktp" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.propinsi') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="propinsi_ktp" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.kota') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="kota_ktp" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.kodepos') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="postal_ktp" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.telp') }} *</label>
                                <div class="col-md-7">
                                    <input type="text" name="telp" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.fax') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="fax" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.hp1') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="hp1" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.hp2') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="hp2" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Facebook ID</label>
                                <div class="col-md-7">
                                    <input type="text" name="facebook_id" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Linkedin ID</label>
                                <div class="col-md-7">
                                    <input type="text" name="linkedin_id" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.balance') }} *</label>
                                <div class="col-md-7">
                                    <input type="text" name="amount_balance" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.ktpimg') }} *</label>
                                <div class="col-md-7">
                                    <input type="file" name="ktp_img" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.npwpimg') }} *</label>
                                <div class="col-md-7">
                                    <input type="file" name="npwp_img" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.kkimg') }} *</label>
                                <div class="col-md-7">
                                    <input type="file" name="kk_img" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.listrik') }}</label>
                                <div class="col-md-7">
                                    <input type="file" name="tagihan_listrik" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.air') }}</label>
                                <div class="col-md-7">
                                    <input type="file" name="tagihan_air" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.fototelp') }}</label>
                                <div class="col-md-7">
                                    <input type="file" name="tagihan_telp" class="form-control">
                                </div>
                            </div>
                            <!--div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.ketkerja') }}</label>
                                <div class="col-md-7">
                                    <input type="file" name="surat_ket_kerja" class="form-control">
                                </div>
                            </div-->
                            <div class="form-group">
                                <label class="col-md-3 control-label">BI Checking</label>
                                <div class="col-md-7">
                                    <input type="file" name="bi_checking" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.akta') }} *</label>
                                <div class="col-md-7">
                                    <input type="file" name="akta_img" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.ssp') }}</label>
                                <div class="col-md-7">
                                    <input type="file" name="ssp" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.custreview') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="cust_verified" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.reviewdate') }}</label>
                                <div class="col-md-7">
                                    <input type="date" name="cust_verified_date" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.jmlanggota') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="jml_anggota" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.wajib') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="simp_wajib" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.pokok') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="simp_pokok" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.dana') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="sumber_dana" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.usaha') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="bentuk_usaha" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.noakta') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="nomor_akta" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.domisili') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="domisili_akta" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.tanggungjwb') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="nama_penanggungjawab" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.cp') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="contact_person" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pemohon.posisicp') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="contact_position" class="form-control">
                                </div>
                            </div>
							
							
                          <input type="hidden" name="userid" value="{{$user->id}}">
                                
							   
                                <div class="form-group striped-col">
                                    <label class="col-md-3 control-label">Property</label>
                                    <div class="col-md-7">
                                        <select id="propid" name="propid" class="form-control" style="width: 100%">
                                          @foreach($prop as $p)
                                            <option value="{{ $p->id }}">
                                              {{$p->nama}}
                                            </option>
                                          @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Developer</label>
                                        <div class="col-md-7">
                                            <select id="devid" name="devid" class="form-control" style="width: 100%">
                                              @foreach($dev as $d)
                                                <option value="{{ $d->id }}">
                                                  {{$d->nama}}
                                                </option>
                                              @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group striped-col">
                                        <label class="col-md-3 control-label">Lender</label>
                                        <div class="col-md-7">
                                            <select id="investorid" name="investorid" class="form-control" style="width: 100%">
                                              @foreach($inves as $i)
                                                <option value="{{ $i->id }}">
                                                  {{$i->nama}}
                                                </option>
                                              @endforeach
                                            </select>
                                        </div>
                                    </div>
                              
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tujuan / Keperluan</label>
                                    <div class="col-md-7">
                                        <select id="tujuan" name="tujuan" class="form-control" style="width: 100%">
                                          @foreach($tujuan as $code=>$t)
                                            <option value="{{ $code }}">
                                              {{$t}}
                                            </option>
                                          @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-3 control-label">Jenis Aplikasi</label>
                                    <div class="col-md-7">
                                        <select id="jenis" name="jenis" class="form-control" style="width: 100%">
                                          @foreach($jenis as $code=>$j)
                                            <option value="{{ $code }}">
                                              {{$j}}
                                            </option>
                                          @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Uang Muka *</label>
                                    <div class="col-md-7">
                                        <input type="text" name="uang_muka" id="autonum1" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-3 control-label">Harga Property *</label>
                                    <div class="col-md-7">
                                        <input type="text" name="harga_property" id="autonum6" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Sistem Pembayaran</label>
                                    <div class="col-md-7">
                                        <select id="bayar" name="sis_bayar" class="form-control" style="width: 100%">
                                          @foreach($bayar as $code=>$b)
                                            <option value="{{ $code }}">
                                              {{$b}}
                                            </option>
                                          @endforeach
                                        </select>
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="col-md-3 control-label">Skema </label>
                                    <div class="col-md-7">
                                        <select id="scheme" name="scheme" class="form-control" style="width: 100%">
                                         
                                            <option value="skema_a">
                                            Skema A: Investor Membayar FULL kas keras ke developer  
                                            </option>
											<option value="skema_b">
                                            Skema B: Investor Hanya Membayar DP ke Developer Mengikuti harga DP  
                                            </option>
										 
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-3 control-label">Jumlah Pinjaman *</label>
                                    <div class="col-md-7">
                                        <input type="text" name="pinjaman" id="autonum2" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Pinjaman Bulanan *</label>
                                    <div class="col-md-7">
                                        <input type="text" name="pinjaman_bln" id="autonum3" class="form-control" required>
                                        <p class="help-block">pinjaman bulanan yang diajukan</p>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-3 control-label">Pinjaman (tahun) *</label>
                                    <div class="col-md-7">
                                        <input type="text" name="periode" class="form-control" required>
                                        <p class="help-block">dalam tahun</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Pinjaman (bulan) *</label>
                                    <div class="col-md-7">
                                        <input type="text" name="periode" class="form-control" required>
                                        <p class="help-block">dalam bulan</p>
                                    </div>
                                </div>
                                <div class="form-group striped-col">
                                    <label class="col-md-3 control-label">Status</label>
                                    <div class="col-md-7">
                                        <select id="status" name="status" class="form-control" style="width: 100%">
                                          @foreach($status as $code=>$s)
                                            <option value="{{ $code }}">
                                              {{$s}}
                                            </option>
                                          @endforeach
                                        </select>
                                    </div>
                                </div>
                               
                              
                               
                                
							
							
							
							
