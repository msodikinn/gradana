                                <div class="form-group">
                                    <label class="col-md-3 control-label">{{ trans('pemohon/keluarga.nama_keluarga') }}</label>
                                    <div class="col-md-7">
                                        <input type="text" name="nama_keluarga" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">{{ trans('pemohon/keluarga.alamat_keluarga') }}</label>
                                    <div class="col-md-7">
                                        <input type="text" name="alamat_keluarga" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">{{ trans('pemohon/keluarga.kelurahan_keluarga') }}</label>
                                    <div class="col-md-7">
                                        <input type="text" name="kelurahan_keluarga" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">{{ trans('pemohon/keluarga.kecamatan_keluarga') }}</label>
                                    <div class="col-md-7">
                                        <input type="text" name="kecamatan_keluarga" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">{{ trans('pemohon/keluarga.propinsi_keluarga') }}</label>
                                    <div class="col-md-7">
                                        <input type="text" name="propinsi_keluarga" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">{{ trans('pemohon/keluarga.kota_keluarga') }}</label>
                                    <div class="col-md-7">
                                        <input type="text" name="kota_keluarga" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">{{ trans('pemohon/keluarga.telp_keluarga') }}</label>
                                    <div class="col-md-7">
                                        <input type="text" name="telp_keluarga" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">{{ trans('pemohon/keluarga.hp_keluarga') }}</label>
                                    <div class="col-md-7">
                                        <input type="text" name="hp_keluarga" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">{{ trans('pemohon/keluarga.hub_keluarga') }}</label>
                                    <div class="col-md-7">
                                        <input type="text" name="hub_keluarga" class="form-control" >
                                    </div>
                                </div>
