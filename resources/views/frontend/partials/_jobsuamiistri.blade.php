                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.jenis_pekerjaan_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <select id="jenis_pekerjaan_jobsuami" name="jenis_pekerjaan_jobsuami" class="form-control" style="width: 100%">
                                      @foreach($jenis as $code=>$j)
                                        <option value="{{ $code }}">
                                          {{$j}}
                                        </option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.nama_perusahaan_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="nama_perusahaan_jobsuami" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.bidang_usaha_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <select id="bidang_usaha_jobsuami" name="bidang_usaha_jobsuami" class="form-control" style="width: 100%">
                                      @foreach($usaha as $code=>$j)
                                        <option value="{{ $code }}">
                                          {{$j}}
                                        </option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.bidang_usaha_detail_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="bidang_usaha_detail_jobsuami" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.tanggal_pendirian_jobsuami') }}</label>
                                <div class="col-md-3">
                                    <input type="date" name="tanggal_pendirian_jobsuami" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.alamat_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="alamat_jobsuami" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.kelurahan_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="kelurahan_jobsuami" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.kecamatan_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="kecamatan_jobsuami" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.kota_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="kota_jobsuami" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.postal_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="postal_jobsuami" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.tlp_ktr_hunting_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="tlp_ktr_hunting_jobsuami" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.tlp_ktr_direct_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="tlp_ktr_direct_jobsuami" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.fax_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="fax_jobsuami" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.unit_kerja_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="unit_kerja_jobsuami" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.jabatan_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="jabatan_jobsuami" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.bulan_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="bulan_jobsuami" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.tahun_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="tahun_jobsuami" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                              <h4 class="col-md-3  control-label" style="margin-bottom: 20px;">Untuk wiraswasta/profesi</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.omset_bln_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="omset_bln_jobsuami" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.kepemilikan_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="kepemilikan_jobsuami" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.margin_untung_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="margin_untung_jobsuami" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.nama_perusahaan_ago_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="nama_perusahaan_ago_jobsuami" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.jenis_usaha_ago_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="jenis_usaha_ago_jobsuami" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.jabatan_ago_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="jabatan_ago_jobsuami" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.unit_kerja_ago_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="unit_kerja_ago_jobsuami" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.telp_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="telp_jobsuami" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.bulan_ago_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="bulan_ago_jobsuami" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/jobpasangan.tahun_ago_jobsuami') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="tahun_ago_jobsuami" class="form-control">
                                </div>
                            </div>
