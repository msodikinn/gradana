                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pasangan.status_suami') }}</label>
                                <div class="col-md-6">
                                    <select id="status_suami" name="status_suami" class="form-control" style="width: 100%">
                                      @foreach($status as $code=>$s)
                                        <option value="{{ $code }}">
                                          {{$s}}
                                        </option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pasangan.nama_suami') }} *</label>
                                <div class="col-md-6">
                                    <input type="text" name="nama_suami" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pasangan.tempat_lahir_suami') }}</label>
                                <div class="col-md-6">
                                    <input type="text" name="tempat_lahir_suami" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pasangan.tgl_lahir_suami') }}</label>
                                <div class="col-md-3">
                                    <input type="date" name="tgl_lahir_suami" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pasangan.ktp_suami') }} *</label>
                                <div class="col-md-6">
                                    <input type="text" name="ktp_suami" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pasangan.npwp_suami') }} *</label>
                                <div class="col-md-6">
                                    <input type="text" name="npwp_suami" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pasangan.telp_suami') }} *</label>
                                <div class="col-md-6">
                                    <input type="text" name="telp_suami" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/pasangan.hp_suami') }}</label>
                                <div class="col-md-6">
                                    <input type="text" name="hp_suami" class="form-control">
                                </div>
                            </div>
