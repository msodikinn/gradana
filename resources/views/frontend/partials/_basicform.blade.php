                                        {!! Form::open(array('url' => route('applicantCreate'), 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'basic', 'files'=> true)) !!}
                                            <div style="width: 60%; margin:0 auto;">
                                                <p align="center" class="lead">{{ trans('basicform.basicdesc') }}</p>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">{{ trans('basicform.nama') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="namauser" class="form-control" value="{{ $user->fullname }}" readonly>
                                                    <input type="hidden" name="userid" value="{{ $user->id }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">{{ trans('basicform.namaclean') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="fullname_clean" class="form-control" required>
                                                    <p class="help-block">{{ trans('basicform.namacleandesc') }}</p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">{{ trans('basicform.statuskawin') }}</label>
                                                <div class="col-md-5">
                                                    <select id="status_kawin" name="status_kawin" class="form-control" style="width: 100%">
                                                      @foreach($kawin as $code=>$s)
                                                        <option value="{{ $code }}">
                                                          {{$s}}
                                                        </option>
                                                      @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label">{{ trans('basicform.pendidikan') }}</label>
                                                <div class="col-md-5">
                                                    <select id="pendidikan" name="pendidikan" class="form-control" style="width: 100%">
                                                      @foreach($pendidikan as $code=>$p)
                                                        <option value="{{ $code }}">
                                                          {{$p}}
                                                        </option>
                                                      @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">{{ trans('basicform.statusrumah') }}</label>
                                                <div class="col-md-5">
                                                    <select id="status_rumah" name="status_rumah" class="form-control" style="width: 100%">
                                                      @foreach($rumah as $code=>$r)
                                                        <option value="{{ $code }}">
                                                          {{$r}}
                                                        </option>
                                                      @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">{{ trans('basicform.lahir') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="tmp_lahir" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">KTP *</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="ktp" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">NPWP *</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="npwp" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">{{ trans('basicform.ibu') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="nama_ibu" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">{{ trans('basicform.tanggungan') }}</label>
                                                <div class="col-md-2">
                                                    <input type="text" name="tanggungan" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">{{ trans('basicform.alamatktp') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="alamat_ktp" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">{{ trans('basicform.telp') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="telp" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">{{ trans('basicform.balance') }}</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="amount_balance" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">{{ trans('basicform.ktpimg') }}</label>
                                                <div class="col-md-5">
                                                    <input type="file" name="ktp_img" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">{{ trans('basicform.npwpimg') }}</label>
                                                <div class="col-md-5">
                                                    <input type="file" name="npwp_img" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">{{ trans('basicform.kkimg') }}</label>
                                                <div class="col-md-5">
                                                    <input type="file" name="kk_img" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">{{ trans('basicform.aktaimg') }}</label>
                                                <div class="col-md-5">
                                                    <input type="file" name="akta_img" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label"></label>
                                                <div class="col-md-5">
                                                    <input id="agree" name="agree" type="checkbox">
                                                    <label for="agree">&nbsp;By checking, I agree to the <a id="term" href="#" data-toggle="modal" data-target="#largeModal">terms and conditions.</a></label>
                                                </div>
                                            </div>
                                            {{-- <div class="form-group form-actions">
                                                <div class="col-md-9">
                                                    <input type="submit" class="btn btn-primary pull-right" value="{{ trans('basicform.submit') }}">
                                                </div>
                                            </div> --}}

                                            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true" style="width:90%; margin:0 auto;">
                                              <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="largeModalLabel">Terms and Conditions for Lender</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    @include('frontend.partials._termBorrower')
                                                  </div>
                                                  <div class="modal-footer">
                                                      <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('investorform.cancel') }}</button>
                                                      <button type="submit" class="btn btn-primary">{{ trans('basicform.submit') }}</button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>

                                        {!! Form::close() !!}
