        <div class="slider-container rev_slider_wrapper" style="height: 600px;">
          <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options='{"delay": 9000, "gridwidth": 80%, "gridheight": 700}'>
            <ul>
              <li data-transition="fade">
                <img src="{{ asset('assets/img/home1.jpg') }}"
                  alt=""
                  data-bgposition="center center"
                  data-bgfit="cover"
                  data-bgrepeat="no-repeat"
                  data-kenburns="on"
                  data-duration="9000"
                  data-ease="Linear.easeNone"
                  data-scalestart="115"
                  data-scaleend="100"
                  data-rotatestart="0"
                  data-rotateend="0"
                  data-offsetstart="0 -200"
                  data-offsetend="0 200"
                  data-bgparallax="0"
                  class="rev-slidebg">

                <div class="tp-caption tp-caption-overlay tp-caption-overlay-primary main-label"
                  data-x="center"
                  data-y="205"
                  data-start="1000"
                  data-whitespace="nowrap"
                  data-transform_in="y:[100%];s:500;"
                  data-transform_out="opacity:0;s:500;"
                  data-mask_in="x:0px;y:0px;">{!! trans('slide.satu') !!}
                </div>

                <div class="tp-caption tp-caption-overlay-opacity bottom-label"
                  data-x="center"
                  data-y="298"
                  data-start="2000"
                  data-transform_in="y:[100%];s:500;">{!! trans('slide.satusub') !!}
                </div>
              </li>
              <li data-transition="fade">
                <img src="{{ asset('assets/img/home2.jpg') }}"
                  alt=""
                  data-bgposition="center center"
                  data-bgfit="cover"
                  data-bgrepeat="no-repeat"
                  data-kenburns="on"
                  data-duration="9000"
                  data-ease="Linear.easeNone"
                  data-scalestart="115"
                  data-scaleend="100"
                  data-rotatestart="0"
                  data-rotateend="0"
                  data-offsetstart="0 400px"
                  data-offsetend="0 -400px"
                  data-bgparallax="0"
                  class="rev-slidebg">

                <div class="tp-caption tp-caption-overlay tp-caption-overlay-primary main-label"
                  data-x="center"
                  data-y="205"
                  data-start="1000"
                  data-whitespace="nowrap"
                  data-transform_in="y:[100%];s:500;"
                  data-transform_out="opacity:0;s:500;"
                  data-mask_in="x:0px;y:0px;">{!! trans('slide.dua') !!}
                </div>

                <div class="tp-caption tp-caption-overlay-opacity bottom-label"
                  data-x="center"
                  data-y="298"
                  data-start="2000"
                  data-transform_in="y:[100%];s:500;">{!! trans('slide.duasub') !!}
                </div>
              </li>
              <li data-transition="fade">
                <img src="{{ asset('assets/img/home5.jpg') }}"
                  alt=""
                  data-bgposition="center center"
                  data-bgfit="cover"
                  data-bgrepeat="no-repeat"
                  data-kenburns="on"
                  data-duration="9000"
                  data-ease="Linear.easeNone"
                  data-scalestart="115"
                  data-scaleend="100"
                  data-rotatestart="0"
                  data-rotateend="0"
                  data-offsetstart="0 400px"
                  data-offsetend="0 -400px"
                  data-bgparallax="0"
                  class="rev-slidebg">

                <div class="tp-caption tp-caption-overlay tp-caption-overlay-primary main-label"
                  data-x="center"
                  data-y="205"
                  data-start="1000"
                  data-whitespace="nowrap"
                  data-transform_in="y:[100%];s:500;"
                  data-transform_out="opacity:0;s:500;"
                  data-mask_in="x:0px;y:0px;">{!! trans('slide.tiga') !!}
                </div>

                  <div class="tp-caption tp-caption-overlay-opacity bottom-label"
                    data-x="center"
                    data-y="298"
                    data-start="2000"><center>{!! trans('slide.tigasub') !!}</center>
                  </div>

              </li>
            </ul>
          </div>
        </div>
