                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/perbankan.sumber_info_perbankan') }}</label>
                                <div class="col-md-7">
                                    <input type="text" name="sumber_info_perbankan" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/perbankan.rekening_koran') }}</label>
                                <div class="col-md-7">
                                    <input type="file" name="rekening_koran" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ trans('pemohon/perbankan.rekening_koran2') }}</label>
                                <div class="col-md-7">
                                    <input type="file" name="rekening_koran2" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-7">
                                    <input id="agree2" name="agree" type="checkbox">
                                    <label for="agree">&nbsp;By submitting, I agree to the <a id="term2" href="#" data-toggle="modal" data-target="#largeModal2">terms and conditions.</a></label>
                                </div>
                            </div>

                            <div class="modal fade" id="largeModal2" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true" style="width:90%; margin:0 auto;">
                              <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="largeModalLabel">Terms and Conditions for Borrower</h4>
                                  </div>
                                  <div class="modal-body">
                                    @include('frontend.partials._termBorrower')
                                  </div>
                                  <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('investorform.cancel') }}</button>
                                      <button type="submit" class="btn btn-primary">{{ trans('basicform.submit') }}</button>
                                  </div>
                                </div>
                              </div>
                            </div>
