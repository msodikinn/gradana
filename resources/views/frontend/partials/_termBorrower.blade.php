<center><h2>SYARAT DAN KETENTUAN GRADANA <br>
BAGI PEMINJAM</h2></center>
<p>www.Gradana.com (“Situs”) adalah Situs yang dikelola oleh PT Gradana Indonesia (“Gradana”) yang untuk selanjutnya disebut “Kami”. Syarat dan ketentuan berikut merupakan perjanjian hukum (“PERJANJIAN ”) antara Investee dengan Gradana yang mengatur penggunaan layanan Gradana Indonesia.</p>
<p>Investee harus membaca dan memahami semua persyaratan dan ketentuan dalam Perjanjian ini sebelum menggunakan fitur layanan dan/atau menerima konten yang terdapat di dalam Situs. Apabila Investee menekan tombol “Setuju” pada kolom yang disediakan, maka Investee telah menerima dan menyetujui seluruh persyaratan dan ketentuan dalam Perjanjian untuk menggunakan dan menerima layanan dan akses atas seluruh konten atau fitur layanan yang terdapat dalam Situs.</p>

<strong>A.  PENDAHULUAN</strong>
<ol>
  <li>Perjanjian ini diatur dan diinterpretasikan berdasarkan Hukum Republik Indonesia.</li>
  <li>Kami dapat mengubah atau memperbarui Perjanjian ini setiap waktu dengan mencantumkan Perjanjian yang telah diubah atau diperbarui di dalam Situs dan/atau media lain. Perjanjian yang telah diubah dan diperbarui tersebut akan berlaku setelah 3 (tiga) hari kalender terhitung sejak tanggal diumumkan di Situs. Investee dianggap mengetahui dan menyetujui perubahan atau pembaruan atas Perjanjian layanan Situs apabila Investee terus menggunakan Situs setelah perubahan atau pembaruan tersebut berlaku dan tercantum pada Situs. Oleh karenanya Investee diharapkan untuk memeriksa halaman ini secara berkala agar mengetahui perubahan-perubahan tersebut.</li>
  <li>Kami juga dapat mengubah, menghapus, menambah atau memperbarui fitur atau fasilitas dalam Situs ini setiap waktu tanpa mengumumkan terlebih dahulu mengenai hal tersebut.</li>
</ol>

<strong>B.  LATAR BELAKANG KERJASAMA</strong>
<ol>
  <li>Situs yang Kami kelola adalah platform pendanaan point to point dimana Gradana akan mempertemukan Investor dan Investee melalui platform ini dimana investor dapat membiayai pembelian properti untuk kepentingan Investee berdasarkan Skema A atau Skema B (“Platform”).</li>
  <li>Dalam Skema A, Investor akan membiayai penuh harga properti kepada pengembang dan memperoleh properti tersebut untuk kepentingan Investee. Investee akan membayar cicilan dana talangan kepada Investor berdasarkan harga KPA / KPR. Pada saat cicilan Investee mencapai sedikitnya 20% dari harga KPA/ KPR, Investee akan mengajukan dan memperoleh pinjaman dari bank untuk melunasi sisa dari harga KPA/KPR tersebut kepada Investor dan melanjutkan cicilan dengan bank.</li>
  <li>Dalam Skema B, Investor akan membayar sebagian dari harga properti sebagai dana talangan kepada pengembang dan memperoleh properti tersebut untuk kepentingan Investee. Sama halnya dengan Skema A, Investee akan membayar cicilan terhadap dana talangan kepada Investor berdasarkan harga KPA / KPR. Pada saat Investee melunasi cicilan dana talangan tersebut, maka Investee akan mengajukan dan memperoleh pinjaman dari bank untuk melanjutkan cicilan dengan bank.</li>
  <li>Investee dapat membayar cicilan dana talangan setiap bulannya kepada Investor.</li>
  <li>Dana talangan sedikitnya senilai 20% dari harga properti atau nilai yang ditentukan oleh pengembang, Investor dan Investee atau nilai lain yang disepakati antara para pihak.</li>
  <li>Setelah Investee membayar lunas dana talangan kepada Investor, Investee dapatmelanjutkan cicilan pembelian properti kepada Bank atau Investor (yang sama atau lainnya) apabila Investee tidak mendapatkan persetujuan dari Bank.</li>
</ol>

<strong>C.  MEKANISME KERJASAMA:</strong>
<ol>
  <li>Investee (atau perwakilan penjualan pengembang) akan menghubungi Gradana setelah Investee memilih unit yang akan dibeli (dari pengembang / pemilik properti) setelah Investee membayar Booking Fee dan menandatangani Form Penjualan.</li>
  <li>Investee berkewajiban untuk melengkapi formulir aplikasi dan questionaire yang diberikan oleh Gradana dengan sebenar-benarnya serta melengkapi dokumen-dokumen persyaratan yang diminta dan membayar biaya yang berlaku.</li>
  <li>Setelah data diterima secara lengkap oleh Gradana, pihak Gradana akan melakukan penilaian dan membuatkan profile bagi Investee untuk ditampilkan di Situs Gradana. Mengingat bahwa masa reservasi unit antara pengembang dan Gradana hanya berlaku selama 14 hari sejak pengembang memberikan pemberitahuan awal terkait maksud Investee untuk melakukan pembelian unit melalui Gradana, maka Investee diharapkan dapat mengumpulkan data yang diminta secepatnya agar pihak Gradana dapat melakukan penilaian dan menampilkan profile Investee beserta properti yang ingin dibeli  kepada calon Investor.</li>
  <li>Apabila ada Investor yang berminat dan memilih Investee, kedua belah pihak akan menandatangani suatu perjanjian tertulis untuk memberikan dana talangan untuk kepentingan Investee, yang akan disaksikan oleh Gradan dan Pengembang.</li>
  <li>Investor akan menandatangani Surat Pesanan dan/atau Perjanjian Pengikatan Jual Beli (“PPJB”) dengan pengembang sehingga dokumen yang sekarang ada atas nama Investor, dimana didalamnya tercantum bahwa (i) perjanjian ini dapat dinovasikan kepada Pembeli (tanpa biaya admin apapun) setelah segala kewajiban Investee kepada Investor telah selesai dan (ii) Investor memberikan kuasa kepada Pengembang untuk mengalihkan kepemilikan Unit Properti yang dibiayai oleh Investor kepada Pembeli pada saat Pembeli telah memenuhi seluruh kewajiban pembayarannya kepada Investor dan Pengembang.</li>
  <li>Investee akan membayar cicilan bulanan kepada Investor sesuai perjanjian yang telah ditandatangani. Apabila Investee lalai dalam melakukan pembayaran cicilan bulanan, maka Investee dapat dikenakan sanksi denda atau unit yang dibiayai dapat diambilalih oleh Investor apabila Investee lalai melakukan pembayaran cicilan selama 3 bulan secara berturut-turut.</li>
  <li>Setelah Investee memenuhi seluruh kewajiban pembayaran dana talangan kepada Investor, dan Investee berhasil memperoleh pinjaman KPA/KPR dari bank untuk melunasi sisa pembayaran unit, maka Investee akan menandatangani Akta Jual Beli dengan pengembang. PPJB akan berakhir dengan  ditandatanganinya Akta Jual Beli antara pengembang dan Investee.</li>
  <li>Apabila Investee tidak berhasil mendapatkan pinjaman dari bank untuk melunasi sisa harga unit setelah Investee memenuhi kewajibannya dalam melunasi dana talangan kepada Investor, maka:

<p>(i) Untuk Skema A,  Investee dapat melanjutkan cicilan terhadap unit kepada Investor berdasarkan kesepakatan antara Investor dan Investee, atau Investee dapat memilih pengembalian uang ; atau</p>

<p>(ii)  Untuk Skema B, Investee dapat melakukan pembayaran cicilan secara langsung kepada Investor apabila Investor setuju untuk membayar lunas sisa dari Harga Tunai Skema B, atau Investee dapat memilih pengembalian uang.
Pengaturan lebih lanjut terkait hal diatas tercantum dalam perjanjian antara Investee dan Investor.</p></li>
</ol>

<strong>D.  BIAYA</strong>
<ol>
  <li>Biaya admisitrasi dan verifikasi data yang akan dikenakan oleh Gradana terhadap Investee adalah Rp 500,000 yang berlaku selama 3 bulan sejak tanggal pendaftaran.</li>
  <li>Apabila ternyata dalam waktu 7 hari kerja setelah pendaftaran Investee disetujui Gradana dan profile Investee dipost di portal, namun tidak ada Investor yang memilih untuk membiayai pembelian unit oleh Investee, Investee dapat memilih unit property lain dan Gradana akan mencarikan Investor lain.</li>
  <li>Apabila sewaktu-waktu dalam masa 3 bulan sejak pendaftaran, Investee memilih untuk memberhentikan atau membatalkan pencarian Investor, Gradana akan mengembalikan sebagian biaya administrasi dan verifikasi data sejumlah Rp 250,000. Apabila Investee memilih untuk meneruskan jasa pencarian setelah 3 bulan, maka Investee wajib membayar biaya tambahan sebesar Rp.250,000 yang berlaku untuk 3 bulan ke depannya.</li>
</ol>

<strong>E.  INFORMASI INVESTEE</strong>
<ol>
  <li>Investee diwajibkan memberi data informasi pribadi yang sebenarnya. Selain itu Investee juga diwajibkan memberi keterangan kontak yang sebenarnya.</li>
  <li>Kami berhak untuk tidak memproses registrasi Investee yang tidak memenuhi persyaratan atau yang dicurigai tidak memberikan informasi yang sebenarnya.</li>
  <li>Kami berhak untuk membatasi atau menghentikan proses penghubungan Investor dengan Investee jika kami memiliki alasan untuk mencurigai bahwa Investee telah melanggar ketentuan dari Perjanjian ini atau peraturan perundang-undangan yang berlaku.</li>
  <li>Apabila ditemukan bahwa data-data, informasi atau dokumen yang diberikan oleh Investee adalah palsu atau tidak benar, maka uang aplikasi sejumlah Rp 500,000 akan hangus dan nama Investee akan dimasukan di daftar hitam dan tidak dapat melakukan aplikasi kembali ke depannya.</li>
  <li>Sanksi pidana atas pemalsuan atau pemberian keterangan palsu berlaku bagi Investee yang memberikan informasi atau data tidak benar atau palsu.</li>
</ol>
