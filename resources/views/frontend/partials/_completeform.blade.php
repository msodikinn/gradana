<div class="container">
    <!--Content Section Start -->
    <div class="row">
        <div class="box">
            @include('notifications')
            @if($errors->has())
                @foreach ($errors->all() as $error)
                    <div class="text-danger">{{ $error }}</div>
                @endforeach
            @endif

            {!! Form::open(array('url' => route('applicantCreate'), 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true, 'id' => 'commentForm')) !!} {{-- ini dirubah untuk save dalam banyak table berbeda --}}
                <div style="width: 90%; margin:0 auto;">
                        <p align="center" class="lead">{{ trans('completeform.titledesc') }}</p>
                </div>

                <div id="rootwizard">
                    <div>
                        <ul style="display: table; margin: auto; margin-bottom: 30px;">
                            <li><a href="#tab1" data-toggle="tab">1. {{ trans('completeform.pemohon') }}</a></li>
                            <li><a href="#tab2" data-toggle="tab">2. {{ trans('completeform.jobpemohon') }}</a></li>
                            <li><a href="#tab3" data-toggle="tab">3. {{ trans('completeform.pasangan') }}</a></li>
                            <li><a href="#tab4" data-toggle="tab">4. {{ trans('completeform.jobpasangan') }}</a></li>
                            <li><a href="#tab5" data-toggle="tab">5. {{ trans('completeform.keluarga') }}</a></li>
                            <li><a href="#tab6" data-toggle="tab">6. {{ trans('completeform.penghasilan') }}</a></li>
                            <li><a href="#tab7" data-toggle="tab">7. {{ trans('completeform.perbankan') }}</a></li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        
					
						
						<div class="tab-pane" id="tab1">
                            @include('frontend.partials._pemohonform')
                        </div>

                        <div class="tab-pane" id="tab2" disabled="disabled">
                            @include('frontend.partials._jobpemohon')
                        </div>

                        <div class="tab-pane" id="tab3" disabled="disabled">
                            @include('frontend.partials._suamiistri')
                        </div>

                        <div class="tab-pane" id="tab4" disabled="disabled">
                            @include('frontend.partials._jobsuamiistri')
                        </div>

                        <div class="tab-pane" id="tab5" disabled="disabled">
                            @include('frontend.partials._keluarga')
                        </div>

                        <div class="tab-pane" id="tab6" disabled="disabled">
                            @include('frontend.partials._penghasilan')
                        </div>

                        <div class="tab-pane" id="tab7" disabled="disabled">
                            @include('frontend.partials._perbankan')
                        </div>

                        <ul class="pager wizard">
                            <li class="previous"><a href="#applicantForm">{{ trans('completeform.prev') }}</a></li>
                            <li class="next"><a href="#applicantForm">{{ trans('completeform.next') }}</a></li>
                            <!-- <input type="submit" class="btn btn-block btn-primary" value="Sign up" name="submit"> -->
                            <li class="next finish" style="display:none;"><a href="javascript: submitform()">{{ trans('completeform.finish') }}</a></li>
                        </ul>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!-- //Content Section End -->
</div>
