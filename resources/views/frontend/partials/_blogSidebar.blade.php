            <div class="col-md-3">
              <aside class="sidebar">

                {{-- <form>
                  <div class="input-group input-group-lg">
                    <input class="form-control" placeholder="Search..." name="s" id="s" type="text">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                </form>

                <hr> --}}

                {{-- <h4 class="heading-primary">Categories</h4>
                <ul class="nav nav-list mb-xlg">
                  <li><a href="#">Technology</a></li>
                </ul> --}}

                <div class="tabs mb-xlg">
                  <h4 class="heading-primary">Recent Posts</h4>
                  <div class="tab-content" style="border: 0">
                    <div class="tab-pane active" id="recentPosts">
                      <ul class="simple-post-list">
                        @foreach ($recent as $r)
                          <li>
                            <div class="post-image">
                              <div class="img-thumbnail">
                                <a href="blog-post.html">
                                  <img src="{{ '/uploads/blog/'.$r->image }}" style="max-width: 50px;">
                                </a>
                              </div>
                            </div>
                            <div class="post-info">
                              <a href="{{ '/blog/'.$r->slug }}">{{ $r->title }}</a>
                              <div class="post-meta">
                                 {{ date('M d, y', strtotime($r->publishdate)) }}
                              </div>
                            </div>
                          </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </div>

              </aside>
            </div>
