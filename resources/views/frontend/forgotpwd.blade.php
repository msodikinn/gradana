@extends('frontend/layouts/default')

@section('title')
  Forgot Password
@parent
@stop

@section('header_styles')

@stop

@section('content')
    <div class="body">
        @include('frontend.layouts._header')
        <div role="main" class="main">
          <section class="page-header">
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <ul class="breadcrumb">
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12" style="margin-top: 45px">
                </div>
              </div>
            </div>
          </section>
          <div class="container">
              <div class="row">
                  <div class="box animation flipInX">
                      <center><h3 class="heading-primary">Forgot Password</h3></center>
                      <div style="width:60%; margin:0 auto;">
                          <p>Enter your email to send the password</p>
                          @include('notifications')
                          <form action="{{ route('forgot-password') }}" class="omb_loginForm" autocomplete="off" method="POST">
                              {!! Form::token() !!}
                              <div class="form-group">
                                  <label class="sr-only"></label>
                                  <input type="email" class="form-control email" name="email" placeholder="Email"
                                         value="{!! old('email') !!}">
                                  <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                              </div>
                              <div class="form-group">
                                  <input class="form-control btn btn-primary btn-block" type="submit" value="Reset Your Password">
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
          @include('frontend.layouts._footer')
        </div>
    </div>
@stop

@section('footer_styles')
    {{-- kosong --}}
@stop
