@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
Coming Soon
@parent
@stop

@section('header_styles')
    <style>
        #header .header-body {
            min-height: 50px;
        }
    </style>
 @stop

@section('content')
    <div class="body coming-soon">
      <header id="header" data-plugin-options='{"stickyEnabled": false}'>
        <div class="header-body">
          <div class="header-top">
            <div class="container">
              <p>
                Get in touch! {{-- <span class="ml-xs"><i class="fa fa-phone"></i> (123) 456-789</span> --}}<span> <a href="mailto:admin@gradana.com">admin@gradana.com</a></span>
              </p>
              <ul class="header-social-icons social-icons hidden-xs">
                <li class="social-icons-facebook"><a href="https://www.facebook.com/profile.php?id=100012728464456" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                <li class="social-icons-twitter"><a href="http://www.twitter.com/gradanaweb" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                <li class="social-icons-linkedin"><a href="https://www.linkedin.com/in/gradana-property-8121a1124" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </header>

      <div role="main" class="main">
        <div class="container">
          <div class="row">
            <div class="col-md-12 center">
              <div class="logo">
                <a href="/">
                  <img src="{{ asset('assets/img/gradana_logo_fin_350.jpg') }}" alt="Gradana">
                </a>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 center">
              <h1 class="mb-sm small">New Mortgage Financing Solution to Live A Dream Home</h1>
              <p class="mb-none lead">OUR WEBSITE IS LAUNCHED SOON!</p>
              <p class="mb-none lead">Write your email address here for newsletter subscription:</p>
            </div>
          </div>
          </div>
          <div class="row" style="margin-top: 50px; margin-bottom: 20px">
            <div class="col-md-6 col-md-offset-3" id="subs">
              <div class="newsletter">
                <h5 class="heading-primary" style="margin-left: 20px;">Sign up to get notified when we launch</h5>
                {!! Form::open(array('class' => 'form-horizontal','url' => route('sendsub'), 'method' => 'post')) !!}
                {{-- <form action="{{ route('subscribe-comingsoon') }}" method="POST" class="form-horizontal">
                  {{ csrf_field() }} --}}
                  <div class="input-group">
                    <input class="form-control" placeholder="Email Address" name="email" type="text" style="margin-left: 20px;" required>
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit" style="margin-right: 20px;margin-left: 20px;">Subscribe Now</button>
                    </span>
                  </div>
                {!! Form::close() !!}
                {{-- </form> --}}
              </div>
            </div>
          </div>
        </div>
      </div>

      <footer class="short" id="footer">
        <div class="container">
          <div class="row">
            <div class="col-md-8">
              <h4 class="heading-primary">About Gradana</h4>
              <p>Gradana is a platform that makes people more easily find mortgage fund sources outside banks. On Gradana, we simplify mortgage application process to be more user friendly and time efficient. We invite creditors to distribute loan based on their own discretions for mortgage applicants that pass our credit assessment process.</p>
              <hr class="light">
            </div>
            <div class="col-md-3 col-md-offset-1">
              <h5 class="mb-sm">Contact Us</h5>
              <p class="mb-none">Palem Raya Street No. 440 - West Jakarta</p>
              <ul class="list list-icons list-icons-sm">
                <li><i class="fa fa-envelope"></i> <a href="mailto:admin@gradana.com">admin@gradana.com</a></li>
              </ul>
              <ul class="social-icons">
                <li class="social-icons-facebook"><a href="https://www.facebook.com/profile.php?id=100012728464456" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                <li class="social-icons-twitter"><a href="http://www.twitter.com/gradanaweb" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                <li class="social-icons-linkedin"><a href="https://www.linkedin.com/in/gradana-property-8121a1124" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="footer-copyright">
          <div class="container">
            <div class="row">
              <div class="col-md-1" style="margin-right: 0px; padding-right: 0px;">
                <a href="index.html" class="logo">
                  <img alt="Porto Website Template" class="img-responsive" src="{{ asset('assets/img/gradana-trans-small-gray.png') }}">
                </a>
              </div>
              <div class="col-md-11" style="margin-top: 30px;">
                <p>Gradana.com © Copyright 2016. All Rights Reserved.</p>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
@stop

