@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
  FAQ
@parent
@stop

@section('header_styles')
    <!-- Current Page CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">
    <!-- Demo CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
@stop

@section('content')
  <div class="body">
    @include('frontend.layouts._header')
    <div role="main" class="main">
      <section class="page-header">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <ul class="breadcrumb">
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12" style="margin-top: 45px">
            </div>
          </div>
        </div>
      </section>
      <div class="container">

        <h2>Frequently Asked <strong>Questions</strong></h2>

        <div class="row">
          <div class="col-md-12">
            <p class="lead">
              {{ trans('faq.desc') }}
            </p>
          </div>
        </div>

        <hr>

        <div class="row">
          <div class="col-md-12">

            <div class="row">
              <div class="col-md-12">
                <p class="lead">
                  {!! trans('faq.peminjam') !!}
                </p>
              </div>
            </div>

            <div class="toggle toggle-primary" data-plugin-toggle>
              <section class="toggle active">
                <label>{{ trans('faq.faqpeminjam1') }}</label>
                <p style="height:0px;">{{ trans('faq.faqpeminjam1desc') }}</p>
              </section>

              <section class="toggle">
                <label>{{ trans('faq.faqpeminjam2') }}</label>
                <p style="height:0px;">{{ trans('faq.faqpeminjam2desc') }}</p>
              </section>

              <section class="toggle">
                <label>{{ trans('faq.faqpeminjam3') }}</label>
                <p style="height:0px;">{{ trans('faq.faqpeminjam3desc') }}</p>
              </section>

              <section class="toggle">
                <label>{{ trans('faq.faqpeminjam4') }}</label>
                <p style="height:0px;">{{ trans('faq.faqpeminjam4desc') }}</p>
              </section>
              <section class="toggle">
                <label>{{ trans('faq.faqpeminjam5') }}</label>
                <p style="height:0px;">{{ trans('faq.faqpeminjam5desc') }}</p>
              </section>
              <section class="toggle">
                <label>{{ trans('faq.faqpeminjam6') }}</label>
                <p style="height:0px;">{{ trans('faq.faqpeminjam6desc') }}</p>
              </section>
              <section class="toggle">
                <label>{{ trans('faq.faqpeminjam7') }}</label>
                <p style="height:0px;">{{ trans('faq.faqpeminjam7desc') }}</p>
              </section>
              <section class="toggle">
                <label>{{ trans('faq.faqpeminjam8') }}</label>
                <p style="height:0px;">{{ trans('faq.faqpeminjam8desc') }}</p>
              </section>
              <section class="toggle">
                <label>{{ trans('faq.faqpeminjam9') }}</label>
                <p style="height:0px;">{{ trans('faq.faqpeminjam9desc') }}</p>
              </section>
              <section class="toggle">
                <label>{{ trans('faq.faqpeminjam10') }}</label>
                <p style="height:0px;">{{ trans('faq.faqpeminjam10desc') }}</p>
              </section>
              <section class="toggle">
                <label>{{ trans('faq.faqpeminjam11') }}</label>
                <p style="height:0px;">{{ trans('faq.faqpeminjam11desc') }}</p>
              </section>
              <section class="toggle">
                <label>{{ trans('faq.faqpeminjam12') }}</label>
                <p style="height:0px;">{{ trans('faq.faqpeminjam12desc') }}</p>
              </section>
              <section class="toggle">
                <label>{{ trans('faq.faqpeminjam13') }}</label>
                <p style="height:0px;">{{ trans('faq.faqpeminjam13desc') }}</p>
              </section>
              <section class="toggle">
                <label>{{ trans('faq.faqpeminjam14') }}</label>
                <p style="height:0px;">{{ trans('faq.faqpeminjam14desc') }}</p>
              </section>
              <section class="toggle">
                <label>{{ trans('faq.faqpeminjam15') }}</label>
                <p style="height:0px;">{{ trans('faq.faqpeminjam15desc') }}</p>
              </section>

              <hr>

              <div class="row">
                <div class="col-md-12">
                  <p class="lead">
                    {!! trans('faq.investors') !!}
                  </p>
                </div>
              </div>

              <section class="toggle">
                <label>{{ trans('faq.faqinvestors1') }}</label>
                <p style="height:0px;">{{ trans('faq.faqinvestors1desc') }}</p>
              </section>
              <section class="toggle">
                <label>{{ trans('faq.faqinvestors2') }}</label>
                <p style="height:0px;">{{ trans('faq.faqinvestors2desc') }}</p>
              </section>
              <section class="toggle">
                <label>{{ trans('faq.faqinvestors3') }}</label>
                <p style="height:0px;">{{ trans('faq.faqinvestors3desc') }}</p>
              </section>
              <section class="toggle">
                <label>{{ trans('faq.faqinvestors4') }}</label>
                <p style="height:0px;">{{ trans('faq.faqinvestors4desc') }}</p>
              </section>
              <section class="toggle">
                <label>{{ trans('faq.faqinvestors5') }}</label>
                <p style="height:0px;">{{ trans('faq.faqinvestors5desc') }}</p>
              </section>
            </div>

          </div>

        </div>

      </div>
    </div>
    @include('frontend.layouts._footer')
  </div>
@stop

@section('footer_styles')

@stop
