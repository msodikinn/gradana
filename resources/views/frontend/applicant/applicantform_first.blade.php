@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
History Lender
@parent
@stop

@section('header_styles')
  <!-- Current Page CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">
  <!-- Demo CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
  <style>
    section.section {
      margin: 0;
    }
    .counters strong {
      display: inline-table;
    }
  </style>
@stop

@section('content')
  <div class="body">
    @include('frontend.layouts._header')
    <div role="main" class="main">
    @include('frontend.partials._borrowerjumbotron')<br><br>
      <div class="container">
             <div class="row">
        <div class="col-md-12" id="applicantForm">
          <h2 align="center" style="margin-bottom: 10px;">REGISTRATION FORM</h2>
          <div class="divider divider-primary divider-small divider-small-center mb-xl">
          <hr>
          </div>
          <div class="row" style="margin-right: 0px; margin-left: 0px;">
            <div class="col-md-12">
              <div class="tabs tabs-bottom tabs-center tabs-simple">
              {!! Form::open(array('url' => route('applicantCreate'), 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'basic', 'files'=> true)) !!}

                  <div class="form-group">
                    <label class="col-md-4 control-label">Fullname</label>
                    <div class="col-md-5">
                      <input type="text" name="fullname" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Address</label>
                    <div class="col-md-5">
                      <input type="text" name="address" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Phone</label>
                    <div class="col-md-5">
                      <input type="text" name="phone" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Email</label>
                    <div class="col-md-5">
                      <input type="email" name="email" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="text-center">
                      <a href="applicant/form" class="btn btn-primary">CONTINUE TO NEXT STEP</a><br>
                      OR<br>
                      <a href="applicant/quick-signup" type="submit" class="btn btn-primary">QUICK SIGN UP</a><br>
                      (Gradana team will contact you within 24 hours to help you complete the registration)
                    </div>
                  </div>

              {!! Form::close() !!}

              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
    @include('frontend.layouts._footer')
  </div>
@stop

@section('footer_styles')
  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>

  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
@stop
