@if(empty($detailPenghasilan))
<div class="row">
  <div class="col-md-12">
    <div class="alert alert-danger">
      <strong>Data Penghasilan is empty</strong>
    </div>
  </div>
</div>
@else
<div class="col-md-12">
    <table class="table table-bordered">
        <tr><td width="200">ID</td><td>{{ $detailPenghasilan->id }}</td></tr>
        <tr><td>Nama Pemohon</td><td>{{ $detailPenghasilan->user->fullname }}</td></tr>
        <tr><td>Joint Income</td><td>{{ $detailPenghasilan->joint_income }}</td></tr>
        <tr><td>Penghasilan Pemohon</td><td>{{ $detailPenghasilan->pemohon }}</td></tr>
        <tr><td>Penghasilan Suami/Istri</td><td>{{ $detailPenghasilan->suami_istri }}</td></tr>
        <tr><td>Penghasilan Tambahan</td><td>{{ $detailPenghasilan->tambahan }}</td></tr>
        <tr><td>Biaya Rumah Tangga</td><td>{{ $detailPenghasilan->biaya_rt }}</td></tr>
        <tr><td>Angsuran Lain</td><td>{{ $detailPenghasilan->angsuran_lain }}</td></tr>
    </table>
</div>
@endif
