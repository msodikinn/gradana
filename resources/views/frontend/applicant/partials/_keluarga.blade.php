@if(empty($detailKeluarga))
<div class="row">
  <div class="col-md-12">
    <div class="alert alert-danger">
      <strong>Data Keluarga is empty</strong>
    </div>
  </div>
</div>
@else
<div class="col-md-12">
    <table class="table table-bordered">
        <tr><td width="200">ID</td><td>{{ $detailKeluarga->id }}</td></tr>
        <tr><td>Nama Pemohon</td><td>{{ $detailKeluarga->user->fullname }}</td></tr>
        <tr><td>Kepala Keluarga</td><td>{{ $detailKeluarga->nama }}</td></tr>
        <tr><td>Alamat</td><td>{{ $detailKeluarga->alamat }}</td></tr>
        <tr><td>kelurahan</td><td>{{ $detailKeluarga->kelurahan }}</td></tr>
        <tr><td>kecamatan</td><td>{{ $detailKeluarga->kecamatan }}</td></tr>
        <tr><td>propinsi</td><td>{{ $detailKeluarga->propinsi }}</td></tr>
        <tr><td>Kota</td><td>{{ $detailKeluarga->kota }}</td></tr>
        <tr><td>Telepon</td><td>{{ $detailKeluarga->telp }}</td></tr>
        <tr><td>Handphone</td><td>{{ $detailKeluarga->hp }}</td></tr>
        <tr><td>Hubungan</td><td>{{ $detailKeluarga->hub }}</td></tr>
    </table>
</div>
@endif
