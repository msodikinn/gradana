@if(empty($detailJobPemohon))
<div class="row">
  <div class="col-md-12">
    <div class="alert alert-danger">
      <strong>Data Job is empty</strong>
    </div>
  </div>
</div>
@else
<div class="col-md-6">
    <table class="table table-bordered">
        <tr><td width="200">Id</td><td>{{ $detailJobPemohon->id }}</td></tr>
        <tr><td>Userid</td><td>{{ $detailJobPemohon->user->fullname }}</td></tr>
        <tr><td>Jenis</td><td>{{ $detailJobPemohon->jenis_pekerjaan }}</td></tr>
        <tr><td>Nama Perusahaan</td><td>{{ $detailJobPemohon->nama_perusahaan }}</td></tr>
        <tr><td>Bidang Usaha</td><td>{{ $detailJobPemohon->bidang_usaha }}</td></tr>
        <tr><td>Bidang Usaha Detail</td><td>{{ $detailJobPemohon->bidang_usaha_detail }}</td></tr>
        <tr><td>Tanggal Pendirian</td><td>{{ format($detailJobPemohon->tanggal_pendirian) }}</td></tr>
        <tr><td>Alamat</td><td>{{ $detailJobPemohon->alamat }}</td></tr>
        <tr><td>Kelurahan</td><td>{{ $detailJobPemohon->kelurahan }}</td></tr>
        <tr><td>Kecamatan</td><td>{{ $detailJobPemohon->kecamatan }}</td></tr>
        <tr><td>Kota</td><td>{{ $detailJobPemohon->kota }}</td></tr>
        <tr><td>Postal</td><td>{{ $detailJobPemohon->postal }}</td></tr>
        <tr><td>Tlp Ktr Hunting</td><td>{{ $detailJobPemohon->tlp_ktr_hunting }}</td></tr>
        <tr><td>Tlp Ktr Direct</td><td>{{ $detailJobPemohon->tlp_ktr_direct }}</td></tr>
        <tr><td>Fax</td><td>{{ $detailJobPemohon->fax }}</td></tr>
    </table>
</div>
<div class="col-md-6">
    <table class="table table-bordered">
        <tr><td width="200">Unit Kerja</td><td>{{ $detailJobPemohon->unit_kerja }}</td></tr>
        <tr><td>Jabatan</td><td>{{ $detailJobPemohon->jabatan }}</td></tr>
        <tr><td>Bulan</td><td>{{ $detailJobPemohon->bulan }}</td></tr>
        <tr><td>Tahun</td><td>{{ $detailJobPemohon->tahun }}</td></tr>
        <tr><td>Omset Bln</td><td>{{ $detailJobPemohon->omset_bln }}</td></tr>
        <tr><td>Kepemilikan</td><td>{{ $detailJobPemohon->kepemilikan }}</td></tr>
        <tr><td>Margin Untung</td><td>{{ $detailJobPemohon->margin_untung }}</td></tr>
        <tr><td>Nama Perusahaan Lama</td><td>{{ $detailJobPemohon->nama_perusahaan_ago }}</td></tr>
        <tr><td>Bidang Usaha</td><td>{{ $detailJobPemohon->jenis_usaha_ago }}</td></tr>
        <tr><td>Jabatan</td><td>{{ $detailJobPemohon->jabatan_ago }}</td></tr>
        <tr><td>Unit Kerja</td><td>{{ $detailJobPemohon->unit_kerja_ago }}</td></tr>
        <tr><td>Telp</td><td>{{ $detailJobPemohon->telp }}</td></tr>
        <tr><td>Lama Bekerja (Bulan)</td><td>{{ $detailJobPemohon->bulan_ago }}</td></tr>
        <tr><td>Lama Bekerja (Tahun)</td><td>{{ $detailJobPemohon->tahun_ago }}</td></tr>
    </table>
</div>
@endif
