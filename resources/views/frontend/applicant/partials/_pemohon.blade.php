@if(empty($detailPemohon))
<div class="row">
  <div class="col-md-12">
    <div class="alert alert-danger">
      <strong>Data Profile is empty</strong>
    </div>
  </div>
</div>
@else
<div class="col-md-6">
    <table class="table table-bordered">
        <tr><td width="200">ID</td><td>{{ $detailPemohon->id }}</td></tr>
        <tr><td>Nama Pemohon</td><td>{{ $detailPemohon->users->fullname }}</td></tr>
        <tr><td>Full Name Clean</td><td>{{ $detailPemohon->fullname_clean }}</td></tr>
        <tr><td>Tempat Lahir</td><td>{{ $detailPemohon->tmp_lahir }}</td></tr>
        <tr><td>KTP</td><td>{{ $detailPemohon->ktp }}</td></tr>
        <tr><td>NPWP</td><td>{{ $detailPemohon->npwp }}</td></tr>
        <tr><td>Nama Ibu</td><td>{{ $detailPemohon->nama_ibu }}</td></tr>
        <tr><td>Status Kawin</td><td>{{ $detailPemohon->status_kawin }}</td></tr>
        <tr><td>Tanggungan</td><td>{{ $detailPemohon->tanggungan }}</td></tr>
        <tr><td>Pendidikan</td><td>{{ $detailPemohon->pendidikan }}</td></tr>
        <tr><td>Status Rumah</td><td>{{ $detailPemohon->status_rumah }}</td></tr>
        <tr><td>Alamat KTP</td><td>{{ $detailPemohon->alamat_ktp }}</td></tr>
        <tr><td>Kelurahan</td><td>{{ $detailPemohon->kelurahan_ktp }}</td></tr>
        <tr><td>Kecamatan</td><td>{{ $detailPemohon->kecamatan_ktp }}</td></tr>
        <tr><td>Propinsi</td><td>{{ $detailPemohon->propinsi_ktp }}</td></tr>
        <tr><td>Kota</td><td>{{ $detailPemohon->kota_ktp }}</td></tr>
        <tr><td>Kodepos</td><td>{{ $detailPemohon->postal_ktp }}</td></tr>
        <tr><td>Telepon</td><td>{{ $detailPemohon->telp }}</td></tr>
    </table>
</div>
<div class="col-md-6">
    <table class="table table-bordered">
        <tr><td width="200">Fax</td><td>{{ $detailPemohon->fax }}</td></tr>
        <tr><td>Handphone 1</td><td>{{ $detailPemohon->hp1 }}</td></tr>
        <tr><td>Handphone 2</td><td>{{ $detailPemohon->hp2 }}</td></tr>
        <tr><td>Facebook id</td><td>{{ $detailPemohon->facebook_id }}</td></tr>
        <tr><td>Linkedin id</td><td>{{ $detailPemohon->linkedin_id }}</td></tr>
        <tr><td>Amount balance</td><td>{{ $detailPemohon->amount_balance }}</td></tr>
        <tr><td>Cust verified</td><td>{{ $detailPemohon->cust_verified }}</td></tr>
        <tr><td>Cust verified date</td><td>{{ $detailPemohon->cust_verified_date }}</td></tr>
        <tr><td>Jumlah anggota</td><td>{{ $detailPemohon->jml_anggota }}</td></tr>
        <tr><td>Simpanan wajib</td><td>{{ $detailPemohon->simp_wajib }}</td></tr>
        <tr><td>Simpanan pokok</td><td>{{ $detailPemohon->simp_pokok }}</td></tr>
        <tr><td>Sumber dana</td><td>{{ $detailPemohon->sumber_dana }}</td></tr>
        <tr><td>Bentuk usaha</td><td>{{ $detailPemohon->bentuk_usaha }}</td></tr>
        <tr><td>Nomor akta</td><td>{{ $detailPemohon->nomor_akta }}</td></tr>
        <tr><td>Domisili akta</td><td>{{ $detailPemohon->domisili_akta }}</td></tr>
        <tr><td>Nama penanggung jawab</td><td>{{ $detailPemohon->nama_penanggungjawab }}</td></tr>
        <tr><td>Contact person</td><td>{{ $detailPemohon->contact_person }}</td></tr>
        <tr><td>Contact position</td><td>{{ $detailPemohon->contact_position }}</td></tr>
    </table>
</div>
<div class="lightbox" data-plugin-options='{"delegate": "a", "type": "image", "gallery": {"enabled": true}}'>
    <div class="col-md-6">
        <table class="table table-bordered">
            <tr>
                <td style="height: 350px;" width="200">KTP img</td>
                <td>
                    <a class="img-thumbnail mb-xs mr-xs" href="{!! $detailPemohon->ktp_img == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detailPemohon->ktp_img !!}">
                        {!! $detailPemohon->ktp_img == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detailPemohon->ktp_img.'" class="img-responsive" style="max-height: 350px">' !!}
                    </a>
                </td>
            </tr>
            <tr>
                <td>NPWP img</td>
                <td>
                    <a class="img-thumbnail mb-xs mr-xs" href="{!! $detailPemohon->npwp_img == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detailPemohon->npwp_img !!}">
                        {!! $detailPemohon->npwp_img == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detailPemohon->npwp_img.'" class="img-responsive" style="max-height: 350px">' !!}
                    </a>
                </td>
            </tr>
            <tr>
                <td>Akta img</td>
                <td>
                    <a class="img-thumbnail mb-xs mr-xs" href="{!! $detailPemohon->akta_img == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detailPemohon->akta_img !!}">
                        {!! $detailPemohon->akta_img == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detailPemohon->akta_img.'" class="img-responsive" style="max-height: 350px">' !!}
                    </a>
                </td>
            </tr>
            <tr>
                <td>KK img</td>
                <td>
                    <a class="img-thumbnail mb-xs mr-xs" href="{!! $detailPemohon->kk_img == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detailPemohon->kk_img !!}">
                        {!! $detailPemohon->kk_img == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detailPemohon->kk_img.'" class="img-responsive" style="max-height: 350px">' !!}
                    </a>
                </td>
            </tr>
            <tr>
                <td>BI checking</td>
                <td>
                    <a class="img-thumbnail mb-xs mr-xs" href="{!! $detailPemohon->bi_checking == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detailPemohon->bi_checking !!}">
                        {!! $detailPemohon->bi_checking == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detailPemohon->bi_checking.'" class="img-responsive" style="max-height: 350px">' !!}
                    </a>
                </td>
            </tr>
        </table>
    </div>
    <div class="col-md-6">
        <table class="table table-bordered">
            <tr>
                <td style="height: 350px;" width="200">Tagihan listrik</td>
                <td>
                    <a class="img-thumbnail mb-xs mr-xs" href="{!! $detailPemohon->tagihan_listrik == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detailPemohon->tagihan_listrik !!}">
                        {!! $detailPemohon->tagihan_listrik == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detailPemohon->tagihan_listrik.'" class="img-responsive" style="max-height: 350px">' !!}
                    </a>
                </td>
            </tr>
            <tr>
                <td>Tagihan air</td>
                <td>
                    <a class="img-thumbnail mb-xs mr-xs" href="{!! $detailPemohon->tagihan_air == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detailPemohon->tagihan_air !!}">
                        {!! $detailPemohon->tagihan_air == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detailPemohon->tagihan_air.'" class="img-responsive" style="max-height: 350px">' !!}
                    </a>
                </td>
            </tr>
            <tr>
                <td>Tagihan telp</td>
                <td>
                    <a class="img-thumbnail mb-xs mr-xs" href="{!! $detailPemohon->tagihan_telp == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detailPemohon->tagihan_telp !!}">
                        {!! $detailPemohon->tagihan_telp == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detailPemohon->tagihan_telp.'" class="img-responsive" style="max-height: 350px">' !!}
                    </a>
                </td>
            </tr>
            <tr>
                <td>Surat ket kerja</td>
                <td>
                    <a class="img-thumbnail mb-xs mr-xs" href="{!! $detailPemohon->surat_ket_kerja == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detailPemohon->surat_ket_kerja !!}">
                        {!! $detailPemohon->surat_ket_kerja == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detailPemohon->surat_ket_kerja.'" class="img-responsive" style="max-height: 350px">' !!}
                    </a>
                </td>
            </tr>
            <tr>
                <td>SSP</td>
                <td>
                    <a class="img-thumbnail mb-xs mr-xs" href="{!! $detailPemohon->ssp == '' ? '/assets/images/rekeningdefault.jpg' : '/'.$detailPemohon->ssp !!}">
                        {!! $detailPemohon->ssp == '' ? '<img src="/assets/images/rekeningdefault.jpg" class="img-responsive">' : '<img src="'.'/'.$detailPemohon->ssp.'" class="img-responsive" style="max-height: 350px">' !!}
                    </a>
                </td>
            </tr>
        </table>
    </div>
</div>
@endif
