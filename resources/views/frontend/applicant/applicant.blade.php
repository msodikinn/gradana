@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
Applicant
@parent
@stop

@section('header_styles')
    <!-- Current Page CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">

    <!-- Demo CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
    <style>
      section.section {
        margin: 0;
      }
    </style>
@stop

@section('content')
    <div class="body">
      @include('frontend.layouts._header')
      <div role="main" class="main">
        @include('frontend.partials._peminjamjumbotron')
        <section class="mt-xl mb-none pb-none">
          <div class="row">
            <div style="width: 80%; margin:0 auto;">
              {{-- <h2>Loan Process</h2>
              <p class="lead mb-xlg">Pellentesque pellentesque Lorem ipsum dolor sit amet.</p>
              <div class="divider divider-primary divider-small divider-small-center mb-xl">
                <hr>
              </div> --}}
              <img src="{!! trans('applicant.image') !!}" class="img-responsive" style="width:100%; max-width:1440px;">
            </div>
          </div>
          <div class="container" style="margin-top: 40px;">
            <div class="featured-boxes featured-boxes-style-8" style="margin-bottom: 60px">
              <div class="featured-boxes featured-boxes-style-8">
                <center><h2>{{ trans('applicant.why') }}</h2></center>
                  <div style="width: 70%; margin:0 auto;">
                    <ol class="list">
                      {!! trans('applicant.whydesc') !!}
                    </ol>
                  </div>
              </div>
            </div>
          </div>

          <div class="container">
            <div class="featured-boxes featured-boxes-style-8" style="margin-bottom: 60px">
              <div class="featured-boxes featured-boxes-style-8">
                <center><h2>{{ trans('applicant.terms') }}</h2></center>
                <div style="width: 60%; margin:0 auto;">
                  <ul class="list list-icons">
                    {!! trans('applicant.termsdesc') !!}
                  </ul>
                </div>
              </div>
            </div>
            {{-- <div class="featured-boxes featured-boxes-style-8" style="margin-bottom: 60px">
              <div class="featured-boxes featured-boxes-style-8">
                <center><h2>loan simulation</h2></center>
                <div style="width: 60%; margin:0 auto;">
                  ...
                </div>
              </div>
            </div> --}}
            <div class="col-md-6" style="margin-bottom: 50px">
              <h2><strong>FAQ</strong> (Frequently Asked Question)</h2>
              <div class="row">
                {!! trans('applicant.faq') !!}
              </div>
              <a href="/faq">{{ trans('applicant.more') }}</a>
            </div>

            <div class="col-md-6">
              <h2>{!! trans('applicant.borrow') !!}</h2>
              <div class="row">
                <div class="owl-carousel owl-theme mb-none" data-plugin-options='{"items": 1}'>
                  <div>
                    <div class="col-md-12">
                      <div class="testimonial testimonial-primary">
                        <blockquote>
                          <p>{{ trans('applicant.testi1') }}</p>
                        </blockquote>
                        <div class="testimonial-arrow-down"></div>
                        <div class="testimonial-author">
                          <div class="testimonial-author-thumbnail img-thumbnail">
                            <img src="{{ asset('assets/img/client-1.jpg') }}" alt="">
                          </div>
                          <p>{!! trans('applicant.testi1person') !!}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div>
                    <div class="col-md-12">
                      <div class="testimonial testimonial-primary">
                        <blockquote>
                          <p>{{ trans('applicant.testi2') }}</p>
                        </blockquote>
                        <div class="testimonial-arrow-down"></div>
                        <div class="testimonial-author">
                          <div class="testimonial-author-thumbnail img-thumbnail">
                            <img src="{{ asset('assets/img/client-1.jpg') }}" alt="">
                          </div>
                          <p>{!! trans('applicant.testi2person') !!}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12 center" style="padding-bottom: 40px">
            <a class="btn btn-3d btn-primary btn-lg" href="/applicant/form">{{ trans('applicant.daftar') }} <i class="fa fa-angle-right pl-xs"></i></a>
          </div>
        </section>
      </div>
      @include('frontend.layouts._footer')
    </div>
@stop

@section('footer_styles')
    <!-- Current Page Vendor and Views -->
    <script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>

    <!-- Current Page Vendor and Views -->
    <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
@stop
