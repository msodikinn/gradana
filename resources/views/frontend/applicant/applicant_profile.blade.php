
@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
Applicant
@parent
@stop

@section('header_styles')
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/iCheck/css/minimal/blue.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/user_account.css') }}">
    <style>
        .table>thead>tr>th {
            border-bottom: 0px;
        }
        .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border-bottom-width: 0px;
        }
        .table-bordered {
            border: 0px;
        }
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 0px;
        }
        .table>thead>tr>th {
            vertical-align: bottom;
            border-bottom: 0px;
        }
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 0px;
        }
    </style>
@stop

@section('content')
    <div class="body">
      @include('frontend.layouts._header')

      <div role="main" class="main">
      <section class="page-header">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <ul class="breadcrumb">
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12" style="margin-top: 45px">
            </div>
          </div>
        </div>
      </section>
      <section class="mt-xl mb-none pb-none">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @include('notifications')
                    <div>
                        <h3 class="heading-primary text-uppercase mb-md">User Profile</h3>
                        @if(count($detailPemohon) == 0) <p>Data profil permohonan kosong, <a href="/applicant/form">klik disini</a> untuk mengisi</p> @endif
                        {{-- @if($user->status == "Investor")
                          @if(is_null($investor))
                            <p>Data profil investor kosong, <a href="/investor-profile">klik disini</a> untuk mengisi</p>
                          @else
                            <p><a href="/admin/investor/borrower" class="btn btn-primary">Available Borrower</a></p>
                          @endif
                        @endif --}}
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div align="center">
                                    <span class="img-thumbnail">
                                        <img alt="" class="img-responsive" src="{{ '/'.$user->pic }}">
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div style="margin-bottom: 10px;">
                                    <a href="{{ '/my-account/edit' }}" class="btn btn-sm btn-primary"><i class="fa fa-gear"></i> Edit</a>
                                </div>
                                <table class="table">
                                    <tr>
                                        <td>Full Name</td>
                                        <td>{{ $user->fullname }}</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>{{ $user->email }}</td>
                                    </tr>
                                    <tr>
                                        <td>Gender</td>
                                        <td>{{ $user->gender }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Lahir</td>
                                        @if($user->dob=='0000-00-00')
                                            <td></td>
                                        @else
                                            <td>{{ format($user->dob) }}</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <td>Negara</td>
                                        <td>{{ $user->country }}</td>
                                    </tr>
                                    <tr>
                                        <td>State</td>
                                        <td>{{ $user->state }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kota</td>
                                        <td>{{ $user->city }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kecamatan</td>
                                        <td>{{ $user->kecamatan }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kelurahan</td>
                                        <td>{{ $user->kelurahan }}</td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>{{ $user->address }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kodepos</td>
                                        <td>{{ $user->postal }}</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>
                                            @if($user->deleted_at)
                                                Deleted
                                            @elseif($activation = Activation::completed($user))
                                                Activated
                                            @else
                                                Pending
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Created At</td>
                                        <td>{!! $user->created_at->diffForHumans() !!}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="row">
                                <div class="tabs tabs-bottom tabs-center tabs-simple">
                                    <h3 class="heading-primary text-uppercase mb-md">Profil Pemohon</h3>
                                    <p>Data permohonan berikut digunakan untuk mengajukan form aplikasi peminjaman</p>
                                    <ul class="nav nav-tabs nav-justified">
                                        <li class="active">
                                            <a href="#tab1" data-toggle="tab">Profile</a>
                                        </li>
                                        <li>
                                            <a href="#tab2" data-toggle="tab">Job Borrower</a>
                                        </li>
                                        <li>
                                            <a href="#tab3" data-toggle="tab">Data Pasangan</a>
                                        </li>
                                        <li>
                                            <a href="#tab4" data-toggle="tab">Job Pasangan</a>
                                        </li>
                                        <li>
                                            <a href="#tab5" data-toggle="tab">Keluarga</a>
                                        </li>
                                        <li>
                                            <a href="#tab6" data-toggle="tab">Penghasilan</a>
                                        </li>
                                        <li>
                                            <a href="#tab7" data-toggle="tab">Perbankan</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="tab1" class="tab-pane fade active in">
                                            @include('frontend.applicant.partials._pemohon')
                                        </div>
                                        <div id="tab2" class="tab-pane fade">
                                            @include('frontend.applicant.partials._jobPemohon')
                                        </div>
                                        <div id="tab3" class="tab-pane fade">
                                            @include('frontend.applicant.partials._suamiIstri')
                                        </div>
                                        <div id="tab4" class="tab-pane fade">
                                            @include('frontend.applicant.partials._jobSuamiIstri')
                                        </div>
                                        <div id="tab5" class="tab-pane fade">
                                            @include('frontend.applicant.partials._keluarga')
                                        </div>
                                        <div id="tab6" class="tab-pane fade">
                                            @include('frontend.applicant.partials._penghasilan')
                                        </div>
                                        <div id="tab7" class="tab-pane fade">
                                            @include('frontend.applicant.partials._perbankan')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </section>

      @include('frontend.layouts._footer')
    </div>
@stop

@section('footer_styles')
    <script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>
    <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>

    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/js/frontend/user_account.js') }}"></script>
    <script src="{{ asset('assets/frontend/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/frontend/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('assets/frontend/js/examples/examples.lightboxes.js') }}"></script>
@stop
