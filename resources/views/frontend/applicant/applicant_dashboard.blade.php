@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
History Lender
@parent
@stop

@section('header_styles')
  <!-- Current Page CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">
  <!-- Demo CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
  <style>
    section.section {
      margin: 0;
    }
    .counters strong {
      display: inline-table;
    }
  </style>
@stop

@section('content')
  <div class="body">
    @include('frontend.layouts._header')
    <div role="main" class="main">
    @include('frontend.partials._borrowerjumbotron')<br><br>
            <div class="container">

        <div class="row">
          <div class="counters with-borders">
            <div class="col-md-3 col-sm-6">
              <div class="counter counter-primary" style='height:129px;'>
                <!--strong data-to="20000" data-append="+">0</strong-->
				<strong style="font-size:30px;">Rp 
				@if(!empty($rupiah))
				{{ $rupiah }} 
				@else
				0
			    @endif
				</strong>
                <label>Harga KPR </label>
              </div>
            </div>
            <div class="col-md-3 col-sm-6">
              <div class="counter counter-primary">
                <!--strong data-to="15">0</strong-->
				<strong style="font-size:30px;"> 
				@if(!empty($applications->periode))	
			<!--	{{ $applications->periode }}--> 1 dari {{ $applications->jangka_waktu }}
				@else
				0	
				@endif
				</strong>
                <label>Cicilan</label>
              </div>
            </div>
            <div class="col-md-3 col-sm-6">
              <div class="counter counter-primary">
                <!--strong data-to="352">0</strong-->
				<strong style="font-size:30px;"> Rp 
				@if(!empty($rp))
				{{ $rp }} 
				@else
				0	
				@endif
				</strong>
                <label>Pembayaran Perbulan  </label>
              </div>
            </div>
            <div class="col-md-3 col-sm-6">
              <div class="counter counter-primary">
                <strong style="font-size:30px;">0 x </strong>
                <label>Telat Bayar</label>
              </div>
            </div>
		  
		  </div>
		  
        </div>
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
          <h4>APPLICATION STATUS</h4>
          <div class="col-md-12">
            <hr>
          </div>
          <h5><label>Details</label></h5>



          <div class="col-md-12 mt-xl">
            <ul class="list list-icons">
			
              @foreach ($app as $application)
                <li>Status : {{$application->status}}</li>
                <li>Credit Scoring : 3 </li>
                <li>Interest : 17%</li>
                <li>Monthly Installment : 1 dari  {{ $application->jangka_waktu}}</li>
                <li>Tenures : </li>
              @endforeach
			  
            </ul>
          </div>
        </div>
        <div class="col-md-12">
          <hr>
        </div>
        <div class="row">
		
        @foreach ($properties as $property)
            <div class="col-md-4">
              <div class="owl-carousel owl-theme" data-plugin-options='{"items": 1, "margin": 10}'>
                <div>
                  <span class="img-thumbnail">
                    <img alt="" height="300" class="img-responsive" src="{{ asset('assets/img/team-3.jpg') }}">
                  </span>
                </div>
              </div>
            </div>
            <div class="col-md-8">

              <h2 class="mb-none">{{$property->nama}}</h2>
              <p><button type="button" value="{{$property->harga_maximum}}" class="btn btn-primary pull-left mb-xl">Rp {{number_format($property->harga_maximum)}}</button></p>

            </div>
        @endforeach
		
          </div>
          <div class="row">

            <div class="col-md-12">
            <hr>
          </div>
          <h4>Details</h4>
          <div class="col-md-12 mt-xl">
            <ul class="list list-icons">
				
              <li>Project's name : </li>
              <li>Developer : Dev : @if(!empty($application->devid)) {{$applications->devid}} @else 0 @endif </li>
              <li>Property Type : @if(!empty($application->propid)) {{$applications->propid}} @else 0 @endif </li>
              
              <li>Price : @if(!empty($properties[0]->harga_maximum)) {{$properties[0]->harga_maximum}} @else 0 @endif </li>
              <li>Year Completed : @if(!empty($properties[0]->hgb_expire_date)) {{$properties[0]->hgb_expire_date }} @else 0 @endif  </li>
              <li>Unit type : </li>
              <li>Building area : @if(!empty($properties[0]->lokasi)) {{$properties[0]->lokasi }} @else 0 @endif </li>
              <li>land area : @if(!empty($properties[0]->lokasi)) {{$properties[0]->lokasi }} @else 0 @endif </li>
            
			</ul>
          </div>
          <h4>Documents</h4>
          <div class="col-md-12 mt-xl">
            <ul class="list list-icons">
              <li></li>
            </ul>
          </div>
          </div>
          <div class="col-md-12">
            <hr>
          </div>
        <div class="row">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Username</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    @include('frontend.layouts._footer')
  </div>
@stop

@section('footer_styles')
  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>

  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
@stop
