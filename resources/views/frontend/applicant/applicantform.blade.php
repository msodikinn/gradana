@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
Borrower
@parent
@stop

@section('header_styles')
    <!-- Current Page CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">
    {{-- <link href="{{ asset('assets/css/pages/form_layouts.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{asset('assets/vendors/iCheck/css/all.css')}}" />
    <link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet"/>

    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/pages/wizard.css') }}" rel="stylesheet">
    <style>
        .help-block {
            display: table-cell;
            color: rgb(148, 148, 148);
        }
        .select2-container .select2-selection--single .select2-selection__rendered {
            display: table;
        }
        .modal-lg {
          width:90%;
        }
    </style>
@stop

@section('content')
    <div class="body">
      @include('frontend.layouts._header')
      <div role="main" class="main">
         <section class="page-header">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <ul class="breadcrumb">
                </ul>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12" style="margin-top: 45px">
              </div>
            </div>
          </div>
        </section>
        <section class="mt-xl mb-none pb-none">
          <div class="row">
            <div class="col-md-12" id="applicantForm">
                <h2 align="center" style="margin-bottom: 10px;">{{ trans('basicform.title') }}</h2>
                <p align="center" class="lead mb-xlg">{{ trans('basicform.titledesc') }}</p>
                <div class="divider divider-primary divider-small divider-small-center mb-xl">
                    <hr>
                </div>
                <div class="row" style="margin-right: 0px; margin-left: 0px;">
                    <div class="col-md-12">
                        <div class="tabs tabs-bottom tabs-center tabs-simple">
                            <ul class="nav nav-tabs">
                                <!--li class="active">
                                    <a href="#tabsNavigationSimple1" data-toggle="tab">Basic Form</a>
                                </li-->
                                <li>
                                    <a href="#tabsNavigationSimple2" data-toggle="tab">Registration Form</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tabsNavigationSimple1">
                                    <!--@include('frontend.partials._basicform')-->
									@include('frontend.partials._completeform')
                                </div>
                                <div class="tab-pane" id="tabsNavigationSimple2">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </section>
      </div>
      @include('frontend.layouts._footer')
    </div>
@stop

@section('footer_styles')
    <!-- Current Page Vendor and Views -->
    <script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>

    <!-- Current Page Vendor and Views -->
    <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
    {{-- <script src="{{ asset('assets/vendors/intl-tel-input/js/intlTelInput.min.js') }}" type="text/javascript"></script> --}}
    <script src="{{ asset('assets/js/pages/validation.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/adduser.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script>
      $(document).ready(function() {
          $('#status_kawin').select2()
          $('#pendidikan').select2()
          $('#status_rumah').select2()
          $('#userid').select2()
          $('#jenis_pekerjaan').select2()
          $('#bidang_usaha').select2()
          $('#status').select2()
          $('#jenis_pekerjaan').select2()
          $('#bidang_usaha').select2()
      });
    </script>
    <script>
        $(document).ready(function() {
            $("#term").click(function() {
                $('#agree').prop('checked', true);
            });
            $("#term2").click(function() {
                $('#agree2').prop('checked', true);
            });
        });
        // $(document).ready(function(){
        //     $("input[type='checkbox'],input[type='radio']").iCheck({
        //         checkboxClass: 'icheckbox_minimal-blue',
        //         radioClass: 'iradio_minimal-blue'
        //     });
        // });
    </script>
    <script>
        function submitform()
        {
          document.getElementById("commentForm").submit();
        }
    </script>
@stop
