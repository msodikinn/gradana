@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
  Blog
@parent
@stop

@section('header_styles')
    <!-- Current Page CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">
    <!-- Demo CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
@stop

@section('content')
  <div class="body">
    @include('frontend.layouts._header')
    <div role="main" class="main">

      <section class="page-header">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <ul class="breadcrumb">
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12" style="margin-top: 45px">
            </div>
          </div>
        </div>
      </section>

      <div class="container">

        <div class="row">
          <div class="col-md-12">
            <div class="blog-posts">

              @foreach ($blogs as $b)
                <article class="post post-large" style="border-bottom: 0">
                  <div class="post-image">
                    <div class="owl-carousel owl-theme" data-plugin-options='{"items":1}'>
                      <div>
                        <div class="img-thumbnail">
                          <center><img class="img-responsive" src="{{ '/uploads/blog/'.$b->image }}" alt=""></center>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="post-date">
                    <span class="day">{!! tanggal($b->publishdate) !!}</span>
                    <span class="month">{!! bulan($b->publishdate) !!}</span>
                  </div>

                  <div class="post-content">

                    <h2><a href="{{ 'blog/'.$b->slug }}">{{ $b->title }}</a></h2>
                    <p>{!! str_limit(preg_replace( "/\r|\n/", " ", $b->content), 900) !!}</p>

                    <div class="post-meta">
                      <a href="{{ 'blog/'.$b->slug }}" class="btn btn-xs btn-primary pull-right">Read more...</a>
                    </div>

                  </div>
                </article>
                <hr style="margin-bottom: 50px">
              @endforeach

              {!! $blogs->links() !!}

            </div>
          </div>

        </div>

      </div>
    </div>
    @include('frontend.layouts._footer')
  </div>
@stop

@section('footer_styles')

@stop
