@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
Investor Form Lender
@parent
@stop

@section('header_styles')
  <!-- Current Page CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">
  <!-- Demo CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
  <style>
    section.section {
      margin: 0;
    }
    .counters strong {
      display: inline-table;
    }
  </style>
@stop

@section('content')
  <div class="body">
    @include('frontend.layouts._header')
    <div role="main" class="main">
    @include('frontend.partials._borrowerjumbotron')<br><br>
      <div class="container">
             <div class="row">
        <div class="col-md-12" id="applicantForm">
          <h2 align="center" style="margin-bottom: 10px;">REGISTRASI FORM </h2>
          <div class="divider divider-primary divider-small divider-small-center mb-xl">
          <hr>
          </div>
          <div class="row" style="margin-right: 0px; margin-left: 0px;">
            <div class="col-md-12">
              <div class="tabs tabs-bottom tabs-center tabs-simple">
              {!! Form::open(array('url' => route('investorpost'), 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'basic', 'files'=> true)) !!}

                  <div class="form-group">
                    <label class="col-md-4 control-label">Nama Lengkap</label>
                    <div class="col-md-5">
                      <input type="text" name="nama" class="form-control" value="{{ $identity->fullname }}" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Alamat</label>
                    <div class="col-md-5">
                      <input type="text" name="alamat" class="form-control" value="" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">TTD</label>
                    <div class="col-md-5">
                      <input type="date" name="birth" class="form-control" value="" required>
                    </div>
                  </div>
				   <div class="form-group">
                    <label class="col-md-4 control-label">Telpon</label>
                    <div class="col-md-5">
                      <input type="text" name="telp" class="form-control" value="" required>
                    </div>
                  </div>
				  <div class="form-group">
                    <label class="col-md-4 control-label">Email</label>
                    <div class="col-md-5">
                      <input type="email" name="email" class="form-control" value="" required>
                    </div>
                  </div>
				  <div class="form-group">
                    <label class="col-md-4 control-label">Pekerjaan</label>
                    <div class="col-md-5">
                      <input type="text" name="job" class="form-control" value="" required>
                    </div>
                  </div>
				<div class="form-group">
                    <label class="col-md-4 control-label">
					Saya memilih Mengalokasi total dana sebesar <br> jumlah berikut dalam skema gradana:  
					</label>
                    <div class="col-md-5">
                      <input type="text" name="alokasi_dana" class="form-control" value="" required> )* Format: 100.000.000
                    </div>
                  </div>
				<div class="form-group">
                    <label class="col-md-4 control-label">Saya Mengalokasikan dana saya untuk</label>
                    <div class="col-md-5">
                     <select name='jml_alokasi' style='width:500px;'>
						<option value=''>Select</option>
						<option value='1'>1 peminjam</option>
						<option value='2 '>2 Peminjam</option>
						<option value='3'>3 Peminjam</option>
						<option value='> 3'> > 3 Peminjam</option>
					 </select>
                    </div>
                  </div>		
				<div class="form-group">
                    <label class="col-md-4 control-label">Bila menmungkinkan,saya memilih untuk menyalurkan pinjam dengan tenor selaman</label>
                    <div class="col-md-5">
                      <select name='tenor' style='width:500px;'>
						<option value=''>Select</option>
						<option value='24 bulan'>24 bulan</option>
						<option value='36 bulan'>36 bulan</option>
					  </select>
                    </div>
                  </div>  
                <div class="form-group">
                    <label class="col-md-4 control-label">opsi Penempatan Dana yang ingin saya pilih</label>
                    <div class="col-md-5">
                      
					  <select name='penempatan_dana' style='width:500px;'>
						<option value=''>Select</option>
						<option value='skema talangan DP dengan membayar kas keras penuh ke developer'>A.skema talangan DP dengan membayar kas keras penuh ke developer</option>
						<option value='skema talangan DP dengan membayar jumlah DP saja ke developer'>B.skema talangan DP dengan membayar jumlah DP saja ke developer</option>
					  </select>
                    </div>
                  </div>
       			  <div class="form-group">
                     <button class="btn btn-primary pull-right mb-xl" type="submit">Save</button>
                   </div>
				

              {!! Form::close() !!}

              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
    @include('frontend.layouts._footer')
  </div>
@stop

@section('footer_styles')
  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>

  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
@stop
