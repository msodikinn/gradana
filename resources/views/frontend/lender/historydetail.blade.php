@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
Available Loans Detail
@parent
@stop

@section('header_styles')
  <!-- Current Page CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">
  <!-- Demo CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
  <style>
    section.section {
      margin: 0;
    }
    .counters strong {
      display: inline-table;
    }
  </style>
@stop

@section('content')
  <div class="body">
    @include('frontend.layouts._header')
    <div role="main" class="main">
    @include('frontend.partials._lenderjumbotron')<br><br>
      <div class="container">
         
          
         
          <h4>Details</h4>
          <div class="col-md-12 mt-xl">
            <!--ul class="list list-icons"-->
			<table width=100%>
			<tr>
			<td width=40%>Tanggal Ditanda Tangani</td><td> : </td > <td> {{ format($bayar->tgl_pembayaran) }}</td>
			</tr>
			<tr>
            <td>Nama Peminjam  </td> <td>:</td> {{ format($bayar->nasabah) }} <td> </td>
			</tr>
			<tr>
            <td> Nama Develpoer </td><td>:</td> <td> </td>
			</tr>
			<tr>	
			<td> Skema Peminjaman, A atau B. </td><td>:</td> <td> </td>
			</tr>
			<tr>	
			<td>Dana Total Yang Disetor Ke Developer</td><td>:</td> <td> </td>
			</tr>
			<tr>
			<td> Alamat Developert<td> <td>:</td> <td> </td>
			</tr>
			<tr>
			<td> Cicilan Perbulan  </td> <td>: </td> <td> Rp {{ number_format($bayar->amount,0,",",".") }} </td>
			</tr>
			<tr><td> Setoran ke Developer </td> <td> : </td> <td> </td>
			</tr>
			<tr>
            <td> Jumlah Uang Muka Yang Dibayar Peminjam </td><td>:</td> <td> </td>
			</tr>
			<tr>	
            <td> Harga KPR yang akan dibayarkan peminjam </td> <td>: </td> <td>{{ $bayar->bulan }} </td>
			</tr>
			<tr>
            <td>Jumlah Uang Muka</td><td>:</td> <td>{{ number_format($bayar->amount,2) }}</td>
			</tr>
            <tr> 
			<td>Tanggal Jatuh Tempo</td><td> : </td> <td> </td>
			</tr>
			<td> Keuntungan Yang Sudah Direalisasikan </td> <td> : </td> <td> Rp 2.000.000</td>
			</tr>
			<tr> 
            <td>Keuntungan Yang Sudah Diterima</td><td>:</td> <td>Rp 3.000.000</td>
			</tr>
			<tr>
             <td>Nilai Kredit Scoring</td><td>:</td><td>3 dari 36x</td>
			 </tr>
			 <tr>
			  <td>Investor</td><td> : </td> <td>Deny</td>
			 </tr>
			 <tr>
			 <td>Harga Kpr</td><td> : </td> <td> </td>
			 </tr>
			 <tr>
			 <td> Bunga Pertahun </td> <td>:</td> <td>Rp 2.000.000</td>
			 </tr>
			 <tr>
			 <td>Frekuensi Telat Bayar </td><td>:</td> <td>0 %</td>
			 </tr>
			  <tr>
			 <td>Morgeunit Price </td><td>:</td> <td> </td>
			 </tr>
             </table>
            
          </div>
         
          </div>
        </div>
    </div>
    @include('frontend.layouts._footer')
  </div>
@stop

@section('footer_styles')
  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>

  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
@stop
