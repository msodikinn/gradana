@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
History Lender
@parent
@stop

@section('header_styles')
  <!-- Current Page CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">
  <!-- Demo CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
  <style>
    section.section {
      margin: 0;
    }
    .counters strong {
      display: inline-table;
    }
  </style>
@stop

@section('content')
  <div class="body">
    @include('frontend.layouts._header')
    <div role="main" class="main">
    @include('frontend.partials._lenderjumbotron')<br><br>
      <div class="container">
      <h4>All Activities</h4>
        <div class="row">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Tanggal TTD</th>
                <th>Nama Peminjam</th>
                <th>Cicilan perbulan</th>
                <th>Setoran ke developer </th>
                <th>#</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($byr as $b)
              <tr>
                <td>{{ format($b->tgl_pembayaran) }}</td>
                <td>{{ $b->nasabah }}</td>
                <td>{{ uang($b->amount) }}</td>
                <td>{{ $b->status_pembayaran }}</td>
				 <td><a href="{{ url('lender/historydetail/')}}/{{$b->id}}">Detail</a></td>
			  </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    @include('frontend.layouts._footer')
  </div>
@stop

@section('footer_styles')
  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>

  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
@stop
