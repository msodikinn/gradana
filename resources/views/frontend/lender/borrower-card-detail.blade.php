@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
Borrower Card Detail Lender
@parent
@stop

@section('header_styles')
  <!-- Current Page CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">
  <!-- Demo CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
  <style>
    section.section {
      margin: 0;
    }
    .counters strong {
      display: inline-table;
    }
  </style>
@stop

@section('content')
  <div class="body">
    @include('frontend.layouts._header')
    <div role="main" class="main">
    @include('frontend.partials._lenderjumbotron')<br><br>
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="col-md-4">
              <div class="owl-carousel owl-theme" data-plugin-options='{"items": 1, "margin": 10}'>
                <div>
                  <span class="img-thumbnail">
                    <img alt="" height="300" class="img-responsive" src="{{ asset('assets/img/team-3.jpg') }}">
                  </span>
                </div>
              </div>
            </div>
            <div class="col-md-8">
              <h2 class="mb-none">{{ $user->fullname }}</h2>
              <h4 class="heading-primary">Web Designer</h4>
              <hr class="solid">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc <a href="#">vehicula</a> lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing sit amet. In eu justo a felis faucibus ornare vel id metus. Vestibulum ante ipsum primis in faucibus.</p>
            </div>
          </div>
          <div class="col-md-6">
          <h4>Payment History</h4>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Payment Method</th>
                  <th>Amount IDR</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
               @foreach ($byr as $b)
                <tr>
                  <td>{{ format($b->tgl_pembayaran) }}</td>
                  <td></td>
                  <td>{{ uang($b->amount) }}</td>
                  <td>{{ $b->status_pembayaran }}</td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
          <div class="row">
            <div class="col-md-12">
            <hr>
          </div>
          <h4>Details</h4>
          <div class="col-md-12 mt-xl">
            <ul class="list list-icons">
              <li>Project's name : {{ $property->nama }}</li>
              <li>Developer : {{ $property->developer->nama }}</li>
              <li>Property Type : {{ $property->tipe }}</li>
              <li>Registered on : </li>
              <li>Price : {{ $property->harga_jual }}</li>
              <li>Year Completed : </li>
              <li>Unit type : </li>
              <li>Building area : {{ $property->luas_bangunan }}</li>
              <li>Surface area : {{ $property->luas_tanah }}</li>
            </ul>
          </div>
          <h4>Details</h4>
          <div class="col-md-12 mt-xl">
            <ul class="list list-icons">
            </ul>
          </div>
          </div>
        </div>
    </div>
    @include('frontend.layouts._footer')
  </div>
@stop

@section('footer_styles')
  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>

  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
@stop
