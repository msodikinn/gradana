@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
Borrower Card Lender
@parent
@stop

@section('header_styles')
  <!-- Current Page CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">
  <!-- Demo CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
  <style>
    section.section {
      margin: 0;
    }
    .counters strong {
      display: inline-table;
    }
  </style>
@stop

@section('content')
  <div class="body">
    @include('frontend.layouts._header')
    <div role="main" class="main">
    @include('frontend.partials._lenderjumbotron')<br><br>
      <div class="container">
        <div class="row">
         @foreach($availableBorrowers as $availableBorrower)
          <div class="col-md-4">
          <a href="{{ route('borrower-card-detail', $availableBorrower->id) }}">
            <span class="thumb-info thumb-info-side-image thumb-info-no-zoom" style="padding: 5% 5% 5% 5%;">
              <span class="thumb-info-side-image-wrapper">

                @if ($availableBorrower->user->pic == null)
                  <img src="{{ defaultpic() }}" class="img-responsive" alt="" style="width: 100px;">
                @else
                  <img src="{{'/'.$availableBorrower->user->pic}}">
                @endif

              </span>

              <span class="thumb-info-caption">
                <span class="thumb-info-caption-text">
                  <h5 class="text-semibold text-uppercase mb-xs">{{$availableBorrower->user->fullname}}</h5>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </span>
              </span>
            </span>
          </a>
          </div>
        @endforeach
        </div>
      </div>
    </div>
    @include('frontend.layouts._footer')
  </div>
@stop

@section('footer_styles')
  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>

  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
@stop
