@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
Property
@parent
@stop

@section('header_styles')
  <!-- Current Page CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">
  <!-- Demo CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
  <style>
    section.section {
      margin: 0;
    }
    .counters strong {
      display: inline-table;
    }
  </style>
@stop

@section('content')
  <div class="body">
    @include('frontend.layouts._header')
    <div role="main" class="main">
    @include('frontend.partials._propertyjumbotron')<br><br>

        <div class="container">
          <div class="row mb-xl">
            <div align="center">
              <h1 class="font-weight-semibold text-uppercase" style="margin-bottom: 5px;">{{ trans('property.title') }}</h1>
              <p class="lead mb-none">{{ trans('property.titledesc') }}</p>
            </div>
            <div class="divider divider-primary divider-small divider-small-center mb-xl" style="margin-bottom: 50px;">
              <hr>
            </div>
            <div class="col-md-12 mt-xl">
              <div class="row">
                <div>
                  <p class="">{{ trans('property.desc') }}</p>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <hr>
            </div>
          </div>

          <div class="row">
            <div class="col-md-8">
            <div align="CENTER">
              <h3>DEVELOPERS</h3>
            </div>
            <div class="owl-carousel owl-theme" data-plugin-options='{"items": 4, "autoplay": true, "autoplayTimeout": 3000}'>
              <div>
                <img class="img-responsive" src="assets/frontend/img/logos/logo-1.png" alt="">
              </div>
              <div>
                <img class="img-responsive" src="assets/frontend/img/logos/logo-2.png" alt="">
              </div>
              <div>
                <img class="img-responsive" src="assets/frontend/img/logos/logo-3.png" alt="">
              </div>
              <div>
                <img class="img-responsive" src="assets/frontend/img/logos/logo-4.png" alt="">
              </div>
              <div>
                <img class="img-responsive" src="assets/frontend/img/logos/logo-5.png" alt="">
              </div>
              <div>
                <img class="img-responsive" src="assets/frontend/img/logos/logo-6.png" alt="">
              </div>
              <div>
                <img class="img-responsive" src="assets/frontend/img/logos/logo-4.png" alt="">
              </div>
              <div>
                <img class="img-responsive" src="assets/frontend/img/logos/logo-2.png" alt="">
              </div>
            </div>

            </div>
            <div class="col-md-4">
            <h4>Become partner</h4>
              <p>
                The fastest way to grow your business
              </p>

              <div class="get-started">
                <a href="#" class="btn btn-lg btn-primary">Join Now!</a>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <hr>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
            <h3 align="center">PROPERTY</h3>
            @foreach($propertys as $property)
            <div class="col-md-3">
              <span class="thumb-info thumb-info-hide-wrapper-bg">
                <span class="thumb-info-wrapper">
                <a href="{{ route('property-detail', $property->id) }}">
                  <img src="assets/frontend/img/projects/project-4.jpg" class="img-responsive" alt="">
                </a>
                  <span class="thumb-info-title">
                    <span class="thumb-info-inner">{{$property->nama}}</span>
                    <span class="thumb-info-type">Project Type</span>
                  </span>
                </span>
                <span class="thumb-info-caption">
                  <span class="thumb-info-caption-text">{{$property->deskripsi}}</span>
                  <span class="thumb-info-social-icons">
                    Starting from
                    Rp. {{$property->harga_jual}}
                  </span>
                </span>
              </span>
            </div>
            @endforeach
            </div>
          </div>
        </div>
    </div>
    @include('frontend.layouts._footer')
  </div>
@stop

@section('footer_styles')
  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>

  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
@stop
