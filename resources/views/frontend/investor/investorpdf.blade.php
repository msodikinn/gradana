<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Lender Information</title>
  <style>
  .page-break {
      page-break-after: always;
  }
  .footer {
      width: 100%;
      text-align: right;
      position: fixed;
  }
  .footer {
      bottom: 0px;
  }
  </style>
</head>
<body>
    <center><h1>Gradana Terms and Conditions</h1></center>
    <p>Berikut adalah informasi Pemberi Pinjaman (Lender) dan Terms and Conditions yang harus disetujui untuk berinvestasi di website kami (Gradana.com). Mohon data informasi berikut diperhatikan dan disimpan dengan baik sebagai pedoman Anda.</p>
    <center><h2>Informasi Pemberi Pinjaman</h2></center>
        <center><img src="{!! url($inves->img) !!}" class="img-responsive" style="max-height:280px;"></center>
        <div style="width:80%;margin:0 auto;">
          <table>
            <tbody>
              <tr><td height="15">User ID:</td><td>{{ $inves->userid }}</td></tr>
              <tr><td height="15">Nama User Gradana:</td><td>{{ $inves->user->fullname }}</td></tr>
              <tr><td height="15">Kategori ID:</td><td>{{ $inves->investor_cat->id }}</td></tr>
              <tr><td height="15">Nama Kategori:</td><td>{{ $inves->investor_cat->nama }}</td></tr>
              <tr><td height="15">Keterangan:</td><td>{{ $inves->investor_cat->keterangan }}</td></tr>
              <tr><td height="15">Pemberi Pinjaman ID:</td><td>{{ $inves->id }}</td></tr>
              <tr><td width="150" height="15">Nama Pemberi Pinjaman:</td><td>{{ $inves->nama }}</td></tr>
              <tr><td height="15">Jenis Usaha:</td><td>{{ $inves->jenis_usaha }}</td></tr>
              <tr><td height="15">Email:</td><td>{{ $inves->email }}</td></tr>
              <tr><td height="15">Alamat:</td><td>{{ $inves->alamat }}</td></tr>
              <tr><td height="15">No. Telepon:</td><td>{{ $inves->telp }}</td></tr>
              <tr><td height="15">No. Handphone:</td><td>{{ $inves->hp }}</td></tr>
              <tr><td height="15">Alokasi Dana:</td><td>{{ uang($alokasi->alokasi_dana) }}</td></tr>
              <tr><td height="15">Jumlah Alokasi Dana:</td><td>{{ $alokasi->jml_alokasi }}</td></tr>
              <tr><td height="15">Tenor:</td><td>{{ $alokasi->tenor }}</td></tr>
              <tr><td height="15">Penempatan Dana:</td><td>{{ $alokasi->penempatan_dana }}</td></tr>
              <tr><td height="15">Status:</td><td>{{ $inves->approved == 1 ? 'Approved' : 'Unapproved' }}</td></tr>
              <tr><td height="15">Created At:</td><td>{{ formatLengkap($inves->created_at) }}</td></tr>
            </tbody>
          </table>
        </div>
        <div class="footer">
            <em>assigned by {{ $inves->nama }}</em>
        </div>
        <div class="page-break"></div>
        @include('frontend.partials._termInvestor')
</body>
</html>
