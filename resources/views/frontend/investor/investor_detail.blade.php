@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
Profil Lender
@parent
@stop

@section('header_styles')
    <!-- Current Page CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">

    <!-- Demo CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">

    <style>
      section.section {
        margin: 0;
      }
    </style>
@stop

@section('content')

@stop

@section('footer_styles')

@stop
