
@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
Applicant
@parent
@stop

@section('header_styles')
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/iCheck/css/minimal/blue.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/user_account.css') }}">
    <style>
        .table>thead>tr>th {
            border-bottom: 0px;
        }
        .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border-bottom-width: 0px;
        }
        .table-bordered {
            border: 0px;
        }
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 0px;
        }
        .table>thead>tr>th {
            vertical-align: bottom;
            border-bottom: 0px;
        }
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 0px;
        }
    </style>
@stop

@section('content')
    <div class="body">
      @include('frontend.layouts._header')

      <div role="main" class="main">
      <section class="page-header">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <ul class="breadcrumb">
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12" style="margin-top: 45px">
            </div>
          </div>
        </div>
      </section>
      <section class="mt-xl mb-none pb-none">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @include('notifications')
                    <div>
                        <h3 class="heading-primary text-uppercase mb-md">User Profile</h3>
                        @if(count($investor) == 0) <p>Data profil pemberi pinjaman kosong, <a href="/investor/form">klik disini</a> untuk mengisi</p> @endif
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div align="center">
                                    <span class="img-thumbnail">
                                        <img alt="" class="img-responsive" src="{{ '/'.$user->pic }}">
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div style="margin-bottom: 10px;">
                                    <!--a href="{{ 'admin/users/'.Sentinel::getuser()->id.'/edit' }}" class="btn btn-sm btn-primary"><i class="fa fa-gear"></i> Edit</-->
                                    <a href="{{ '/my-account/edit/' }}" class="btn btn-sm btn-primary"><i class="fa fa-gear"></i> Edit</a>
									
                                </div>
                                <table class="table">
                                    <tr>
                                        <td>Full Name</td>
                                        <td>{{ $user->fullname }}</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>{{ $user->email }}</td>
                                    </tr>
                                    <tr>
                                        <td>Gender</td>
                                        <td>{{ $user->gender }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Lahir</td>
                                        @if($user->dob=='0000-00-00')
                                            <td></td>
                                        @else
                                            <td>{{ format($user->dob) }}</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <td>Negara</td>
                                        <td>{{ $user->country }}</td>
                                    </tr>
                                    <tr>
                                        <td>State</td>
                                        <td>{{ $user->state }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kota</td>
                                        <td>{{ $user->city }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kecamatan</td>
                                        <td>{{ $user->kecamatan }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kelurahan</td>
                                        <td>{{ $user->kelurahan }}</td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>{{ $user->address }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kodepos</td>
                                        <td>{{ $user->postal }}</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>
                                            @if($user->deleted_at)
                                                Deleted
                                            @elseif($activation = Activation::completed($user))
                                                Activated
                                            @else
                                                Pending
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Created At</td>
                                        <td>{!! $user->created_at->diffForHumans() !!}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div><hr style="margin-bottom:40px;"></div>
                                    <h3 class="heading-primary text-uppercase mb-md">Profil Pemberi Pinjaman</h3>
                                    <p>Data pemberi pinjaman berikut digunakan untuk mengajukan form pemberian dana kepada peminjam</p>
                                    @if (count($investor) !== 0)
                                        <div class="col-md-4">
                                            <div align="center">
                                                <span class="img-thumbnail">
                                                    @if ($investor->pic)
                                                        <img alt="" class="img-responsive" src="{{ '/'.$investor->pic }}">
                                                    @else
                                                        <img src="{{ defaultcompany() }}" class="img-responsive">
                                                    @endif

                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div style="margin-bottom: 10px;">
                                                <a href="{{ '/admin/investor/edit/'.$investor->id }}" class="btn btn-sm btn-primary"><i class="fa fa-gear"></i> Edit</a>
                                                {{-- @if (count($investor) !== 0)
                                                    <a href="/admin/investor/alokasi" class="btn btn-sm btn-primary">Alokasi Dana</a>
                                                    <a href="/admin/investor/borrower" class="btn btn-sm btn-primary">Available Borrower</a>
                                                    <a href="/admin/investorpayment" class="btn btn-sm btn-primary">Payment</a>
                                                @endif --}}
                                            </div>
                                            <table class="table">
                                                <tr><td style="width: 150px">ID</td><td>{{ $investor->id }}</td></tr>
                                                <tr><td>Kategori</td><td>{{ $investor->investor_cat->nama }}</td></tr>
                                                <tr><td>Keterangan</td><td>{{ $investor->investor_cat->keterangan }}</td></tr>
                                                <tr><td>Jenis Usaha</td><td>{{ $investor->jenis_usaha }}</td></tr>
                                                <tr><td>Nama Lender</td><td>{{ $investor->nama }}</td></tr>
                                                <tr><td>Email</td><td>{{ $investor->email }}</td></tr>
                                                <tr><td>Alamat</td><td>{{ $investor->alamat }}</td></tr>
                                                <tr><td>No. Telepon</td><td>{{ $investor->telp }}</td></tr>
                                                <tr><td>No. Handphone</td><td>{{ $investor->hp }}</td></tr>
                                            </table>
                                        </div>
                                    @else
                                        <p>Data profil Lender kosong, <a href="/admin/investor/create">klik disini</a> untuk mengisi</p>
                                    @endif
                                </div>
                                <div class="col-md-12">
                                    <div><hr style="margin-bottom:40px;"></div>
                                    <h3 class="heading-primary text-uppercase mb-md">Alokasi Dana</h3>
                                    <p>Alokasi dana Anda yang sudah masuk di sistem</p>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Alokasi Dana</th>
                                                <th>Jumlah Alokasi</th>
                                                <th>Tenor</th>
                                                <th>Penempatan Dana</th>
                                                <th>{{-- Edit --}}</th>
                                                <th>{{-- Delete --}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($alokasi as $a)
                                                <tr role="row" class="odd">
                                                    <td><a href="{{ $a->detail() }}">{{ $a->id }}</a></td>
                                                    <td>{{ $a->alokasi_dana }}</td>
                                                    <td>{{ $a->jml_alokasi }}</td>
                                                    <td>{{ $a->tenor }}</td>
                                                    <td>{{ $a->penempatan_dana }}</td>
                                                    <td><a class="edit" href="{{ $a->editUrl() }}">Edit</a></td>
                                                    <td><a class="edit" href="{{ $a->delUrl() }}">delete</a></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </section>

      @include('frontend.layouts._footer')
    </div>
@stop

@section('footer_styles')
    <script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>
    <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>

    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/js/frontend/user_account.js') }}"></script>
    <script src="{{ asset('assets/frontend/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/frontend/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('assets/frontend/js/examples/examples.lightboxes.js') }}"></script>
@stop
