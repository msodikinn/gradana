@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
Lender Form
@parent
@stop

@section('header_styles')
    <!-- Current Page CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">

    <!-- Demo CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">

    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <style>
      section.section {
        margin: 0;
      }

      .modal-lg {
        width:90%;
      }

    </style>
    <style>
    </style>
@stop

@section('content')
    <div class="body">
      @include('frontend.layouts._header')
      <section class="page-header">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <ul class="breadcrumb">
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12" style="margin-top: 45px">
            </div>
          </div>
        </div>
      </section>
      <div role="main" class="main">
        <section class="mt-xl mb-none pb-none">
          <div class="row">
            <div class="col-md-12" id="applicantForm">
                <h2 align="center" style="margin-bottom: 10px;">{{ trans('investorform.title') }}</h2>
                <p align="center" class="lead mb-xlg">{{ trans('investorform.titledesc') }} </p>
                <div class="divider divider-primary divider-small divider-small-center mb-xl">
                    <hr>
                </div>
                <div class="row" style="margin-right: 0px; margin-left: 0px;">
                    @include('frontend.partials._investorform')
                </div>
            </div>
          </div>
        </section>
      </div>
      @include('frontend.layouts._footer')
    </div>

@stop

@section('footer_styles')
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
          $('#catid').select2()
          $('#jml_alokasi').select2()
          $('#tenor').select2()
          $('#penempatan_dana').select2()
      });
      $(document).ready(function() {
          $("#term").click(function() {
              $('#agree').prop('checked', true);
          });
      });
    </script>
@stop
