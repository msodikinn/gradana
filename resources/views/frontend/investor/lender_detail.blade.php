@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
Applicant
@parent
@stop

@section('header_styles')
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/iCheck/css/minimal/blue.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/user_account.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />

@stop

@section('content')
    <div class="body">
      @include('frontend.layouts._header')
      <div role="main" class="main">
      <section class="page-header">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <ul class="breadcrumb">
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12" style="margin-top: 45px">
            </div>
          </div>
        </div>
      </section>
      <section class="mt-xl mb-none pb-none">
        <div class="container">
            <div class="row">
                <div class="row">
                    <div class="col-md-12">
                        <div class="position-center table-responsive">
                            @include('notifications')
                            <ul class="nav  nav-tabs ">
                                <li class="active">
                                    <a href="#tab1" data-toggle="tab">
                                        <i class="livicon" data-name="address-book" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                                        Data Lender</a>
                                </li>
                                <li>
                                    <a href="#tab2" data-toggle="tab">
                                        <i class="livicon" data-name="notebook" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                                        Data Application</a>
                                </li>
                                <li>
                                    <a href="#tab3" data-toggle="tab">
                                        <i class="livicon" data-name="money" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                                        Payment</a>
                                </li>
                            </ul>
                            <div  class="tab-content mar-top">
                                <div id="tab1" class="tab-pane fade active in">
                                    @include('admin.users.partials._lender')
                                </div>
                                <div id="tab2" class="tab-pane fade">
                                    @include('admin.users.partials._aplikasi')
                                </div>
                                <div id="tab3" class="tab-pane fade">
                                    @include('admin.users.partials._pembayaran')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </section>
      @include('frontend.layouts._footer')
    </div>
@stop

@section('footer_styles')
    <script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>
    <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/frontend/user_account.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
@stop
