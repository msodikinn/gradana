@extends('frontend/layouts/default')

@section('title')
Lender
@parent
@stop

@section('header_styles')
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
    <style>
      section.section {
        margin: 0;
      }
    </style>
@stop

@section('content')
    <div class="body">
      @include('frontend.layouts._header')
      <div role="main" class="main">
        @include('frontend.partials._investorjumbotron')
        <section class="mt-xl mb-none pb-none">
          <div class="row">
            <div style="width: 100%; margin:0 auto;">
              <center>{!! trans('investor1.image') !!}</center>
            </div>
          </div>
          <div class="container" style="margin-top: 40px;">
            <div class="featured-boxes featured-boxes-style-8" style="margin-bottom: 60px">
              <div class="featured-boxes featured-boxes-style-8">
                {!! trans('investor1.why') !!}
              </div>
            </div>
          </div>

          <div class="container">
            <div class="featured-boxes featured-boxes-style-8" style="margin-bottom: 50px">
              <div class="featured-boxes featured-boxes-style-8">
                {!! trans('investor1.lend') !!}
              </div>
            </div>

            <div class="featured-boxes featured-boxes-style-8" style="margin-bottom: 60px">
              <div class="featured-boxes featured-boxes-style-8">
                {!! trans('investor1.comparison') !!}
              </div>
            </div>

            <div class="col-md-6" style="margin-bottom: 50px">
              {!! trans('investor1.faq') !!}
              </div>
              <a href="/faq">See more...</a>
            </div>

            <div class="col-md-6" style="margin-bottom: 50px">
              {!! trans('investor1.testimonititle') !!}
              <div class="row">
                <div class="owl-carousel owl-theme mb-none" data-plugin-options='{"items": 1}'>
                  {!! trans('investor1.testimoni') !!}
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12 center" style="padding-bottom: 40px">
            <a class="btn btn-3d btn-primary btn-lg" href="/investor/form">{{ trans('investor1.klik') }}<i class="fa fa-angle-right pl-xs"></i></a>
          </div>
        </section>
      </div>
      @include('frontend.layouts._footer')
    </div>
@stop

@section('footer_styles')
    <script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>

    <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
@stop
