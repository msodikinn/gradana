@extends('frontend/layouts/default')

@section('title')
  Forgot Password
@parent
@stop

@section('header_styles')

@stop

@section('content')
    <div class="body">
      @include('frontend.layouts._header')
      <div role="main" class="main">
        <section class="page-header">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <ul class="breadcrumb">
                </ul>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12" style="margin-top: 45px">
              </div>
            </div>
          </div>
        </section>
        <div class="container">
            <div class="row">
                <div class="box animation flipInX">
                    <center><h3 class="heading-primary">Reset your Password</h3></center>
                    <div style="width:60%; margin:0 auto;">
                        <p>Enter your new password details</p>
                        @include('notifications')
                        <form action="{{ route('forgot-password-confirm',compact(['userId','passwordResetCode'])) }}" class="omb_loginForm"  autocomplete="off" method="POST">
                            {!! Form::token() !!}
                            <input type="password" class="form-control" name="password" placeholder="New Password">
                            <span class="help-block">{{ $errors->first('password', ':message') }}</span>
                            {{-- <input type="password" class="form-control" name="password_confirm" placeholder="Confirm New Password">
                            <span class="help-block">{{ $errors->first('password_confirm', ':message') }}</span> --}}
                            <input type="submit" class="btn btn-block btn-primary" value="Submit to Reset Password" style="margin-top:10px;">
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
@stop

@section('footer_styles')
    {{-- kosong --}}
@stop
