@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
Home
@parent
@stop

@section('header_styles')
		<!-- Current Page CSS -->
		<link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">

		<!-- Demo CSS -->
		<link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
    <style>
      section.section {
        margin: 0;
      }
      .counters strong {
        display: inline-table;
      }
      #investee:hover {
          background-color: #f1592a;
      }
      #investee:hover h2 {
        color: white;
      }
      #investee:hover #pinvestee {
        color:white;
      }
      #investor:hover {
          background-color: #fcb040;
      }
      #investor:hover h2 {
        color: white;
      }
      #investor:hover p {
        color:white;
      }
      h1 {
        margin: 0;
      }
    </style>
@stop

@section('content')
		<div class="body">
      @include('frontend.layouts._header')
			<div role="main" class="main">
				@include('frontend.partials._slides')
              <section class="section section-default section-default-scale-8">
                <div class="container">
                  {!! Form::open(array('class' => 'form-horizontal','url' => route('sendsub'), 'method' => 'post')) !!}
                  {{-- <div class="col-md-12 center">

                    <h2 class="font-weight-semibold text-light">{!! trans('index.buletin.title') !!}</h2>
                    <p class="lead mb-xlg">{!! trans('index.buletin.sub') !!}</p>
                  </div> --}}
                  <div class="form-group center">
                    <div class="col-md-2">
                      <label>Email *</label>
                    </div>
                    <div class="col-md-8">
                      <input type="hidden" name="home" value="a">
                      <input type="email" placeholder="{!! trans('index.buletin.place') !!}" value="" maxlength="100" class="form-control" name="email" id="email" required>
                    </div>
                    <div class="col-md-2">
                      <input type="submit" value="Subscribe" class="btn btn-primary btn-md">
                    </div>
                  </div>
                  {!! Form::close() !!}
                </div>
              </section>
				<div class="container">
          <div class="row mt-xl">
            <div style="width:90%; margin:0 auto; " align="center">
              <h2>{{ trans('index.quote') }} </h2>
              {{--<footer>William, <cite title="Gradana CEO">Gradana CEO</cite></footer>--}}
            </div>
          </div>
          <div class="featured-boxes featured-boxes-style-8">
            <div class="row mt-xl">
              {{-- @if ($countApproved != 0)
                <div class="counters counters-text-dark">
                  <div class="col-md-3 col-sm-3">
                    <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="300">
                      <i class="fa fa-user"></i>
                      <strong data-to="{{ $countApproved }}" data-append="+">0</strong>
                      <label>{{ trans('index.dashboard.total') }}</label>
                      <p class="text-color-primary mb-xl">{{ trans('index.dashboard.totaldesc') }}</p>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3">
                    <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="600">
                      <i class="fa fa-desktop"></i>
                      <strong data-to="{{ $sumApproved }}">0</strong>
                      <label>{{ trans('index.dashboard.pinjaman') }}</label>
                      <p class="text-color-primary mb-xl">{{ trans('index.dashboard.pinjamandesc') }}</p>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3">
                    <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="900">
                      <i class="fa fa-ticket"></i>
                      <strong data-to="{{ $success }}">0</strong><strong>%</strong>
                      <label>{{ trans('index.dashboard.sukses') }}</label>
                      <p class="text-color-primary mb-xl">{{ trans('index.dashboard.suksesdesc') }}</p>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3">
                    <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="1200">
                      <i class="fa fa-clock-o"></i>
                      <strong data-to="{{ $badCredit }}">0</strong><strong>%</strong>
                      <label>{{ trans('index.dashboard.macet') }}</label>
                      <p class="text-color-primary mb-xl">{{ trans('index.dashboard.macetdesc') }}</p>
                    </div>
                  </div>
                </div>
              @endif --}}
            </div>
          </div>
          <div class="featured-boxes featured-boxes-style-8">
            <div class="row" style="margin-bottom: 30px;">
              <div class="col-md-6">
                <a href="/register">
                  <div class="featured-box featured-box-primary{{-- tertiary --}} featured-box-text-left">
                    <div class="box-content" id="investee">
                      <div class="row">
                        <div class="text-center">
                          <h2>{{ trans('index.borrower') }}</h2>
                        </div>
                        {{-- <div class="col-md-3">
                          <div class="align-right">
                            <i class="icon-featured fa fa-user"></i>
                          </div>
                        </div> --}}
                      </div>
                      <div class="row text-center">
                        <div class="col-md-12">
                          <p id="pinvestee" class="lead">{{ trans('index.borrowerdesc') }}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
              <div class="col-md-6">
                <a href="/register">
                  <div class="featured-box featured-box-secondary{{-- quaternary --}} featured-box-text-left">
                    <div class="box-content" id="investor">
                      <div class="row">
                        <div class="text-center">
                          <h2>{{ trans('index.investor') }}</h2>
                        </div>
                      </div>
                      <div class="row text-center">
                        <div class="col-md-12">
                          <p class="lead">{{ trans('index.investordesc') }}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            </div>
            <div class="featured-boxes featured-boxes-style-8 col-sm-12" align="center">
              <h2>{{ trans('index.why.choose') }}</h2>
              <div class="col-md-4">
                {!!trans('index.why.simple')!!}
              </div>
              <div class="col-md-4">
                {!! trans('index.why.secured') !!}
              </div>
              <div class="col-md-4">
                {!! trans('index.why.competitive') !!}
              </div>
            </div>
            <div class="col-md-12 center" style="padding-bottom: 40px">
              <a class="btn btn-primary mt-md" href="/faq">{{ trans('index.faq') }}<i class="fa fa-angle-right pl-xs"></i></a>
            </div>
          </div>
        </div>

        <section class="mt-xl mb-none pb-none">
          <div class="row">
            <div style="width: 90%; margin:0 auto;" class="center">
              {!! trans('index.work') !!}
              {{-- <div class="divider divider-primary divider-small divider-small-center mb-xl">
                <hr>
              </div> --}}
              <div style="width: 90%;margin:0 auto;max-width:1400px;">
                {!! trans('index.why.image') !!}
              </div>
            </div>
          </div>
        </section>
				<section class="mt-xl mb-none pb-none">
					<div class="container">
            {{-- <div class="featured-boxes featured-boxes-style-8">
              <div class="featured-boxes featured-boxes-style-8 center">
                <h2>Media Coverage</h2>
                <p class="lead mb-xlg">Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Quisque rutrum pellentesque imperdiet. Quisque rutrum pellentesque imperdiet. Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Quisque rutrum pellentesque imperdiet.</p>
              </div>
            </div>
            <hr class="tall"> --}}


          </div>
				</section>
			</div>
      @include('frontend.layouts._footer')
		</div>
@stop

@section('footer_styles')
		<!-- Current Page Vendor and Views -->
		<script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>

		<!-- Current Page Vendor and Views -->
		<script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
		<script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
@stop
