@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
About Us
@parent
@stop

@section('header_styles')
    <!-- Current Page CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">

    <!-- Demo CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
    <style>
      section.section {
        margin: 0;
      }
      .counters strong {
        display: inline-table;
      }
    </style>
@stop

@section('content')
		<div class="body">
			@include('frontend.layouts._header')
			<div role="main" class="main">
      @include('frontend.partials._aboutjumbotron')<br><br>
       {{--  <section class="page-header">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <ul class="breadcrumb">
                </ul>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12" style="margin-top: 45px">
              </div>
            </div>
          </div>
        </section> --}}

				<div class="container">
        {{-- <div class="row">
            <div style="width: 80%; margin:0 auto;">
              <h2>Loan Process</h2>
              <p class="lead mb-xlg">Pellentesque pellentesque Lorem ipsum dolor sit amet.</p>
              <div class="divider divider-primary divider-small divider-small-center mb-xl">
                <hr>
              </div>
              <img src="{!! trans('about.image') !!}" class="img-responsive" style="width:100%; max-width:1440px;">
            </div>
          </div> --}}
                    <div class="row mb-xl">
                        <div align="center">
                          <h1 class="font-weight-semibold text-uppercase" style="margin-bottom: 5px;">{{ trans('about.title') }}</h1>
                          <p class="lead mb-none">{{ trans('about.titledesc') }}</p>
                        </div>
                        <div class="divider divider-primary divider-small divider-small-center mb-xl" style="margin-bottom: 50px;">
                          <hr>
                        </div>
                        <div class="col-md-10 col-md-offset-1 mt-xl">
                            <div class="row">
                                <div>
                                    <h4 class="font-weight-semibold"><a href="">{{ trans('about.who') }}</a></h4>
                                    <p class="">{{ trans('about.whodesc') }}</p>
                                </div>
                                <div>
                                    <h4 class="font-weight-semibold"><a href="">{{ trans('about.why') }}</a></h4>
                                    <p class=""><label>{{ trans('about.why1') }}</label></p>
                                    <p class="">{{ trans('about.why1desc') }}</p>
                                    <p class=""><label>{{ trans('about.why2') }}</label></p>
                                    <p class="">{{ trans('about.why2desc') }}</p>
                                    <p class=""><label>{{ trans('about.why3') }}</label></p>
                                    <p class="">{{ trans('about.why3desc') }}</p>
                                </div>

                            </div>
                        </div>
						{{-- <div class="col-md-3 hidden-xs mt-xl">
							<div class="row">
								<div class="col-md-6">
									<img class="img-responsive mt-xl mb-xl" src="{{ asset('assets/img/our-office-4.jpg') }}" alt="Office">
									<img class="img-responsive mb-xl" src="{{ asset('assets/img/our-office-6.jpg') }}" alt="Office">
								</div>
								<div class="col-md-6">
									<img class="img-responsive mb-xl" src="{{ asset('assets/img/our-office-5.jpg') }}" alt="Office">
									<img class="img-responsive mb-xl" src="{{ asset('assets/img/our-office-7.jpg') }}" alt="Office">
								</div>
							</div>
						</div> --}}
					</div>
				</div>

				{{-- <section class="m-xl pb-xl">
					<div class="container">
						<div class="row">
							<div class="col-md-12 center">
								<h2 class="mb-none mt-xl font-weight-semibold">Meet Our Team.</h2>
								<p class="lead mb-none">We have solid team. </p>
								<div class="divider divider-primary divider-small divider-small-center mb-xl">
									<hr>
								</div>
							</div>
						</div>
						<div class="row mt-lg">
							<div class="col-md-8 col-md-offset-2">
								<div class="row mt-lg">
									<div class="col-md-3 col-xs-12 center mb-lg">
										<h4 class="mt-md mb-none">David Doe</h4>
										<p class="mb-none">CEO</p>
										<span class="thumb-info-social-icons mt-sm pb-none">
											<a href="http://www.facebook.com" target="_blank"><i class="fa fa-facebook"></i><span>Facebook</span></a>
											<a href="http://www.twitter.com"><i class="fa fa-twitter"></i><span>Twitter</span></a>
											<a href="http://www.linkedin.com"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
										</span>
									</div>
									<div class="col-md-3 col-xs-12 center mb-lg">
										<h4 class="mt-md mb-none">Jeff Doe</h4>
										<p class="mb-none">CMO</p>
										<span class="thumb-info-social-icons mt-sm pb-none">
											<a href="mailto:mail@example.com"><i class="fa fa-envelope"></i><span>Email</span></a>
											<a href="http://www.linkedin.com"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
										</span>
									</div>
									<div class="col-md-3 col-xs-12 center mb-lg">
										<h4 class="mt-md mb-none">Craig Doe</h4>
										<p class="mb-none">COO</p>
										<span class="thumb-info-social-icons mt-sm pb-none">
											<a href="http://www.facebook.com" target="_blank"><i class="fa fa-facebook"></i><span>Facebook</span></a>
											<a href="http://www.twitter.com"><i class="fa fa-twitter"></i><span>Twitter</span></a>
											<a href="http://www.linkedin.com"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
										</span>
									</div>
									<div class="col-md-3 col-xs-12 center mb-lg">
										<h4 class="mt-md mb-none">Richard Doe</h4>
										<p class="mb-none">President</p>
										<span class="thumb-info-social-icons mt-sm pb-none">
											<a href="http://www.linkedin.com"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section> --}}

				{{-- <section class="section section-default section-footer">
					<div class="container">
						<div class="row">
							<div class="col-md-12 center">
								<h2 class="mb-none mt-xl font-weight-semibold">Clients.</h2>
								<p class="lead mb-none">Pellentesque pellentesque eget tempor tellus. </p>
								<div class="divider divider-primary divider-small divider-small-center mb-xl">
									<hr>
								</div>
							</div>
						</div>
						<div class="row mt-lg">
							<div class="content-grid content-grid-dashed mt-xlg mb-lg">
								<div class="row content-grid-row">
									<div class="content-grid-item col-md-4 center">
										<img class="img-responsive" src="{{ asset('assets/img/logo-1.png') }}" alt="">
									</div>
									<div class="content-grid-item col-md-4 center">
										<img class="img-responsive" src="{{ asset('assets/img/logo-2.png') }}" alt="">
									</div>
									<div class="content-grid-item col-md-4 center">
										<img class="img-responsive" src="{{ asset('assets/img/logo-3.png') }}" alt="">
									</div>
								</div>
								<div class="row content-grid-row">
									<div class="content-grid-item col-md-4 center">
										<img class="img-responsive" src="{{ asset('assets/img/logo-4.png') }}" alt="">
									</div>
									<div class="content-grid-item col-md-4 center">
										<img class="img-responsive" src="{{ asset('assets/img/logo-5.png') }}" alt="">
									</div>
									<div class="content-grid-item col-md-4 center">
										<img class="img-responsive" src="{{ asset('assets/img/logo-6.png') }}" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</section> --}}
			</div>

			@include('frontend.layouts._footer')
		</div>
@stop

@section('footer_styles')
		<!-- Current Page Vendor and Views -->
		<script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>

		<!-- Current Page Vendor and Views -->
		<script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
		<script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
@stop
