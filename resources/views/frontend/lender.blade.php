@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
Lender
@parent
@stop

@section('header_styles')
  <!-- Current Page CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">
  <!-- Demo CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
  <style>
    section.section {
      margin: 0;
    }
    .counters strong {
      display: inline-table;
    }
  </style>
@stop

@section('content')
  <div class="body">
    @include('frontend.layouts._header')
    <div role="main" class="main">
    @include('frontend.partials._lenderjumbotron')<br><br>
      <div class="container">
        <div class="row mb-xl">
          <div align="center">
            <img src="{!! trans('lender.image') !!}" width="100%" height="auto" style="padding: 0% 20% 0% 20%;">
          </div>
          <div class="col-md-12">
            <hr>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row mb-xl">
          <div align="center">
            <p class="lead mb-none">{{ trans('lender.why') }}</p>
          </div>
          <div class="col-md-10 col-md-offset-1 mt-xl">
            <ul class="list list-icons">
              {!! trans('lender.whydesc') !!}
            </ul>
          </div>
          <div class="col-md-12">
            <hr>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row mb-xl">
          <div align="center">
            <p class="lead mb-none">{{ trans('lender.terms') }}</p>
          </div>
          <div class="col-md-10 col-md-offset-1 mt-xl">
            <ul class="list list-icons">
              {!! trans('lender.termsdesc') !!}
            </ul>
          </div>
          <div class="col-md-12">
            <hr>
          </div>
        </div>
      </div>
     <div class="container">
        <div class="row mb-xl">
          <div class="col-md-12 mt-xl">
            <div class="owl-carousel owl-theme nav-bottom rounded-nav mb-none" data-plugin-options='{"items": 1, "autoHeight": true, "loop": false, "nav": true, "dots": false}'>
              <div>
                <div class="col-md-12">
                  <div class="testimonial testimonial-style-5 testimonial-with-quotes mb-none">
                    <blockquote>
                      <p>{!! trans('lender.testi1') !!}</p>
                    </blockquote>
                    <div class="testimonial-arrow-down"></div>
                    <div class="testimonial-author">
                      {{--<img src="{{ ('assets/img/client-1.jpg') }}" class="img-responsive img-circle" alt="">--}}
                      <p>{!! trans('lender.testi1person') !!}</p>
                    </div>
                  </div>
                </div>
              </div>
              <div>
                <div class="col-md-12">
                  <div class="testimonial testimonial-style-5 testimonial-with-quotes mb-none">
                    <blockquote>
                      <p>{!! trans('lender.testi2') !!}</p>
                    </blockquote>
                    <div class="testimonial-arrow-down"></div>
                    <div class="testimonial-author">
                      <!--img src="{{ ('assets/img/client-1.jpg') }}" class="img-responsive img-circle" alt=""-->
                      <p>{!! trans('lender.testi2person') !!}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <hr>
      </div>
      <div class="container">
        <div class="col-md-12">
          <div class="toggle toggle-primary" data-plugin-toggle>
            <div align="center">
              <p class="lead mb-none">FAQ</p>
            </div>
            <section class="toggle">
              <label>{{ trans('faq.faqinvestors1') }}</label>
              <p style="height:0px;">{{ trans('faq.faqinvestors1desc') }}</p>
            </section>
            <section class="toggle">
              <label>{{ trans('faq.faqinvestors2') }}</label>
              <p style="height:0px;">{{ trans('faq.faqinvestors2desc') }}</p>
            </section>
            <section class="toggle">
              <label>{{ trans('faq.faqinvestors3') }}</label>
              <p style="height:0px;">{{ trans('faq.faqinvestors3desc') }}</p>
            </section>
            <section class="toggle">
              <label>{{ trans('faq.faqinvestors4') }}</label>
              <p style="height:0px;">{{ trans('faq.faqinvestors4desc') }}</p>
            </section>
            <section class="toggle">
              <label>{{ trans('faq.faqinvestors5') }}</label>
              <p style="height:0px;">{{ trans('faq.faqinvestors5desc') }}</p>
            </section>
          </div>
        </div>
      </div>
    </div>
    @include('frontend.layouts._footer')
  </div>
@stop

@section('footer_styles')
  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>

  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
@stop
