@extends('frontend/layouts/default')

{{-- Page title --}}
@section('title')
Property Detail
@parent
@stop

@section('header_styles')
  <!-- Current Page CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/settings.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/layers.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/frontend/vendor/rs-plugin/css/navigation.css') }}">
  <!-- Demo CSS -->
  <link rel="stylesheet" href="{{ asset('assets/frontend/css/demos/demo-digital-agency.css') }}">
  <style>
    section.section {
      margin: 0;
    }
    .counters strong {
      display: inline-table;
    }
  </style>
@stop

@section('content')
  <div class="body">
    @include('frontend.layouts._header')
    <div role="main" class="main">
    @include('frontend.partials._propertyjumbotron')<br><br>
      <div class="container">
        <div class="row mb-xl">
          <div align="center">
            <h1 class="font-weight-semibold text-uppercase" style="margin-bottom: 5px;">{{ trans('property.title') }}</h1>
          </div>
          <div class="row">
            <div class="col-md-12">
              <hr>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <article class="post post-medium">
              <div class="row">
                <div class="col-md-4">
                  <div class="post-image">
                    <div class="owl-carousel owl-theme" data-plugin-options='{"items":1}'>
                      <div>
                        <div class="img-thumbnail">
                          <img class="img-responsive" src="assets/frontend/img/blog/blog-image-1.jpg" alt="">
                        </div>
                      </div>
                      <div>
                        <div class="img-thumbnail">
                          <img class="img-responsive" src="assets/frontend/img/blog/blog-image-2.jpg" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-8">
                  <div class="post-content">
                    <div class="row">
                      <div class="col-md-8">
                        <label>{{$views->nama}}</label>
                      </div>
                      <div class="col-md-4">
                        <div class="post-meta" align="right">
                          <span><i class="fa fa-calendar"></i> January 10, 2016 </span>
                          <span><i class="fa fa-tag"></i> <a href="#">Duis</a>, <a href="#">News</a> </span>
                        </div>
                      </div>
                    </div>
                    <p>{{$views->deskripsi}}</p>
                    <div id="googlemaps" class="google-map"></div>
                  </div>
                </div>
              </div>
            </article>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <br>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <br>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
        </div>
        <div class="row">
        <h4>Simulation</h4>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-6">
                <div class="panel-body">


                    <div class="form-group">
                      <label class="col-md-3 control-label">Property Price</label>
                      <div class="col-md-6">
                        <input name="property_price" class="form-control" value="{{$views->harga_jual}}" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3 control-label">Down Payment</label>
                      <div class="col-md-6">
                        <select name="down_payment" id="selectdp" class="form-control ">
                          @for ($i = 5; $i <= 50; $i=$i+5)
                              <option value="{{ $i/100 }}">{{ $i }} %</option>
                          @endfor
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3 control-label">Installment</label>
                      <div class="col-md-6">
                        <select name="installment" id="selectinstallment" class="form-control">
                          <option value="12">12 Bulan</option>
                          <option value="24">24 Bulan</option>
                          <option value="36">36 Bulan</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3 control-label">Interest</label>
                      <div class="col-md-6">
                        <select name="interest" id="selectinterest" class="form-control ">
                          @for ($i = 12; $i <= 20; $i++)
                              <option value="{{ $i }}">{{ $i }} %</option>
                          @endfor
                        </select>
                      </div>
                    </div>

                    <button type="button" class="btn btn-md  btn-primary calculate">Submit</button>
                </div>
              </div>
              <div class="col-md-6">
                <div class="featured-box featured-box-primary">
                  <div class="box-content">
                    <div class="form-group">
                      <label class="col-md-6 control-label">Monthly Installment</label>
                      <div class="col-md-6">
                        <div class="monthlyinstallment"></div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-6 control-label">Administration Fee</label>
                      <div class="col-md-6">
                        <div class="feeadmin"></div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-6 control-label">Insurance</label>
                      <div class="col-md-6">
                        <div class="insurance"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <section class="call-to-action call-to-action-default with-button-arrow call-to-action-in-footer">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <div class="call-to-action-content">
                  <h3>Get your <stron>dream home</strong> now with <strong>GRADANA</strong></h3>
                </div>
                <div class="call-to-action-btn">
                  <a href="http://themeforest.net/item/porto-responsive-html5-template/4106987" target="_blank" class="btn btn-lg btn-primary">Apply</a><span class="arrow hlb hidden-xs hidden-sm hidden-md" data-appear-animation="rotateInUpLeft" style="top: -12px;"></span>
                </div>
              </div>
            </div>
          </div>
        </section>
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <hr>
            <div class="row">
              <ul class="image-gallery sort-destination lightbox" data-sort-id="portfolio" data-plugin-options='{"delegate": "a.lightbox-portfolio", "type": "image", "gallery": {"enabled": true}}'>
              <h4>Related <strong>Searches</strong></h4>
                <li class="col-md-3 col-sm-6 col-xs-12 isotope-item websites">
                  <div class="image-gallery-item">
                    <a href="assets/frontend/img/projects/project.jpg" class="lightbox-portfolio">
                      <span class="thumb-info">
                        <span class="thumb-info-wrapper">
                          <img src="assets/frontend/img/projects/project.jpg" class="img-responsive" alt="">
                          <span class="thumb-info-title">
                            <span class="thumb-info-inner">Project Title</span>
                            <span class="thumb-info-type">Project Type</span>
                          </span>
                          <span class="thumb-info-action">
                            <span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
                          </span>
                        </span>
                      </span>
                    </a>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    @include('frontend.layouts._footer')
  </div>
@stop

@section('footer_styles')

  <script type="text/javascript">
    $(document).ready(function() {
      $(".calculate").click(function() {
        var property_price = $('input[name="property_price"]').val();
        var down_payment = $("#selectdp").val();
        var installment = $("#selectinstallment").val();
        var interest = $("#selectinterest").val();

        var asuransi_rate;

        if (installment == 12){
            asuransi_rate = 2/100;
        } else if (installment == 24){
            asuransi_rate = 2.5/100;
        } else if (installment == 36){
            asuransi_rate = 3/100;
        }

        var dp_amount = down_payment*property_price;
        var asuransi = dp_amount*asuransi_rate;
        var tenor_tahun = installment/12;

        var asumsi_interest = 1+(interest/100);
        var hasil_pangkat = Math.pow(asumsi_interest,tenor_tahun);

        var cicilan =  (dp_amount*(asumsi_interest))/installment;

        var administrationfee = 500000;

        function toRp(a,b,c,d,e){e=function(f){return f.split('').reverse().join('')};b=e(parseInt(a,10).toString());for(c=0,d='';c<b.length;c++){d+=b[c];if((c+1)%3===0&&c!==(b.length-1)){d+='.';}}return'Rp.\t'+e(d)}

        var cicilan_rp = toRp(cicilan);
        var asuransi_rp = toRp(asuransi);
        var administrationfee_rp = toRp(administrationfee);

        // $('.property_price').html(administrationfee);
        // $('.down_payment').html(down_payment);
        // $('.installment').html(installment);
        // $('.interest').html(interest);

        $('.feeadmin').html(administrationfee_rp);
        $('.insurance').html(asuransi_rp);
        $('.monthlyinstallment').html(cicilan_rp);
      });
    });
  </script>

  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/js/views/view.contact.js') }}"></script>

  <!-- Current Page Vendor and Views -->
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
  <script src="{{ asset('assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
  <script src="http://maps.google.com/maps/api/js"></script>
  <script>

    /*
    Map Settings

      Find the Latitude and Longitude of your address:
        - http://universimmedia.pagesperso-orange.fr/geo/loc.htm
        - http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude/

    */

    // Map Markers
    var mapMarkers = [{
      address: "New York, NY 10017",
      html: "<strong>New York Office</strong><br>New York, NY 10017",
      icon: {
        image: "img/pin.png",
        iconsize: [26, 46],
        iconanchor: [12, 46]
      },
      popup: true
    }];

    // Map Initial Location
    var initLatitude = 40.75198;
    var initLongitude = -73.96978;

    // Map Extended Settings
    var mapSettings = {
      controls: {
        draggable: (($.browser.mobile) ? false : true),
        panControl: true,
        zoomControl: true,
        mapTypeControl: true,
        scaleControl: true,
        streetViewControl: true,
        overviewMapControl: true
      },
      scrollwheel: false,
      markers: mapMarkers,
      latitude: initLatitude,
      longitude: initLongitude,
      zoom: 16
    };

    var map = $("#googlemaps").gMap(mapSettings);

    // Map Center At
    var mapCenterAt = function(options, e) {
      e.preventDefault();
      $("#googlemaps").gMap("centerAt", options);
    }

  </script>
@stop
