<?php
/**
 * Language file for form fields for user account management
 *
 */

return array(

    'ph-title' => 'Post title here...',
    'ph-content' => 'Place some text here',
    'meta-title' => 'Post Meta Title here...',
    'meta-description' => 'Post Meta Description here...',
    'll-postcategory' => 'Post category',
    'select-category' => 'Select a Category',
    'tags' => 'Tags...',
    'lb-featured-img' => 'Featured image',
    'select-file' => 'Select file',
    'change' => 'Change',
    'publish' => 'Publish',
    'discard' => 'Discard',
    'ph-name' => 'Your name',
    'ph-email' => 'Your email',
    'ph-website' => 'Your website',
    'ph-comment' => 'Your comment',
    'save-comment' => 'Save comment',
    'save' => 'Save',
    'publishedstatus' => 'Published Status',
);
