<?php
/**
* Language file for Gallery error/success messages
*
*/

return array(

    'gallery_exists'        => 'Galleryalready exists!',
    'gallery_not_found'     => 'Gallery[:id] does not exist.',
    'gallery_name_required' => 'The name field is required',
    'users_exists'        => 'Gallerycontains users, Gallerycan not be deleted',

    'success' => array(
        'create' => 'Gallerywas successfully created.',
        'update' => 'Gallerywas successfully updated.',
        'delete' => 'Gallerywas successfully deleted.',
    ),

    'delete' => array(
        'create' => 'There was an issue creating the group. Please try again.',
        'update' => 'There was an issue updating the group. Please try again.',
        'delete' => 'There was an issue deleting the group. Please try again.',
    ),

    'error' => array(
        'gallery_exists' => 'A Galleryalready exists with that name, names must be unique for groups.',
        'gallery_role_exists' => 'Another role with same slug exists, please choose another name',
        'no_role_exists' => 'No Role exists with that id'
    ),

);
