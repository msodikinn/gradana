<?php

return array(
    'image' => '<img src="/assets/img/grafik-pemberi-pinjaman2_ENG.jpg" class="img-responsive" style="width:100%; max-width:1440px;">',
    'why'   => '<center><h2>Why Gradana Investing Can Be Alternative Options?</h2></center>
                    <div style="width: 80%; margin:0 auto;">
                      <ol class="list">
                        <li> More attarctive yields compared to time deposit </li>
                        <li> Backed with the selected property as a secure and strong assett collateral; As long as the Lender has not fulfilled all of his / her obligations until all his / her Down Payment installment is fully paid off, you are acknowledged as the rightful owner of the selected property</li>
                        <li> A reliable verification and credit scoring system which is derived from commercial banking’s credit analysis process</li>
                        <li> Partnership and collaboration with trusted property developers which have passed stringent due diligence process which includes but not limited to the land and project status as well as financial health</li>
                        <li> Transparent agreement structures which are comprehensive to protect and accomodate all stakeholders, including you as a Lender</li>
                        <li> Online system which monitors payment / installment and loan portfolio status that can be accessed anytime</li>
                        <li> Fast, effiecient and secure overall process</li>
                      </ol>
                    </div>',

    'lend'  => '<center><h1>Lending Terms</h1></center>
                <div style="width: 80%; margin:0 auto;">
                  <ul class="list list-icons">
                    <li><i class="fa fa-check"></i> Indonesian citizen</li>
                    <li><i class="fa fa-check"></i> Submit accurate and complete data which is supported by supporting documents as part of our requirements, at the very latest 3 days after initial registration is done</li>
                    <li><i class="fa fa-check"></i> Commit the payments required under the selected Scheme (may it be Scheme A- in the form of full price Hard Cash, or Scheme B – in the form of DP only) on time</li>
                    <li><i class="fa fa-check"></i> Sign and execute the agreement with the Borrower and comply with all clauses</li>
                  </ul>
                </div>',

    'comparison' => '<h1><center>Comparison Rate</center></h1>
                <p class="lead" align="center">Comparison of rates between put money on deposit / bank, investment gold, stocks, and gradana</p>
                <div class="row">
                  <div class="col-md-1"></div>
                  <div class="col-md-10">
                    <table class="table table-condensed">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Deposito/Bank</th>
                          <th>Investasi Emas</th>
                          <th>Saham</th>
                          <th>Gradana</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>Mark</td>
                          <td>Otto</td>
                          <td>@mdo</td>
                          <td>@fat</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>Jacob</td>
                          <td>Thornton</td>
                          <td>@fat</td>
                          <td>@fat</td>
                        </tr>
                        <tr>
                          <td> 3</td>
                          <td>Larry</td>
                          <td>the Bird</td>
                          <td>@twitter</td>
                          <td>@fat</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-1"></div>
                </div>',

    'faq' => '<h1><strong>FAQ</strong> (Frequently Asked Question)</h1>
              <div class="row">
                <div class="toggle toggle-primary" data-plugin-toggle>
                  <section class="toggle">
                    <label>When will I receive the payments from the Borrower?</label>
                    <p>At once after the agreement is signed and executed, which is every xxx of each month</p>
                  </section>
                  <section class="toggle">
                  <label>Can I give loans to more than 1 Borrower?</label>
                    <p>Of course. As long as the fund is sufficient, you may choose to place all of your funds under one Borrower or divide it across multiple Borrowers.</p>
                  </section>
                  <section class="toggle">
                    <label>Can the Borrower get loans from more than one Lender?</label>
                    <p>Since the current BPHTB / property transaction tax can only provide for one name to be liable for and to prevent further complications / issues down the line, CURRENTLY the Borrower can only get a loan from one Lender. This is also in line with the nature of Gradana’s business model which is as a peer to peer lending platform, not a crowd-funding platform.</p>
                  </section>
                </div>',

    'testimonititle' => '<h1>Lenders <strong>Testimonial</strong></h1>',
    'testimoni' => '<div>
                    <div class="col-md-12">
                      <div class="testimonial testimonial-primary">
                        <blockquote>
                          <p>I am not an agressive investor and am not too familiar with stocks or other financial instruments. My days are occupied with taking care / overseeing the operations of my stores and restaurants as well as my family, hence I don’t have a lot of time to monitor my investments. I feel that with Gradana as a tool, I can invest the funds that I have in a relatively secure way and its rate of return is better than if I were to put in time deposit.</p>
                        </blockquote>
                        <div class="testimonial-arrow-down"></div>
                        <div class="testimonial-author">
                          <div class="testimonial-author-thumbnail img-thumbnail">
                            <img src="/assets/img/client-1.jpg" alt="">
                          </div>
                          <p><strong>Mrs Hartati</strong><span>entrepreneur, 54 years old</span></p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div>
                    <div class="col-md-12">
                      <div class="testimonial testimonial-primary">
                        <blockquote>
                          <p>Initially I am simply a seasoned  property Investor since I actually love / have a passion in property. However with the current weak economic condition which also impacted the property sector, it is now more difficult to lease my apartment and it is also a bad timing if I were to sell since not many people are in the market to buy right now. With Gradana I can diversify my investment portfolio in a field that correlates well with something that I understand and believe in, which is property.</p>
                        </blockquote>
                        <div class="testimonial-arrow-down"></div>
                        <div class="testimonial-author">
                          <div class="testimonial-author-thumbnail img-thumbnail">
                            <img src="/assets/img/client-1.jpg" alt="">
                          </div>
                          <p><strong>Mr Herry</strong><span>Property Investor, 60 years old</span></p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div>
                    <div class="col-md-12">
                      <div class="testimonial testimonial-primary">
                        <blockquote>
                          <p>I like Gradana’s concept which is summarized well in its tagline “People helping people”. Housing is one of humans’ most basic essential needs which often times is neglected or hard to fulfill due to its significant / high cost. With Gradana, I can help my brothers / sisters to be able to afford their dream home, whereas at the same time I also get an attarctive returns. What an interesting value proposition, whereby all the needs and the expectations of all the parties involved are being adressed and answered at the same time.</p>
                        </blockquote>
                        <div class="testimonial-arrow-down"></div>
                        <div class="testimonial-author">
                          <div class="testimonial-author-thumbnail img-thumbnail">
                            <img src="/assets/img/client-1.jpg" alt="">
                          </div>
                          <p><strong>Mr. FB</strong><span>economist</span></p>
                        </div>
                      </div>
                    </div>
                  </div>',

    'slidetitle' => 'Join us as Lenders in Gradana!',
    'slidedesc' => 'Now you can make your idle fund more productive with more attractive yield rather than having it stay in bank account.
      <br>It is secured and has more attractive yield while also <br>having an additional sense of purpose: to help others to afford their dream house',
    'klik' => 'Join Us Now',
);
