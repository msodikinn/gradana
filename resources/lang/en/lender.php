<?php

return array(
    'title-jumbotron'      => 'Join us as lenders in Gradana!',
    'titledesc-jumbotron' => '<span style=font-size:20px;>Now you can make your idle fund more productive with more attractive yield rather than having it stay in bank<br> account. It  is secured and has more attractive yield while also having an additional sense of <br>purpose: to help others to afford their dream house. </span> ',
    'image'  => '',/*'assets/img/grafik_pemberi_pinjaman3_ENG.jpg',*/
    'why' => 'Why Gradana Investing Can Be Alternative Options?',
    'whydesc'  => '<li><i class="fa fa-check"></i> More attarctive yields compared to time deposit </li>
                        <li><i class="fa fa-check"></i> Backed with the selected property as a secure and strong assett collateral; As long as the Lender has not fulfilled all of his / her obligations until all his / her Down Payment installment is fully paid off, you are acknowledged as the rightful owner of the selected property</li>
                        <li><i class="fa fa-check"></i> A reliable verification and credit scoring system which is derived from commercial banking’s credit analysis process</li>
                        <li><i class="fa fa-check"></i> Partnership and collaboration with trusted property developers which have passed stringent due diligence process which includes but not limited to the land and project status as well as financial health</li>
                        <li><i class="fa fa-check"></i> Transparent agreement structures which are comprehensive to protect and accomodate all stakeholders, including you as a Lender</li>
                        <li><i class="fa fa-check"></i> Online system which monitors payment / installment and loan portfolio status that can be accessed anytime</li>
                        <li><i class="fa fa-check"></i> Fast, effiecient and secure overall process</li>',
    'terms' => 'Lending Terms',
    'termsdesc' => '<li> <i class="fa fa-check"></i> Indonesian citizen</li>
                    <li> <i class="fa fa-check"></i> Submit accurate and complete data which is supported by supporting documents as part of our requirements, at the very latest 3 days after initial registration is done</li>
                    <li> <i class="fa fa-check"></i> Commit the payments required under the selected Scheme (may it be Scheme A- in the form of full price Hard Cash, or Scheme B – in the form of DP only) on time</li>
                    <li> <i class="fa fa-check"></i> Sign and execute the agreement with the Borrower and comply with all clauses</li>',
    'clienttesti' => 'Testimonial',
    'testi1'      => 'I am not an agressive investor and am not too familiar with stocks or other financial instruments. My days are occupied with taking care / overseeing the operations of my stores and restaurants as well as my family, hence I don’t have a lot of time to monitor my investments. I feel that with Gradana as a tool, I can invest the funds that I have in a relatively secure way and its rate of return is better than if I were to put in time deposit.',
    'testi1person' => '<strong>Mrs Hartati</strong><span>entrepreneur, 54 years old</span>',
    'testi2'      => 'Initially I am simply a seasoned  property Investor since I actually love / have a passion in property. However with the current weak economic condition which also impacted the property sector, it is now more difficult to lease my apartment and it is also a bad timing if I were to sell since not many people are in the market to buy right now. With Gradana I can diversify my investment portfolio in a field that correlates well with something that I understand and believe in, which is property.',
    'testi2person' => '<strong>Mr Herry</strong><span>Property Investor, 60 years old</span>',
    'button'    => 'Join'

);
