<?php

return array(
    'title'   => 'ABOUT GRADANA',
    'desc'    => 'Gradana is a platform that makes people more easily find mortgage fund sources outside banks. On Gradana, we simplify mortgage application process to be more user friendly and time efficient. We invite creditors to distribute loan based on their own discretions for mortgage applicants that pass our credit assessment process.',
    'contact' => 'CONTACT US',
    'alamat'  => 'Palem Raya Street No. 440 - West Jakarta',
);
