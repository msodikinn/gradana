<?php

return array(
    'quote'       => "Gradana is a portal where lenders and borrowers meet for mortgage down-payment financing purpose",
    'borrower'       => "I'm Borrower",
    'investor'         => "I'm Lender",
    'borrowerdesc'   => 'I am interested to get financing for the Down Payment of my mortgage',
    'investordesc'     => 'I am interested to contribute or  be part of Gradana as a lender',
    'faq'            => 'FAQ Page',

    'dashboard' => array(
        'total'         => 'Total Loans',
        'totaldesc'     => 'has been approved',
        'pinjaman'      => 'Loan Amount (rupiah)',
        'pinjamandesc'  => 'currently running',
        'sukses'        => 'Total loan success',
        'suksesdesc'    => 'full payment',
        'macet'         => 'Bad Credit',
        'macetdesc'     => 'payments on hold',
    ),

    'why' => array(
        'choose'        => 'Why Choose Gradana',
        'simple'        => '<p><img src="/assets/img/simpler.png" width="55" height="50"></p><h4>Simpler and Easier</h4>
              <p>Lenders can use Gradana portal as alternative to optimize yield of the idle funds at banks.<br>
              For borrowers, Gradana helps you get financing for mortgage DP  with clear and concised process. Preparing cash for DP is no longer an issue to buy a house.</p><br>',
        'competitive'   => '<p><img src="/assets/img/competitive.png" width="55" height="50"></p><h4>Competitive Yield</h4>
              <p>For lenders, idle funds can be wisely used for borrowers’ DP financing with more competitive yield compared to bank interest.</p><br>',
        'secured'       => '<p><img src="/assets/img/safe.png" width="55" height="50"></p><h4>Secured with Property Collateral</h4>
              <p>While empowering others to afford a dream house and earning attracive yield, lenders get backed with secured property collateral. Before borrowers completely fulfill their duties, lenders are the legitimate owners of the properties funded.</p><br>',
        'image'         => '<img src="/assets/img/grafik_pemberi_pinjaman3_ENG_.jpg" class="img-responsive" style="margin-bottom:50px;">
                <img src="/assets/img/grafik-peminjam-2_ENG-home.jpg" class="img-responsive" style="margin-bottom:50px;">',
    ),

    'work'  => '<h1>How Gradana Works?</h1>
              <p class="lead mb-xlg">Lenders and borrowers can initiate the process by signing up on Gradana website. </p>',
    'buletin' => array(
        'title' => 'Newsletter',
        'sub'   => 'Please fill out the form below to subscribe to our newsletter',
        'place' => 'Enter Email Address',
    ),

    'blog' => array(
        'latest'    => '<h1>Latest <strong>Blog</strong> Posts</h1>',
        'more'      => 'read more',
    ),

    'testimoni' => array(
        'title'    => '<h1>Client <strong>Testimonial</strong></h1>',
        'isi1'      => '<p>Gradana’s scheme is very safe. For years I’ve invested in properties, and this scheme is well tested in the industry by myself. Rather than having idle money in banks, it’s wiser to make it more productive while helping others to afford a house for their families. The yield is also attractive!</p>',
        'isi2'      => '<p>I didn’t need to submit all the documents in hardcopy. It’s easy because I can easily upload them on the website. I got my application approved in less than 2 weeks!</p>',
        'isi3'      => '<p>At the beginning I thought it would be overwhelming. But Gradana’s admin turned out very helpful and willing to explain the detail process to me step by step. They also helped me fill the forms ‘till complete</p>',
        'isi4'      => '<p>Gradana is truly an innovative portal that can be alternative for DP financing. DP itself so far has been one of major concerns on buyers’ side. Also, Gradana helps developers create more lively township given that it potentially attracts more end-users as oppossed to property lenders</p>',
        'tahun'     => 'years old',
    ),

);
