<?php

return array(
    'titledesc'  => 'Please find the terms and conditions for Gradana borrowers as below. After you click the “Agree” icon below, the complete document will be automatically sent to your email address.',
    'pemohon' => 'Applicant',
    'jobpemohon' => 'Job Applicant',
    'pasangan' => 'Partner',
    'jobpasangan' => 'Job Partner',
    'keluarga' => 'Family',
    'penghasilan' => 'Earning',
    'perbankan' => 'Banking',
    'prev' => 'Previous',
    'next' => 'Next',
    'finish' => 'Finish',
);
