<?php

return array(
    'title'      => 'Lender Profile Form',
    'titledesc'      => 'Please fill form below to add your Lender profile',
    'nama'  => 'Lender Name',
    'image' => 'Image',
    'kategori' => 'Category',
    'usaha' => 'Business Type *',
    'email' => 'Email',
    'alamat' => 'Address',
    'telp' => 'Telephone *',
    'save' => 'Submit payment and send email',
    'cancel' => 'Cancel',
    'hp' => 'Handphone',
    'alokasi' => 'Funding Allocation',
    'alokasidesc' => 'I choose to allocate the total funds of the following amounts in the Gradana scheme (IDR)',
    'jml_alokasi' => 'Amount of Allocation',
    'jml_alokasidesc' => 'I choose to allocate my funds to 1 , 2, 3 or >3 borrower',
    'tenor' => 'Tenor',
    'tenordesc' => 'If possible, I choose to channel loans with above tenor',
    'penempatan' => 'Funds Placement',
    'penempatandesc' => 'Placement option I want to choose is',
    'tnt'   => 'I have read points in the attached conditions, and agree entirely without exception.'
);
