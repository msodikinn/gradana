<?php

return array(
    'title'   => 'CONTACT US',
    'desc'    => "We’re happy to hear from you! Drop us a message using contact form below and we’ll get back to you within 24 hours.",
    'address' => '<strong>Address:</strong> Palem Raya Street No. 440 - West Jakarta',
    'name'    => 'Your Name',
    'email'   => 'Your Email',
    'subject' => 'Subject',
    'message' => 'Message',
    'send'    => 'Send Message',
);
