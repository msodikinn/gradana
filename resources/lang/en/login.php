<?php

return array(
    'return' => "I'm a Returning Member",
    'email' => 'E-mail Address',
    'lost'  => 'Lost Password?',
    'noaccount' => 'No account yet?',
    'register' => 'Register here',
    'or' => '-- Or login with: --'
);
