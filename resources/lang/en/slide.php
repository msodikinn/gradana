<?php

return array(

    'satu'    => 'WELCOME TO GRADANA',
    'satusub' => 'The New Mortgage Financing Solution to get Your Dream Home',
    'dua'     => 'ABOUT GRADANA',
    'duasub'  => 'Gradana provides DP installment solution for your dream house. It is easy, quick and reliable',
    'tiga'    => 'EASY PROCESS',
    'tigasub' => '<b>Where lenders and borrowers meet</b><br><span style=font-size:20px;>
                      <br>Gradana is a portal where lenders and borrowers meet for <br>mortgage down-payment financing purpose
                      <br>Lenders have full discretion to decide which borrowers whose DP financing will be granted</span>
                  ',

);
