<?php

return array(
    'nama_keluarga'       => 'Joint Income',
    'alamat_keluarga'  => 'Take Home Pay *',
    'kelurahan_keluarga' => 'Partner Earnings *',
    'kecamatan_keluarga'      => 'Additional Earnings',
    'propinsi_keluarga'        => 'Household Costs *',
    'kota_keluarga' => 'Another Costs',
);
