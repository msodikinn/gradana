<?php

return array(
    'jenis_pekerjaan_jobpm'       => 'Type of Work',
    'nama_perusahaan_jobpm'  => 'Company Name *',
    'bidang_usaha_jobpm' => 'Business Fields',
    'bidang_usaha_detail_jobpm'      => 'Detail Business Fields *',
    'tanggal_pendirian_jobpm'        => 'Date of Establishment *',
    'alamat_jobpm'=> 'Job Address *',
    'kelurahan_jobpm' => 'Kelurahan',
    'kecamatan_jobpm' => 'District',
    'kota_jobpm'=> 'City',
    'postal_jobpm'  => 'Postal Code',
    'tlp_ktr_hunting_jobpm'  => 'Office Call (Hunting)',
    'tlp_ktr_direct_jobpm'  => 'Office Call (Direct) *',
    'fax_jobpm'   => 'Fax',
    'unit_kerja_jobpm'       => 'Work Unit *',
    'jabatan_jobpm'    => 'Position *',
    'bulan_jobpm'       => 'Work Since (Month) *',
    'tahun_jobpm'        => 'Work Since (Year) *',
    'omset_bln_jobpm'        => 'The turnover per month',
    'kepemilikan_jobpm'        => 'Percentage of Ownership',
    'margin_untung_jobpm'    => 'Percentage Profit Margin',
    'nama_perusahaan_ago_jobpm'     => 'Company Name Ago',
    'jenis_usaha_ago_jobpm'    => 'Type of Work Ago',
    'jabatan_ago_jobpm'      => 'Position Ago',
    'unit_kerja_ago_jobpm'    => 'Work Unit Ago',
    'telp_jobpm'        => 'Telephone *',
    'bulan_ago_jobpm'   => 'Work Since (Month) Ago',
    'tahun_ago_jobpm'   => 'Work Since (Year) Ago',
);
