<?php

return array(
    'status_suami'       => 'Status',
    'nama_suami'  => 'Name',
    'tempat_lahir_suami' => 'Birth Place',
    'tgl_lahir_suami'      => 'Birth Date',
    'ktp_suami'        => 'KTP',
    'npwp_suami'=> 'NPWP',
    'telp_suami' => 'Telephone',
    'hp_suami' => 'Handphone',
);
