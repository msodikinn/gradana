<?php

return array(
    'nama_keluarga'       => 'Name of Head of Family *',
    'alamat_keluarga'  => 'Address *',
    'kelurahan_keluarga' => 'Kelurahan',
    'kecamatan_keluarga'      => 'District',
    'propinsi_keluarga'        => 'Province',
    'kota_keluarga'=> 'City',
    'telp_keluarga' => 'Telephone *',
    'hp_keluarga' => 'Handphone *',
    'hub_keluarga'=> 'Relationship *',
);
