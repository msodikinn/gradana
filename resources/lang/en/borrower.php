<?php

return array(
    'title-jumbotron'      => 'Do you need loan for your<br> mortgage down payment? ',
    'titledesc-jumbotron' => '<span style=font-size:20px;>Gradana is here to assist you in getting a loan to finance your mortgage down payment at reasonable cost, <br>so you can install it in monthly basis based on your financial ability. With Gradana, <br>you don’t need to come up with a whole lot of cash for the DP of your dream house!</span>',
    'image'  => '',/*'assets/img/grafik_peminjam_3_ENG.jpg',*/
    'why' => 'Why Borrowing at Gradana?',
    'whydesc'  => '<li><i class="fa fa-check"></i> Considering the fact that a  mortgage Down Payment is a must and the sum is not small and cannot be covered by banks, now you can afford your dream home by installing your Down Payment through Gradana.</li>
                    <li><i class="fa fa-check"></i> Faster and simpler; application form and signatory can all be done online.</li>
                    <li><i class="fa fa-check"></i> All for a small fee that won’t burden you.</li>
                    <li><i class="fa fa-check"></i> The Down Payment installments tenure can be set as per your preference and ability to pay.</li>
                    <li><i class="fa fa-check"></i> Secure, at par with loan applications that you do in banks.</li>
                    <li><i class="fa fa-check"></i> Verified Lender list and network. The Lenders are ready to help you get your dream home.</li>',
    'terms' => 'Terms for Loan',
    'termsdesc' => '<li> <i class="fa fa-check"></i> Indonesian citizen.</li>
                    <li> <i class="fa fa-check"></i> Has a permanent occupation, and has been working with the current employer for at least 1 year.</li>
                    <li> <i class="fa fa-check"></i> Submission of valid and complete information together with its supporting documents, at the very latest 3 days after the initial registration.</li>
                    <li> <i class="fa fa-check"></i> Administrative fee payment of Rp 500,000,-</li>
                    <li> <i class="fa fa-check"></i> Acknowledgement of the credit scoring that has been verified by Gradana team.</li>
                    <li> <i class="fa fa-check"></i> Signing and execution of the agreement between the Borrower and the Lender.</li>
                    <li> <i class="fa fa-check"></i> On time payments of DP installments to Lender.</li>',
    'clienttesti' => 'Client Testimonial',
    'testi1'      => 'I dont need to submit all the documents in hardcopy. It’s easy because I can easily upload them in the website. I got my application approved in less than 2 weeks!',
    'testi1person' => '<strong>TE, borrowers</strong><span>38 years old, employee</span>',
    'testi2'      => 'At the beginning I thought it would be complicated. But Gradana’s admin turned out to be very helpful and takes time to explain the detail process to me step by step. They also helped me fill the forms ‘till complete.',
    'testi2person' => '<strong>SO, borrowers</strong><span>26 years old, employee</span>',
    'buton'     => 'Join',

);
