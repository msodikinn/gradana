<?php
return array(
    'title'      => 'Do you need loan for <br>mortgage down payment?',
    'titledesc'  => 'Gradana is here to assist you in getting a loan to finance your mortgage down payment at reasonable cost, <br>so you can install it in monthly basis based on your financial ability. With Gradana, <br>you don’t need to come up with a whole lot of cash for the DP of your dream house!',
    'image'  => 'assets/img/grafik-peminjam-2_ENG.jpg',
    'why'       => 'Why Borrowing at GRANADA?',
    'whydesc'  => '<li> Considering the fact that a  mortgage Down Payment is a must and the sum is not small and cannot be covered by banks, now you can afford your dream home by installing your Down Payment through Gradana.</li>
          <li> Faster and simpler; application form and signatory can all be done online.</li>
          <li> All for a small fee that won’t burden you.</li>
          <li> The Down Payment installments tenure can be set as per your preference and ability to pay.</li>
          <li> Secure, at par with loan applications that you do in banks.</li>
          <li> Verified Lender list and network. The Lenders are ready to help you get your dream home.</li>',
    'terms' => 'Terms for Loan',
    'termsdesc' => '<li><i class="fa fa-check"></i> Indonesian citizen.</li>
                    <li><i class="fa fa-check"></i> Has a permanent occupation, and has been working with the current employer for at least 1 year.</li>
                    <li><i class="fa fa-check"></i> Submission of valid and complete information together with its supporting documents, at the very latest 3 days after the initial registration.</li>
                    <li><i class="fa fa-check"></i> Administrative fee payment of Rp 500,000,-</li>
                    <li><i class="fa fa-check"></i> Acknowledgement of the credit scoring that has been verified by Gradana team.</li>
                    <li><i class="fa fa-check"></i> Signing and execution of the agreement between the Borrower and the Lender.</li>
                    <li><i class="fa fa-check"></i> On time payments of DP installments to Lender.</li>',
    'faq' => '<div class="toggle toggle-primary" data-plugin-toggle>
                  <section class="toggle">
                    <label>How is my lending rate determined??</label>
                    <p>Based on the information that you submitted during the application. Gradana has a credit scoring system that mirrors the commercial bank system, where your financial capacity will be the main factor in determining your risk profile as a Borrower. The better is your credit score, the lower is your lending rate that you have to pay back to the Lender.</p>
                  </section>
                  <section class="toggle">
                    <label>When do I have to pay installments, is there a specific date?</label>
                    <p>You will make payments every the 1st or the 15th of the month, by which it will not be sooner than 14 days after the signing of the agreement between the Lender and the Borrower.</p>
                  </section>
                  <section class="toggle">
                    <label>Would it be possible to pay-off my loan before the end of my tenure?</label>
                    <p>Yes, however depsite the earlier payment, the loan rate stipulated in the agreement will still apply and it will be multiplied with the remaining DP amount that you pay off earlier as our Lender would expect this rate in exchange of the assistance that they rendered.</p>
                  </section>
                </div>',
    'more'  => 'See more...',
    'borrow' => 'Borrowers <strong>Testimony</strong>',
    'testi1'      => 'I dont need to submit all the documents in hardcopy. It’s easy because I can easily upload them in the website. I got my application approved in less than 2 weeks!',
    'testi1person' => '<strong>TE, borrowers</strong><span>38 years old, employee</span>',
    'testi2'      => 'At the beginning I thought it would be complicated. But Gradana’s admin turned out to be very helpful and takes time to explain the detail process to me step by step. They also helped me fill the forms ‘till complete.',
    'testi2person' => '<strong>SO, borrowers</strong><span>26 years old, employee</span>',
    'daftar' => 'Register Now',
);
