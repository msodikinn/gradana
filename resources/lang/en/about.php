<?php
return array(
    'title-jumbotron'      => 'About GRADANA',
    'titledesc-jumbotron' => 'Gradana provides DP installment solution for your dream house. It is easy, quick and reliable.',
    'titledesc'      => 'New Mortgage Financing Solution to Live A Dream Home',
    'title' => 'Welcome to Gradana',
    'who' => 'Who We Are',
    'whodesc' => 'Gradana is a portal where lenders and borrowers meet for mortgage down-payment financing purpose. 
Lenders have full discretion to decide which borrowers whose DP financing will be granted',
    'why'  => 'Why Choose Gradana',
    'why1' => 'Simpler and Easier',
    'why1desc' => 'Lenders can use Gradana portal as alternative to optimize yield of the idle funds at banks. 
For borrowers, Gradana helps you get financing for mortgage DP  with clear and concised process. Preparing cash for DP is no longer an issue to buy a house.',
    'why2' => 'Competitive yield',
    'why2desc' => 'For lenders, idle funds can be wisely used for borrowers’ DP financing with more competitive yield compared to bank interest. ',
    'why3' => 'Secured with property collateral',
    'why3desc' => 'While empowering others to afford a dream house and earning attracive yield, lenders get backed with secured property collateral. Before borrowers completely fulfill their duties, lenders are the legitimate owners of the properties funded.',
);
