<?php

return array(
    'titledesc'  => 'Setelah Anda mengklik "I agree" di bawah ini, dokumen lengkap akan secara otomatis dikirimkan ke alamat email Anda.',
    'pemohon' => 'Pemohon',
    'jobpemohon' => 'Pekerjaan Pemohon',
    'pasangan' => 'Pasangan',
    'jobpasangan' => 'Pekerjaan Pasangan',
    'keluarga' => 'Keluarga',
    'penghasilan' => 'Penghasilan',
    'perbankan' => 'Perbankan',
    'prev' => 'Sebelumnya',
    'next' => 'Selanjutnya',
    'finish' => 'Selesai',
);
