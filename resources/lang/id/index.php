<?php

return array(
    'quote'       => "Gradana merupakan portal yang mempertemukan peminjam dan pemberi pinjaman untuk kebutuhan cicilan uang muka (DP) rumah",
    'borrower'       => 'Saya Peminjam',
    'investor'         => 'Saya Pemberi Pinjaman',
    'borrowerdesc'   => 'Saya ingin mendapatkan pinjaman untuk pendanaan uang muka (DP) rumah saya',
    'investordesc'     => 'Saya tertarik untuk menjadi pendana/ penyalur dana di Gradana',
    'faq'            => 'Halaman FAQ',

    'dashboard' => array(
        'total'         => 'Total Pinjaman',
        'totaldesc'     => 'yang telah diapprove',
        'pinjaman'      => 'Jumlah Pinjaman (rupiah)',
        'pinjamandesc'  => 'yang sedang berjalan',
        'sukses'        => 'Total Pinjaman Sukses',
        'suksesdesc'    => 'pembayaran lunas',
        'macet'         => 'Kredit Macet',
        'macetdesc'     => 'pembayaran on hold',
    ),

    'why' => array(
        'choose'         => 'Mengapa Memilih GRADANA',
        'simple'     => '<p><img src="/assets/img/simpler.png" width="55" height="50"></p><h4>Lebih praktis dan cepat</h4>
              <p>Pemberi pinjaman dapat menggunakan Gradana sebagai sarana alternatif untuk mengoptimalkan tingkat pengembalian dari dana yang tidak terpakai.<br>
              Sementara bagi peminjam, Gradana membantu Anda untuk mendapatkan pembiayaan uang muka rumah tanpa persyaratan yang bertele-tele. Membeli rumah kini tidak perlu lagi pusing menyiapkan uang untuk DP yang besar.</p>',
        'competitive'   => '<p><img src="/assets/img/competitive.png" width="55" height="50"></p><h4>Tingkat pengembalian yang kompetitif</h4>
              <p>Bagi pemberi pinjaman, dana yang menganggur di bank atau di rumah secara bijak dapat disalurkan kepada peminjam dengan tingkat pengembalian yang lebih kompetitif dari bunga deposito.</p>',
        'secured'       => '<p><img src="/assets/img/safe.png" width="55" height="50"></p><h4>Aman dengan Jaminan Properti</h4>
              <p>Sambil membantu sesama untuk mendapatkan rumah impian dan mendapatkan tingkat pengembalian kompetitif, pemberi pinjaman memiliki jaminan properti yang aman atas pinjaman yang diberikan. Sebelum kewajiban peminjam dituntaskan, Anda sebagai pemberi pinjaman merupakan pemilik legal yang sah atas rumah yang Anda biayai.</p></center>',
        'image'         => '<img src="/assets/img/grafik_peminjam_3_.jpg" class="img-responsive" style="margin-bottom:50px;">
                <img src="/assets/img/grafik_pemberi_pinjaman3.jpg" class="img-responsive" style="margin-bottom:50px;">',

    ),

    'work'  => '<h1>Cara Kerja GRADANA</h1>
              <p class="lead mb-xlg">Pemberi pinjaman dan peminjam dapat memulai proses dengan mendaftar secara online di website Gradana.</p>',
    'buletin' => array(
        'title' => 'Buletin',
        'sub'   => 'Silahkan isi formulir di bawah untuk berlangganan buletin kami',
        'place' => 'Masukkan alamat Email',
    ),

    'blog' => array(
        'latest' => '<h1>Artikel <strong>Blog</strong> Terakhir</h1>',
        'more'   => 'selengkapnya',
    ),

    'testimoni' => array(
        'title' => '<h1><strong>Testimoni</strong> Klien Kami?</h1>',
        'isi1'  => '<p>Skema Gradana sangat aman. Saya berinvestasi di properti sudah bertahun-tahun, dan skema ini sudah teruji oleh saya sendiri. Daripada uang nganggur di bank, lebih baik diputar sembari bantu orang lain membeli rumah untuk keluarganya. Yield-nya pun menarik!</p>',
        'isi2'  => '<p>Dokumen-dokumen untuk pengajuan gak perlu repot dikirim hard copy nya, praktis bisa upload langsung di website. Kurang dari 2 minggu aplikasi saya sudah beres!</p>',
        'isi3'  => '<p>Awalnya saya kira bakal ribet untuk proses pengajuannya. Tapi ternyata admin Gradana sabar menjelaskan langkahnya satu per satu. Bahkan aplikasi saya dibantu untuk diisikan juga.</p>',
        'isi4'  => '<p>Gradana benar-benar menjadi portal yang inovatif untuk alternatif pembiayaan DP yang selama ini menjadi salah satu concern terbesar bagi pembeli properti. Adanya Gradana juga membantu developer untuk menciptakan lingkungan yang lebih hidup, mengingat Gradana berpotensi untuk menarik lebih banyak penghuni ketimbang investor properti.</p>',
        'tahun' => 'tahun',
    ),
);
