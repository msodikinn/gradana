<?php

return array(
    'title'      => 'Form Profil Pemberi Pinjaman',
    'titledesc'      => 'Silahkan mengisi profil Pemberi Pinjaman Anda',
    'nama'  => 'Nama Pemberi Pinjaman',
    'image' => 'Foto',
    'kategori' => 'Kategori',
    'usaha' => 'Pekerjaan *',
    'email' => 'Email',
    'alamat' => 'Alamat',
    'telp' => 'Telepon *',
    'save' => 'Simpan pembayaran dan kirim email',
    'cancel' => 'Batalkan',
    'hp' => 'Handphone',
    'alokasi' => 'Alokasi Dana',
    'alokasidesc' => 'Saya memilih untuk mengalokasikan total dana sebesar jumlah berikut dalam skema Gradana (Rp)',
    'jml_alokasi' => 'Jumlah Alokasi Dana',
    'jml_alokasidesc' => 'Saya memilih untuk mengalokasikan dana saya untuk 1 peminjam, 2, 3 atau >3',
    'tenor' => 'Tenor',
    'tenordesc' => 'Bila memungkinkan, saya memilih untuk menyalurkan pinjaman dengan tenor selama',
    'penempatan' => 'Penempatan Dana',
    'penempatandesc' => 'Opsi penempatan dana yang ingin saya pilih adalah',
    'tnt'   => 'Saya telah membaca poin-poin di dalam ketentuan terlampir, dan menyetujui seluruhnya tanpa kecuali.',
);
