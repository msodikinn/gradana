<?php

return array(
    'return' => "Saya punya akun",
    'email' => 'Alamat E-mail',
    'lost'  => 'Lupa Password?',
    'noaccount' => 'Belum punya akun?',
    'register' => 'Register disini',
    'or' => '-- Atau login dengan: --',
);
