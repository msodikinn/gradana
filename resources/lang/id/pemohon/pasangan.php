<?php

return array(
    'status_suami'       => 'Status',
    'nama_suami'  => 'Nama',
    'tempat_lahir_suami' => 'Tempat Lahir',
    'tgl_lahir_suami'      => 'Tanggal Lahir',
    'ktp_suami'        => 'KTP',
    'npwp_suami'=> 'NPWP',
    'telp_suami' => 'Telepon',
    'hp_suami' => 'Handphone',
);
