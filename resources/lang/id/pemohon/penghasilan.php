<?php

return array(
    'nama_keluarga'       => 'Penghasilan Gabungan',
    'alamat_keluarga'  => 'Penghasilan Pemohon (THP) *',
    'kelurahan_keluarga' => 'Penghasilan Suami/Istri *',
    'kecamatan_keluarga'      => 'Penghasilan Tambahan',
    'propinsi_keluarga'        => 'Biaya Rumah Tangga *',
    'kota_keluarga' => 'Angsuran Lain',
);
