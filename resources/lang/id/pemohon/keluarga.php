<?php

return array(
    'nama_keluarga'       => 'Nama Kepala Keluarga *',
    'alamat_keluarga'  => 'Alamat *',
    'kelurahan_keluarga' => 'Kelurahan',
    'kecamatan_keluarga'      => 'Kecamatan',
    'propinsi_keluarga'        => 'Propinsi',
    'kota_keluarga'=> 'Kota',
    'telp_keluarga' => 'Telepon *',
    'hp_keluarga' => 'Handphone *',
    'hub_keluarga'=> 'Hubungan *',
);
