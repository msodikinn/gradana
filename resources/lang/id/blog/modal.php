<?php

/**
* Language file for blog delete modal
*
*/
return array(

    'body'			=> 'Apakah Anda ingin menghapus blog ini? Aksi ini tidak dapat dikembalikan.',
    'cancel'		=> 'Batal',
    'confirm'		=> 'Hapus',
    'title'         => 'Hapus Blog',

);
