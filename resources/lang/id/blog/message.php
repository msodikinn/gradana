<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'blog_exists'              => 'Blog telah ada!',
    'blog_not_found'           => 'Blog [:id] tidak ada.',

    'success' => array(
        'create'    => 'Blog berhasil dibuat.',
        'update'    => 'Blog berhasil diubah.',
        'delete'    => 'Blog berhasil dihapus.',
    ),

    'error' => array(
        'create'    => 'Ada masalah ketika membuat blog. Silahkan coba lagi.',
        'update'    => 'Ada masalah ketika mengubah blog. Silahkan coba lagi.',
        'delete'    => 'Ada masalah ketika menghapus blog. Silahkan coba lagi.',
    ),

);
