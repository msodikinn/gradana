<?php
/**
* Language file for blog section titles
*
*/

return array(
  'title'     => 'Judul',
  'create' => 'Buat Blog Baru',
  'edit' => 'Ubah Blog',
  'management' => 'Kelola Blog',
  'add-blog' => 'Buat Blog',
  'blog' => 'Blog',
  'blogs' => 'Blog',
  'bloglist' => 'Daftar Blogs',
  'blogdetail' => 'Detail Blog',
  'comments' => 'KOMENTAR',
  'leavecomment' => 'TINGGALKAN KOMENTAR',
);
