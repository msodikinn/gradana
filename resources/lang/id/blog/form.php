<?php
/**
 * Language file for form fields for user account management
 *
 */

return array(

    'ph-title' => 'Masukkan judul disini...',
    'ph-content' => 'Masukkan teks disini',
    'meta-title' => 'Masukkan meta judul disini...',
    'meta-description' => 'Masukkan meta deskripsi disini...',
    'll-postcategory' => 'Kategori Artikel',
    'select-category' => 'Pilih Kategori',
    'tags' => 'Tag...',
    'lb-featured-img' => 'Gambar Utama',
    'select-file' => 'Pilih File',
    'change' => 'Ubah',
    'publish' => 'Publikasikan',
    'discard' => 'Batalkan',
    'ph-name' => 'Nama Anda',
    'ph-email' => 'Email Anda',
    'ph-website' => 'Website Anda',
    'ph-comment' => 'Komentar Anda',
    'save-comment' => 'Simpan Komentar',
    'save' => 'Simpan',
    'publishedstatus' => 'Status Publikasi',
);
