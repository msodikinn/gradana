<?php
/**
* Language file for blog category table headings
*
*/

return array(

    'id'         => 'Id',
    'title'       => 'Judul',
    'comments'      => 'Jumlah komentar',
    'created_at' => 'Dibuat pada',
    'actions'	 => 'Aksi',
    'view-blog-comment' => 'lihat blog dan komentar',
    'update-blog' => 'ubah blog',
    'delete-blog' => 'hapus blog'

);
