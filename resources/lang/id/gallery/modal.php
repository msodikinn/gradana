<?php
return array(
    'title'         => 'Delete Gallery',
    'body'			=> 'Are you sure to delete this gallery? This operation is irreversible.',
    'cancel'		=> 'Cancel',
    'confirm'		=> 'Delete',
);
