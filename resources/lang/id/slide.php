<?php

return array(

    'satu'    => 'Selamat datang di GRADANA',
    'satusub' => 'Solusi pinjaman baru untuk rumah impian anda',
    'dua'     => 'Tentang Gradana',
    'duasub'  => 'Gradana menawarkan solusi cicilan DP rumah yang mudah, cepat, dan terpercaya',
    'tiga'    => 'PROSES YANG MUDAH',
    'tigasub' => '<b style=font-size:20px;>Pertemuan antara Peminjam dan Pemberi Pinjaman</b><br><br>
                  <span style=font-size:18px;>Gradana merupakan portal yang mempertemukan peminjam dan pemberi pinjaman untuk <br>kebutuhan cicilan uang muka (DP) rumah <br>
                  Pemberi pinjaman memiliki kebebasan penuh untuk memilih peminjam mana yang akan dibiayai pembayaran DP rumahnya</span>',

);
