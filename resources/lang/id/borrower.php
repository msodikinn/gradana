<?php

return array(
    'title-jumbotron'      => 'Anda membutuhkan pinjaman <br>untuk cicilan uang muka KPR?',
    'titledesc-jumbotron' => '<span style="font-size:16px;"> Gradana disini untuk membantu Anda dalam mendapatkan pinjaman untuk membiayai hipotek Anda dengan biaya murah, <br> Sehingga Anda dapat menyicilnya secara bulanan berdasarkan kemampuan keuangan Anda. <br> Dengan Gradana,  Anda tidak perlu mengeluarkan uang banyak untuk DP rumah impian Anda!</span>',
    'image'  => '',/*'assets/img/grafik_peminjam_3.jpg',*/
    'why' => 'Mengapa meminjam di Gradana?',
    'whydesc'  => '<li><i class="fa fa-check"></i> Mengingat jumlah DP rumah tidaklah kecil dan cicilan tersebut tidak dapat dilayani oleh bank, kini Anda dapat mencicil uang muka rumah impian melalui Gradana.</li>
                      <li><i class="fa fa-check"></i> Lebih cepat dan lebih sederhana; dokumen aplikasi dan penandatanganan pun dapat dilakukan secara online.</li>
                      <li><i class="fa fa-check"></i> Biaya yang ringan dan tidak memberatkan Anda.</li>
                      <li><i class="fa fa-check"></i> Periode cicilan DP rumah dapat Anda atur sesuai kemampuan.</li>
                      <li><i class="fa fa-check"></i> Aman, seaman Anda mendaftar pinjaman di Bank .</li>
                      <li><i class="fa fa-check"></i> Daftar pemberi pinjaman yang terpercaya, yang siap membantu Anda untuk mendapatkan rumah impian.</li>',
    'terms' => 'Syarat Peminjaman',
    'termsdesc' => '<li> <i class="fa fa-check"></i> Warga Negara Indonesia</li>
                    <li> <i class="fa fa-check"></i> Memiliki pekerjaan tetap paling tidak selama 1 tahun.</li>
                    <li> <i class="fa fa-check"></i> Mencantumkan informasi   yang valid, lengkap dan disertai dokumen pendukung sesuai persyaratan, paling lambat 3 hari setelah pendaftaran awal dilakukan.</li>
                    <li> <i class="fa fa-check"></i> Membayar biaya administrasi sebesar Rp 500.000,-</li>
                    <li> <i class="fa fa-check"></i> Menyetujui hasil credit scoring yang dihasilkan pasca verifikasi data oleh Tim Gradana.</li>
                    <li> <i class="fa fa-check"></i> Menandatangani perjanjian dengan pemberi pinjaman.</li>
                    <li> <i class="fa fa-check"></i> Membayar cicilan DP tepat waktu kepada pemberi pinjaman.</li>',
    'clienttesti' => 'Testimonial Client',
    'testi1'      => 'Dokumen-dokumen untuk pengajuan gak perlu repot dikirim hard copy nya, praktis bisa upload langsung di website. Kurang dari 2 minggu aplikasi saya sudah beres!',
    'testi1person' => '<strong>TE, peminjam</strong><span>38 tahun, pegawai swasta</span>',
    'testi2'      => 'Awalnya saya kira bakal ribet untuk proses pengajuannya. Tapi ternyata admin Gradana sabar menjelaskan langkahnya satu per satu. Bahkan aplikasi saya dibantu untuk diisikan juga.',
    'testi2person' => '<strong>SO, peminjam</strong><span>26 tahun, pegawai swasta</span>',
    'daftar' => 'Klik Daftar Sekarang',
    'buton'    => 'Gabung'
);
