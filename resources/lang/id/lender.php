<?php

return array(
    'title-jumbotron'      => 'Bergabung menjadi Pemberi Pinjaman',
    'titledesc-jumbotron' => 'Kini Anda dapat memutar uang sembari mendapatkan tingkat pengembalian yang lebih menarik daripada<br><br> bunga deposito, aman, dan pastinya membantu sesama untuk mendapatkan rumah impian. ',
    'image'  => '',/*'assets/img/grafik_pemberi_pinjaman3_.jpg',*/
    'why' => 'Mengapa Gradana Dapat Menjadi Alternatif Pilihan Berinvestasi?',
    'whydesc'  => '<li><i class="fa fa-check"></i> Tingkat pengembalian yang lebih menarik dibandingkan bunga deposito </li>
                  <li><i class="fa fa-check"></i> Memiliki jaminan aset yang jelas dan aman, yaitu properti yang Anda biayai; Selama peminjam belum menunaikan kewajibannya hingga cicilan DP sepenuhnya dilunasi, Anda merupakan pemilik yang sah dari properti bersangkutan</li>
                  <li><i class="fa fa-check"></i> Sistem verifikasi dan credit scoring yang dapat diandalkan seperti layaknya proses analisis kredit di bank komersial</li>
                  <li><i class="fa fa-check"></i> Kerja sama dengan pengembang properti yang terpercaya, dan sudah melalui tahap diligence yang ketat terhadap status tanah, status proyek dan status finansial masing-masing pengembang</li>
                  <li><i class="fa fa-check"></i> Klausul perjanjian yang transparan, komprehensif dan protektif bagi kepentingan masing-masing pihak, termasuk Anda sebagai pemberi pinjaman </li>
                  <li><i class="fa fa-check"></i> Sistem monitoring pembayaran cicilan dan status portofolio pinjaman yang dapat Anda akses sewaktu-waktu  secara online</li>
                  <li><i class="fa fa-check"></i> Keseluruhan proses yang cepat, praktis, dan terpercaya </li>',
    'terms' => 'Syarat Pemberian Pinjaman',
    'termsdesc' => '<li> <i class="fa fa-check"></i> Warga Negara Indonesia</li>
                    <li> <i class="fa fa-check"></i> Mencantumkan informasi   yang valid, lengkap dan disertai dokumen pendukung sesuai persyaratan, paling lambat 3 hari setelah pendaftaran awal dilakukan</li>
                    <li> <i class="fa fa-check"></i> Melakukan komitmen pembayaran talangan sesuai skema yang dipilih (baik Skema A – berupa kas keras harga penuh, atau Skema B – berupa DP only) secara tepat waktu/li>
                    <li> <i class="fa fa-check"></i> Menandatangani perjanjian dengan peminjam dan menaati setiap klausul yang berlaku</li>',
    'clienttesti' => 'Testimonial',
    'testi1'      => 'Saya bukan Pemberi Pinjaman yang agresif dan hanya orang awam yang tidak terlalu mengerti banyak mengenai saham atau investasi pasar keuangan lainnya. Sehari-hari saya sibuk mengurusi operasional toko dan restoran dan keluarga saya, sehingga saya pun tidak mempunyai banyak waktu untuk memantau investasi. Gradana merupakan sarana baru di mana saya bisa menginvestasikan dana yang saya miliki dengan cara yang relatif aman dan nilai pengembalian yang lebih menarik dibandingkan saya menaruhnya di deposito bank.',
    'testi1person' => '<strong>Ibu Hartati</strong><span>pengusaha, 54 tahun</span>',
    'testi2'      => 'Awalnya saya adalah Pemberi Pinjaman Property karena memang saya suka dan mempunyai passion di bidang ini. Namun dengan melemahnya kondisi ekonomi yang juga berimbas ke pasar properti, saat ini cukup sulit untuk mendapatkan penyewa bagi unit apartemen yang telah saya beli. Ingin dijual pun sulit karena tidak banyak orang yang mau membeli. Dengan Gradana, saya dapat mendiversifikasi portfolio investasi saya di bidang yang berkesinambungan dan saya yakini, yaitu properti.',
    'testi2person' => '<strong>Bapak Herry</strong><span>Pemberi Pinjaman property, 60 tahun</span>',
    'daftar' => 'Klik Daftar Sekarang',
    'button'    => 'Gabung'

);
