<?php

return array(
    'image'       => '<img src="/assets/img/grafik-pemberi-pinjaman2.jpg" class="img-responsive" style="width:100%; max-width:1440px;">',
    'why'       => '<center><h2>Mengapa Investasi Gradana Bisa Menjadi Pilihan Alternatif?</h2></center>
                    <div style="width: 80%; margin:0 auto;">
                      <ol class="list">
                        <li> Tingkat pengembalian yang lebih menarik dibandingkan bunga deposito </li>
                        <li> Memiliki jaminan aset yang jelas dan aman, yaitu properti yang Anda biayai; Selama peminjam belum menunaikan kewajibannya hingga cicilan DP sepenuhnya dilunasi, Anda merupakan pemilik yang sah dari properti bersangkutan</li>
                        <li> Sistem verifikasi dan credit scoring yang dapat diandalkan seperti layaknya proses analisis kredit di bank komersial</li>
                        <li> Kerja sama dengan pengembang properti yang terpercaya, dan sudah melalui tahap diligence yang ketat terhadap status tanah, status proyek dan status finansial masing-masing pengembang</li>
                        <li> Klausul perjanjian yang transparan, komprehensif dan protektif bagi kepentingan masing-masing pihak, termasuk Anda sebagai pemberi pinjaman </li>
                        <li> Sistem monitoring pembayaran cicilan dan status portofolio pinjaman yang dapat Anda akses sewaktu-waktu  secara online</li>
                        <li> Keseluruhan proses yang cepat, praktis, dan terpercaya </li>
                      </ol>
                    </div>',

    'lend'  => '<center><h1>Syarat Pemberian Pinjaman</h1></center>
                <div style="width: 80%; margin:0 auto;">
                  <ul class="list list-icons">
                    <li><i class="fa fa-check"></i> Warga Negara Indonesia</li>
                    <li><i class="fa fa-check"></i> Mencantumkan informasi   yang valid, lengkap dan disertai dokumen pendukung sesuai persyaratan, paling lambat 3 hari setelah pendaftaran awal dilakukan</li>
                    <li><i class="fa fa-check"></i> Melakukan komitmen pembayaran talangan sesuai skema yang dipilih (baik Skema A – berupa kas keras harga penuh, atau Skema B – berupa DP only) secara tepat waktu/li>
                    <li><i class="fa fa-check"></i> Menandatangani perjanjian dengan peminjam dan menaati setiap klausul yang berlaku</li>
                  </ul>
                </div>',

    'comparison' => '<h1><center>Perbandingan Rate</center></h1>
                <p class="lead" align="center">Perbandingan rate di deposito/ bank, investasi emas, saham, dan gradana</p>
                <div class="row">
                  <div class="col-md-1"></div>
                  <div class="col-md-10">
                    <table class="table table-condensed">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Deposito/Bank</th>
                          <th>Investasi Emas</th>
                          <th>Saham</th>
                          <th>Gradana</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>Mark</td>
                          <td>Otto</td>
                          <td>@mdo</td>
                          <td>@fat</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>Jacob</td>
                          <td>Thornton</td>
                          <td>@fat</td>
                          <td>@fat</td>
                        </tr>
                        <tr>
                          <td> 3</td>
                          <td>Larry</td>
                          <td>the Bird</td>
                          <td>@twitter</td>
                          <td>@fat</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-1"></div>
                </div>',

    'faq' => '<h1><strong>FAQ</strong> (Frequently Asked Question)</h1>
              <div class="row">
                <div class="toggle toggle-primary" data-plugin-toggle>
                  <section class="toggle">
                    <label>Kapan saya akan menerima cicilan dari peminjam?</label>
                    <p>Setelah perjanjian ditandatangani, setiap tanggal 15  pada setiap bulannya</p>
                  </section>
                  <section class="toggle">
                  <label>Apakah saya boleh memberikan pinjaman kepada lebih dari satu peminjam?</label>
                    <p>Tentu saja. Anda dapat memilih untuk menempatkan seluruh dana anda untuk satu Peminjam ataupun membaginya kepada lebih dari satu Peminjam selama jumlahnya mencukupi</p>
                  </section>
                  <section class="toggle">
                    <label>Apakah peminjam boleh / dapat mendapatkan pinjaman dari lebih satu pemberi pinjaman?</label>
                    <p>Dikarenakan saat ini BPHTB/ pajak dari transkasi pembelian properti hanya dapat dibebankan pada 1 NPWP saja dan untuk mencegah permasalahan di kemudian harinya, SAAT INI Peminjam hanya dapat mendapatkan pinjaman dari satu Pemberi Pinjaman. Hal ini juga mempertimbangkan kondisi di mana saat ini Gradana merupakan sebuah peer to peer lending platform, bukan crowd-funding platform.</p>
                  </section>
                </div>',

    'testimonititle' => '<h1><strong>Testimoni</strong> dari pemberi pinjaman</h1>',
    'testimoni' => '<div>
                    <div class="col-md-12">
                      <div class="testimonial testimonial-primary">
                        <blockquote>
                          <p>Saya bukan Pemberi Pinjaman yang agresif dan hanya orang awam yang tidak terlalu mengerti banyak mengenai saham atau investasi pasar keuangan lainnya. Sehari-hari saya sibuk mengurusi operasional toko dan restoran dan keluarga saya, sehingga saya pun tidak mempunyai banyak waktu untuk memantau investasi. Gradana merupakan sarana baru di mana saya bisa menginvestasikan dana yang saya miliki dengan cara yang relatif aman dan nilai pengembalian yang lebih menarik dibandingkan saya menaruhnya di deposito bank.</p>
                        </blockquote>
                        <div class="testimonial-arrow-down"></div>
                        <div class="testimonial-author">
                          <div class="testimonial-author-thumbnail img-thumbnail">
                            <img src="/assets/img/client-1.jpg" alt="">
                          </div>
                          <p><strong>Ibu Hartati</strong><span>pengusaha, 54 tahun</span></p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div>
                    <div class="col-md-12">
                      <div class="testimonial testimonial-primary">
                        <blockquote>
                          <p>Awalnya saya adalah Pemberi Pinjaman Property karena memang saya suka dan mempunyai passion di bidang ini. Namun dengan melemahnya kondisi ekonomi yang juga berimbas ke pasar properti, saat ini cukup sulit untuk mendapatkan penyewa bagi unit apartemen yang telah saya beli. Ingin dijual pun sulit karena tidak banyak orang yang mau membeli. Dengan Gradana, saya dapat mendiversifikasi portfolio investasi saya di bidang yang berkesinambungan dan saya yakini, yaitu properti.</p>
                        </blockquote>
                        <div class="testimonial-arrow-down"></div>
                        <div class="testimonial-author">
                          <div class="testimonial-author-thumbnail img-thumbnail">
                            <img src="/assets/img/client-1.jpg" alt="">
                          </div>
                          <p><strong>Bapak Herry</strong><span>Pemberi Pinjaman property, 60 tahun</span></p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div>
                    <div class="col-md-12">
                      <div class="testimonial testimonial-primary">
                        <blockquote>
                          <p>Saya suka konsep Gradana sesuai dengan taglinenya yaitu ‘Sesama membantu Sesama’. Rumah merupakan salah satu kebutuhan pokok manusia yang seringkali sulit untuk dipenuhi mengingat nilainya yang relatif tinggi. Dengan Gradana saya dapat membantu sesama untuk membeli rumah yang diimpikan, di mana pada saat yang bersamaan saya pun mendapatkan nilai pengembalian yang menarik. Benar-benar suatu value proposition yang menarik, semua pihak terbantu dan diuntungkan di saat yang bersamaan.</p>
                        </blockquote>
                        <div class="testimonial-arrow-down"></div>
                        <div class="testimonial-author">
                          <div class="testimonial-author-thumbnail img-thumbnail">
                            <img src="/assets/img/client-1.jpg" alt="">
                          </div>
                          <p><strong>Bapak FB</strong><span>pengamat ekonomi</span></p>
                        </div>
                      </div>
                    </div>
                  </div>',

    'slidetitle' => 'Bergabunglah menjadi <br>Pemberi Pinjaman di Gradana!',
    'slidedesc' => 'Kini Anda dapat memutar uang sembari mendapatkan tingkat pengembalian yang lebih menarik <br>daripada bunga deposito, aman, dan pastinya membantu sesama untuk mendapatkan rumah impian',
    'klik' => 'Daftar Sekarang',
);
