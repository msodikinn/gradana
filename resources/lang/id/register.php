<?php

return array(
    'regdesc'      => 'Anda dapat mendaftar sebagai Applicant (peminjam) atau sebagai pemberi pinjaman (Investor) disini',
    'profil'      => 'Profil',
    'data'  => 'Data Pribadi',
    'image' => 'Pilih Foto',
    'who' => 'Apakah GRADANA itu?',
    'change' => 'Ganti',
    'remove' => 'Hapus',
    'fullname' => 'Nama Lengkap',
    'confirm' => 'Konfirmasi Password',
    'as' => 'Saya sebagai',
    'borrow' => 'Peminjam',
    'investor' => 'Investor',
    'tgl' => 'Tanggal Lahir',
    'negara' => 'Negara',
    'address' => 'Alamat',
    'prop' => 'Propinsi',
    'kota' => 'Kota',
    'postal' => 'Kode Pos',
    'prev' => 'Sebelumnya',
    'next' => 'Selanjutnya',
    'silahkan' => 'Silahkan',
    'login' => 'Login disini',
);
