<?php
return array(
    'title'         => 'Delete Picture',
    'body'			=> 'Are you sure to delete this picture? This operation is irreversible.',
    'cancel'		=> 'Cancel',
    'confirm'		=> 'Delete',
);
