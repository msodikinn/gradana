<?php

return array(
    'title'   => 'HUBUNGI KAMI',
    'desc'    => 'Kami menunggu saran Anda! Tuliskan pesan Anda pada formulir kontak di bawah ini. Kami akan merespon Anda dalam kurun waktu 24 jam.',
    'address' => '<strong>Alamat:</strong> Jl. Palem Raya No. 440 - Jakarta Barat',
    'name'    => 'Nama',
    'email'   => 'Email',
    'subject' => 'Subyek',
    'message' => 'Pesan',
    'send'    => 'Kirim Pesan',
);
