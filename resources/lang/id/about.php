<?php

return array(
    'title-jumbotron'      => 'Tentang GRADANA',
    'titledesc-jumbotron' => 'Gradana menawarkan solusi cicilan DP rumah yang mudah, cepat, dan terpercaya.',
    'title' => 'Selamat datang di GRADANA',
    'titledesc'      => 'Solusi Pinjaman Baru untuk Rumah Impian Anda',
    'who' => 'Apakah GRADANA itu?',
    'whodesc' => 'Gradana merupakan portal yang mempertemukan peminjam dan pemberi pinjaman untuk kebutuhan cicilan uang muka (DP) rumah.
Pemberi pinjaman memiliki kebebasan penuh untuk memilih peminjam mana yang akan dibiayai pembayaran DP rumahnya. ',
    'why'  => 'Mengapa Memilih GRADANA',
    'why1' => 'Lebih praktis dan cepat',
    'why1desc' => 'Pemberi pinjaman dapat menggunakan Gradana sebagai sarana alternatif untuk mengoptimalkan tingkat pengembalian dari dana yang tidak terpakai. 
Sementara bagi peminjam, Gradana membantu Anda untuk mendapatkan pembiayaan uang muka rumah tanpa persyaratan yang bertele-tele. Membeli rumah kini tidak perlu lagi pusing menyiapkan uang untuk DP yang besar.',
    'why2' => 'Tingkat pengembalian yang kompetitif',
    'why2desc' => 'Bagi pemberi pinjaman, dana yang menganggur di bank atau di rumah secara bijak dapat disalurkan kepada peminjam dengan tingkat pengembalian yang lebih kompetitif dari bunga deposito.',
    'why3' => 'Aman dengan Jaminan Properti',
    'why3desc' => 'Sambil membantu sesama untuk mendapatkan rumah impian dan mendapatkan tingkat pengembalian kompetitif, pemberi pinjaman memiliki jaminan properti yang aman atas pinjaman yang diberikan. Sebelum kewajiban peminjam dituntaskan, Anda sebagai pemberi pinjaman merupakan pemilik legal yang sah atas rumah yang Anda biayai.',

);
