<?php

return array(
    'title'   => 'TENTANG GRADANA',
    'desc'    => 'Gradana adalah platform yang mempermudah untuk menemukan sumber dana KPR bank luar. Pada Gradana, kami menyederhanakan proses aplikasi KPR menjadi lebih user friendly dan efisien waktu. Kami mengundang kreditor untuk menyalurkan kredit berdasarkan discretions mereka sendiri bagi pelamar KPR yang lulus proses penilaian kredit kami.',
    'contact' => 'HUBUNGI KAMI',
    'alamat'  => 'Jl. Palem Raya No. 440 - Jakarta Barat',
);
