<?php
return array(
    'title'      => 'Anda membutuhkan pinjaman <br>untuk cicilan uang muka KPR?',
    'titledesc'  => 'Gradana is here to assist you in getting a loan to finance your mortgage down payment at reasonable cost, <br>so you can install it in monthly basis based on your financial ability. With Gradana, <br>you don’t need to come up with a whole lot of cash for the DP of your dream house!',
    'image'  => 'assets/img/grafik-peminjam-2.jpg',
    'why'       => 'Mengapa Meminjam di GRANADA?',
    'whydesc'  => '<li> Mengingat jumlah DP rumah tidaklah kecil dan cicilan tersebut tidak dapat dilayani oleh bank, kini Anda dapat mencicil uang muka rumah impian melalui Gradana.</li>
          <li> Lebih cepat dan lebih sederhana; dokumen aplikasi dan penandatanganan pun dapat dilakukan secara online.</li>
          <li> Biaya yang ringan dan tidak memberatkan Anda.</li>
          <li> Periode cicilan DP rumah dapat Anda atur sesuai kemampuan.</li>
          <li> Aman, seaman Anda mendaftar pinjaman di Bank .</li>
          <li> Daftar pemberi pinjaman yang terpercaya, yang siap membantu Anda untuk mendapatkan rumah impian.</li>',
    'terms' => 'Syarat Peminjaman',
    'termsdesc' => '<li><i class="fa fa-check"></i> Warga Negara Indonesia</li>
                    <li><i class="fa fa-check"></i> Memiliki pekerjaan tetap paling tidak selama 1 tahun.</li>
                    <li><i class="fa fa-check"></i> Mencantumkan informasi   yang valid, lengkap dan disertai dokumen pendukung sesuai persyaratan, paling lambat 3 hari setelah pendaftaran awal dilakukan.</li>
                    <li><i class="fa fa-check"></i> Membayar biaya administrasi sebesar Rp 500.000,-</li>
                    <li><i class="fa fa-check"></i> Menyetujui hasil credit scoring yang dihasilkan pasca verifikasi data oleh Tim Gradana.</li>
                    <li><i class="fa fa-check"></i> Menandatangani perjanjian dengan pemberi pinjaman.</li>
                    <li><i class="fa fa-check"></i> Membayar cicilan DP tepat waktu kepada pemberi pinjaman.</li>',
    'faq' => '<div class="toggle toggle-primary" data-plugin-toggle>
                  <section class="toggle">
                    <label>Bagaimana nilai bunga pinjaman saya ditentukan?</label>
                    <p>Berdasarkan informasi yang Anda lampirkan saat pengajuan aplikasi. Gradana memiliki sistem credit scoring yang menyerupai bank komersial, di mana kemampuan finansial Anda akan menjadi faktor utama dalam menentukan profil risiko Anda sebagai peminjam. Semakin baik credit score yang Anda peroleh, semakin ringan biaya pengembalian yang harus Anda bayarkan kepada pemberi pinjaman.</p>
                  </section>
                  <section class="toggle">
                    <label>Kapan saya harus membayar cicilian saya, apakah ada tanggal tertentu?</label>
                    <p>Anda melakukan pembayaran cicilan tiap tanggal 15 di setiap bulannya, tidak lebih cepat dari 14 hari  setelah penandatanganan perjanjian antara peminjam dan pemberi pinjaman dilakukan.</p>
                  </section>
                  <section class="toggle">
                    <label>Apabila saya ingin melunasi sebelum masa cicilan yang ditentukan apakah memungkinkan?</label>
                    <p>Memungkinkan, namun Anda akan dikenakan biaya sebesar tingkat pengembalian (berdasarkan credit scoring Anda) dikalikan sisa cicilan DP Anda.</p>
                  </section>
                </div>',
    'more'  => 'Selengkapnya...',
    'borrow' => '<strong>Testimoni</strong> Peminjam',
    'testi1'      => 'Dokumen-dokumen untuk pengajuan gak perlu repot dikirim hard copy nya, praktis bisa upload langsung di website. Kurang dari 2 minggu aplikasi saya sudah beres!',
    'testi1person' => '<strong>TE, peminjam</strong><span>38 tahun, pegawai swasta</span>',
    'testi2'      => 'Awalnya saya kira bakal ribet untuk proses pengajuannya. Tapi ternyata admin Gradana sabar menjelaskan langkahnya satu per satu. Bahkan aplikasi saya dibantu untuk diisikan juga.',
    'testi2person' => '<strong>SO, peminjam</strong><span>26 tahun, pegawai swasta</span>',
    'daftar' => 'Klik Daftar Sekarang',
);
