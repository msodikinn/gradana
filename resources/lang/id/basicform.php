<?php

return array(
    'title'      => 'Form Peminjam',
    'titledesc'  => 'Pilih bentuk yang ingin Anda gunakan.',
    'basicdesc'  => 'Basic form menampilkan beberapa data yang wajib untuk diisi, admin kami akan menghubungi Anda untuk kelengkapan data berikutnya.',
    'nama'       => 'Nama',
    'namaclean'  => 'Nama Lengkap Clean *',
    'namacleandesc' => 'tanpa gelar dan title',
    'statuskawin' => 'Status Kawin',
    'pendidikan' => 'Pendidikan Terakhir',
    'statusrumah' => 'Status Rumah/Bangunan',
    'lahir'      => 'Tempat Lahir',
    'ibu'        => 'Nama Ibu Kandung *',
    'tanggungan' => 'Jumlah Tanggungan *',
    'alamatktp'  => 'Alamat KTP *',
    'telp'       => 'Telepon',
    'balance'    => 'Jumlah Saldo *',
    'ktpimg'     => 'Foto KTP *',
    'npwpimg'    => 'Foto NPWP *',
    'kkimg'      => 'Foto Kartu Keluarga *',
    'aktaimg'    => 'Foto Akta *',
    'submit'     => 'Simpan permohonan dan kirim email',
    'cancel' => 'Batalkan',
);
